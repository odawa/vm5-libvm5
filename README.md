# README #

### ブランチ ###
- `develop-master` 
> 開発用ブランチ。  
> クラウド(重説機能マージ済み)
> 2016/2月時点ではWinodwsのRawFrame化が完了していないため、更新凍結中。

- `feature/PC-RawFrame-Beta5`
> RawFrame対応版開発ブランチ。
> Mac/iOS/Android/Windows(RawFrame対応版)はこちらのブランチを参照願います。

- `develop-master_preRawFrame`
> 非RawFrame用開発ブランチ。
>  Windows(非RawFrame版)はこちらを参照願います。

- `customize/2015/jusetsu-master`
> 重説向けアプリの本番リリースビルド用
> プラットフォームによる差分は特になし。

### タグ ###