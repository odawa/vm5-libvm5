//
//  AppDelegate.h
//  V5LiteSample
//
//  Created by V-1094 on 2015/01/14.
//  Copyright (c) 2015年 V-cube. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (retain, nonatomic) ViewController *viewController;


@end

