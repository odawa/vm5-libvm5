//
//  ViewController.h
//  V5LiteSample
//
//  Created by V-1094 on 2015/01/14.
//  Copyright (c) 2015年 V-cube. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LibV5Lite.hh"
#include <V5LiteSample-Swift.h>

@interface ViewController : UIViewController

@end

@interface ViewController ()
{
    NSMutableData *webData;
    NSMutableString *soapResults;
    NSXMLParser *xmlParser;
    NSMutableString *vidyoEntityID;
    V5ConferenceEventSinkImpl *impl;
    V5ChatEventSinkImpl *chatimpl;
    
}

@property (weak, nonatomic) IBOutlet UISwitch *switchPrepare;
@property (weak, nonatomic) IBOutlet UISwitch *switchJoin;
@property (weak, nonatomic) IBOutlet UITextField *sessionIDTextField;
@property (weak, nonatomic) IBOutlet UITextField *URLTextField;
@property (weak, nonatomic) IBOutlet UITextField *userNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *chatMsgTextField;

@property LibV5Lite *lib;
- (IBAction)clicked1:(id)sender;
- (IBAction)chatMessage:(id)sender;
- (IBAction)logout:(id)sender;
- (void)setOrientation:(UIDeviceOrientation)orie;
- (void)setFrameDirection: (int)x y:(int)y w:(unsigned int)w h:(unsigned int)h;
- (IBAction)segmentChanged:(id)sender;
- (IBAction)setCamera:(id)sender;

- (IBAction)getParticipant:(id)sender;

@end



