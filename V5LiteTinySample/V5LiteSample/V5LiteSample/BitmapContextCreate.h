//
//  BitmapContextCreate.h
//  V5LiteSample
//
//  Created by shintaro on H27/05/12.
//  Copyright (c) 平成27年 V-cube. All rights reserved.
//

#ifndef V5LiteSample_BitmapContextCreate_h
#define V5LiteSample_BitmapContextCreate_h


#endif
#include <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


@interface BitmapContext : NSObject {}
+ (CGContextRef)CGContextRefCreate:(void *)data width:(int) width height:(int) height bitsPerComponent:(int) bitsPerComponent bytesPerRow:(int) bytesPerRow space:(CGColorSpaceRef) space bitmapInfo:(CGBitmapInfo) bitmapInfo;
@end