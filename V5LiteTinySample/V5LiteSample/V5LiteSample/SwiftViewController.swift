//
//  ViewController.swift
//  V5LiteSample
//
//  Created by Hiromi Ogata on 2015/01/27.
//  Copyright (c) 2015年 V-cube. All rights reserved.
//

import Foundation
import UIKit

class SwiftViewController : UIViewController,VRMSLoginDelegate
{
    //@IBOutlet weak var switchPrepare: UISwitch!
    @IBOutlet weak var switchJoin: UISwitch!
    @IBOutlet weak var usePin: UISwitch!
    @IBOutlet weak var pinCodeTextField: UITextField!
    @IBOutlet weak var userIDTextField: UITextField!
    @IBOutlet weak var userPWTextField: UITextField!
    @IBOutlet weak var URLTextField: UITextField!
    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var RenderView0: UIImageView!
    @IBOutlet weak var RenderView1: UIImageView!
    @IBOutlet weak var RenderView2: UIImageView!
    @IBOutlet weak var RenderView3: UIImageView!
    @IBOutlet weak var chatInputView: UITextView!
    @IBOutlet weak var maxParticipantNumber: UILabel!
    @IBOutlet weak var maxParticipantStepper: UIStepper!

    @IBOutlet weak var MicrophoneVolume: UILabel!
    @IBOutlet weak var MicrophoneStepper: UIStepper!

    @IBOutlet weak var SpeakerVolume: UILabel!
    @IBOutlet weak var SpeakerStepper: UIStepper!
    
    let lib:LibV5Lite = LibV5Lite()
    let login : VRMSLogin = VRMSLogin();
    let conferenceEventSink:V5ConferenceEventSinkImpl = V5ConferenceEventSinkImpl()
    let chatEventSink:V5ChatEventSinkImpl = V5ChatEventSinkImpl()
    let deviceEventSink:V5DeviceEventSinkImpl = V5DeviceEventSinkImpl()
    var roomIndex = 0;
    var cameraIndex:Int32 = 1;
    
    var participantPid:String?
    var forceRender:Bool = false
    
    var renderSelfView:Bool = false
    
    @IBAction func onCameraChanged(sender: AnyObject) {
        if (cameraIndex == 1) {
            cameraIndex = 0
        } else {
            cameraIndex = 1
        }
        lib.selectDevice(V5_DEVICE_TYPE_VIDEO, index: cameraIndex)
    }
    func renderUIImageView(image:UIImage, viewPriority:Int) {
        dispatch_async(dispatch_get_main_queue()) {
            () -> Void in
            var renderView:UIImageView?;
            switch viewPriority {
                case 0:
                    renderView = self.RenderView0
                    break;
                case 1:
                    renderView = self.RenderView1
                    break;
                case 2:
                    renderView = self.RenderView2
                    break;
                default:
                    renderView = self.RenderView3
                    break;
                
            }
            renderView!.image = image
            renderView!.contentMode = UIViewContentMode.ScaleAspectFit
            renderView!.clipsToBounds = true
        }
    }
    
    @IBAction func clickedEnter(sender: AnyObject) {
        
//        while true {
//            if lib.userClientReady() {
//                break
//            }
//            sleep(1000)
//        }
        if(usePin.on)
        {
            login.pinLogin(
                URLTextField.text,
                pinCode:pinCodeTextField.text,
                delegate:self
            );
        }
        else
        {
            login.login(
                URLTextField.text,
                id:userIDTextField.text,
                pw:userPWTextField.text,
                index:roomIndex,
                delegate:self
            );
        }
        
    }
    
    func onGetToken(token:String)
    {
        let pw = "";
        let version = "5.1.0.0";
        var result:V5CODE = lib.beginSession(
            "https://" + URLTextField.text + "/api/v5lite/client/",
            token: token,
            version: version);
        if 0 == result.value {
            switchJoin.setOn(true, animated:true)
        }else {
            switchJoin.setOn(false, animated:true)
        }
        
    }
    
    func onSessionLinked()
    {
        var result:V5CODE = lib.joinConference(userNameTextField.text, roomPasswdInfo: nil);
        
        var MicVolume = Int(lib.getMicrophoneVolume())
        
        MicrophoneVolume.text = String(MicVolume)
        MicrophoneStepper.value = Double(MicVolume)
        MicrophoneStepper.maximumValue = 65535.0
        MicrophoneStepper.minimumValue = 0.0
        
        var SpkVolume = Int(lib.getMicrophoneVolume())
        
        SpeakerVolume.text = String(SpkVolume)
        SpeakerStepper.value = Double(SpkVolume)
        SpeakerStepper.maximumValue = 65535.0
        SpeakerStepper.minimumValue = 0.0
        
    }
    
    @IBAction func clickedLogout(sender: AnyObject) {
        lib.logout()
    }
    @IBAction func onMicChanged(sender: UISwitch) {
        lib.muteMicrophone(!sender.on)
    }
    
    @IBAction func onSpeakerChanged(sender: UISwitch) {
        lib.muteSpeaker(!sender.on)
    }
    
    @IBAction func onVideoChanged(sender: UISwitch) {
        lib.muteCamera(!sender.on)
    }
    
    func onJoinConference()
    {
        var MicVolume = Int(lib.getMicrophoneVolume())
        MicrophoneVolume.text = String(MicVolume)
        MicrophoneStepper.value = Double(MicVolume)
        
        var SpkVolume = Int(lib.getMicrophoneVolume())
        SpeakerVolume.text = String(SpkVolume)
        SpeakerStepper.value = Double(SpkVolume)
    }
    
    @IBAction func chatMessage(sender: AnyObject) {
        lib.sendChatMessage( chatInputView.text )
    }

    @IBOutlet weak var conferenceNameTextField: UITextField!
    
    @IBAction func conferenceNameChange(sender: AnyObject) {
        lib.updateConferenceName(conferenceNameTextField.text)
    }
    
    @IBAction func segmentChanged(sender: UISegmentedControl) {
        roomIndex = sender.selectedSegmentIndex
    }
    
    @IBAction func onLockConference(sender: UISwitch) {
        lib.lockConference(!sender.on);
    }

    @IBAction func onRejectParticipant(sender: AnyObject) {
        var rejectPID = "aaa";
        lib.rejectParticipant(rejectPID);
    }
    
    @IBAction func onCrashSend(sender: AnyObject) {
        lib.SendWorkerCrash()
    }
    @IBAction func changeMaxParticipants(sender: UIStepper) {
        let value = sender.value
        maxParticipantNumber.text = String(Int(value))
        lib.setMaxParticipants(Int32(value))
    }

    @IBAction func changeMicrophoneVolume(sender: UIStepper) {
        let value = sender.value
        MicrophoneVolume.text = String(Int(value))
        lib.setMicrophoneVolume(UInt32(value))
    }

    @IBAction func changeSpeakerVolume(sender: UIStepper) {
        let value = sender.value
        SpeakerVolume.text = String(Int(value))
        lib.setSpeakerVolume(UInt32(value))
    }
    
    @IBAction func onDebugSend(sender: AnyObject) {
//        var GuestState : Bool
//        GuestState = lib.isGuest()
//        if( GuestState == true )
//        {
//            println("isGuest = true")
//        }
//        else
//        {
//            println("isGuest = false")
//        }
        
        lib.SendDebug()
    }
    
    @IBAction func forceRenderLastJoinMember(sender: AnyObject) {
        if !forceRender {
            if let pid = participantPid {
                lib.forceRendering(pid)
                forceRender = true
            }
        } else {
            lib.removeForceRendering()
            forceRender = false
        }
    }
    
    @IBAction func toggleSelfView(sender: AnyObject) {
        lib.renderSelfView(renderSelfView)
        renderSelfView = !renderSelfView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        var login : VRMSLogin = VRMSLogin();
    
        // getLogDir in iOS device
        let path:String = (NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true)[0] as? String)!
        let documentDirectory = path.stringByAppendingString("/")
        let pathToLogDir = documentDirectory.cStringUsingEncoding(NSUTF8StringEncoding)!
        let LogDirPtr = UnsafePointer<CChar>(pathToLogDir)
        
        var viewController = UIView()
        
        var rect = V5Rect(xPos:0, yPos:0, width:0, height:0)
        
        // 共通ライブラリからVIDYO-APIでログ出力を実施する場合、
        // "@AppGui" のレベルを変更することでVIDYOライブラリのログレベルを変更せずに
        // 共通ライブラリ部分のログレベル調整ができる。
        
        let logPrio = "warning info@AppGui info@App info@AppEmcpClient info@LmiApp";
//        let logPrio = "warning info@AppGui";
        
        
        LibV5Lite.libInit(documentDirectory, logPriority: logPrio)
        
        // プロキシのアドレス、ポート、ID、パスワード設定
        //lib.setProxySetting(String("https://172.16.1.86"), port: 443, id: String(""), password: String(""))
//        lib.setProxySetting(String("https://172.16.100.1"), port: 8081, id: String(""), password: String(""))
        
        // VIDYOライブラリのプロキシ情報取得先フラグ設定
//        lib.setProxyIEOption( V5_PROXY_VIDYO_NO_SETTING );
//        lib.setProxyIEOption( V5_PROXY_VIDYO_IE );
//        lib.setProxyIEOption( V5_PROXY_VIDYO_IE_MANUAL );
//        lib.setProxyIEOption( V5_PROXY_VIDYO_IE_SCRIPT );
//        lib.setProxyIEOption( V5_PROXY_VISYO_IE_AUTO_DETECT );

        // プロキシ設定を使う場合はtrue
        lib.setProxyForce(true);
        
        lib.startLogic(nil, videoRect: nil)
        
        conferenceEventSink.viewController = self
        lib.setConferenceEventSink(conferenceEventSink)
        
        lib.setChatEventSink(chatEventSink)
        lib.setDeviceEventSink(deviceEventSink,lib: lib)
        
        chatInputView.layer.borderWidth = 1
        chatInputView.layer.borderColor = UIColor.blackColor().CGColor
        
        maxParticipantNumber.text = "4"
        maxParticipantStepper.value = Double(4.0)
        maxParticipantStepper.maximumValue = 16.0
        maxParticipantStepper.minimumValue = 0.0
        
        MicrophoneStepper.maximumValue = 65535.0
        MicrophoneStepper.minimumValue = 0.0
        SpeakerStepper.maximumValue = 65535.0
        SpeakerStepper.minimumValue = 0.0

        RenderView0.contentMode = UIViewContentMode.ScaleAspectFit
        RenderView0.clipsToBounds = true
        RenderView1.contentMode = UIViewContentMode.ScaleAspectFit
        RenderView1.clipsToBounds = true
        RenderView2.contentMode = UIViewContentMode.ScaleAspectFit
        RenderView2.clipsToBounds = true
        RenderView3.contentMode = UIViewContentMode.ScaleAspectFit
        RenderView3.clipsToBounds = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}