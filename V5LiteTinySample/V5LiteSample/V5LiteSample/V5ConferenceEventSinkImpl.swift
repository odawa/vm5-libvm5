//
// Created by Hiromi on 2015/01/26.
// Copyright (c) 2015 V-cube. All rights reserved.
//

import Foundation
import UIKit

@objc class V5ConferenceEventSinkImpl : NSObject, IV5ConferenceEventSinkProtocol {

    var viewController:UIViewController?
    
    override init() {
    }
    
    func onSessionLinked( displayName: String)
    {
        println("[CALLBACK] onSessionLinked!");
        
        if let view = viewController {

            if view is SwiftViewController {
                let swiftView = view as? SwiftViewController
                swiftView!.onSessionLinked()
            }
        }
        
    }
    
    func onBeginSessionFailed(errInfo: ObjcErrorInfo!)
    {
        println("[CALLBACK] onSessionLinked!");
        println("errInfo.category:\(errInfo.category)")
        println("errInfo.reason:\(errInfo.reason)")
        
    }
    
    func onVersionUnmatched( requireVersion: String )
    {
        println("[CALLBACK] onVersionUnmatched!");
        println("requireVersion:\(requireVersion)")
    }
    
    func onParticipantJoined(participant: ObjcParticipant) {
        println("[CALLBACK] onParticipantJoined!")
        println("displayName:\(participant.displayName)")
        if let view = viewController {
            let swiftView = view as? SwiftViewController
            swiftView!.participantPid = participant.id
        }
    }

    func onParticipantLeaved(participant: ObjcParticipant) {
        println("[CALLBACK] onParticipantLeaved!")
        println("displayName:\(participant.displayName)")
    }
        
    func onReceiveJoinConferenceRequest(token: String!, v5LiteURL V5LiteURL: String!) {
        // Mobile Project don't use this method.
    }
    
    func onJoinConferenceFailed(errInfo: ObjcErrorInfo!) {
        println("[CALLBACK] onJoinConferenceFailed!")
        println("errInfo.category:\(errInfo.category)")
        println("errInfo.reason:\(errInfo.reason)")
    }
    
    func onJoinedConference(info:ObjcConferenceInfo) {
        println("[CALLBACK] onJoinedConference!")
        let status = info.conferenceStatus
        info.participantList.iterate{ participant in
            println("displayName:\(participant.displayName)")
        }
        
        if let view = viewController {
            
            if view is SwiftViewController {
                let swiftView = view as? SwiftViewController
                swiftView!.onJoinConference()
            }
        }
        
    }
    
    func onLeavedConference(errInfo: ObjcErrorInfo!) {
        println("[CALLBACK] onLeavedConference!")
//        println("errInfo.category:\(errInfo.category)")
        println("errInfo.reason:\(errInfo.reason)")
    }
    
    func onReceiveVideoFrame(videoRawFrame:ObjcVideoRawFrame!) {
        
            let colorSpace = CGColorSpaceCreateDeviceRGB()
            if colorSpace == nil {
                return
            }
        
            var width = Int32(videoRawFrame.width)
            var height = Int32(videoRawFrame.height)
            var bitsPerComponent = Int32(8);
            var bytesPerRow = Int32(width * 4)
            let gtx = BitmapContext.CGContextRefCreate(
                videoRawFrame.data, width: width, height: height, bitsPerComponent: bitsPerComponent, bytesPerRow: bytesPerRow, space: colorSpace, bitmapInfo: CGBitmapInfo(CGImageAlphaInfo.PremultipliedFirst.rawValue))
           /* let gtx:CGContextRef = CGBitmapContextCreate(videoRawFrame.data
                ,width
                ,height
                ,bitsPerComponent
                ,bytesPerRow
                ,colorSpace
                ,CGBitmapInfo(CGImageAlphaInfo.PremultipliedFirst.rawValue))*/
            let toCGImage:CGImageRef = CGBitmapContextCreateImage(gtx!.takeRetainedValue())
            let image:UIImage? = UIImage(CGImage:toCGImage)
            if viewController is SwiftViewController {
                if let swiftView = viewController as? SwiftViewController {
                    swiftView.renderUIImageView(image!, viewPriority:Int(videoRawFrame.viewPriority))
                }
            }
    }
    
    func onRemoteSourceAdded(added:ObjcRemoteSourceChanged)
    {
        println("[IV5ChatEventSinkProtocol:onRemoteSourceAdded] In Swift");
    }
    
    func onRemoteSourceRemoved(removed:ObjcRemoteSourceChanged)
    {
        println("[IV5ChatEventSinkProtocol:onRemoteSourceRemoved] In Swift");
    }

    func onParticipantListUpdated(participantList: ObjcParticipantArray!) {
        println("[IV5ChatEventSinkProtocol:onParticipantListUpdated] In Swift");
        
        println("[ParticipantList]");
        for var cnt = UInt(0); cnt < participantList.count(); cnt++ {
            let Participant:ObjcParticipant = participantList[cnt] as ObjcParticipant;
            println("---------------------------");
            println("User Count    : \(cnt)");
            println("ParticipantID : \(Participant.id)");
            println("displayName   : \(Participant.displayName)");
        }

        participantList.iterate { participant in
            println("ParticipantID : \(participant.id)");
            println("displayName   : \(participant.displayName)");
        }
    }
    
    func onConferenceStatusUpdated(status:ObjcConferenceStatus) {
        println("[CALLBACK] onConferenceStatusUpdated!")
        println("roomName:\(status.roomName)")
        println("conferenceName:\(status.conferenceName)")
    }
    
    func onSelectedParticipantsUpdated(numberOfSelected: Int32, selectedURI: [AnyObject]!) {
        println("[CALLBACK] onSelectedParticipantsUpdated! (\(selectedURI.count))")
        for var i = Int(0) ; i < selectedURI.count ; i++ {
            println("participants(\(i)) = \(selectedURI[i])")
        }
    }

    func onReconnectConference(info:ObjcConferenceInfo!) {
        println("[CALLBACK] onReconnectConference!")
        let status = info.conferenceStatus
        info.participantList.iterate{ participant in
            println("displayName:\(participant.displayName)")
        }
    }

    func onAddShare(uri: String!, isSelfShare:Bool) {
    }
    
    func onRemoveShare(uri: String!, isSelfShare:Bool) {
    }

    func onRcvRejectParticipant(participant: ObjcParticipant) {
        println("[CALLBACK] onRcvRejectParticipant!")
        println("displayName:\(participant.displayName)")
    }

    func onAsyncError(errInfo: ObjcErrorInfo!) {
        println("[CALLBACK] onAsyncError!")
        println("errInfo->category:\(errInfo.category)")
        println("errInfo->reason:\(errInfo.reason)")
    }
    func onInviteMailIsSent(errInfo: ObjcErrorInfo, status sts: String!, mailAddress address: String!) {
        println("[INVITE MAIL] status = \(sts).")
    }
    func onPastTimeStart() {
        println("[CALLBACK] onPastTimeStart!")

    }
    func onPastTimeStop() {
        println("[CALLBACK] onPastTimeStop!")
    }
    
    func onConferenceNameChangeFailed(nowConferenceName: String!) {
        println("[CALLBACK] onConferenceNameChangeFailed!")
        println("nowConferenceName = \(nowConferenceName)")

    }
    
    func onFloatingWindow(view: NSObject!) {
        println("[CALLBACK] onFloatingWindow!")
    }
    func onNowMaintenance() {
        
    }
    func onConferenceStatisticsNotice(conferenceStatisticsAllInfo: ObjcConferenceStatisticsAllInfo!) {
        
    }
    
}


