//
//  V5ChatEventSinkImpl.swift
//  V5LiteSample
//
//  Created by V-0757 on 2015/02/04.
//  Copyright (c) 2015年 V-cube. All rights reserved.
//

import Foundation
import UIKit

@objc class V5ChatEventSinkImpl : NSObject, IV5ChatEventSinkProtocol {
    
    override init() {
    }
    
    func onChatMessageAdded(message:ObjcMessage) {
        println("[IV5ChatEventSinkProtocol:onChatMessageAdded] In Swift");
        
        println("RecieveMessage");
        println("message.text           : \(message.text)");
        println("message.from           : \(message.from)");
        println("message.displayName    : \(message.displayName)");
        println("message.timestamp      : \(message.timestamp)");
        println("message.isSelfMessage  : \(message.isSelfMessage)");
        
    }
    
    func onChatLogUpdated(messages:NSMutableArray, length:Int64) {
        println("[IV5ChatEventSinkProtocol:onChatLogUpdated] In Swift");
        
        println("[ChatLog]");
        for var cnt = Int64(0); cnt < length; cnt++ {
            let message:ObjcMessage = (messages.objectAtIndex(Int(cnt)) as? ObjcMessage)!;
            println("---------------------------");
            println("Message Count    : \(cnt)");
            println("message.text           : \(message.text)");
            println("message.from           : \(message.from)");
            println("message.displayName    : \(message.displayName)");
            println("message.timestamp      : \(message.timestamp)");
            println("message.isSelfMessage  : \(message.isSelfMessage)");
        }
    }
}


