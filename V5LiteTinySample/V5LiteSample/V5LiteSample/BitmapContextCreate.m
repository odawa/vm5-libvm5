//
//  BitmapContextCreate.m
//  V5LiteSample
//
//  Created by shintaro on H27/05/12.
//  Copyright (c) 平成27年 V-cube. All rights reserved.
//

#import "BitmapContextCreate.h"
@implementation BitmapContext
+ (CGContextRef)CGContextRefCreate:(void *)data width:(int) width height:(int) height bitsPerComponent:(int) bitsPerComponent bytesPerRow:(int) bytesPerRow space:(CGColorSpaceRef) space bitmapInfo:(CGBitmapInfo) bitmapInfo {
    CGContextRef bitmapContext;
    bitmapContext = CGBitmapContextCreate(data, width,  height,  bitsPerComponent,  bytesPerRow,  space,  bitmapInfo);
    
    /* let gtx:CGContextRef = CGBitmapContextCreate(videoRawFrame.data
     ,width
     ,height
     ,bitsPerComponent
     ,bytesPerRow
     ,colorSpace
     ,CGBitmapInfo(CGImageAlphaInfo.PremultipliedFirst.rawValue))
     let toCGImage:CGImageRef = CGBitmapContextCreateImage(gtx)
     let image:UIImage? = UIImage(CGImage:toCGImage)*/
    
    
    return bitmapContext;
}
@end
