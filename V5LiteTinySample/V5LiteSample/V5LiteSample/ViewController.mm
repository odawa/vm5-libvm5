//
//  ViewController.m
//  V5LiteSample
//
//  Created by V-1094 on 2015/01/14.
//  Copyright (c) 2015年 V-cube. All rights reserved.
//

#import "AppDelegate.h"
#include <iostream>
#import "ViewController.h"

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    _lib = [[LibV5Lite alloc] init];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    documentsDirectory = [documentsDirectory stringByAppendingString:@"/"];

    void *idx = NULL;
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    idx = (__bridge void *)(appDelegate.window);
    appDelegate.viewController = self;

    V5Rect v5rect = { 72, 480, 640, 480};
    V5Rect *v5rp  = &v5rect;
    
    V5CODE cod = [LibV5Lite libInit: documentsDirectory logPriority:NULL];
    [_lib startLogic:idx videoRect:v5rp];
    
    impl = [V5ConferenceEventSinkImpl new];
    [_lib setConferenceEventSink:impl];

    chatimpl = [V5ChatEventSinkImpl new];
    [_lib setChatEventSink:chatimpl];
    
    NSLog(@"libInit(V5Initialize): %@", ((cod == V5OK)?@"OK":@"NG"));
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)clicked1:(id)sender {
//    while (true) {
//        if ([_lib userClientReady]) {
//            break;
//        }
//        usleep(100000);
//    }
    
    //const char dummyToken[] = "euiu03pja35lv9dsgrrsbkoul6";
    //const char vURL[]   = "http://dev-v5lite.vcube.net/api/v5lite/client/";

    NSString* dummyToken   = _sessionIDTextField.text;
    NSString* vURL         = _URLTextField.text;
//    NSString* pw           = @"";
//    NSString* uName        = _userNameTextField.text;
    NSString* version      = @"";
    
    V5CODE co2 = [_lib beginSession:vURL token:dummyToken version:version];
    //V5CODE co2 = [_lib joinConference: dummyToken VcubeURL:vURL screenName:uName roomPassword:pw];
    
    BOOL boo2 = (co2 == V5OK) ? true: false;
    [_switchJoin setOn:boo2 animated:true];
    //[_switchPrepare setOn:(_lib.ud->prepareJoin->status == "200") animated:true];

}


- (IBAction)chatMessage:(id)sender
{
    [_lib sendChatMessage:_chatMsgTextField.text];
}

- (IBAction)getParticipant:(id)sender
{
    NSLog(@"[getParticipant:Button Sender]");
    
}



- (IBAction)onMicChanged:(id)sender {
    BOOL on = [sender isOn];
    [_lib muteMicrophone:((on==YES)?false:true)];
}

- (IBAction)onSpeakerChanged:(id)sender {
    BOOL on = [sender isOn];
    [_lib muteSpeaker: ((on==YES)?false:true)];
}

- (IBAction)onVideoChanged:(id)sender {
    BOOL on = [sender isOn];
    [_lib muteCamera: ((on==YES)?false:true)];
}

- (IBAction)logout:(id)sender {
    [_lib logout];
}

- (void)setOrientation: (UIDeviceOrientation)orie {
    [_lib setOrientation: orie];
}

- (void)setFrameDirection: (int)x y:(int)y w:(unsigned int)w h:(unsigned int)h
{
    [_lib setFrameDirection: x y:y w:w h:h];
}

- (IBAction)segmentChanged:(id)sender {
    UISegmentedControl* seg = (UISegmentedControl*)sender;
    switch (seg.selectedSegmentIndex) {
        case 0:
            _sessionIDTextField.text = @"1b9fb1d29e9f1765b723d221727c638";
            _userNameTextField.text = @"V5Lite test001";
            break;
        case 1:
            _sessionIDTextField.text = @"301202e0208a29c2b4e9794d72b017b";
            _userNameTextField.text = @"V5Lite test002";
            break;
        case 2:
            _sessionIDTextField.text = @"29828b3c761ebe1acd362c5117cf4b2";
            _userNameTextField.text = @"V5Lite test003";
            break;
        case 3:
            _sessionIDTextField.text = @"dc7b548ab2878fae73c5681e244b002";
            _userNameTextField.text = @"V5Lite test004";
            break;
        case 4:
            _sessionIDTextField.text = @"ad564d2cfae76c198648a2cd2054170";
            _userNameTextField.text = @"V5Lite test005";
            break;
        default:
            _sessionIDTextField.text = @"ad564d2cfae76c198648a2cd2054170";
            _userNameTextField.text = @"V5Lite test005";
            break;
    }
}

- (IBAction)setCamera:(id)sender {
    V5DevList* li = [_lib getDeviceList:V5_DEVICE_TYPE_VIDEO];
    int len = [li length];
    if (len < 2) {
        NSLog(@"cannot switch!");
    }
    BOOL on = [sender isOn];

    id mics = [_lib getDeviceList: V5_DEVICE_TYPE_AUDIO_IN];
    NSLog(@"microphones: %@", mics);

    [_lib selectDevice:V5_DEVICE_TYPE_VIDEO index:(on == YES)?1:0];
}

@end
