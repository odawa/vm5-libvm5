#pragma once
#include "V5WindowCli.h"
#include "V5DesktopCli.h"
using namespace System;

public ref struct V5WindowsAndDesktopsCli
{
public:
	array<V5WindowCli^>^ windows;
	array<V5DesktopCli^>^ desktops;
};
