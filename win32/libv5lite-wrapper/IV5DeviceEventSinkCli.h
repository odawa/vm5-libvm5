#pragma once

namespace libv5liteWrapper
{
	public interface class IV5DeviceEventSinkCli
	{
	public:
		virtual void OnAudioInDeviceListChanged(int index);
		virtual void OnAudioOutDeviceListChanged(int index);
		virtual void OnVideoDeviceListChanged(int index);
		virtual void OnSelectedSystemAudioInDevicesChanged(int index);
		virtual void OnSelectedSystemAudioOutDevicesChanged(int index);
	};
}
