#pragma once
#include <V5Lite/V5Client.h>
#include "Utf8UnicodeConverter.h"
#include "V5ConferenceInfoCli.h"
#include "V5ConferenceStatusCli.h"
#include "V5ParticipantCli.h"
#include "V5ChatMessageCli.h"
#include "V5DeviceNameCli.h"
#include "V5Lite/DeviceConfiguration.h"
#include "V5ErrorInfoCli.h"
#include "V5WindowCli.h"
#include "V5DesktopCli.h"
#include "V5WindowsAndDesktopsCli.h"
#include "V5RomoteSourceChangedCli.h"
#include "V5ConferenceStatisticsAllInfoCli.h"

namespace libv5liteWrapper
{
	ref class CliObjectBuilder
	{
	public:
        typedef picojson::value any;
        typedef picojson::value const& any_arg;

        typedef System::Collections::Generic::List<Object^> V5Array;
        typedef System::Collections::Generic::Dictionary<String^, Object^> V5Object;

		CliObjectBuilder();
		virtual ~CliObjectBuilder();

        V5ConferenceInfoCli^ BuildV5ConferenceInfo(V5ConferenceInfo const& conferenceInfo);
        V5ConferenceStatusCli^ BuildV5ConferenceStatus(V5ConferenceStatus const& conferenceStatus);
		V5ParticipantCli^ BuildV5Participant(const V5Participant* participant);
		array<V5ParticipantCli^>^ BuildV5ParticipantArray(const V5Participant* const participants[], unsigned long length);
		array<V5ParticipantCli^>^ BuildV5ParticipantArray(const V5ParticipantList& participantList);
		V5ChatMessageCli^ BuildV5ChatMessage(const V5ChatMessage* chatMessage);
		array<V5ChatMessageCli^>^ BuildV5ChatMessageArray(const V5ChatMessage* const chatMessages[], unsigned long length);
		V5DeviceNameCli^ BuildV5DeviceName(const V5DeviceName* deviceName);
		V5ErrorInfoCli^ BuildV5ErrorInfo(V5ErrorInfo const& errInfo);
        V5WindowsAndDesktopsCli^ BuildV5WindowsAndDesktops(V5WindowsAndDesktops const& v5WindowsAndDesktops);
        V5RomoteSourceChangedCli^ BuidV5RemoteSourceChanged(const V5RemoteSourceChanged& remoteSourceChanged);
        V5ConferenceStatisticsAllInfoCli^ BuildV5ConferenceStatisticsAllInfo(V5ConferenceStatisticsAllInfo const& confStatInfo);

        Object^ BuildV5AnyObject(any_arg any);

	private:
		V5WindowCli^ BuildV5Window(const V5Window& v5Window);
		V5DesktopCli^ BuildV5Desktop(const V5Desktop& v5Desktop);

	private:
		Utf8UnicodeConverter^ m_utf8UnicodeConverter;
	};
}
