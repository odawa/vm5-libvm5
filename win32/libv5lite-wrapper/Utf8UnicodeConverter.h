#pragma once
#include <vector>
using namespace System;

namespace libv5liteWrapper
{
	public ref class Utf8UnicodeConverter
	{
	public:
		Utf8UnicodeConverter();

		static String^ ToUnicode(const char* utf8);
		static void ToUtf8(std::vector<char>& utf8, String^% unicode);
	};
}
