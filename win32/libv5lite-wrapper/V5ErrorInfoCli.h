#pragma once
#include <V5Lite/V5Enumerations.h>
using namespace System;

public ref struct V5ErrorInfoCli
{
public:
    vrms5cli::V5ErrorCode code_deprecated;
	vrms5cli::V5Component category;
	vrms5cli::V5ErrorReason reason;
	String^ status;
};
