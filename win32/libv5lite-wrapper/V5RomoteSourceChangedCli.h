#pragma once
#include <V5Lite/V5Enumerations.h>
using namespace System;

public ref struct V5RomoteSourceChangedCli
{
public:
    vrms5cli::V5MediaSourceType sourceType;
    String^ participantURI;
    String^ displayName;
    String^ sourceName;

};
