#include "stdafx.h"
#include "CliObjectBuilder.h"

namespace libv5liteWrapper
{
	CliObjectBuilder::CliObjectBuilder()
	{
		m_utf8UnicodeConverter = gcnew Utf8UnicodeConverter();
	}

	CliObjectBuilder::~CliObjectBuilder()
	{
		delete m_utf8UnicodeConverter;
	}

	V5ConferenceInfoCli^ CliObjectBuilder::BuildV5ConferenceInfo(V5ConferenceInfo const& conferenceInfo)
	{
		V5ConferenceInfoCli^ conferenceInfoCli = gcnew V5ConferenceInfoCli();
		conferenceInfoCli->conferenceStatus = BuildV5ConferenceStatus(*conferenceInfo.ConfSts);
		conferenceInfoCli->participants = BuildV5ParticipantArray(conferenceInfo.participants, conferenceInfo.length);
		conferenceInfoCli->participantNumber = conferenceInfo.length;
		conferenceInfoCli->selfParticipant = BuildV5Participant(conferenceInfo.selfParticipant);
		return conferenceInfoCli;
	}

    V5ConferenceStatusCli^ CliObjectBuilder::BuildV5ConferenceStatus(V5ConferenceStatus const& conferenceStatus)
	{
		V5ConferenceStatusCli^ conferenceStatusCli = gcnew V5ConferenceStatusCli();
		conferenceStatusCli->ConferenceName = m_utf8UnicodeConverter->ToUnicode(conferenceStatus.ConferenceName);
		conferenceStatusCli->RoomName = m_utf8UnicodeConverter->ToUnicode(conferenceStatus.RoomName);
		conferenceStatusCli->ConferenceURL = gcnew String(conferenceStatus.ConferenceURL);
		conferenceStatusCli->ConferencePinCode = gcnew String(conferenceStatus.ConferencePinCode);
		conferenceStatusCli->DocumentSender = m_utf8UnicodeConverter->ToUnicode(conferenceStatus.DocumentSender);
		conferenceStatusCli->ConferenceStartTime = conferenceStatus.ConferenceStartTime;
		conferenceStatusCli->ConferenceEndTime = conferenceStatus.ConferenceEndTime;
		conferenceStatusCli->RemainTime = conferenceStatus.RemainTime;
		conferenceStatusCli->PastTime = conferenceStatus.PastTime;
		conferenceStatusCli->MaintenanceStartTime = conferenceStatus.MaintenanceStartTime;
		conferenceStatusCli->MaintenanceRemainTime = conferenceStatus.MaintenanceRemainTime;
		conferenceStatusCli->MaintenanceEndTime = conferenceStatus.MaintenanceEndTime;
		conferenceStatusCli->isReserved = conferenceStatus.isReserved;
		conferenceStatusCli->isRecoading = conferenceStatus.isRecoading;
		conferenceStatusCli->isLocked = conferenceStatus.isLocked;
		conferenceStatusCli->OldConferenceName = m_utf8UnicodeConverter->ToUnicode(conferenceStatus.OldConferenceName);
		conferenceStatusCli->OldDocumentSender = m_utf8UnicodeConverter->ToUnicode(conferenceStatus.OldDocumentSender);
		conferenceStatusCli->OldRemainTime = conferenceStatus.OldRemainTime;
		conferenceStatusCli->OldMaintenanceStartTime = conferenceStatus.OldMaintenanceStartTime;
		conferenceStatusCli->OldMaintenanceRemainTime = conferenceStatus.OldMaintenanceRemainTime;
		conferenceStatusCli->OldMaintenanceEndTime = conferenceStatus.OldMaintenanceEndTime;
		conferenceStatusCli->OldisRecoading = conferenceStatus.OldisRecoading;
		conferenceStatusCli->OldisLocked = conferenceStatus.OldisLocked;
		conferenceStatusCli->ConferenceNameChanged = conferenceStatus.ConferenceNameChanged;
		conferenceStatusCli->DocumentSenderChanged = conferenceStatus.DocumentSenderChanged;
		conferenceStatusCli->RemainTimeChanged = conferenceStatus.RemainTimeChanged;
		conferenceStatusCli->MaintenanceStartTimeChanged = conferenceStatus.MaintenanceStartTimeChanged;
		conferenceStatusCli->MaintenanceRemainTimeChanged = conferenceStatus.MaintenanceRemainTimeChanged;
		conferenceStatusCli->MaintenanceEndTimeChanged = conferenceStatus.MaintenanceEndTimeChanged;
		conferenceStatusCli->isRecoadingChanged = conferenceStatus.isRecoadingChanged;
		conferenceStatusCli->isLockedChanged = conferenceStatus.isLockedChanged;
		return conferenceStatusCli;
	}

	V5ParticipantCli^ CliObjectBuilder::BuildV5Participant(const V5Participant* participant)
	{
		V5ParticipantCli^ participantCli = gcnew V5ParticipantCli();
		participantCli->id = gcnew String(participant->id);
		participantCli->displayName = m_utf8UnicodeConverter->ToUnicode(participant->displayName);
		return participantCli;
	}

	array<V5ParticipantCli^>^ CliObjectBuilder::BuildV5ParticipantArray(const V5Participant* const participants[], unsigned long length)
	{
		array<V5ParticipantCli^>^ participantArrayCli = gcnew array<V5ParticipantCli^>(length);
		for (unsigned long i = 0; i < length; ++i)
		{
			participantArrayCli[i] = BuildV5Participant(participants[i]);
		}
		return participantArrayCli;
	}

	array<V5ParticipantCli^>^ CliObjectBuilder::BuildV5ParticipantArray(const V5ParticipantList& participantList)
	{
		unsigned long length = participantList.size();
		array<V5ParticipantCli^>^ participantArrayCli = gcnew array<V5ParticipantCli^>(length);
		for (unsigned long i = 0; i < length; ++i)
		{
			participantArrayCli[i] = BuildV5Participant(participantList[i]);
		}
		return participantArrayCli;
	}

	V5ChatMessageCli^ CliObjectBuilder::BuildV5ChatMessage(const V5ChatMessage* chatMessage)
	{
		V5ChatMessageCli^ chatMessageCli = gcnew V5ChatMessageCli();

		//後で実装
		//要改行コード変換
		chatMessageCli->text = m_utf8UnicodeConverter->ToUnicode(chatMessage->text);
		chatMessageCli->from = m_utf8UnicodeConverter->ToUnicode(chatMessage->from);
		chatMessageCli->displayName = m_utf8UnicodeConverter->ToUnicode(chatMessage->displayName);
		chatMessageCli->timestamp = chatMessage->timestamp;
		chatMessageCli->isSelfMessage = chatMessage->isSelfMessage;
		return chatMessageCli;
	}

	array<V5ChatMessageCli^>^ CliObjectBuilder::BuildV5ChatMessageArray(const V5ChatMessage* const chatMessages[], unsigned long length)
	{
		array<V5ChatMessageCli^>^ chatMessageArrayCli = gcnew array<V5ChatMessageCli^>(length);
		for (unsigned long i = 0; i < length; ++i)
		{
			chatMessageArrayCli[i] = BuildV5ChatMessage(chatMessages[i]);
		}
		return chatMessageArrayCli;
	}

	V5DeviceNameCli^ CliObjectBuilder::BuildV5DeviceName(const V5DeviceName* deviceName)
	{
		V5DeviceNameCli^ deviceNameCli = gcnew V5DeviceNameCli();
		deviceNameCli->name = m_utf8UnicodeConverter->ToUnicode(deviceName->name.c_str());
		deviceNameCli->phyDevName = m_utf8UnicodeConverter->ToUnicode(deviceName->phyDevName.c_str());
		return deviceNameCli;
	}

	V5ErrorInfoCli^ CliObjectBuilder::BuildV5ErrorInfo(const V5ErrorInfo& errInfo)
	{
		V5ErrorInfoCli^ v5ErrorInfoCli = gcnew V5ErrorInfoCli();
        v5ErrorInfoCli->code_deprecated = static_cast<vrms5cli::V5ErrorCode>(errInfo.code_deprecated);
		v5ErrorInfoCli->category = static_cast<vrms5cli::V5Component>(errInfo.category);
		v5ErrorInfoCli->reason = static_cast<vrms5cli::V5ErrorReason>(errInfo.reason);
		v5ErrorInfoCli->status = m_utf8UnicodeConverter->ToUnicode(errInfo.status.c_str());

		return v5ErrorInfoCli;
	}

    V5WindowsAndDesktopsCli^ CliObjectBuilder::BuildV5WindowsAndDesktops(V5WindowsAndDesktops const& windowsAndDesktops)
	{
		V5WindowsAndDesktopsCli^ v5WindowsAndDesktopsCli = gcnew V5WindowsAndDesktopsCli();
		int v5WindowNum = windowsAndDesktops.windows.size();
		array<V5WindowCli^>^ v5WindowArrayCli = gcnew array<V5WindowCli^>(v5WindowNum);
		for (int i = 0; i < v5WindowNum; ++i)
		{
			v5WindowArrayCli[i] = BuildV5Window(windowsAndDesktops.windows[i]);
		}
		v5WindowsAndDesktopsCli->windows = v5WindowArrayCli;

		int v5DesktopNum = windowsAndDesktops.desktops.size();
		array<V5DesktopCli^>^ v5DesktopArrayCli = gcnew array<V5DesktopCli^>(v5DesktopNum);
		for (int i = 0; i < v5DesktopNum; ++i)
		{
			v5DesktopArrayCli[i] = BuildV5Desktop(windowsAndDesktops.desktops[i]);
		}
		v5WindowsAndDesktopsCli->desktops = v5DesktopArrayCli;
		return v5WindowsAndDesktopsCli;
	}

    V5RomoteSourceChangedCli^ CliObjectBuilder::BuidV5RemoteSourceChanged(const V5RemoteSourceChanged& src)
    {
        V5RomoteSourceChangedCli^ dest = gcnew V5RomoteSourceChangedCli();
		dest->sourceType = static_cast<vrms5cli::V5MediaSourceType>(src.sourceType);
        dest->participantURI = m_utf8UnicodeConverter->ToUnicode(src.participantURI);
        dest->displayName = m_utf8UnicodeConverter->ToUnicode(src.displayName);
        dest->sourceName = m_utf8UnicodeConverter->ToUnicode(src.sourceName);
        return dest;
    }

    V5ConferenceStatisticsAllInfoCli^ CliObjectBuilder::BuildV5ConferenceStatisticsAllInfo(V5ConferenceStatisticsAllInfo const& confStatInfo)
    {
        return gcnew V5ConferenceStatisticsAllInfoCli(confStatInfo);
    }

    V5WindowCli^ CliObjectBuilder::BuildV5Window(const V5Window& v5Window)
	{
		V5WindowCli^ v5WindowCli = gcnew V5WindowCli();
		IntPtr managedHWND(v5Window.appWindowId);
		v5WindowCli->appWindowId = managedHWND;
        v5WindowCli->appWindowAppName = m_utf8UnicodeConverter->ToUnicode(v5Window.appWindowAppName.c_str());
		return v5WindowCli;
	}

	V5DesktopCli^ CliObjectBuilder::BuildV5Desktop(const V5Desktop& v5Desktop)
	{
		V5DesktopCli^ v5DesktopCli = gcnew V5DesktopCli();
		UINT managedHWND(v5Desktop.sysDesktopId);
		v5DesktopCli->sysDesktopId = managedHWND;
		v5DesktopCli->sysDesktopName = m_utf8UnicodeConverter->ToUnicode(v5Desktop.sysDesktopName.c_str());
		v5DesktopCli->sysDesktopRect.xPos = v5Desktop.sysDesktopRect.xPos;
		v5DesktopCli->sysDesktopRect.yPos = v5Desktop.sysDesktopRect.yPos;
		v5DesktopCli->sysDesktopRect.width = v5Desktop.sysDesktopRect.width;
		v5DesktopCli->sysDesktopRect.height = v5Desktop.sysDesktopRect.height;
		return v5DesktopCli;
	}

    Object^ CliObjectBuilder::BuildV5AnyObject(any_arg any)
    {
        if (any.is<bool>())
        {
            return any.get<bool>();
        }
        else if (any.is<double>())
        {
            return any.get<double>();
        }
        else if (any.is<std::string>())
        {
            return Utf8UnicodeConverter::ToUnicode(any.get<std::string>().c_str());
        }
        else if (any.is<any::array>())
        {
            auto& ary = any.get<any::array>();
            V5Array^ sb = gcnew V5Array();
            sb->Capacity = ary.size();
            for (auto& element : ary)
            {
                sb->Add(BuildV5AnyObject(element));
            }
            return sb;
        }
        else if (any.is<any::object>())
        {
            auto& object = any.get<any::object>();
            V5Object^ hashMap = gcnew V5Object();
            for (auto element : object)
            {
                auto key = Utf8UnicodeConverter::ToUnicode(element.first.c_str());
                hashMap->Add(key, BuildV5AnyObject(element.second));
            }
            return hashMap;
        }
        return nullptr;
    }

}
