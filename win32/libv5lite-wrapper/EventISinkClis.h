#pragma once
#include "IV5ConferenceEventSinkCli.h"
#include "IV5ChatEventSinkCli.h"
#include "IV5DeviceEventSinkCli.h"

namespace libv5liteWrapper
{
	public ref struct EventISinkClis
	{
	public:
		IV5ConferenceEventSinkCli^ iV5ConferenceEventSinkCli;
		IV5ChatEventSinkCli^ iV5ChatEventSinkCli;
		IV5DeviceEventSinkCli^ iV5DeviceEventSinkCli;
	};
}
