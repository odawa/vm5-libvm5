#pragma once
using namespace System;

public ref struct V5ConferenceStatusCli
{
public:
	String^ ConferenceName;
	String^ RoomName;
	String^ ConferenceURL;
	String^ ConferencePinCode;
	String^ DocumentSender;
	Int64	ConferenceStartTime;
	Int64	ConferenceEndTime;
	Int64	RemainTime;
	Int64	PastTime;
	Int64   MaintenanceStartTime;
	Int64	MaintenanceRemainTime;
	Int64   MaintenanceEndTime;
	Boolean isReserved;
	Boolean isRecoading;
	Boolean isLocked;

	// 変更前パラメータ
	String^		OldConferenceName;
	String^		OldDocumentSender;
	Int64		OldRemainTime;
	Int64		OldMaintenanceStartTime;
	Int64		OldMaintenanceRemainTime;
	Int64		OldMaintenanceEndTime;
	Boolean		OldisRecoading;
	Boolean		OldisLocked;

	// 変更フラグ(true=変更あり)
	Boolean		ConferenceNameChanged;
	Boolean		DocumentSenderChanged;
	Boolean		RemainTimeChanged;
	Boolean		MaintenanceStartTimeChanged;
	Boolean		MaintenanceRemainTimeChanged;
	Boolean		MaintenanceEndTimeChanged;
	Boolean		isRecoadingChanged;
	Boolean		isLockedChanged;
};
