#pragma once
#include "V5ChatMessageCli.h"
using namespace System;

namespace libv5liteWrapper
{
	public interface class IV5ChatEventSinkCli
	{
	public:
		virtual void OnChatMessageAdded(const V5ChatMessageCli^ message);
		virtual void OnChatLogUpdated(array<V5ChatMessageCli^>^ messages, unsigned long length);
		virtual void OnErrorNotification();
	};
}
