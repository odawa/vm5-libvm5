#pragma once
#include <V5Lite/V5DataTypes.h>
#include "Utf8UnicodeConverter.h"

public ref struct V5StatisticsCommunicationInfoCli
{
    bool vmCommunicationStatus;
    bool vmCommunicationViaVidyoProxy;
    bool vmCommunicationViaWebProxy;
    bool vrCommunicationStatus;
    bool vrCommunicationViaVidyoProxy;
    bool vrCommunicationViaWebProxy;

internal:
    V5StatisticsCommunicationInfoCli(V5StatisticsCommunicationInfo const& src)
        : vmCommunicationStatus(src.vmCommunicationStatus)
        , vmCommunicationViaVidyoProxy(src.vmCommunicationViaVidyoProxy)
        , vmCommunicationViaWebProxy(src.vmCommunicationViaWebProxy)
        , vrCommunicationStatus(src.vrCommunicationStatus)
        , vrCommunicationViaVidyoProxy(src.vrCommunicationViaVidyoProxy)
        , vrCommunicationViaWebProxy(src.vrCommunicationViaWebProxy)
    {
    }
};

public ref struct V5StatisticsEndpointStatusCli
{
    unsigned int endPointStatus;
internal:
    V5StatisticsEndpointStatusCli(V5StatisticsEndpointStatus const& src)
        : endPointStatus(src.endPointStatus)
    {
    }
};

public ref struct V5StatisticsBandwidthInfoCli
{
    unsigned int availSendBwVideo;
	unsigned int availSendBwAudio;
	unsigned int availSendBwApplication;
	unsigned int availSendBwMax;
	unsigned int actualSendBwVideo;
	unsigned int actualSendBwAudio;
	unsigned int actualSendBwApplication;
	unsigned int actualSendBwMax;
	unsigned int availRecvBwVideo;
	unsigned int availRecvBwAudio;
	unsigned int availRecvBwApplication;
	unsigned int availRecvBwMax;
	unsigned int actualRecvBwVideo;
	unsigned int actualRecvBwAudio;
	unsigned int actualRecvBwApplication;
	unsigned int actualRecvBwMax;

internal:
    V5StatisticsBandwidthInfoCli(V5StatisticsBandwidthInfo const& src)
		: availSendBwVideo(src.AvailSendBwVideo)
		, availSendBwAudio(src.AvailSendBwAudio)
		, availSendBwApplication(src.AvailSendBwApplication)
		, availSendBwMax(src.AvailSendBwMax)
		, actualSendBwVideo(src.ActualSendBwVideo)
		, actualSendBwAudio(src.ActualSendBwAudio)
		, actualSendBwApplication(src.ActualSendBwApplication)
		, actualSendBwMax(src.ActualSendBwMax)
		, availRecvBwVideo(src.AvailRecvBwVideo)
		, availRecvBwAudio(src.AvailRecvBwAudio)
		, availRecvBwApplication(src.AvailRecvBwApplication)
		, availRecvBwMax(src.AvailRecvBwMax)
		, actualRecvBwVideo(src.ActualRecvBwVideo)
		, actualRecvBwAudio(src.ActualRecvBwAudio)
		, actualRecvBwApplication(src.ActualRecvBwApplication)
		, actualRecvBwMax(src.ActualRecvBwMax)
    {
    }
};

public ref struct V5StatisticsConferenceInfoCli
{
    bool recording;
    bool webcast;

internal:
    V5StatisticsConferenceInfoCli(V5StatisticsConferenceInfo const& src)
        : recording(src.recording)
        , webcast(src.webcast)
    {
    }
};

public ref struct V5StatisticsConnectivityCli
{
    String^         serverAddress;
    String^         serverPort;
    bool            serverSecured;
    String^         vmIdentity;
    String^         userName;
    String^         portalAddress;
    String^         portalVersion;
    String^         locationTag;
    String^         vidyoProxyAddress;
    String^         vidyoProxyPort;
    bool            guestLogin;
    String^         clientExternalIPAddress;
    unsigned int    proxyType;
    String^         reverseProxyAddress;
    String^         reverseProxyPort;

internal:
    V5StatisticsConnectivityCli(V5StatisticsConnectivity const& src)
        : serverAddress(ToUnicode(src.serverAddress))
        , serverPort(ToUnicode(src.serverPort))
        , serverSecured(src.serverSecured)
        , vmIdentity(ToUnicode(src.vmIdentity))
        , userName(ToUnicode(src.userName))
        , portalAddress(ToUnicode(src.portalAddress))
        , portalVersion(ToUnicode(src.portalVersion))
        , locationTag(ToUnicode(src.locationTag))
        , vidyoProxyAddress(ToUnicode(src.vidyoProxyAddress))
        , vidyoProxyPort(ToUnicode(src.vidyoProxyPort))
        , guestLogin(src.guestLogin)
        , clientExternalIPAddress(ToUnicode(src.clientExternalIPAddress))
        , proxyType(src.proxyType)
        , reverseProxyAddress(ToUnicode(src.reverseProxyAddress))
        , reverseProxyPort(ToUnicode(src.reverseProxyPort))
    {
    }

private:
    static String^ ToUnicode(char const* utf8)
    {
        return libv5liteWrapper::Utf8UnicodeConverter::ToUnicode(utf8);
    }
};

public ref struct V5StatisticsSessionDisplayInfoCli
{
    unsigned int    sessionDisplayContext;
    String^         sessionDisplayText;

internal:
    V5StatisticsSessionDisplayInfoCli(V5StatisticsSessionDisplayInfo const& src)
        : sessionDisplayContext(src.sessionDisplayContext)
        , sessionDisplayText(ToUnicode(src.sessionDisplayText))
    {

    }

private:
    static String^ ToUnicode(char const* utf8)
    {
        return libv5liteWrapper::Utf8UnicodeConverter::ToUnicode(utf8);
    }
};

public ref struct V5StatisticsMediaInfoCli
{
    unsigned int    numIFrames;
    unsigned int    numFirs;
    unsigned int    numNacks;
    unsigned int    mediaRTT;

internal:
    V5StatisticsMediaInfoCli(V5StatisticsMediaInfo const& src)
        : numIFrames(src.numIFrames)
        , numFirs(src.numFirs)
        , numNacks(src.numNacks)
        , mediaRTT(src.mediaRTT)
    {
    }
};

public ref struct V5StatisticsParticipantInfoCli
{
    String^         Name;
    String^         URI;
    unsigned long   bytesRcvd; //Reserved for future use
    unsigned long   numFirsSent; //Reserved for future use.
    unsigned long   numNacksSent; //Reserved for future use.
    unsigned long   numDistinctNacksSent;//Reserved for future use.
    unsigned long   receivedFrameRate;
    unsigned long   decodedFrameRate;
    unsigned long   displayedFrameRate;
    unsigned long   receivedPacketRate;//Not currently implemented
    unsigned long   receivedBpsVideo;
    unsigned long   receivedBpsAudio;//Reserved for future use
    unsigned long   receivedWidth;
    unsigned long   receivedHeight;
    unsigned long   receivedBytesVideo;
    unsigned long   receivedBytesAudio;

internal:
    V5StatisticsParticipantInfoCli(unsigned int index, V5StatisticsParticipantInfo const& src)
        : Name(ToUnicode(src.Name[index]))
        , URI(ToUnicode(src.URI[index]))
        , bytesRcvd(src.bytesRcvd[index])
        , numFirsSent(src.numFirsSent[index])
        , numNacksSent(src.numNacksSent[index])
        , numDistinctNacksSent(src.numDistinctNacksSent[index])
        , receivedFrameRate(src.receivedFrameRate[index])
        , decodedFrameRate(src.decodedFrameRate[index])
        , displayedFrameRate(src.displayedFrameRate[index])
        , receivedPacketRate(src.receivedPacketRate[index])
        , receivedBpsVideo(src.receivedBpsVideo[index])
        , receivedBpsAudio(src.receivedBpsAudio[index])
        , receivedWidth(src.receivedWidth[index])
        , receivedHeight(src.receivedHeight[index])
        , receivedBytesVideo(src.receivedBytesVideo[index])
        , receivedBytesAudio(src.receivedBytesAudio[index])
    {
    }

private:
    static String^ ToUnicode(char const* utf8)
    {
        return libv5liteWrapper::Utf8UnicodeConverter::ToUnicode(utf8);
    }
};

public ref struct V5StatisticsRateShaperInfoCli
{
    unsigned int delayVideoPriorytyNormal;
    unsigned int numPacketsVideoPriorytyNormal;
    unsigned int numFramesVideoPriorytyNormal;
    unsigned int numDroppedVideoPriorytyNormal;
    unsigned int delayVideoPriorutyRetransmit;
    unsigned int numPacketsVideoPriorutyRetransmit;
    unsigned int numFramesVideoPriorutyRetransmit;
    unsigned int numDroppedVideoPriorutyRetransmit;
    unsigned int delayAppPriorityNormal;
    unsigned int numPacketsAppPriorityNormal;
    unsigned int numFramesAppPriorityNormal;
    unsigned int numDroppedAppPriorityNormal;
    unsigned int delayAppPriorityRetransmit;
    unsigned int numPacketsAppPriorityRetransmit;
    unsigned int numFramesAppPriorityRetransmit;
    unsigned int numDroppedAppPriorityRetransmit;

internal:
    V5StatisticsRateShaperInfoCli(V5StatisticsRateShaperInfo const& src)
        : delayVideoPriorytyNormal(src.delayVideoPriorytyNormal)
        , numPacketsVideoPriorytyNormal(src.numPacketsVideoPriorytyNormal)
        , numFramesVideoPriorytyNormal(src.numFramesVideoPriorytyNormal)
        , numDroppedVideoPriorytyNormal(src.numDroppedVideoPriorytyNormal)
        , delayVideoPriorutyRetransmit(src.delayVideoPriorutyRetransmit)
        , numPacketsVideoPriorutyRetransmit(src.numPacketsVideoPriorutyRetransmit)
        , numFramesVideoPriorutyRetransmit(src.numFramesVideoPriorutyRetransmit)
        , numDroppedVideoPriorutyRetransmit(src.numDroppedVideoPriorutyRetransmit)
        , delayAppPriorityNormal(src.delayAppPriorityNormal)
        , numPacketsAppPriorityNormal(src.numPacketsAppPriorityNormal)
        , numFramesAppPriorityNormal(src.numFramesAppPriorityNormal)
        , numDroppedAppPriorityNormal(src.numDroppedAppPriorityNormal)
        , delayAppPriorityRetransmit(src.delayAppPriorityRetransmit)
        , numPacketsAppPriorityRetransmit(src.numPacketsAppPriorityRetransmit)
        , numFramesAppPriorityRetransmit(src.numFramesAppPriorityRetransmit)
        , numDroppedAppPriorityRetransmit(src.numDroppedAppPriorityRetransmit)
    {
    }
};

public ref struct V5StatisticsFrameRateInfoCli
{
    unsigned int captureFrameRate;
    unsigned int encodeFrameRate;
    unsigned int sendFrameRate; 

internal:
    V5StatisticsFrameRateInfoCli(V5StatisticsFrameRateInfo const& src)
        : captureFrameRate(src.captureFrameRate)
        , encodeFrameRate(src.encodeFrameRate)
        , sendFrameRate(src.sendFrameRate)
    {
    }
};

public ref struct V5VRectCli
{
    int             xPos;
    int             yPos;
    unsigned int    width;
    unsigned int    height;

internal:
    V5VRectCli(V5VRect const& src)
        : xPos(src.xPos)
        , yPos(src.yPos)
        , width(src.width)
        , height(src.height)
    {
    }

};


//  +([0-9a-zA-Z]+);
// , $1(ToUnicode(src.$1))
public ref struct V5StatisticsParticipantDataCli
{
    String^         uri;
    String^         name;
    V5VRectCli^     videoResolution;
    unsigned int    videoKBitsPerSecRecv;
    unsigned int    audioKBitsPerSecRecv;
    unsigned int    firs;
    unsigned int    nacks;
    unsigned int    videoFrameRate;
    unsigned int    videoDecodedFrameRate;
    unsigned int    videoDisplayedFrameRate;

internal:
    V5StatisticsParticipantDataCli(V5StatisticsParticipantData const& src)
        : uri(ToUnicode(src.uri))
        , name(ToUnicode(src.uri))
        , videoResolution(gcnew V5VRectCli(src.videoResolution))
        , videoKBitsPerSecRecv(src.videoKBitsPerSecRecv)
        , audioKBitsPerSecRecv(src.audioKBitsPerSecRecv)
        , firs(src.firs)
        , nacks(src.nacks)
        , videoFrameRate(src.videoFrameRate)
        , videoDecodedFrameRate(src.videoDecodedFrameRate)
        , videoDisplayedFrameRate(src.videoDisplayedFrameRate)
    {

    }

private:
    static String^ ToUnicode(char const* utf8)
    {
        return libv5liteWrapper::Utf8UnicodeConverter::ToUnicode(utf8);
    }
};

public ref struct V5StatisticsParticipantStatisticsListCli
{
    array<V5StatisticsParticipantDataCli^>^  statistics;

internal:
    V5StatisticsParticipantStatisticsListCli(V5StatisticsParticipantStatisticsList const& src)
        : statistics(convert(src))
    {
    }

private:
    static array<V5StatisticsParticipantDataCli^>^ convert(V5StatisticsParticipantStatisticsList const& src)
    {
        auto result = gcnew array<V5StatisticsParticipantDataCli^>(src.numParticipants);
        for (unsigned int i = 0; i < src.numParticipants; i++)
        {
            result[i] = gcnew V5StatisticsParticipantDataCli(src.statistics[i]);
        }
        return result;
    }
};

public ref struct V5ConferenceStatisticsAllInfoCli
{
    V5StatisticsCommunicationInfoCli^           communicationInfo;
	V5StatisticsEndpointStatusCli^              endpointStatus;
	V5StatisticsBandwidthInfoCli^               bandwidthInfo;
	V5StatisticsConferenceInfoCli^              conferenceInfo;
	V5StatisticsConnectivityCli^                connectivity;
	V5StatisticsSessionDisplayInfoCli^          sessionDisplayInfo;
	V5StatisticsMediaInfoCli^                   mediaInfo;
    array<V5StatisticsParticipantInfoCli^>^     participantInfo;
	V5StatisticsRateShaperInfoCli^              rateShaperInfo;
	V5StatisticsFrameRateInfoCli^               frameRateInfo;
	V5StatisticsParticipantStatisticsListCli^   participantStatisticsList;

internal:
    V5ConferenceStatisticsAllInfoCli(V5ConferenceStatisticsAllInfo const& confStatInfo)
		: communicationInfo(gcnew V5StatisticsCommunicationInfoCli(confStatInfo.communicationInfo))
		, endpointStatus(gcnew V5StatisticsEndpointStatusCli(confStatInfo.endpointStatus))
		, bandwidthInfo(gcnew V5StatisticsBandwidthInfoCli(confStatInfo.bandwidthInfo))
		, conferenceInfo(gcnew V5StatisticsConferenceInfoCli(confStatInfo.conferenceInfo))
		, connectivity(gcnew V5StatisticsConnectivityCli(confStatInfo.connectivity))
		, sessionDisplayInfo(gcnew V5StatisticsSessionDisplayInfoCli(confStatInfo.sessionDisplayInfo))
		, mediaInfo(gcnew V5StatisticsMediaInfoCli(confStatInfo.mediaInfo))
		, participantInfo(convertForm(confStatInfo.participantInfo))
		, rateShaperInfo(gcnew V5StatisticsRateShaperInfoCli(confStatInfo.rateShaperInfo))
        , frameRateInfo(gcnew V5StatisticsFrameRateInfoCli(confStatInfo.frameRateInfo))
        , participantStatisticsList(gcnew V5StatisticsParticipantStatisticsListCli(confStatInfo.participantStatisticsList))
    {
    }

private:
    static array<V5StatisticsParticipantInfoCli^>^ convertForm(V5StatisticsParticipantInfo const& src)
    {
        auto dest = gcnew array<V5StatisticsParticipantInfoCli^>(src.numberParticipants);
        for (unsigned long i = 0; i < src.numberParticipants; i++)
        {
            dest[i] = gcnew V5StatisticsParticipantInfoCli(i, src);
        }
        return dest;
    }
};

