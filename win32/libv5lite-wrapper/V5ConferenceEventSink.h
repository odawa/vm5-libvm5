#pragma once
#define LIBV5LITE_USE_DLL
#include <string>
#include <vector>
#include <V5Lite/V5Client.h>
#include "IV5ConferenceEventSinkCli.h"
#include "CliObjectBuilder.h"
#include <msclr/gcroot.h>

namespace libv5liteWrapper {
	class V5ConferenceEventSink : public IV5ConferenceEventSink
	{
	public:
        typedef CliObjectBuilder::V5Array V5Array;
        typedef CliObjectBuilder::V5Object V5Object;

		V5ConferenceEventSink();
		virtual ~V5ConferenceEventSink();

		//IV5ConferenceEventSink
        virtual void onSessionLinked(const char *displayName, any_arg logoUrl) override;
        virtual void onBeginSessionFailed(V5ErrorInfo const& errInfo) override;
        virtual void onVersionUnmatched(const char *requireVersion) override;
        virtual void onParticipantJoined(V5Participant const& participant) override;
        virtual void onParticipantLeaved(V5Participant const& participant) override;
        virtual void onReceiveJoinConferenceRequest(const char *token, const char *V5LiteURL) override;
        virtual void onJoinConferenceFailed(V5ErrorInfo const& errInfo) override;
        virtual void onReceiveVideoFrame(VideoRawFrame const& rawFrame) override;
        virtual void onJoinedConference(V5ConferenceInfo const& conferenceInfo) override;
        virtual void onLeavedConference(V5ErrorInfo const& errInfo) override;
        virtual void onRemoteSourceAdded(V5RemoteSourceChanged const& change) override;
        virtual void onRemoteSourceRemoved(V5RemoteSourceChanged const& change) override;
        virtual void onParticipantListUpdated(const V5Participant * const participants[], unsigned long length) override;
        virtual void onConferenceStatusUpdated(V5ConferenceStatus const& conferenceStatus) override;
        virtual void onAddShare(const char *uri, bool isSelfShare) override;
        virtual void onRemoveShare(const char *uri, bool isSelfShare) override;
        virtual void onSelectedParticipantsUpdated(std::vector<std::string> const& selectedURI) override;
		virtual void onReconnectConference(V5ConferenceInfo const& conferenceInfo);
		virtual void onAsyncError(V5ErrorInfo const& errInfo) override;
		virtual void onInviteMailIsSent(V5ErrorInfo const& errInfo, const char* status, const char* mailAddress) override;
        virtual void onFloatingWindow(V5WindowId window) override;

		virtual void onRcvRejectParticipant(V5Participant const& participant) override;
        virtual void onPastTimeStart() override;
		virtual void onPastTimeStop() override;
        virtual void onConferenceNameChangeFailed(const char* nowConferenceName) override;

		virtual void onNowMaintenance(long long maintenanceStartTime, long long maintenanceEndTime) override;

        virtual void onConferenceStatisticsNotice(V5ConferenceStatisticsAllInfo const& confStatInfo) override;

		virtual void onConferenceEndTimeOverNotice(long minutes) override;

        virtual void onLibFatalError(V5ErrorInfo const& errInfo) override;

        void SetIV5ConferenceEventSinkCli(IV5ConferenceEventSinkCli^ iV5ConferenceEventSinkCli);
		void RemoveIV5ConferenceEventSinkCli();

    private:
        Object^ convertToObject(any_arg any);

	private:
		msclr::gcroot<IV5ConferenceEventSinkCli^> m_iV5ConferenceEventSinkCli = nullptr;
		msclr::gcroot<CliObjectBuilder^> m_cliObjectBuilder;
	};
}
