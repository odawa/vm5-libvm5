#include "stdafx.h"
#include "V5ConferenceEventSink.h"
#include "V5ConferenceInfoCli.h"
#include "V5ConferenceStatusCli.h"
#include "V5ParticipantCli.h"
#include "V5ErrorInfoCli.h"
#include "V5ConferenceStatisticsAllInfoCli.h"

using namespace System::Collections::Generic;

namespace libv5liteWrapper {
	
    V5ConferenceEventSink::V5ConferenceEventSink()
	{
		m_cliObjectBuilder = gcnew CliObjectBuilder();
	}

	V5ConferenceEventSink::~V5ConferenceEventSink()
	{
		delete m_cliObjectBuilder;
		RemoveIV5ConferenceEventSinkCli();
	}

    void V5ConferenceEventSink::onSessionLinked(const char *displayName, any_arg logoUrl)
    {
        if (static_cast<IV5ConferenceEventSinkCli^>(m_iV5ConferenceEventSinkCli) == nullptr)	return;

        String^ displayNameStr = Utf8UnicodeConverter::ToUnicode(displayName);
        auto logoUrlCli = dynamic_cast<V5Object^>(m_cliObjectBuilder->BuildV5AnyObject(logoUrl));

        m_iV5ConferenceEventSinkCli->OnSessionLinked(displayNameStr, logoUrlCli);
    }

    void V5ConferenceEventSink::onBeginSessionFailed(V5ErrorInfo const& errInfo)
    {
        if (static_cast<IV5ConferenceEventSinkCli^>(m_iV5ConferenceEventSinkCli) == nullptr)	return;
        V5ErrorInfoCli^ v5ErrorInfoCli = m_cliObjectBuilder->BuildV5ErrorInfo(errInfo);
        m_iV5ConferenceEventSinkCli->OnBeginSessionFailed(v5ErrorInfoCli);
    }
   
    void V5ConferenceEventSink::onVersionUnmatched(const char *requireVersion)
    {
        if (static_cast<IV5ConferenceEventSinkCli^>(m_iV5ConferenceEventSinkCli) == nullptr)	return;
        String^ requireVersionStr = gcnew String(requireVersion);
        m_iV5ConferenceEventSinkCli->OnVersionUnmatched(requireVersionStr);
    }

    void V5ConferenceEventSink::onParticipantJoined(V5Participant const& participant)
	{
		if (static_cast<IV5ConferenceEventSinkCli^>(m_iV5ConferenceEventSinkCli) == nullptr)	return;
		V5ParticipantCli^ participantCli = m_cliObjectBuilder->BuildV5Participant(&participant);
		m_iV5ConferenceEventSinkCli->OnParticipantJoined(participantCli);
	}

	void V5ConferenceEventSink::onParticipantLeaved(V5Participant const& participant)
	{
		if (static_cast<IV5ConferenceEventSinkCli^>(m_iV5ConferenceEventSinkCli) == nullptr)	return;
		V5ParticipantCli^ participantCli = m_cliObjectBuilder->BuildV5Participant(&participant);
		m_iV5ConferenceEventSinkCli->OnParticipantLeaved(participantCli);
	}

	void V5ConferenceEventSink::onReceiveJoinConferenceRequest(const char *token, const char *v5LiteUrl)
	{
		if (static_cast<IV5ConferenceEventSinkCli^>(m_iV5ConferenceEventSinkCli) == nullptr)	return;

		String^ tokenStr = gcnew String(token);
		String^ v5LiteUrlStr = gcnew String(v5LiteUrl);
		m_iV5ConferenceEventSinkCli->OnReceiveJoinConferenceRequest(tokenStr, v5LiteUrlStr);
	}

	void V5ConferenceEventSink::onJoinConferenceFailed(V5ErrorInfo const& errInfo)
	{
		if (static_cast<IV5ConferenceEventSinkCli^>(m_iV5ConferenceEventSinkCli) == nullptr)	return;
		V5ErrorInfoCli^ v5ErrorInfoCli = m_cliObjectBuilder->BuildV5ErrorInfo(errInfo);
		m_iV5ConferenceEventSinkCli->OnJoinConferenceFailed(v5ErrorInfoCli);
	}

	//後回し
	void V5ConferenceEventSink::onReceiveVideoFrame(VideoRawFrame const& rawFrame)
	{
		if (static_cast<IV5ConferenceEventSinkCli^>(m_iV5ConferenceEventSinkCli) == nullptr)	return;
	}

    void V5ConferenceEventSink::onJoinedConference(V5ConferenceInfo const& conferenceInfo)
	{
		if (static_cast<IV5ConferenceEventSinkCli^>(m_iV5ConferenceEventSinkCli) == nullptr)	return;
		V5ConferenceInfoCli^ conferenceInfoCli = m_cliObjectBuilder->BuildV5ConferenceInfo(conferenceInfo);
		m_iV5ConferenceEventSinkCli->OnJoinedConference(conferenceInfoCli);
	}

    void V5ConferenceEventSink::onLeavedConference(V5ErrorInfo const& errInfo)
	{
		if (static_cast<IV5ConferenceEventSinkCli^>(m_iV5ConferenceEventSinkCli) == nullptr)	return;
		V5ErrorInfoCli^ v5ErrorInfoCli = m_cliObjectBuilder->BuildV5ErrorInfo(errInfo);
		m_iV5ConferenceEventSinkCli->OnLeavedConference(v5ErrorInfoCli);
	}

    void V5ConferenceEventSink::onRemoteSourceAdded(V5RemoteSourceChanged const& added)
    {
        if (static_cast<IV5ConferenceEventSinkCli^>(m_iV5ConferenceEventSinkCli) == nullptr)	return;
        V5RomoteSourceChangedCli^ dest = m_cliObjectBuilder->BuidV5RemoteSourceChanged(added);
        m_iV5ConferenceEventSinkCli->OnRemoteSourceAdded(dest);
    }

    void V5ConferenceEventSink::onRemoteSourceRemoved(V5RemoteSourceChanged const& removed)
    {
        if (static_cast<IV5ConferenceEventSinkCli^>(m_iV5ConferenceEventSinkCli) == nullptr)	return;
        V5RomoteSourceChangedCli^ dest = m_cliObjectBuilder->BuidV5RemoteSourceChanged(removed);
        m_iV5ConferenceEventSinkCli->OnRemoteSourceRemove(dest);
    }

	void V5ConferenceEventSink::onParticipantListUpdated(const V5Participant * const participants[], unsigned long length)
	{
		if (static_cast<IV5ConferenceEventSinkCli^>(m_iV5ConferenceEventSinkCli) == nullptr)	return;
		array<V5ParticipantCli^>^ participantsCli = m_cliObjectBuilder->BuildV5ParticipantArray(participants, length);
		m_iV5ConferenceEventSinkCli->OnParticipantListUpdated(participantsCli, length);
	}

    void V5ConferenceEventSink::onConferenceStatusUpdated(V5ConferenceStatus const& conferenceStatus)
	{
		if (static_cast<IV5ConferenceEventSinkCli^>(m_iV5ConferenceEventSinkCli) == nullptr)	return;
		V5ConferenceStatusCli^ conferenceStatusCli = m_cliObjectBuilder->BuildV5ConferenceStatus(conferenceStatus);
		m_iV5ConferenceEventSinkCli->OnConferenceStatusUpdated(conferenceStatusCli);
	}

    void V5ConferenceEventSink::onAddShare(const char *uri, bool isSelfShare)
	{
		if (static_cast<IV5ConferenceEventSinkCli^>(m_iV5ConferenceEventSinkCli) == nullptr)	return;
		String^ urlStr = gcnew String(uri);
        m_iV5ConferenceEventSinkCli->OnAddShare(urlStr, isSelfShare);
	}

    void V5ConferenceEventSink::onRemoveShare(const char *uri, bool isSelfShare)
	{
		if (static_cast<IV5ConferenceEventSinkCli^>(m_iV5ConferenceEventSinkCli) == nullptr)	return;
		String^ urlStr = gcnew String(uri);
        m_iV5ConferenceEventSinkCli->OnRemoveShare(urlStr, isSelfShare);
	}

	//Windows版では使用しなさそうなので、selectedURIのWPFへの反映は後回し
	void V5ConferenceEventSink::onSelectedParticipantsUpdated(std::vector<std::string> const& selectedURI)
	{
		if (static_cast<IV5ConferenceEventSinkCli^>(m_iV5ConferenceEventSinkCli) == nullptr)	return;
        m_iV5ConferenceEventSinkCli->OnSelectedParticipantsUpdated(selectedURI.size());
	}

	void V5ConferenceEventSink::onReconnectConference(V5ConferenceInfo const& conferenceInfo)
	{
		if (static_cast<IV5ConferenceEventSinkCli^>(m_iV5ConferenceEventSinkCli) == nullptr)	return;
		V5ConferenceInfoCli^ conferenceInfoCli = m_cliObjectBuilder->BuildV5ConferenceInfo(conferenceInfo);
		m_iV5ConferenceEventSinkCli->OnReconnectConference(conferenceInfoCli);
	}

	void V5ConferenceEventSink::onAsyncError(V5ErrorInfo const& errInfo)
	{
		if (static_cast<IV5ConferenceEventSinkCli^>(m_iV5ConferenceEventSinkCli) == nullptr)	return;
		V5ErrorInfoCli^ v5ErrorInfoCli = m_cliObjectBuilder->BuildV5ErrorInfo(errInfo);
		m_iV5ConferenceEventSinkCli->OnAsyncError(v5ErrorInfoCli);
	}

	void V5ConferenceEventSink::onInviteMailIsSent(V5ErrorInfo const& errInfo, const char* status, const char* mailAddress)
	{
		if (static_cast<IV5ConferenceEventSinkCli^>(m_iV5ConferenceEventSinkCli) == nullptr)	return;
		V5ErrorInfoCli^ v5ErrorInfoCli = m_cliObjectBuilder->BuildV5ErrorInfo(errInfo);
		String^ statusStr = gcnew String(status);
		String^ mailAddressStr = Utf8UnicodeConverter::ToUnicode(mailAddress);

		m_iV5ConferenceEventSinkCli->OnInviteMailIsSent(v5ErrorInfoCli, statusStr, mailAddressStr);
	}

    void V5ConferenceEventSink::onFloatingWindow(V5WindowId window)
    {
        if (static_cast<IV5ConferenceEventSinkCli^>(m_iV5ConferenceEventSinkCli) == nullptr)	return;
        m_iV5ConferenceEventSinkCli->OnFloatingWindow(static_cast<UIntPtr>(window));
    }

	void V5ConferenceEventSink::onRcvRejectParticipant(V5Participant const& participant)
	{
		if (static_cast<IV5ConferenceEventSinkCli^>(m_iV5ConferenceEventSinkCli) == nullptr)	return;
		V5ParticipantCli^ v5ParticipantCli = m_cliObjectBuilder->BuildV5Participant(&participant);
		m_iV5ConferenceEventSinkCli->OnRcvRejectParticipant(v5ParticipantCli);
	}

	void V5ConferenceEventSink::onPastTimeStart()
	{
		if (static_cast<IV5ConferenceEventSinkCli^>(m_iV5ConferenceEventSinkCli) == nullptr)	return;
		m_iV5ConferenceEventSinkCli->OnPastTimeStart();
	}

	void V5ConferenceEventSink::onPastTimeStop()
	{
		if (static_cast<IV5ConferenceEventSinkCli^>(m_iV5ConferenceEventSinkCli) == nullptr)	return;
		m_iV5ConferenceEventSinkCli->OnPastTimeStop();
	}

    void V5ConferenceEventSink::onConferenceNameChangeFailed(const char* nowConferenceName)
    {
        if (static_cast<IV5ConferenceEventSinkCli^>(m_iV5ConferenceEventSinkCli) == nullptr)	return;
        String^ nowConferenceNameStr = Utf8UnicodeConverter::ToUnicode(nowConferenceName);
        m_iV5ConferenceEventSinkCli->OnConferenceNameChangeFailed(nowConferenceNameStr);
    }

	void V5ConferenceEventSink::onNowMaintenance(long long maintenanceStartTime, long long maintenanceEndTime)
    {
        if (static_cast<IV5ConferenceEventSinkCli^>(m_iV5ConferenceEventSinkCli) == nullptr)	return;
		m_iV5ConferenceEventSinkCli->OnNowMaintenance(maintenanceStartTime, maintenanceEndTime);
    }

    void V5ConferenceEventSink::onConferenceStatisticsNotice(V5ConferenceStatisticsAllInfo const& confStatInfo)
    {
        if (static_cast<IV5ConferenceEventSinkCli^>(m_iV5ConferenceEventSinkCli) == nullptr)	return;
        V5ConferenceStatisticsAllInfoCli^ confStatInfoCli = m_cliObjectBuilder->BuildV5ConferenceStatisticsAllInfo(confStatInfo);
        m_iV5ConferenceEventSinkCli->OnConferenceStatisticsNotice(confStatInfoCli);
    }

	void V5ConferenceEventSink::onConferenceEndTimeOverNotice(long minutes)
	{
		if (static_cast<IV5ConferenceEventSinkCli^>(m_iV5ConferenceEventSinkCli) == nullptr)	return;
		m_iV5ConferenceEventSinkCli->OnConferenceEndTimeOverNotice(minutes);
	}

    void V5ConferenceEventSink::onLibFatalError(V5ErrorInfo const& errInfo)
    {
        if (static_cast<IV5ConferenceEventSinkCli^>(m_iV5ConferenceEventSinkCli) == nullptr)	return;
        V5ErrorInfoCli^ v5ErrorInfoCli = m_cliObjectBuilder->BuildV5ErrorInfo(errInfo);
        m_iV5ConferenceEventSinkCli->OnLibFatalError(v5ErrorInfoCli);
    }

	void V5ConferenceEventSink::SetIV5ConferenceEventSinkCli(IV5ConferenceEventSinkCli^ iV5ConferenceEventSinkCli)
	{
		m_iV5ConferenceEventSinkCli = iV5ConferenceEventSinkCli;
	}

	void V5ConferenceEventSink::RemoveIV5ConferenceEventSinkCli()
	{
		m_iV5ConferenceEventSinkCli = nullptr;
	}
}
