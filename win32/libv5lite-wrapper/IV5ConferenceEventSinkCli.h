#pragma once
#include "V5ConferenceInfoCli.h"
#include "V5ErrorInfoCli.h"
#include "V5RomoteSourceChangedCli.h"
#include "V5ConferenceStatisticsAllInfoCli.h"
using namespace System;

namespace libv5liteWrapper
{
	public interface class IV5ConferenceEventSinkCli
	{
    public:
        typedef System::Collections::Generic::List<Object^> V5Array;
        typedef System::Collections::Generic::Dictionary<String^, Object^> V5Object;

    public:
        virtual void OnSessionLinked(const String^ displayName, V5Object^ logoUrl);
        virtual void OnBeginSessionFailed(const V5ErrorInfoCli^ errInfo);
        virtual void OnVersionUnmatched(const String^ requireVersion);
        virtual void OnParticipantJoined(const V5ParticipantCli^ participant);
		virtual void OnParticipantLeaved(const V5ParticipantCli^ participant);
		virtual void OnReceiveJoinConferenceRequest(const String^ token, const String^ V5LiteURL);
		virtual void OnJoinConferenceFailed(const V5ErrorInfoCli^ errInfo);
		//virtual void onReceiveVideoFrame(VideoRawFrame &rawFrame) = 0;
		virtual void OnJoinedConference(const V5ConferenceInfoCli^ conferenceInfo);
		virtual void OnLeavedConference(const V5ErrorInfoCli^ errInfo);
        virtual void OnRemoteSourceAdded(const V5RomoteSourceChangedCli^ added);
        virtual void OnRemoteSourceRemove(const V5RomoteSourceChangedCli^ removed);
        virtual void OnParticipantListUpdated(array<V5ParticipantCli^>^ participants, unsigned long length);
		virtual void OnConferenceStatusUpdated(const V5ConferenceStatusCli^ conferenceStatus);
        virtual void OnAddShare(const String^ uri, bool isSelfShare);
        virtual void OnRemoveShare(const String^ uri, bool isSelfShare);
		virtual void OnSelectedParticipantsUpdated(int number);
		virtual void OnReconnectConference(V5ConferenceInfoCli^ conferenceInfo);
		virtual void OnAsyncError(const V5ErrorInfoCli^ errInfo);
		virtual void OnInviteMailIsSent(const V5ErrorInfoCli^ errInfo, const String^ statusStr, const String^ mailAddressStr);
        virtual void OnFloatingWindow(UIntPtr window);

		virtual void OnRcvRejectParticipant(V5ParticipantCli^ participant);
		virtual void OnPastTimeStart();
		virtual void OnPastTimeStop();
        virtual void OnConferenceNameChangeFailed(const String^ requireVersion);

		virtual void OnNowMaintenance(long long maintenanceStartTime, long long maintenanceEndTime);

        virtual void OnConferenceStatisticsNotice(const V5ConferenceStatisticsAllInfoCli^ confStatInfo);

		virtual void OnConferenceEndTimeOverNotice(long minutes);
        virtual void OnLibFatalError(V5ErrorInfoCli^ errInfo);

    };
}
