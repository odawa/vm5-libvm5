#pragma once
#include <V5Lite/V5Enumerations.h>

public ref struct V5PasswdInfoCli
{
public:
    String^ passwd;
    String^ salt;
    UINT hashCount;
    vrms5cli::V5HashType hashType;

};
