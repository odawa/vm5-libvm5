#pragma once

#include <Windows.h>
#pragma comment(lib, "User32.lib")
#pragma comment(lib, "Gdi32.lib")
using namespace System;
using namespace System::Runtime::InteropServices;
using namespace System::Windows::Interop;

namespace libv5liteWrapper
{
	LRESULT CALLBACK MyWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
	public ref class HwndHostForVidyo : public HwndHost
	{
	private:
		HWND m_hWnd;
		int m_width;
		int m_height;

	public:
		HwndHostForVidyo(int width, int height);

	protected:
		virtual HandleRef BuildWindowCore(HandleRef hwndParent) override;
		virtual void DestroyWindowCore(HandleRef hwnd) override;
	};
}
