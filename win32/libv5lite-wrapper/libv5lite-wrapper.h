// libv5lite-wrapper.h

#pragma once
#define LIBV5LITE_USE_DLL
#include <V5Lite/V5Client.h>
#include "V5ConferenceEventSink.h"
#include "V5ChatEventSink.h"
#include "V5DeviceEventSink.h"
#include "IV5ConferenceEventSinkCli.h"
#include "Utf8UnicodeConverter.h"
#include "EventISinkClis.h"
#include "CliObjectBuilder.h"
#include "V5DeviceNameCli.h"
#include "V5WindowsAndDesktopsCli.h"
#include "ref_shared_ptr.h"
#include "V5PasswdInfoCli.h"
using namespace System;

namespace libv5liteWrapper
{
	public value class Rect
	{
	public:
		Int32 x;
		Int32 y;
		Int32 width;
		Int32 height;
	};

	public ref class LibV5Lite
	{
	public:
		LibV5Lite();
		LibV5Lite(EventISinkClis^ eventISinkClis);
		~LibV5Lite();
//		!LibV5Lite() { this->~LibV5Lite(); }

        static vrms5cli::V5CODE Initalize(String^ logPath, String^ logPriority);
		static vrms5cli::V5CODE Uninitalize();
        
		vrms5cli::V5CODE StartLogic(IntPtr pParentWndHnd, Rect rect, String^ fontPath, String^ certificatePath);
		vrms5cli::V5CODE StopLogic();

		vrms5cli::V5CODE BeginSession(String^vcubeUrl, String^ token, String^ appVersion);
		vrms5cli::V5CODE JoinConference(String^displayName, V5PasswdInfoCli^ passwdInfo);
        void LeaveConferenceSync();
        vrms5cli::AsyncResult LeaveConferenceAsync();
		vrms5cli::V5CODE SendInviteMail(String^ mailAddress);

		bool IsBusy();
		bool IsGuest();
		bool IsCameraMuted();
		bool IsMicrophoneMuted();
		bool IsSpeakerMuted();
		void MuteCamera(bool muted);
		void MuteMicrophone(bool muted);
		void MuteSpeaker(bool muted);

		void SendChatMessage(String^ sendMessage);

		int GetDeviceNumber(vrms5cli::V5DeviceType type);
		int GetCurrentDeviceNo(vrms5cli::V5DeviceType type);
		V5DeviceNameCli^ GetMicrophoneItem(int index);
		V5DeviceNameCli^ GetSpeakerItem(int index);
		V5DeviceNameCli^ GetCameraItem(int index);
		void SetConf(vrms5cli::V5DeviceType type, int index);

		void LockConference(bool lockStatus);
		void RejectParticipant(String^ rejectPID);
		void UpdateConferenceName(String^ conferenceName);

		V5ConferenceStatusCli^ GetConferenceStatus();
		array<V5ParticipantCli^>^ GetParticipants();
		V5ParticipantCli^ GetSelfParticipant();

		void EnableActiveSpeakerLayout(bool showActiveSpeaker);
		void RenderSelfView(bool render);
		void ChangeSelfPreviewMode(vrms5cli::V5ClientPreviewMode previewMode);

        void ShareAppWindow(IntPtr pHand);
        void ShareSysDesktop(UINT desktopId);
        void Unshare();
		void StartWatchShared(String^ uri);
		void StopWatchShared(String^ uri);

		void V5WindowResize(Rect rect);
		V5WindowsAndDesktopsCli^ GetWindowsAndDesktops();

		void SetProxySetting(String^ address, long port, String^ id, String^ password);
		void SetProxyForce(bool force);
		void SetProxyIEOption(vrms5cli::V5ProxyVidyoIEType proxyVidyoIEType);

        void StartStatisticsInfo(int millisecs);
        void StartStatisticsInfo();
        void StopStatisticsInfo();

	private:
		vrms5tools::ref_shared_ptr<vrms5::V5Client> client;
		vrms5tools::ref_shared_ptr<V5ConferenceEventSink> m_v5ConferenceEventSink;
		vrms5tools::ref_shared_ptr<V5ChatEventSink> m_v5ChatEventSink;
		vrms5tools::ref_shared_ptr<V5DeviceEventSink> m_v5DeviceEventSink;
		Utf8UnicodeConverter^ m_utf8UnicodeConverter;
		CliObjectBuilder^ m_cliObjectBuilder;
	};
}
