#pragma once
using namespace System;

public ref struct V5ChatMessageCli
{
public:
	String^ text;
	String^ from;
	String^ displayName;
	Int64 timestamp;
	Boolean isSelfMessage;
};
