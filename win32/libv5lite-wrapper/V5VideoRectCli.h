#pragma once
using namespace System;

public value struct V5VideoRectCli
{
public:
	signed int   xPos;
	signed int   yPos;
	unsigned int width;
	unsigned int height;
};
