#include "stdafx.h"
#include "V5DeviceEventSink.h"

namespace libv5liteWrapper
{
	V5DeviceEventSink::V5DeviceEventSink()
	{
	}

	V5DeviceEventSink::~V5DeviceEventSink()
	{
		RemoveIV5DeviceEventSinkCli();
	}

	void V5DeviceEventSink::onAudioInDeviceListChanged(int index)
	{
		if (static_cast<IV5DeviceEventSinkCli^>(m_iV5DeviceEventSinkCli) == nullptr)	return;
		m_iV5DeviceEventSinkCli->OnAudioInDeviceListChanged(index);
	}

	void V5DeviceEventSink::onAudioOutDeviceListChanged(int index)
	{
		if (static_cast<IV5DeviceEventSinkCli^>(m_iV5DeviceEventSinkCli) == nullptr)	return;
		m_iV5DeviceEventSinkCli->OnAudioOutDeviceListChanged(index);
	}

	void V5DeviceEventSink::onVideoDeviceListChanged(int index)
	{
		if (static_cast<IV5DeviceEventSinkCli^>(m_iV5DeviceEventSinkCli) == nullptr)	return;
		m_iV5DeviceEventSinkCli->OnVideoDeviceListChanged(index);
	}

	void V5DeviceEventSink::onSelectedSystemAudioInDevicesChanged(int index)
	{
		if (static_cast<IV5DeviceEventSinkCli^>(m_iV5DeviceEventSinkCli) == nullptr)	return;
		m_iV5DeviceEventSinkCli->OnSelectedSystemAudioInDevicesChanged(index);
	}

	void V5DeviceEventSink::onSelectedSystemAudioOutDevicesChanged(int index)
	{
		if (static_cast<IV5DeviceEventSinkCli^>(m_iV5DeviceEventSinkCli) == nullptr)	return;
		m_iV5DeviceEventSinkCli->OnSelectedSystemAudioOutDevicesChanged(index);
	}

	void V5DeviceEventSink::SetIV5DeviceEventSinkCli(IV5DeviceEventSinkCli^ iV5DeviceEventSinkCli)
	{
		m_iV5DeviceEventSinkCli = iV5DeviceEventSinkCli;
	}

	void V5DeviceEventSink::RemoveIV5DeviceEventSinkCli()
	{
		m_iV5DeviceEventSinkCli = nullptr;
	}
}
