#pragma once
#define LIBV5LITE_USE_DLL
#include <V5Lite/V5Client.h>
#include "IV5DeviceEventSinkCli.h"
#include "CliObjectBuilder.h"
#include <msclr/gcroot.h>

namespace libv5liteWrapper
{
	class V5DeviceEventSink : public IV5DeviceEventSink
	{
	public:
		V5DeviceEventSink();
		virtual ~V5DeviceEventSink();

		//IV5DeviceEventSink
		virtual void onAudioInDeviceListChanged(int index);
		virtual void onAudioOutDeviceListChanged(int index);
		virtual void onVideoDeviceListChanged(int index);
		virtual void onSelectedSystemAudioInDevicesChanged(int index);
		virtual void onSelectedSystemAudioOutDevicesChanged(int index);

		void SetIV5DeviceEventSinkCli(IV5DeviceEventSinkCli^ iV5DeviceEventSinkCli);
		void RemoveIV5DeviceEventSinkCli();

	private:
		msclr::gcroot<IV5DeviceEventSinkCli^> m_iV5DeviceEventSinkCli = nullptr;
		msclr::gcroot<CliObjectBuilder^> m_cliObjectBuilder;
	};
}
