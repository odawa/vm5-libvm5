#pragma once

#include "V5ConferenceStatusCli.h"
#include "V5ParticipantCli.h"

public ref struct V5ConferenceInfoCli
{
public:
	V5ConferenceStatusCli^ conferenceStatus;
	array<V5ParticipantCli^>^ participants;
	UInt32 participantNumber;
	V5ParticipantCli^ selfParticipant;
};
