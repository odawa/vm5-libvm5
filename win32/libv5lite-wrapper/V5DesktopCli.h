#pragma once
using namespace System;
#include "V5VideoRectCli.h"

public ref struct V5DesktopCli
{
public:
	UINT sysDesktopId;
	String^ sysDesktopName;
	V5VideoRectCli sysDesktopRect;
};
