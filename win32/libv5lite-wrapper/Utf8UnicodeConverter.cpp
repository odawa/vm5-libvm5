#include "stdafx.h"
#include <Windows.h>
#include "Utf8UnicodeConverter.h"

namespace libv5liteWrapper
{
	Utf8UnicodeConverter::Utf8UnicodeConverter()
	{
	}

	String^ Utf8UnicodeConverter::ToUnicode(const char* utf8)
	{
		int utf8Len = strlen(utf8);
		int length = MultiByteToWideChar(CP_UTF8, 0, utf8, utf8Len, NULL, 0);
		std::vector<wchar_t> unicode(length + 1);
		MultiByteToWideChar(CP_UTF8, 0, utf8, utf8Len, unicode.data(), length);
		unicode[length] = 0x00;
		return gcnew String(&unicode[0]);
	}

	void Utf8UnicodeConverter::ToUtf8(std::vector<char>& utf8Str, String^% unicode)
	{
		if (unicode == nullptr || unicode->Length <= 0)
		{
			utf8Str.resize(1);
			utf8Str[0] = '\x00';
			return;
		}

		//Stringをwchar_tに変換する
		array<Char>^ wchar = unicode->ToCharArray();
		pin_ptr<Char> wcharPtr = &wchar[0];

		int unicodeLen = wcslen(wcharPtr);
		int length = WideCharToMultiByte(CP_UTF8, 0, wcharPtr, unicodeLen, NULL, 0, NULL, NULL);
		utf8Str.resize(length + 1);
		WideCharToMultiByte(CP_UTF8, 0, wcharPtr, unicodeLen, utf8Str.data(), utf8Str.size(), NULL, NULL);
		utf8Str[length] = '\x00';
	}
}
