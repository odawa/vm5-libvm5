#pragma once
#define LIBV5LITE_USE_DLL
#include <V5Lite/V5Client.h>
#include "IV5ChatEventSinkCli.h"
#include "CliObjectBuilder.h"
#include <msclr/gcroot.h>

namespace libv5liteWrapper
{
	class V5ChatEventSink : public IV5ChatEventSink
	{
	public:
		V5ChatEventSink();
		virtual ~V5ChatEventSink();

		//IV5ChatEventSink
		virtual void onChatMessageAdded(const V5ChatMessage& message);
		virtual void onChatLogUpdated(const V5ChatMessage * const messages[], unsigned long length);
		virtual void onErrorNotification();

		void SetIV5ChatEventSinkCli(IV5ChatEventSinkCli^ iV5ChatEventSinkCli);
		void RemoveIV5ChatEventSinkCli();

	private:
		msclr::gcroot<IV5ChatEventSinkCli^> m_iV5ChatEventSinkCli = nullptr;
		msclr::gcroot<CliObjectBuilder^> m_cliObjectBuilder;
	};
}
