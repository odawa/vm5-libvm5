#include "stdafx.h"
#include "V5ChatEventSink.h"

namespace libv5liteWrapper
{
	V5ChatEventSink::V5ChatEventSink()
	{
		m_cliObjectBuilder = gcnew CliObjectBuilder();
	}

	V5ChatEventSink::~V5ChatEventSink()
	{
		delete m_cliObjectBuilder;
		RemoveIV5ChatEventSinkCli();
	}

	void V5ChatEventSink::onChatMessageAdded(const V5ChatMessage& message)
	{
		if (static_cast<IV5ChatEventSinkCli^>(m_iV5ChatEventSinkCli) == nullptr)	return;
		V5ChatMessageCli^ chatMessageCli = m_cliObjectBuilder->BuildV5ChatMessage(&message);
		m_iV5ChatEventSinkCli->OnChatMessageAdded(chatMessageCli);
	}

	void V5ChatEventSink::onChatLogUpdated(const V5ChatMessage * const messages[], unsigned long length)
	{
		if (static_cast<IV5ChatEventSinkCli^>(m_iV5ChatEventSinkCli) == nullptr)	return;

		array<V5ChatMessageCli^>^ chatMessagesCli = m_cliObjectBuilder->BuildV5ChatMessageArray(messages, length);
		m_iV5ChatEventSinkCli->OnChatLogUpdated(chatMessagesCli, length);
	}

	void V5ChatEventSink::onErrorNotification()
	{
		if (static_cast<IV5ChatEventSinkCli^>(m_iV5ChatEventSinkCli) == nullptr)	return;
		m_iV5ChatEventSinkCli->OnErrorNotification();
	}

	void V5ChatEventSink::SetIV5ChatEventSinkCli(IV5ChatEventSinkCli^ iV5ChatEventSinkCli)
	{
		m_iV5ChatEventSinkCli = iV5ChatEventSinkCli;
	}

	void V5ChatEventSink::RemoveIV5ChatEventSinkCli()
	{
		m_iV5ChatEventSinkCli = nullptr;
	}
}
