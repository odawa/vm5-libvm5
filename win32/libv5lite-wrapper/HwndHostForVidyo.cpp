#include "stdafx.h"

#include "HwndHostForVidyo.h"

namespace libv5liteWrapper
{
	HwndHostForVidyo::HwndHostForVidyo(int width, int height)
	{
		m_width = width;
		m_height = height;
	}

	HandleRef HwndHostForVidyo::BuildWindowCore(HandleRef hwndParent)
	{
		HWND parentHwnd = (HWND)hwndParent.Handle.ToPointer();
		HINSTANCE hInstance = (HINSTANCE)GetModuleHandle(NULL);

		WNDCLASSEX wcex;
		wcex.cbSize = sizeof(WNDCLASSEX);
		wcex.style = CS_HREDRAW | CS_VREDRAW;
		wcex.lpfnWndProc = (WNDPROC)MyWndProc;
		wcex.cbClsExtra = 0;
		wcex.cbWndExtra = 0;
		wcex.hInstance = hInstance;
		wcex.hIcon = LoadIcon(NULL, IDI_APPLICATION);
		wcex.hCursor = LoadCursor(NULL, IDC_ARROW);
		wcex.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
		//			wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
		wcex.lpszMenuName = NULL;
		wcex.lpszClassName = L"VcubeMeetingVidyoWindow";
		wcex.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

		// ウインドウクラスを登録します。
		RegisterClassEx(&wcex);

		m_hWnd = CreateWindow(L"VcubeMeetingVidyoWindow", // ウインドウクラス名
			L"", // キャプション文字列
			WS_OVERLAPPEDWINDOW | WS_CHILD, // ウインドウのスタイル
			0, // 水平位置
			0, // 垂直位置
			m_width, // 幅
			m_height, // 高さ
			parentHwnd, // 親ウインドウ
			NULL, // ウインドウメニュー
			hInstance, // インスタンスハンドル
			NULL); // WM_CREATE情報

		LONG lStyle = GetWindowLong(m_hWnd, GWL_STYLE);
		lStyle &= ~WS_THICKFRAME;
		lStyle &= ~WS_CAPTION;
		lStyle &= ~WS_SYSMENU;
		lStyle = SetWindowLong(m_hWnd, GWL_STYLE, lStyle);

		ShowWindow(m_hWnd, SW_SHOWNORMAL);
		UpdateWindow(m_hWnd);

		return HandleRef(this, IntPtr(m_hWnd));
	}

	void HwndHostForVidyo::DestroyWindowCore(HandleRef hwnd)
	{
	}

	LRESULT CALLBACK MyWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
	{
		// メッセージの種類に応じて処理を分岐します。
		switch (message)
		{
			//	//後で消す
			//case WM_PAINT:
			//{
			//	RECT rect;
			//	GetWindowRect(hWnd, &rect);

			//	HDC hdc;
			//	PAINTSTRUCT ps;
			//	HPEN hpen;
			//	HBRUSH hbrush;
			//	hdc = BeginPaint(hWnd, &ps);
			//	hpen = CreatePen(PS_SOLID, 1, 0XFF << 16);
			//	hbrush = CreateSolidBrush(RGB(30, 30, 30));
			//	SelectObject(hdc, hpen);
			//	SelectObject(hdc, GetStockObject(GRAY_BRUSH));
			//	Rectangle(hdc, 0, 0, rect.right - rect.left, rect.bottom - rect.top);
			//	EndPaint(hWnd, &ps);
			//	DeleteObject(hpen);
			//	DeleteObject(hbrush);
			//	return 0;
			//}

		case WM_DESTROY:
			// ウインドウが破棄されたときの処理
			PostQuitMessage(0);
			return 0;
		default:
			// デフォルトの処理
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
	}
}
