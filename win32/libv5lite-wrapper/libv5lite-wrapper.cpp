 // これは メイン DLL ファイルです。

#include "stdafx.h"
#include "libv5lite-wrapper.h"
#include "V5Lite/DeviceConfiguration.h"
using namespace System;
using namespace System::Runtime::InteropServices;

namespace
{
    struct HGlobalDeleter
    {
        void operator()(const void* p)
        {
            Marshal::FreeHGlobal(IntPtr(const_cast<void*>(p)));
        }
    };

    inline std::unique_ptr<const char, HGlobalDeleter> toCStr(String^ str)
    {
        auto cstr = static_cast<const char*>(Marshal::StringToHGlobalAnsi(str).ToPointer());
        return std::move(std::unique_ptr<const char, HGlobalDeleter>(cstr));
    }
}

namespace libv5liteWrapper
{
    using vrms5::V5Client;

	LibV5Lite::LibV5Lite()
	{
		client = V5Client::createInstance();
	}

	LibV5Lite::LibV5Lite(EventISinkClis^ eventISinkClis)
	{
		m_utf8UnicodeConverter = gcnew Utf8UnicodeConverter();
		m_cliObjectBuilder = gcnew CliObjectBuilder();
		client = V5Client::createInstance();

		//conferenceEventSink
		m_v5ConferenceEventSink.reset(new V5ConferenceEventSink());
		m_v5ConferenceEventSink->SetIV5ConferenceEventSinkCli(eventISinkClis->iV5ConferenceEventSinkCli);
		client->setConferenceEventSink(m_v5ConferenceEventSink.toStd());

		//chatEventSink
		m_v5ChatEventSink.reset(new V5ChatEventSink());
		m_v5ChatEventSink->SetIV5ChatEventSinkCli(eventISinkClis->iV5ChatEventSinkCli);
		client->setChatEventSink(m_v5ChatEventSink.toStd());

		//deviceEventSink
		m_v5DeviceEventSink.reset(new V5DeviceEventSink());
		m_v5DeviceEventSink->SetIV5DeviceEventSinkCli(eventISinkClis->iV5DeviceEventSinkCli);
		client->setDeviceEventSink(m_v5DeviceEventSink.toStd());

		client->startHTTPD();
	}

	LibV5Lite::~LibV5Lite()
	{
        if (client)
            client->stopHTTPD();

		client.reset();
		delete m_utf8UnicodeConverter;
		delete m_cliObjectBuilder;
	}

	vrms5cli::V5CODE LibV5Lite::Initalize(String^ logPath, String^ logPriority)
    {
        auto utf8UnicodeConverter = gcnew Utf8UnicodeConverter();

        std::vector<char> logPathWork;
        utf8UnicodeConverter->ToUtf8(logPathWork, logPath);

        char *prioStr = (char*)Marshal::StringToHGlobalAnsi(logPriority).ToPointer();
        std::unique_ptr<char, HGlobalDeleter> p2(prioStr);      // 削除予約

        delete utf8UnicodeConverter;
        return static_cast<vrms5cli::V5CODE>(vrms5::V5Client::initialize(&logPathWork[0], prioStr));
    }

	vrms5cli::V5CODE LibV5Lite::Uninitalize()
    {
		return static_cast<vrms5cli::V5CODE>(vrms5::V5Client::finalize());
    }

	vrms5cli::V5CODE LibV5Lite::StartLogic(IntPtr pParentWndHnd, Rect rect, String^ fontPath, String^ certificatePath)
    {
        V5Rect *v5Rect = new V5Rect();
        v5Rect->xPos = rect.x;
        v5Rect->yPos = rect.y;
        v5Rect->width = rect.width;
        v5Rect->height = rect.height;
        V5WindowId v5Id = (V5WindowId)pParentWndHnd.ToInt32();

        std::vector<char> certificatePathWork;
        m_utf8UnicodeConverter->ToUtf8(certificatePathWork, certificatePath);

        std::vector<char> fontPathWork;
        m_utf8UnicodeConverter->ToUtf8(fontPathWork, fontPath);

		auto ret = static_cast<vrms5cli::V5CODE>(client->startLogic(v5Id, v5Rect, &fontPathWork[0], &certificatePathWork[0]));
        return ret;
    }

	vrms5cli::V5CODE LibV5Lite::StopLogic()
    {
		return static_cast<vrms5cli::V5CODE>(client->stopLogic());
    }

	vrms5cli::V5CODE LibV5Lite::BeginSession(String^vcubeUrl, String^ token, String^ appVersion)
    {
        auto vcubeUrlStr = toCStr(vcubeUrl);        // 解放予約済み
        auto tokenStr = toCStr(token);              // 解放予約済み
        auto appVersionStr = toCStr(appVersion);    // 解放予約済み
		return static_cast<vrms5cli::V5CODE>(client->beginSession(vcubeUrlStr.get(), tokenStr.get(), appVersionStr.get()));
    }

	vrms5cli::V5CODE LibV5Lite::JoinConference(String^displayName, V5PasswdInfoCli^ passwdInfo)
	{
		std::vector<char> displayNameStrWork;
		m_utf8UnicodeConverter->ToUtf8(displayNameStrWork, displayName);
		char *displayNameStr = &displayNameStrWork[0];

        std::unique_ptr<const char, HGlobalDeleter> passStrPtr;
        std::unique_ptr<const char, HGlobalDeleter> saltStrPtr;
        std::shared_ptr<V5PasswordInfo> passwd;
        if (passwdInfo)
        {
            passStrPtr = toCStr(passwdInfo->passwd);            // 解放予約済み

            passwd = std::make_shared<V5PasswordInfo>();
            passwd->hashCount = passwdInfo->hashCount;
            passwd->hashType = static_cast<V5HashType>(passwdInfo->hashType);
            passwd->passwd = passStrPtr.get();
            if (passwdInfo->salt)
            {
                saltStrPtr = toCStr(passwdInfo->salt);          // 解放予約済み
                passwd->salt = std::string(saltStrPtr.get());
            }

        }
		return static_cast<vrms5cli::V5CODE>(client->joinConference(displayNameStr, passwd));
	}

    void LibV5Lite::LeaveConferenceSync()
    {
        return client->leaveConferenceSync();
    }

    vrms5cli::AsyncResult LibV5Lite::LeaveConferenceAsync()
    {
        return static_cast<vrms5cli::AsyncResult>(client->leaveConferenceAsync());
    }

	vrms5cli::V5CODE LibV5Lite::SendInviteMail(String^ mailAddress)
	{
		char *mailAddressStr = (char*)Marshal::StringToHGlobalAnsi(mailAddress).ToPointer();
		std::unique_ptr<char, HGlobalDeleter> p1(mailAddressStr);
		return static_cast<vrms5cli::V5CODE>(client->sendInviteMail(mailAddressStr));
	}
	
	bool LibV5Lite::IsBusy()
	{
		return client->isBusy();
	}

	bool LibV5Lite::IsGuest()
	{
		return client->isGuest();
	}

    bool LibV5Lite::IsCameraMuted()
	{
		return client->isCameraMuted();
	}

	bool LibV5Lite::IsMicrophoneMuted()
	{
		return client->isMicrophoneMuted();
	}

	bool LibV5Lite::IsSpeakerMuted()
	{
		return client->isSpeakerMuted();
	}

	void LibV5Lite::MuteCamera(bool muted)
	{
		return client->muteCamera(muted);
	}

	void LibV5Lite::MuteMicrophone(bool muted)
	{
		return client->muteMicrophone(muted);
	}

	void LibV5Lite::MuteSpeaker(bool muted)
	{
		return client->muteSpeaker(muted);
	}

	void LibV5Lite::SendChatMessage(String^ sendMessage)
	{
		std::vector<char> sendMessageStrWork;
		m_utf8UnicodeConverter->ToUtf8(sendMessageStrWork, sendMessage);
		char *sendMessageStr = &sendMessageStrWork[0];
		client->sendChatMessage(sendMessageStr);
	}

	int LibV5Lite::GetDeviceNumber(vrms5cli::V5DeviceType type)
	{
		std::shared_ptr<V5DeviceConfiguration> deviceConfig = client->getDeviceConfiguration();
		return deviceConfig->getNumber(static_cast<V5DeviceType>(type));
	}

	int LibV5Lite::GetCurrentDeviceNo(vrms5cli::V5DeviceType type)
	{
		std::shared_ptr<V5DeviceConfiguration> deviceConfig = client->getDeviceConfiguration();
		return deviceConfig->getCurrent(static_cast<V5DeviceType>(type));
	}

	V5DeviceNameCli^ LibV5Lite::GetMicrophoneItem(int index)
	{
		std::shared_ptr<V5DeviceConfiguration> deviceConfig = client->getDeviceConfiguration();
		std::shared_ptr<V5DeviceName> deviceName = deviceConfig->getMicrophoneItem(index);
		V5DeviceNameCli^ deviceNameCli = m_cliObjectBuilder->BuildV5DeviceName(deviceName.get());
		return deviceNameCli;
	}

	V5DeviceNameCli^ LibV5Lite::GetSpeakerItem(int index)
	{
		std::shared_ptr<V5DeviceConfiguration> deviceConfig = client->getDeviceConfiguration();
		std::shared_ptr<V5DeviceName> deviceName = deviceConfig->getSpeakerItem(index);
		V5DeviceNameCli^ deviceNameCli = m_cliObjectBuilder->BuildV5DeviceName(deviceName.get());
		return deviceNameCli;
	}

	V5DeviceNameCli^ LibV5Lite::GetCameraItem(int index)
	{
		std::shared_ptr<V5DeviceConfiguration> deviceConfig = client->getDeviceConfiguration();
		std::shared_ptr<V5DeviceName> deviceName = deviceConfig->getCameraItem(index);
		V5DeviceNameCli^ deviceNameCli = m_cliObjectBuilder->BuildV5DeviceName(deviceName.get());
		return deviceNameCli;
	}

	void LibV5Lite::SetConf(vrms5cli::V5DeviceType type, int index)
	{
		std::shared_ptr<V5DeviceConfiguration> deviceConfig = client->getDeviceConfiguration();
		deviceConfig->setConf(static_cast<V5DeviceType>(type), index);
	}

	void LibV5Lite::LockConference(bool lockStatus)
	{
		client->lockConference(lockStatus);
	}

	void LibV5Lite::RejectParticipant(String^ rejectPID)
	{
		char *rejectPIDStr = (char*)Marshal::StringToHGlobalAnsi(rejectPID).ToPointer();
		client->rejectParticipant(rejectPIDStr);
		Marshal::FreeHGlobal(IntPtr(rejectPIDStr));
	}

	void LibV5Lite::UpdateConferenceName(String^ conferenceName)
	{
		std::vector<char> conferenceNameWork;
		m_utf8UnicodeConverter->ToUtf8(conferenceNameWork, conferenceName);
		char *conferenceNameStr = &conferenceNameWork[0];
		client->updateConferenceName(conferenceNameStr);
	}

	V5ConferenceStatusCli^ LibV5Lite::GetConferenceStatus()
	{
        std::shared_ptr<const V5ConferenceStatus> conferenceStatus = client->getConferenceStatus();
		return m_cliObjectBuilder->BuildV5ConferenceStatus(*conferenceStatus);
	}

	array<V5ParticipantCli^>^ LibV5Lite::GetParticipants()
	{
		V5ParticipantList participantList = client->getParticipants();
		array<V5ParticipantCli^>^ participantArrayCli = m_cliObjectBuilder->BuildV5ParticipantArray(participantList);
		return participantArrayCli;
	}

	V5ParticipantCli^ LibV5Lite::GetSelfParticipant()
	{
		auto selfParticipant = client->getSelfParticipant();
		V5ParticipantCli^ selfParticipantCli = m_cliObjectBuilder->BuildV5Participant(selfParticipant.get());
		return selfParticipantCli;
	}

	void LibV5Lite::EnableActiveSpeakerLayout(bool showActiveSpeaker)
	{
		client->enableActiveSpeakerLayout(showActiveSpeaker);
	}

	void LibV5Lite::RenderSelfView(bool render)
	{
		client->renderSelfView(render);
	}

	void LibV5Lite::ChangeSelfPreviewMode(vrms5cli::V5ClientPreviewMode previewMode)
	{
		client->changeSelfPreviewMode(static_cast<V5ClientPreviewMode>(previewMode));
	}

	void LibV5Lite::ShareAppWindow(IntPtr pHand)
    {
        //メソッド引数に関数を渡すとデバッグで中に入れないので、分割している
        auto v5WindowId = static_cast<V5WindowId>(pHand.ToPointer());
        client->shareAppWindow(v5WindowId, 0, 0);
    }

    void LibV5Lite::ShareSysDesktop(UINT desktopId)
    {
        client->shareSysDesktop(desktopId, 0, 0);
    }

    void LibV5Lite::Unshare()
	{
		client->unshare();
	}

	void LibV5Lite::StartWatchShared(String^ uri)
	{
		char *uriStr = (char*)Marshal::StringToHGlobalAnsi(uri).ToPointer();
		client->startWatchShared(uriStr);
		Marshal::FreeHGlobal(IntPtr(uriStr));
	}

	void LibV5Lite::StopWatchShared(String^ uri)
	{
		char *uriStr = (char*)Marshal::StringToHGlobalAnsi(uri).ToPointer();
		client->stopWatchShared(uriStr);
		Marshal::FreeHGlobal(IntPtr(uriStr));
	}

	void LibV5Lite::V5WindowResize(Rect rect)
	{
		V5Rect v5Rect;
		v5Rect.xPos = rect.x;
		v5Rect.yPos = rect.y;
		v5Rect.width = rect.width;
		v5Rect.height = rect.height;
		client->setFrameDirection(v5Rect);
	}

	V5WindowsAndDesktopsCli^ LibV5Lite::GetWindowsAndDesktops()
	{
		auto windowsAndDesktops = client->getWindowsAndDesktops();
		return m_cliObjectBuilder->BuildV5WindowsAndDesktops(*windowsAndDesktops);
	}

	void LibV5Lite::SetProxySetting(String^ address, long port, String^ id, String^ password)
	{
		std::string addr_ = "";
		std::string id_ = "";
		std::string password_ = "";
		if (address != "")
		{
			char *addressPtr = (char*)Marshal::StringToHGlobalAnsi(address).ToPointer();
			addr_ = std::string(addressPtr);
			Marshal::FreeHGlobal(IntPtr(addressPtr));
		}

		if (id != "")
		{
			char *idPtr = (char*)Marshal::StringToHGlobalAnsi(id).ToPointer();
			id_ = std::string(idPtr);
			Marshal::FreeHGlobal(IntPtr(idPtr));
		}

		if (password != "")
		{
			char *passwordPtr = (char*)Marshal::StringToHGlobalAnsi(password).ToPointer();
			password_ = std::string(passwordPtr);
			Marshal::FreeHGlobal(IntPtr(passwordPtr));
		}

		client->setProxySetting(addr_, port, id_, password_);
	}

	void LibV5Lite::SetProxyForce(bool force)
	{
		client->setProxyForce(force);
	}

	void LibV5Lite::SetProxyIEOption(vrms5cli::V5ProxyVidyoIEType proxyVidyoIEType)
	{
		client->setProxyIEOption(static_cast<V5ProxyVidyoIEType>(proxyVidyoIEType));
	}

    void LibV5Lite::StartStatisticsInfo(int millisecs)
    {
        client->startStatisticsInfo(millisecs);
    }

    void LibV5Lite::StartStatisticsInfo()
    {
        client->startStatisticsInfo();
    }

    void LibV5Lite::StopStatisticsInfo()
    {
        client->stopStatisticsInfo();
    }

}
