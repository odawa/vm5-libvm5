﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Interop;
using libv5liteWrapper;
using vrms5cli;

namespace V5Lite
{
    /// <summary>
    /// App.xaml の相互作用ロジック
    /// </summary>
    public partial class App : Application
    {
        private TaskTrayIconComponent m_taskTrayIconComponent;
        private LibV5Lite m_libv5lite = null;
        private MainWindow m_mainWindow;
        private LoginWindow m_loginWindow = null;

        private V5ChatEventSinkCli m_v5ChatEventSinkCli = null;
        private V5ConferenceEventSinkCli m_v5ConferenceEventSink = null;
        private V5DeviceEventSinkCli m_v5DeviceEventSink = null;

        private string m_vidyoRootCertWorkFolder = "";
        private string m_logFolder = "";

        private void Application_Startup(object sender, StartupEventArgs e)
        {
            this.ShutdownMode = System.Windows.ShutdownMode.OnExplicitShutdown;
            m_taskTrayIconComponent = new TaskTrayIconComponent();

            PrepareVidyoCertFolder();
            PrepareLogFolder();

            LibV5Lite.Initalize(m_logFolder, @"error");

            m_v5ConferenceEventSink = new V5ConferenceEventSinkCli();
            m_v5ConferenceEventSink.OnReceiveJoinConferenceRequestEvent += OnReceiveJoinConferenceRequest;
            m_v5ConferenceEventSink.OnJoinedConferenceEvent += OnJoinedConference;
            m_v5ConferenceEventSink.OnJoinConferenceFailedEvent += OnJoinConferenceFailed;
            m_v5ConferenceEventSink.OnLeavedConferenceEvent += OnLeavedConference;

            m_v5ChatEventSinkCli = new V5ChatEventSinkCli();

            m_v5DeviceEventSink = new V5DeviceEventSinkCli();

            EventSinkClis eventSinkClis = new EventSinkClis();
            eventSinkClis.v5ConferenceEventSinkCli = m_v5ConferenceEventSink;
            eventSinkClis.v5ChatEventSinkCli = m_v5ChatEventSinkCli;
            eventSinkClis.v5DeviceEventSinkCli = m_v5DeviceEventSink;
            EventISinkClis eventISinkClis = new EventISinkClis();
            eventISinkClis.iV5ConferenceEventSinkCli = m_v5ConferenceEventSink;
            eventISinkClis.iV5ChatEventSinkCli = m_v5ChatEventSinkCli;
            eventISinkClis.iV5DeviceEventSinkCli = m_v5DeviceEventSink;

            m_libv5lite = new LibV5Lite(eventISinkClis);

            //メインWindowを最小化して実行(Vidyoに渡すhWndを作成)
            m_mainWindow = new MainWindow(m_libv5lite, eventSinkClis);
            m_mainWindow.OnWindowPrepared += OnMainWindowPrepared;
            //            m_mainWindow.WindowState = System.Windows.WindowState.Minimized;
            m_mainWindow.Show();
        }

        private void Application_Exit(object sender, ExitEventArgs e)
        {
            m_taskTrayIconComponent.Dispose();

            m_libv5lite.Dispose();
            LibV5Lite.Uninitalize();
        }

        //メインWindow作成完了
        private void OnMainWindowPrepared()
        {
            ////StartLogic
            //IntPtr handle = new IntPtr(0);
            //libv5liteWrapper.Rect rect = new libv5liteWrapper.Rect();
            //GetHWndAndRectForVidyo(ref handle, ref rect);
            //V5CODECli ret = m_libv5lite.StartLogic(handle, rect, null, null);
            //Console.WriteLine("[OnReceiveJoinConferenceRequest] StartLogic : {0}", ret);
        }

        //ブラウザからの入室リクエスト受信
        private void OnReceiveJoinConferenceRequest(string token, string v5LiteServerUrl)
        {
            Application.Current.Dispatcher.Invoke(
                new Action(() =>
                {
                    m_loginWindow = new LoginWindow(m_libv5lite, m_v5ConferenceEventSink);
                    m_loginWindow.SetLoginInfo(token, v5LiteServerUrl);
                    m_loginWindow.Clear();
                    m_loginWindow.Show();
                    m_loginWindow.Activate();

                    //StartLogic
                    IntPtr handle = new IntPtr(0);
                    libv5liteWrapper.Rect rect = new libv5liteWrapper.Rect();
                    GetHWndAndRectForVidyo(ref handle, ref rect);
                    V5CODE_tag ret = m_libv5lite.StartLogic(handle, rect, null, m_vidyoRootCertWorkFolder);
                    Console.WriteLine("[OnReceiveJoinConferenceRequest] StartLogic : {0}", ret);
                })
            );
        }

        //入室成功
        private void OnJoinedConference()
        {
            Console.WriteLine("");
            Console.WriteLine("---------------------------------------------------");
            Console.WriteLine("OnJoinedConference");
            Console.WriteLine("---------------------------------------------------");
            Console.WriteLine("");


            Application.Current.Dispatcher.Invoke(
                new Action(() =>
                {
                    Console.WriteLine("");
                    Console.WriteLine("===================================================");
                    Console.WriteLine("Dispatched(OnJoinedConference)");
                    Console.WriteLine("===================================================");
                    Console.WriteLine("");


m_mainWindow.ShowSpecially();

                    m_mainWindow.Initialize();
                    m_mainWindow.WindowState = System.Windows.WindowState.Normal;

                    Console.WriteLine("");
                    Console.WriteLine("===================================================");
                    Console.WriteLine("Normalized");
                    Console.WriteLine("===================================================");
                    Console.WriteLine("");



                    m_mainWindow.Activate();
                    m_loginWindow.Close();
                    m_loginWindow = null;
                })
            );
        }

        private void OnJoinConferenceFailed(V5ErrorInfoCli errInfo)
        {
            Application.Current.Dispatcher.Invoke(
                new Action(() =>
                {
                    Console.WriteLine("[OnJoinConferenceFailed] catetory={0} code={1} reason={2}", errInfo.category, errInfo.code_deprecated, errInfo.reason);
                    m_libv5lite.StopLogic();
                    m_loginWindow.Close();
                    m_loginWindow = null;
                })
            );
        }

		private void OnLeavedConference(V5ErrorInfoCli errInfo)
        {
            Application.Current.Dispatcher.Invoke(
                new Action(() =>
                {
                    m_libv5lite.StopLogic();
                    m_mainWindow.HideSpecially();
                })
            );
        }

        private void GetHWndAndRectForVidyo(ref IntPtr handle_, ref libv5liteWrapper.Rect rect_)
        {
            HwndSource source = (HwndSource)HwndSource.FromVisual(m_mainWindow);
            IntPtr handle = source.Handle;
            libv5liteWrapper.Rect rect;
            rect.x = 0;
            rect.y = 0;
            rect.width = 400;
            rect.height = 300;

            handle_ = handle;
            rect_ = rect;
        }

        //Vidyoのライブラリがca-certificates.crtを展開するので、そのフォルダを決めておく
        private void PrepareVidyoCertFolder()
        {
            string localApplicationFolder = System.Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
            m_vidyoRootCertWorkFolder = localApplicationFolder + @"\V-CUBE\V5Lite\Data\";   //\で終わっていないと、Vidyoライブラリに怒られる
            if (!System.IO.Directory.Exists(m_vidyoRootCertWorkFolder))
            {
                System.IO.Directory.CreateDirectory(m_vidyoRootCertWorkFolder);
            }
        }

        private void PrepareLogFolder()
        {
            string localApplicationFolder = System.Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
            m_logFolder = localApplicationFolder + @"\V-CUBE\V5Lite\Log\";  //\で終わること
            if (!System.IO.Directory.Exists(m_logFolder))
            {
                System.IO.Directory.CreateDirectory(m_logFolder);
            }
        }
    }
}
