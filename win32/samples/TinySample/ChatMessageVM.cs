﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Input;
using libv5liteWrapper;

namespace V5Lite
{
    class ChatMessageVM : BindableBase
    {
        private LibV5Lite m_libv5lite = null;
        private V5ChatEventSinkCli m_v5ChatEventSinkCli;

        private ObservableCollection<ChatMessage> m_chatMessages;
        public ObservableCollection<ChatMessage> ChatMessages
        {
            get
            {
                return m_chatMessages;
            }
            private set{}
        }

        private string inputChatMessage;
        public string InputChatMessage {
            get
            {
                return inputChatMessage;
            }
            set
            {
                SetProperty<string>(ref inputChatMessage, value);
                ((DelegateCommand)SendChatMessageCommand).RaiseCanExecuteChanged();
            }
        }
        public ICommand SendChatMessageCommand { get; set; }
        public ICommand InputAltEnterCommand { get; set; }

        //これは特殊
        //ListBoxのスクロールバーをいじるために仕方なく利用
        public ICommand ScrollUpCompletelyCommand { get; set; }

        public ChatMessageVM(LibV5Lite libv5lite, V5ChatEventSinkCli v5ChatEventSinkCli)
        {
            m_libv5lite = libv5lite;
            m_v5ChatEventSinkCli = v5ChatEventSinkCli;
            m_v5ChatEventSinkCli.PropertyChanged += new PropertyChangedEventHandler(OnPropertyChanged);
            m_chatMessages = new ObservableCollection<ChatMessage>();
            SendChatMessageCommand = new DelegateCommand(SendChatMessage, CanSendChatMesssage);
            InputAltEnterCommand = new DelegateCommand(InputAltEnter);
        }

        public void ClearChatMessage()
        {
            m_chatMessages.Clear();
        }

        private void OnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            Application.Current.Dispatcher.Invoke(
                new Action(() =>
                {
                    AddChatMessagesFromServer();
                })
            );
        }

        private void AddChatMessagesFromServer()
        {
            //チャットメッセージ追加後にスクロールするかどうかを先に問い合わせておく
            bool canScrollUp = false;
            if (ScrollUpCompletelyCommand != null)
                canScrollUp = ScrollUpCompletelyCommand.CanExecute(null);

            List<V5ChatMessageCli> chatMessages = m_v5ChatEventSinkCli.ChatMessages;
            int chatNum = chatMessages.Count;
            for (int i = 0; i < chatNum; ++i)
            {
                string chatTime = UnixTimeHelperStatic.FromUnixTime(chatMessages[i].timestamp).ToString("HH:mm");
                m_chatMessages.Add(new ChatMessage(chatMessages[i].displayName, chatTime, chatMessages[i].text));
            }
            RaisePropertyChanged("ChatMessages");
            if (ScrollUpCompletelyCommand != null)
            {
                if (canScrollUp)
                    ScrollUpCompletelyCommand.Execute(null);
            }
        }

        private void SendChatMessage(object obj)
        {
            if (m_libv5lite == null) return;
            if (InputChatMessage == null) return;

            //入力したチャットメッセージを先に空にする
            string sendMessage = String.Copy(InputChatMessage);
            InputChatMessage = null;

            m_libv5lite.SendChatMessage(sendMessage);
        }

        private bool CanSendChatMesssage(object obj)
        {
            return !string.IsNullOrEmpty(InputChatMessage);
        }

        private void InputAltEnter(object obj)
        {
            InputChatMessage += "\r\n";
        }
    }
}
