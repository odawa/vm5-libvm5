﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Input;

namespace V5Lite
{
    public enum VisualCloseManageArea { PcWindowShare };

    /*******************************************************************************************************************************
     * サブメニューなどの画面を開いていて、その画面とその画面を開くボタンを除く領域、をクリックするとその画面を閉じる処理(始まり)
     *******************************************************************************************************************************/
    class VisualCloseManagerWhenClickOutside
    {
        private Window m_window;
        private Dictionary<VisualCloseManageArea, IVisual> m_closeVisuals;
        private Dictionary<VisualCloseManageArea, Visual> m_outOfCloseVisuals;
        private Dictionary<VisualCloseManageArea, MouseButtonEventHandler> m_RemoveUiElementWhenClickOutsideHandlers;

        public VisualCloseManagerWhenClickOutside(Window window)
        {
            m_window = window;
            m_closeVisuals = new Dictionary<VisualCloseManageArea, IVisual>();
            m_outOfCloseVisuals = new Dictionary<VisualCloseManageArea, Visual>();
            m_RemoveUiElementWhenClickOutsideHandlers = new Dictionary<VisualCloseManageArea, MouseButtonEventHandler>();
            m_RemoveUiElementWhenClickOutsideHandlers.Add(VisualCloseManageArea.PcWindowShare, RemovePcWindowShareElementWhenClickOutsideHandler);
        }

        //コンストラクト後にあらかじめ登録しておく
        public void SetOutOfCloseVisual(VisualCloseManageArea area, Visual visual)
        {
            m_outOfCloseVisuals.Add(area, visual);
        }

        public void Start(VisualCloseManageArea area, IVisual visual)
        {
            if (!m_RemoveUiElementWhenClickOutsideHandlers.ContainsKey(VisualCloseManageArea.PcWindowShare)) return;

            m_closeVisuals.Add(area, visual);
            m_window.PreviewMouseDown -= m_RemoveUiElementWhenClickOutsideHandlers[VisualCloseManageArea.PcWindowShare];
            m_window.PreviewMouseDown += m_RemoveUiElementWhenClickOutsideHandlers[VisualCloseManageArea.PcWindowShare];
        }

        public void Stop(VisualCloseManageArea area)
        {
            if (!m_RemoveUiElementWhenClickOutsideHandlers.ContainsKey(VisualCloseManageArea.PcWindowShare)) return;

            m_closeVisuals.Remove(area);
            m_window.PreviewMouseDown -= m_RemoveUiElementWhenClickOutsideHandlers[VisualCloseManageArea.PcWindowShare];
        }

        //メニューを開いていない場合の共通処理
        private void RemoveUiElementWhenClickOutside(VisualCloseManageArea area, MouseButtonEventArgs e)
        {
            //クリック位置のクライアント座標を取得
            Point p = e.GetPosition(null);

            //ボタン領域内の場合、何もしない
            if (m_outOfCloseVisuals.ContainsKey(area))
            {
                if (m_outOfCloseVisuals[area] != null)
                    if (ViewHelperStatic.DoesContain(m_outOfCloseVisuals[area], p)) return;
            }

            //表示領域内の場合、何もしない
            if (m_closeVisuals.ContainsKey(area))
            {
                if (m_closeVisuals[area] != null)
                    if (m_closeVisuals[area].DoesContain(p)) return;
            }

            //閉じる
            if (m_closeVisuals.ContainsKey(VisualCloseManageArea.PcWindowShare))
            {
                if (m_closeVisuals[area] != null)
                    m_closeVisuals[area].CloseVisual();
                m_closeVisuals.Remove(area);
            }

            m_window.PreviewMouseDown -= RemovePcWindowShareElementWhenClickOutsideHandler;
        }


        //PCWindow共有用のハンドラ
        private void RemovePcWindowShareElementWhenClickOutsideHandler(object sender, MouseButtonEventArgs e)
        {
            RemoveUiElementWhenClickOutside(VisualCloseManageArea.PcWindowShare, e);
        }
    }
}
