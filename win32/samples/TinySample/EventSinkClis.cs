﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace V5Lite
{
    public struct EventSinkClis
    {
		public V5ConferenceEventSinkCli v5ConferenceEventSinkCli;
		public V5ChatEventSinkCli v5ChatEventSinkCli;
		public V5DeviceEventSinkCli v5DeviceEventSinkCli;
    }
}
