﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using libv5liteWrapper;
namespace V5Lite
{
    public class V5ChatEventSinkCli : BindableBase, IV5ChatEventSinkCli
    {
        private List<V5ChatMessageCli> chatMessagesFromServer = null;
        private List<V5ChatMessageCli> chatMessagesForPassing = null;
        private Object thisLock = new Object();

        public List<V5ChatMessageCli> ChatMessages
        {
            get
            {
                lock (thisLock)
                {
                    chatMessagesForPassing = chatMessagesFromServer;
                    chatMessagesFromServer = new List<V5ChatMessageCli>();
                }
                return chatMessagesForPassing;
            }
        }

        public V5ChatMessageCli[] AddChatMessages
        {
            set
            {
                lock (thisLock)
                {
                    int num = value.Count();
                    for (int i = 0; i < num; ++i)
                    {
                        chatMessagesFromServer.Add(value[i]);
                    }
                }
                RaisePropertyChanged("ChatMessages");
            }
        }

        public V5ChatMessageCli AddChatMessage
        {
            set
            {
                lock (thisLock)
                {
                    chatMessagesFromServer.Add(value);
                }
                RaisePropertyChanged("ChatMessages");
            }
        }

        public void ClearChatMessages()
        {
            lock (thisLock)
            {
                chatMessagesFromServer.Clear();
            }
        }

        public V5ChatEventSinkCli()
        {
            chatMessagesFromServer = new List<V5ChatMessageCli>();
        }

        public void OnChatLogUpdated(V5ChatMessageCli[] messages, uint length)
        {
            AddChatMessages = messages;
        }

        public void OnChatMessageAdded(V5ChatMessageCli message)
        {
            AddChatMessage = message;
        }

        public void OnErrorNotification()
        {
        }
    }
}
