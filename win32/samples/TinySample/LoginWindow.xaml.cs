﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using libv5liteWrapper;
using System.Security.Cryptography;

namespace V5Lite
{
    /// <summary>
    /// LoginWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class LoginWindow : Window
    {
        private LibV5Lite m_libv5lite = null;
        private string m_token = null;
        private string m_v5LiteServerUrl = null;
        private V5ConferenceEventSinkCli m_conferenceEvents = null;

        public LoginWindow(LibV5Lite libv5lite, V5ConferenceEventSinkCli conferenceEvents)
        {
            m_libv5lite = libv5lite;
            m_conferenceEvents = conferenceEvents;
            m_conferenceEvents.OnSessionLinkedEvent += OnSessionLinked;
            InitializeComponent();
        }

        private void OnSessionLinked(String defaultDisplayName)
        {
            //DisplayName.Text = defaultDisplayName;
            Dispatcher.Invoke(
                new Action(() =>
                {
                    var info = new V5PasswdInfoCli();
                    info.passwd = RoomPassword.Text;
                    info.hashType = vrms5cli.V5HashType_tag.V5_HASH_TYPE_SHA1;
                    info.hashCount = 2350;
                    info.salt = "hogefugamax";

                    for (int i = 0; i < info.hashCount; i ++)
                    {
                        byte[] byteValue = Encoding.UTF8.GetBytes(info.passwd + info.salt);
                        SHA1 crypto = new SHA1CryptoServiceProvider();
                        byte[] hashValue = crypto.ComputeHash(byteValue);
                        StringBuilder hashedText = new StringBuilder();
                        for (int n = 0; n < hashValue.Length; n++)
                        {
                            hashedText.AppendFormat("{0:x2}", hashValue[n]);
                        }
                        info.passwd = hashedText.ToString().ToLower();
                    }
                    Console.WriteLine("ハッシュ値：" + info.passwd);

                    m_libv5lite.JoinConference(DisplayName.Text, info);
                })
            );
        }

        public void Clear()
        {
            DisplayName.Text = null;
        }

        public void SetLoginInfo(string token, string v5LiteServerUrl)
        {
            m_token = token;
            m_v5LiteServerUrl = v5LiteServerUrl;
        }

        private void Login_Click(object sender, RoutedEventArgs e)
        {
            string v5LiteServerUrl_ = m_v5LiteServerUrl;

            //"http://"の"//"がなかったらつける
            if (v5LiteServerUrl_.IndexOf("http:") == 0)
            {
                if (v5LiteServerUrl_.IndexOf("http://") == -1) v5LiteServerUrl_ = v5LiteServerUrl_.Replace("http:", "http://");
            }
            else if (v5LiteServerUrl_.IndexOf("https:") == 0)
            {
                if (v5LiteServerUrl_.IndexOf("https://") == -1) v5LiteServerUrl_ = v5LiteServerUrl_.Replace("https:", "https://");
            }

            //urlの最後が'/'出なかったらつける
            if (!v5LiteServerUrl_.EndsWith("/")) v5LiteServerUrl_ += "/";

            string vcubePortalUrl = v5LiteServerUrl_ + "api/v5lite/client/";

            //自分自身のAssemblyを取得
            System.Reflection.Assembly asm = System.Reflection.Assembly.GetExecutingAssembly();
            //バージョンの取得
            var ver = asm.GetName().Version.ToString();
            ver = "5.2.0.0";

            m_libv5lite.BeginSession(vcubePortalUrl, m_token, ver);
        }
    }
}
