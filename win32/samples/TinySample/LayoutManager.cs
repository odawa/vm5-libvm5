﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace V5Lite
{
    class LayoutManager
    {
        private Layout m_layout;
        private ChangeVideoLayoutCommandVM m_changeVideoLayoutCommandVM;
        private ChangeVideoOnOffCommandVM m_changeVideoOnOffCommandVM;
        private ChangeChatOnOffCommandVM m_changeChatOnOffCommandVM;

        public enum Layout { VideoOnly, VideoAndChat, ChatOnly };

        public ICommand ChangeLayoutCommand { get; set; }

        public void SetChangeLayoutCommandVM(ChangeVideoLayoutCommandVM changeLayoutCommandVM)
        {
            m_layout = Layout.VideoOnly;
            m_changeVideoLayoutCommandVM = changeLayoutCommandVM;
        }

        public void SetChangeVideoOnOffCommandVM(ChangeVideoOnOffCommandVM changeVideoOnOffCommandVM)
        {
            m_changeVideoOnOffCommandVM = changeVideoOnOffCommandVM;
        }

        public void SetChangeChatOnOffCommandVM(ChangeChatOnOffCommandVM changeChatOnOffCommandVM)
        {
            m_changeChatOnOffCommandVM = changeChatOnOffCommandVM;
        }

        //最初にforceでレイアウトを指定すること
        public void ChangeLayout(Layout layout, bool force = false)
        {
            if (force == false)
            {
                if (m_layout == layout) return;
            }

            switch (layout)
            {
                case Layout.VideoOnly:
                    m_layout = layout;
                    m_changeVideoLayoutCommandVM.IsEnabled = true;
                    m_changeVideoOnOffCommandVM.IsVideoOn = true;
                    m_changeChatOnOffCommandVM.IsChatOn = false;

                    //レイアウト変更コマンド実行
                    Console.WriteLine("ChangeLayout m_layout:{0}", m_layout);
                    ChangeLayoutCommand.Execute(Layout.VideoOnly);
                    break;

                case Layout.VideoAndChat:
                    m_layout = layout;
                    m_changeVideoLayoutCommandVM.IsEnabled = true;
                    m_changeVideoOnOffCommandVM.IsVideoOn = true;
                    m_changeChatOnOffCommandVM.IsChatOn = true;

                    //レイアウト変更コマンド実行
                    Console.WriteLine("ChangeLayout m_layout:{0}", m_layout);
                    ChangeLayoutCommand.Execute(Layout.VideoAndChat);
                    break;

                case Layout.ChatOnly:
                    m_layout = layout;
                    m_changeVideoLayoutCommandVM.IsEnabled = false;
                    m_changeVideoOnOffCommandVM.IsVideoOn = false;
                    m_changeChatOnOffCommandVM.IsChatOn = true;

                    //レイアウト変更コマンド実行
                    Console.WriteLine("ChangeLayout m_layout:{0}", m_layout);
                    ChangeLayoutCommand.Execute(Layout.ChatOnly);
                    break;

                default:
                    break;
            }
        }

        public bool ChangeChat()
        {
            switch (m_layout)
            {
                case Layout.VideoOnly:
                    ChangeLayout(Layout.VideoAndChat);
                    return true;

                case Layout.VideoAndChat:
                    ChangeLayout(Layout.VideoOnly);
                    return true;

                case Layout.ChatOnly:
                    return false;

                default:
                    return false;
            }
        }

        public bool ChangeVideo()
        {
            switch (m_layout)
            {
                case Layout.VideoOnly:
                    return false;

                case Layout.VideoAndChat:
                    ChangeLayout(Layout.ChatOnly);
                    return true;

                case Layout.ChatOnly:
                    ChangeLayout(Layout.VideoAndChat);
                    return true;

                default:
                    return false;
            }
        }

        public void ChangeVideoLayout()
        {
            bool isFlat = m_changeVideoOnOffCommandVM.IsFlat;
            m_changeVideoOnOffCommandVM.IsFlat = !isFlat;

            //ここでライブラリに変更を依頼する
            Console.WriteLine("ChangeVideoLayout isFlat:{0}", isFlat);
        }
    }
}
