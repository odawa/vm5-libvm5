﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace V5Lite
{
    class ChangeChatOnOffCommandVM : BindableBase
    {
        private LayoutManager m_layoutManager;
        public ChangeChatOnOffCommandVM(LayoutManager layoutManager)
        {
            m_layoutManager = layoutManager;
            Command = new DelegateCommand(ChangeChat);
            isChatOn = false;
            SetChatImage(isChatOn);
        }

        private bool isChatOn;
        public bool IsChatOn
        {
            get { return isChatOn; }
            set
            {
                if (value == isChatOn) return;
                isChatOn = value;
                SetChatImage(isChatOn);

                Console.WriteLine("Chat isChatOn:{0}", isChatOn);
            }
        }
        
        private string icomImageName;
        public string IcomImageName
        {
            get
            {
                return icomImageName;
            }
            set
            {
                SetProperty<string>(ref icomImageName, value);
            }
        }

        public ICommand Command { get; set; }

        private void ChangeChat(object obj)
        {
            //変更不可の場合は何もしない
            if (!m_layoutManager.ChangeChat())  return;
        }

        private void SetChatImage(bool isChatOn_)
        {
            if (isChatOn_)  IcomImageName = "/images/ButtonChat_up.png";
            else            IcomImageName = "/images/ButtonChat-OFF_up.png";
        }
    }
}

