﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using libv5liteWrapper;

using V5Object = System.Collections.Generic.Dictionary<System.String, System.Object>;
using V5Array = System.Collections.Generic.List<System.Object>;


namespace V5Lite
{
    public class V5ConferenceEventSinkCli : BindableBase, IV5ConferenceEventSinkCli
    {
        public event Action<String> OnSessionLinkedEvent = null;
        public event Action<V5ErrorInfoCli> OnBeginSessionFailedEvent = null;
        public event Action<string, string> OnReceiveJoinConferenceRequestEvent = null;
        public event Action OnJoinedConferenceEvent = null;
        public event Action<V5ErrorInfoCli> OnJoinConferenceFailedEvent = null;
        public event Action<V5ErrorInfoCli> OnLeavedConferenceEvent = null;
        public event Action<string, bool> OnAddShareEvent = null;
        public event Action<string, bool> OnRemoveShareEvent = null;

//        private V5ConferenceInfoCli confInfo;

        //後でロックかける
        private uint participantNumber = 0;
        public uint Participantnumber
        {
            get { return participantNumber; }
            set
            {
                SetProperty<uint>(ref participantNumber, value);
            }
        }


        public void OnSessionLinked(String displayName, V5Object logoUrl)
        {
            if (OnSessionLinkedEvent != null) OnSessionLinkedEvent(displayName);
            Console.WriteLine("[OnSessionLinked]");
        }

        public void OnBeginSessionFailed(V5ErrorInfoCli errInfo)
        {
            if (OnBeginSessionFailedEvent != null) OnBeginSessionFailedEvent(errInfo);
            Console.WriteLine("[OnBeginSessionFailed]");
        }

        public void OnVersionUnmatched(String requireVersion)
        {
            Console.WriteLine("[OnVersionUnmatched]");
        }

        public void OnParticipantJoined(V5ParticipantCli participant)
        {
            //if (m_onParticipantJoinedDelegate != null) m_onParticipantJoinedDelegate(participant);
            Console.WriteLine("[OnParticipantJoined]");
        }

		public void OnParticipantLeaved(V5ParticipantCli participant)
        {
            //if (m_onParticipantLeavedDelegate != null) m_onParticipantLeavedDelegate(participant);
            Console.WriteLine("[OnParticipantLeaved]");
        }
        
        public void OnReceiveJoinConferenceRequest(string token, string v5LiteUrl)
        {
            if (OnReceiveJoinConferenceRequestEvent != null) OnReceiveJoinConferenceRequestEvent(token, v5LiteUrl);
        }

		public void OnJoinConferenceFailed(V5ErrorInfoCli errInfo)
        {
            if (OnJoinConferenceFailedEvent != null) OnJoinConferenceFailedEvent(errInfo);
            Console.WriteLine("[OnJoinConferenceFailed]");
        }

        public void OnJoinedConference(V5ConferenceInfoCli conferenceInfo)
        {
            Participantnumber = 1 + conferenceInfo.participantNumber;

            if (OnJoinedConferenceEvent != null) OnJoinedConferenceEvent();

            Console.WriteLine("OnJoinedConference:{0}", conferenceInfo.participantNumber);
            Console.WriteLine("OnJoinedConference:{0}:{1}:{2}:{3}:{4}", conferenceInfo.conferenceStatus.ConferenceName, conferenceInfo.conferenceStatus.RoomName, conferenceInfo.conferenceStatus.ConferenceURL, conferenceInfo.conferenceStatus.ConferencePinCode, conferenceInfo.conferenceStatus.DocumentSender);
            Console.WriteLine("OnJoinedConference:{0}:{1}:{2}:{3}:{4}", conferenceInfo.conferenceStatus.ConferenceStartTime, conferenceInfo.conferenceStatus.ConferenceEndTime, conferenceInfo.conferenceStatus.ConferenceURL, conferenceInfo.conferenceStatus.RemainTime, conferenceInfo.conferenceStatus.PastTime);
            Console.WriteLine("OnJoinedConference:{0}:{1}:{2}", conferenceInfo.conferenceStatus.isReserved, conferenceInfo.conferenceStatus.isRecoading, conferenceInfo.conferenceStatus.isLocked);
            Console.WriteLine("self:{0}:{1}", conferenceInfo.selfParticipant.id, conferenceInfo.selfParticipant.displayName);
        }

		public void OnLeavedConference(V5ErrorInfoCli errInfo)
        {
            if (OnLeavedConferenceEvent != null) OnLeavedConferenceEvent(errInfo);
            Console.WriteLine("[OnLeavedConference]");
        }

        public void OnRemoteSourceAdded(V5RomoteSourceChangedCli added)
        {
            Console.WriteLine("[OnRemoteSourceAdded]");
        }

        public void OnRemoteSourceRemove(V5RomoteSourceChangedCli removed)
        {
            Console.WriteLine("[OnRemoteSourceRemove]");
        }

        public void OnParticipantListUpdated(V5ParticipantCli[] participants, uint length)
        {
            Participantnumber = 1 + length;

            Console.WriteLine("[OnParticipantListUpdated]");
        }

		public void OnConferenceStatusUpdated(V5ConferenceStatusCli conferenceStatus)
        {
            Console.WriteLine("[OnConferenceStatusUpdated]");
        }

        public void OnConferenceNameChangeFailed(string requireVersion)
        {
            Console.WriteLine("[OnConferenceNameChangeFailed]");
        }

        public void OnAddShare(string uri, bool isSelfShare)
        {
            if (OnAddShareEvent != null) OnAddShareEvent(uri, isSelfShare);
        }

        public void OnRemoveShare(string uri, bool isSelfShare)
        {
            if (OnRemoveShareEvent != null) OnRemoveShareEvent(uri, isSelfShare);
        }

		public void OnSelectedParticipantsUpdated(int number)
        {
            Console.WriteLine("[OnSelectedParticipantsUpdated]");
        }

		public void OnReconnectConference(V5ConferenceInfoCli conferenceInfo)
        {
            Console.WriteLine("[OnReconnectConference]");
        }

        public void OnAsyncError(V5ErrorInfoCli errInfo)
        {
            Console.WriteLine("[OnAsyncError]");
        }

        public void OnInviteMailIsSent(V5ErrorInfoCli errInfo, string status, string mailAddress)
        {
            Console.WriteLine("[OnInviteMailIsSent]");
        }

        public void OnFloatingWindow(UIntPtr hWnd)
        {
            Console.WriteLine("[OnFloatingWindow]");
        }

        public void OnRcvRejectParticipant(V5ParticipantCli participant)
        {
            Console.WriteLine("[OnRcvRejectParticipant]");
        }

		public void OnPastTimeStart()
        {
            Console.WriteLine("[OnPastTimeStart]");
        }

		public void OnPastTimeStop()
        {
            Console.WriteLine("[OnPastTimeStop]");
        }

        public void OnNowMaintenance(long maintenanceStartTime, long maintenanceEndTime)
        {
            Console.WriteLine("[OnNowMaintenance]");
        }

        public void OnConferenceStatisticsNotice(V5ConferenceStatisticsAllInfoCli confStatInfo)
        {
            Console.WriteLine("[OnConferenceStatisticsNotice]");
        }

        public void OnConferenceEndTimeOverNotice(int minutes)
        {
            Console.WriteLine("[OnConferenceEndTimeOverNotice]");
        }

        public void OnLibFatalError(V5ErrorInfoCli errInfo)
        {
            Console.WriteLine("[OnLibFatalError]");
        }

    }
}
