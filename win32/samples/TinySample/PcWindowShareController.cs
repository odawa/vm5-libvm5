﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using libv5liteWrapper;

namespace V5Lite
{
    class PcWindowShareController : IVisual
    {
        private LibV5Lite m_libv5lite;
        private Grid m_target;
        private VisualCloseManagerWhenClickOutside m_visualCloseManagerWhenClickOutside;
        private PcWindowShareVM m_pcWindowForShareVM;
        private PcWindowShareGrid m_PcWindowShareGrid = null;

        public event Action OnSharingStartedEvent = null;
        public event Action OnSharingStoppedEvent = null;

        public PcWindowShareController(LibV5Lite libv5lite, Grid target, VisualCloseManagerWhenClickOutside visualCloseManagerWhenClickOutside)
        {
            m_libv5lite = libv5lite;
            m_target = target;
            m_visualCloseManagerWhenClickOutside = visualCloseManagerWhenClickOutside;
        }

        //IVisual
        public void CloseVisual()
        {
            m_target.Visibility = Visibility.Hidden;
        }
        public bool DoesContain(Point p)
        {
            if (ViewHelperStatic.DoesContain(m_target, p)) return true;
            return false;
        }
        
        public void PrepareToStart()
        {
            //必ずStartから開始する前提
            if (m_PcWindowShareGrid == null)
            {
                m_PcWindowShareGrid = new PcWindowShareGrid();
                m_pcWindowForShareVM = new PcWindowShareVM(m_libv5lite);
                m_pcWindowForShareVM.OnSharingStartedEvent += OnSharingStarted;
                m_pcWindowForShareVM.OnSharingStoppedEvent += OnSharingStopped;
                m_PcWindowShareGrid.DataContext = m_pcWindowForShareVM;
                m_target.Visibility = Visibility.Hidden;
                m_target.Children.Add(m_PcWindowShareGrid);
            }

            //すでに表示していたら何もしない
            if (m_target.Visibility == Visibility.Visible) return;

            //リスト作成指示
            m_pcWindowForShareVM.PrepareToStart();

            //表示
            m_PcWindowShareGrid.PcWindowListBox.IsEnabled = true;
            m_target.Visibility = Visibility.Visible;
            m_visualCloseManagerWhenClickOutside.Start(VisualCloseManageArea.PcWindowShare, this);
        }

        public void PrepareToStop()
        {
            //すでに表示していたら何もしない
            if (m_target.Visibility == Visibility.Visible) return;

            //リスト作成指示
            m_pcWindowForShareVM.PrepareToStop();

            //表示
            m_PcWindowShareGrid.PcWindowListBox.IsEnabled = false;
            m_target.Visibility = Visibility.Visible;
            m_visualCloseManagerWhenClickOutside.Start(VisualCloseManageArea.PcWindowShare, this);
        }

        
        private void OnSharingStarted()
        {
            m_target.Visibility = Visibility.Hidden;
            m_visualCloseManagerWhenClickOutside.Stop(VisualCloseManageArea.PcWindowShare);
            if (OnSharingStartedEvent != null) OnSharingStartedEvent();
        }
        private void OnSharingStopped()
        {
            m_target.Visibility = Visibility.Hidden;
            m_visualCloseManagerWhenClickOutside.Stop(VisualCloseManageArea.PcWindowShare);
            if (OnSharingStoppedEvent != null) OnSharingStoppedEvent();
        }
    }
}
