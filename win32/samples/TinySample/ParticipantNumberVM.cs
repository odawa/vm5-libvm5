﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;

namespace V5Lite
{
    public class ParticipantNumberVM : BindableBase
    {
        private V5ConferenceEventSinkCli m_v5ConferenceEventSinkCli;

        public ParticipantNumberVM(V5ConferenceEventSinkCli v5ConferenceEventSinkCli)
        {
            m_v5ConferenceEventSinkCli = v5ConferenceEventSinkCli;
            m_v5ConferenceEventSinkCli.PropertyChanged += new PropertyChangedEventHandler(OnPropertyChanged);
        }


        private uint number;
        public uint Number
        {
            get { return this.number; }
            set { this.SetProperty(ref this.number, value); }
        }

        private void OnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            Application.Current.Dispatcher.Invoke(
                new Action(() =>
                {
                    Number = m_v5ConferenceEventSinkCli.Participantnumber;
                })
            );
        }
    }
}
