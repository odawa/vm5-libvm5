﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace V5Lite
{
    public class ChatMessage
    {
        public string Name { get; set; }
        public string TimeStamp { get; set; }
        public string Message { get; set; }

        public ChatMessage(string name, string timestamp, string message)
        {
            this.Name = name;
            this.TimeStamp = timestamp;
            this.Message = message;
        }
    }
}
