﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows;
using libv5liteWrapper;

namespace V5Lite
{
    class ChangePcWindowShareOnOffCommandVM : BindableBase
    {
        private LibV5Lite m_libv5lite;
        private PcWindowShareController m_pcWindowShareController;
        public ChangePcWindowShareOnOffCommandVM(LibV5Lite libv5Lite, PcWindowShareController pcWindowShareController)
        {
            m_libv5lite = libv5Lite;
            m_pcWindowShareController = pcWindowShareController;
            Command = new DelegateCommand(ChangePcWindowShare);
            isPcWindowShareOn = false;
            SetImage(isPcWindowShareOn);
        }

        private bool isPcWindowShareOn;
        public bool IsPcWindowShareOn
        {
            get { return isPcWindowShareOn; }
            set
            {
                if (value == isPcWindowShareOn) return;
                isPcWindowShareOn = value;
                SetImage(isPcWindowShareOn);

                Console.WriteLine("PcWindowShare isPcWindowShareOn:{0}", isPcWindowShareOn);
            }
        }
        
        private string icomImageName;
        public string IcomImageName
        {
            get
            {
                return icomImageName;
            }
            set
            {
                SetProperty<string>(ref icomImageName, value);
            }
        }

        public ICommand Command { get; set; }

        private void ChangePcWindowShare(object obj)
        {
            //共有開始
            if (isPcWindowShareOn == false)
            {
                m_pcWindowShareController.PrepareToStart();
            }
            else
            {
                m_pcWindowShareController.PrepareToStop();
            }
        }

        private void SetImage(bool isPcWindowShareOn_)
        {
            if (isPcWindowShareOn_) IcomImageName = "/images/ButtonSharing_up.png";
            else                    IcomImageName = "/images/ButtonSharing-OFF_up.png";
        }

        //外部からイベントに登録できるようにする(public)
        public void OnSharingStarted()
        {
            IsPcWindowShareOn = true;
        }
        public void OnSharingStopped()
        {
            IsPcWindowShareOn = false;
        }
        public void OnAddShare(string uri, bool isSelfShare)
        {
            Application.Current.Dispatcher.Invoke(
                new Action(() =>
                {
                    m_libv5lite.StartWatchShared(uri);
                })
            );
        }
        public void OnRemoveShare(string uri, bool isSelfShare)
        {
            Application.Current.Dispatcher.Invoke(
                new Action(() =>
                {
                    m_libv5lite.StopWatchShared(uri);
                })
            );
        }
    }
}
