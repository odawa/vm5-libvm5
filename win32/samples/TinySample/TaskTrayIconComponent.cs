﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace V5Lite
{
    public partial class TaskTrayIconComponent : Component
    {
        public TaskTrayIconComponent()
        {
            InitializeComponent();
            toolStripMenuItemExit.Click += toolStripMenuItemExit_Click;
        }

        //終了
        void toolStripMenuItemExit_Click(object sender, EventArgs e)
        {
            System.Windows.Application.Current.Shutdown();
        }
    }
}
