﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using System.Windows.Automation.Peers;
using System.Windows.Automation.Provider;

namespace V5Lite
{
    /// <summary>
    /// ChatGrid.xaml の相互作用ロジック
    /// </summary>
    public partial class ChatGrid : Grid
    {
        public ICommand ScrollUpCompletelyCommand { get; set; }

        public ChatGrid()
        {
            InitializeComponent();
            ScrollUpCompletelyCommand = new DelegateCommand(ScrollUpCompletely, CanScrollUpCompletely);
        }

        private bool CanScrollUpCompletely(object parameter)
        {
            var peer = ItemsControlAutomationPeer.CreatePeerForElement(chatListBox);
            var scrollProvider = peer.GetPattern(PatternInterface.Scroll) as IScrollProvider;
            if (!scrollProvider.VerticallyScrollable) return true;

            //垂直スクロールバーが表示されていて一番下でなかったら、メッセージ追加後にスクロールさせない。
            bool canScrollUp = false;
            if (scrollProvider.VerticalScrollPercent >= 100)
                canScrollUp = true;
            return canScrollUp;
        }

        private void ScrollUpCompletely(object parameter)
        {
            if (chatListBox.Items.Count >= 1)
            {
                chatListBox.ScrollIntoView(chatListBox.Items[chatListBox.Items.Count - 1]);
            }
        }
    }
}
