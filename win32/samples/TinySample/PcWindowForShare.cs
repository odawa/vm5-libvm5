﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace V5Lite
{
    class PcShareItem
    {
        public string Name { get; private set; }
        public Action StartShare;

        public PcShareItem(string name)
        {
            this.Name = name;
            this.StartShare = () => { };
        }

    }

}
