﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace V5Lite
{
    class ChangeVideoLayoutCommandVM : BindableBase
    {
        private LayoutManager m_layoutManager;
        public ChangeVideoLayoutCommandVM(LayoutManager layoutManager)
        {
            m_layoutManager = layoutManager;
            Command = new DelegateCommand(ChangeVideoLayout);

            isEnabled = true;
        }

        private bool isEnabled;
        public bool IsEnabled
        {
            get
            {
                return isEnabled;
            }
            set
            {
                SetProperty<bool>(ref isEnabled, value);
            }
        }

        public ICommand Command { get; set; }

        private void ChangeVideoLayout(object obj)
        {
            m_layoutManager.ChangeVideoLayout();
        }
    }
}
