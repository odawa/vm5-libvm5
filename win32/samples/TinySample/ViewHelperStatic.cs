﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace V5Lite
{
    static public class ViewHelperStatic
    {
        static public bool DoesContain(Visual visual, Point mouseClientPoint)
        {
            //クライアント座標系の点を、visual(UI要素)の左上を原点とする座標系に変換する
            Point visualRelativePoint = visual.TransformToAncestor(Window.GetWindow(visual)).Transform(new Point(0, 0));
            Point mouseRelativePoint = mouseClientPoint;
            mouseRelativePoint.Offset(-visualRelativePoint.X, -visualRelativePoint.Y);

            //点がvisual(UI要素)内かどうかを判定する
            HitTestResult result = VisualTreeHelper.HitTest(visual, mouseRelativePoint);
            if (result != null) return true;

            return false;
        }
    }
}
