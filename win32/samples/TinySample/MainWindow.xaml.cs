﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Interop;
using System.Web;
using System.Collections.ObjectModel;
using System.Threading;
using System.Runtime.InteropServices; 
using libv5liteWrapper;
using vrms5cli;

namespace V5Lite
{
    /// <summary>
    /// MainWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class MainWindow : Window
    {
        public event Action OnWindowPrepared = null;

        private LibV5Lite m_libv5lite = null;
        private VisualCloseManagerWhenClickOutside m_visualCloseManagerWhenClickOutside;
        private EventSinkClis m_eventSinkClis;
        ChangeVideoOnOffCommandVM m_changeVideoOnOffCommandVM;
        ParticipantNumberVM m_participantNumberVM;
        private LayoutManager m_layoutManager;

        //チャット
        private ChatGrid m_chatGrid = null;
        private ChatMessageVM m_chatMessageVM = null;

        public MainWindow(LibV5Lite libv5lite, EventSinkClis eventSinkClis)
        {
            m_libv5lite = libv5lite;
            m_eventSinkClis = eventSinkClis;

            InitializeComponent();

            m_layoutManager = new LayoutManager();
            m_layoutManager.ChangeLayoutCommand = new DelegateCommand(ChangeLayout);

            ChangeVideoLayoutCommandVM changeVideoLayoutCommandVM = new ChangeVideoLayoutCommandVM(m_layoutManager);
            LayoutChangeButton.DataContext = changeVideoLayoutCommandVM;

            m_changeVideoOnOffCommandVM = new ChangeVideoOnOffCommandVM(m_layoutManager);
            ChangeVideoOnOffButton.DataContext = m_changeVideoOnOffCommandVM;

            ChangeChatOnOffCommandVM changeChatOnOffCommandVM = new ChangeChatOnOffCommandVM(m_layoutManager);
            ChangeChatOnOffButton.DataContext = changeChatOnOffCommandVM;

            m_layoutManager.SetChangeLayoutCommandVM(changeVideoLayoutCommandVM);
            m_layoutManager.SetChangeVideoOnOffCommandVM(m_changeVideoOnOffCommandVM);
            m_layoutManager.SetChangeChatOnOffCommandVM(changeChatOnOffCommandVM);


            //
            m_visualCloseManagerWhenClickOutside = new VisualCloseManagerWhenClickOutside(this);

            //PCWindow共有
            PcWindowShareController pcWindowShareController = new PcWindowShareController(m_libv5lite, AppShareGridHolder, m_visualCloseManagerWhenClickOutside);
            ChangePcWindowShareOnOffCommandVM changePcWindowShareOnOffCommandVM = new ChangePcWindowShareOnOffCommandVM(m_libv5lite, pcWindowShareController);
            PCWindowShareOnOffButton.DataContext = changePcWindowShareOnOffCommandVM;
            pcWindowShareController.OnSharingStartedEvent += changePcWindowShareOnOffCommandVM.OnSharingStarted;
            pcWindowShareController.OnSharingStoppedEvent += changePcWindowShareOnOffCommandVM.OnSharingStopped;
            m_eventSinkClis.v5ConferenceEventSinkCli.OnAddShareEvent += changePcWindowShareOnOffCommandVM.OnAddShare;
            m_eventSinkClis.v5ConferenceEventSinkCli.OnRemoveShareEvent += changePcWindowShareOnOffCommandVM.OnRemoveShare;

            //チャット
            m_chatGrid = new ChatGrid();
            chatGridHolder.Children.Add(m_chatGrid);
            m_chatMessageVM = new ChatMessageVM(m_libv5lite, m_eventSinkClis.v5ChatEventSinkCli);
            m_chatGrid.DataContext = m_chatMessageVM;
            m_chatMessageVM.ScrollUpCompletelyCommand = m_chatGrid.ScrollUpCompletelyCommand;

            //参加者人数(メニュー下)
            m_participantNumberVM = new ParticipantNumberVM(m_eventSinkClis.v5ConferenceEventSinkCli);
            participantNumberText.DataContext = m_participantNumberVM;

            //アプリケーション共有イベントシンクとつなぐ
            //m_eventSinkClis.v5ConferenceEventSinkCli.OnAddShareEvent += OnAddShare;
            //m_eventSinkClis.v5ConferenceEventSinkCli.OnRemoveShareEvent += OnRemoveShare;

            //表示メッセージクリア
            Messages.Items.Clear();

            //入室関連メッセージ表示
            eventSinkClis.v5ConferenceEventSinkCli.OnJoinedConferenceEvent += OnJoinedConference;
            eventSinkClis.v5ConferenceEventSinkCli.OnJoinConferenceFailedEvent += OnJoinConferenceFailed;
        }

        public void Initialize()
        {
            //レイアウトの初期設定
            m_layoutManager.ChangeLayout(LayoutManager.Layout.VideoOnly, true);

            //映像レイアウトの初期指定
            m_changeVideoOnOffCommandVM.InitializeVideoLayout(true);

            //チャットメッセージクリア            
            m_chatMessageVM.ClearChatMessage();

        }

        private void Window_ContentRendered(object sender, EventArgs e)
        {
            m_visualCloseManagerWhenClickOutside.SetOutOfCloseVisual(VisualCloseManageArea.PcWindowShare, PCWindowShareOnOffButton);

//            if (OnWindowPrepared != null)   OnWindowPrepared();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            m_libv5lite.LeaveConferenceAsync();
//            m_libv5lite.StopLogic();        // 即呼んではならない
            e.Cancel = true;
        }





        /********************************************************************************************
         * http://stackoverflow.com/questions/1399037/loading-a-wpf-window-without-showing-it
         *********************************************************************************************/
        [DllImport("user32.dll")]
        static extern IntPtr SetParent(IntPtr hwnd, IntPtr hwndNewParent);
        private const int HWND_MESSAGE = -3;
        private IntPtr hwnd;
        private IntPtr oldParent;
        protected override void OnSourceInitialized(EventArgs e)
        {
            base.OnSourceInitialized(e);
            HwndSource hwndSource = PresentationSource.FromVisual(this) as HwndSource;
            hwnd = hwndSource.Handle;
            oldParent = SetParent(hwnd, (IntPtr)HWND_MESSAGE);
            Visibility = Visibility.Hidden;

            if (OnWindowPrepared != null) OnWindowPrepared();
        }

        public void ShowSpecially()
        {
            SetParent(hwnd, oldParent);
            Show();
            Activate();
        }

        public void HideSpecially()
        {
            HwndSource hwndSource = PresentationSource.FromVisual(this) as HwndSource;
            hwnd = hwndSource.Handle;
            oldParent = SetParent(hwnd, (IntPtr)HWND_MESSAGE);
            Visibility = Visibility.Hidden;
        }
        /********************************************************************************************/
        
        
        
        
        
        private void ChangeLayout(object obj)
        {
            LayoutManager.Layout layout = (LayoutManager.Layout)obj;
            Console.WriteLine("ChangeLayout(View):{0}", layout);

            //ここでレイアウトの変更をする


        }

        private void OnJoinedConference()
        {
            Dispatcher.Invoke(
                new Action(() =>
                {
                    Messages.Items.Add("[onJoinedConference]");
                    Messages.Items.Refresh();
                })
            );
        }

        private void OnJoinConferenceFailed(V5ErrorInfoCli errInfo)
        {
            Dispatcher.Invoke(
                new Action(() =>
                {
                    string msg = string.Format("[onJoinConferenceFailed]code={0} category={1} reason={2}", errInfo.code_deprecated, errInfo.category, errInfo.reason);
                    Messages.Items.Add(msg);
                    Messages.Items.Refresh();
                })
            );
        }

        private void CameraMuteState_Click(object sender, RoutedEventArgs e)
        {
            Console.WriteLine("<カメラミュート>:{0}", m_libv5lite.IsCameraMuted());
        }

        private void MuteCamera_Checked(object sender, RoutedEventArgs e)
        {
            m_libv5lite.MuteCamera(true);
        }

        private void MuteCamera_Unchecked(object sender, RoutedEventArgs e)
        {
            m_libv5lite.MuteCamera(false);
        }

        private void MicMuteState_Click(object sender, RoutedEventArgs e)
        {
            Console.WriteLine("<マイクミュート>:{0}", m_libv5lite.IsMicrophoneMuted());
        }

        private void MuteMic_Checked(object sender, RoutedEventArgs e)
        {
            m_libv5lite.MuteMicrophone(true);
        }

        private void MuteMic_Unchecked(object sender, RoutedEventArgs e)
        {
            m_libv5lite.MuteMicrophone(false);
        }

        private void SpeakerMuteState_Click(object sender, RoutedEventArgs e)
        {
            Console.WriteLine("<スピーカーミュート>:{0}", m_libv5lite.IsSpeakerMuted());
        }

        private void MuteSpeaker_Checked(object sender, RoutedEventArgs e)
        {
            m_libv5lite.MuteSpeaker(true);
        }

        private void MuteSpeaker_Unchecked(object sender, RoutedEventArgs e)
        {
            m_libv5lite.MuteSpeaker(false);
        }
        
        private void GetSelfParticipant_Click(object sender, RoutedEventArgs e)
        {
            V5ParticipantCli selfParticipant = m_libv5lite.GetSelfParticipant();
            Console.WriteLine("自分情報:{0}:{1}", selfParticipant.id, selfParticipant.displayName);
        }

        private void ConferenceOut_Click(object sender, RoutedEventArgs e)
        {
            m_libv5lite.LeaveConferenceAsync();
//HideSpecially();
        }

        private void GetDeviceNum_Click(object sender, RoutedEventArgs e)
        {
            int deviceNum = m_libv5lite.GetDeviceNumber((V5DeviceType_tag)(1 + device.SelectedIndex));
            Console.WriteLine("デバイス数{0}:{1}", device.SelectedIndex, deviceNum);
        }

        private void GetCurrentDeviceNo_Click(object sender, RoutedEventArgs e)
        {
            int deviceNo = m_libv5lite.GetCurrentDeviceNo((V5DeviceType_tag)(1 + device.SelectedIndex));
            Console.WriteLine("デバイスNo{0}:{1}", device.SelectedIndex, deviceNo);
        }

        private void GetSpeakerList_Click(object sender, RoutedEventArgs e)
        {
            int deviceNum = m_libv5lite.GetDeviceNumber(V5DeviceType_tag.V5_DEVICE_TYPE_AUDIO_OUT);
            for (int i = 0; i < deviceNum; ++i)
            {
                V5DeviceNameCli deviceName = m_libv5lite.GetSpeakerItem(i);
                Console.WriteLine("スピーカー[{0}]{1}:{2}", i, deviceName.name, deviceName.phyDevName);
            }
        }

        private void GetMicList_Click(object sender, RoutedEventArgs e)
        {
//            m_libv5lite.SetConf(V5DeviceTypeCli.V5_DEVICE_TYPE_AUDIO_IN, 0);

            int deviceNum = m_libv5lite.GetDeviceNumber(V5DeviceType_tag.V5_DEVICE_TYPE_AUDIO_IN);
            for (int i = 0; i < deviceNum; ++i)
            {
                V5DeviceNameCli deviceName = m_libv5lite.GetMicrophoneItem(i);
                Console.WriteLine("マイク[{0}]{1}:{2}", i, deviceName.name, deviceName.phyDevName);
            }
        }

        private void GetCameraList_Click(object sender, RoutedEventArgs e)
        {
            int deviceNum = m_libv5lite.GetDeviceNumber(V5DeviceType_tag.V5_DEVICE_TYPE_VIDEO);
            for (int i = 0; i < deviceNum; ++i)
            {
                V5DeviceNameCli deviceName = m_libv5lite.GetCameraItem(i);
                Console.WriteLine("カメラ[{0}]{1}:{2}", i, deviceName.name, deviceName.phyDevName);
            }
        }

        private List<Participant> participantList;
        private void ParticipantList_Checked(object sender, RoutedEventArgs e)
        {
            int mode = 1;
            if (mode == 0)
            {
                Random rnd = new Random();
                int n = rnd.Next(10);
                participantList = new List<Participant>();
                participantList.Add(new Participant("ssfg5467hgfr4-ggk4n4fglknq-ffmnflakj1", "ああう" + n));
                participantList.Add(new Participant("ssfg5467hgfr4-ggk4n4fglknq-ffmnflakj2", "ああう" + n));
                participantList.Add(new Participant("ssfg5467hgfr4-ggk4n4fglknq-ffmnflakj3", "ああう" + n));
            }
            else if (mode == 1)
            {
//                participantList = m_presenter.GetParticipantList();
            }

            participantListBox.ItemsSource = participantList;
        }

        private void ParticipantList_Unchecked(object sender, RoutedEventArgs e)
        {

        }

        private void WindowDesktopList_Click(object sender, RoutedEventArgs e)
        {
            {
                V5WindowsAndDesktopsCli v5WinDesks = m_libv5lite.GetWindowsAndDesktops();
                int winNum = v5WinDesks.windows.Count();
                Console.WriteLine("[アプリケーションウィンドウ]:num={0}", winNum);
                for (int i = 0; i < winNum; ++i)
                {
                    Console.WriteLine("  ({0:x}){1}", v5WinDesks.windows[i].appWindowId.ToInt32(), v5WinDesks.windows[i].appWindowAppName);
                }

                int deskNum = v5WinDesks.desktops.Count();
                Console.WriteLine("[デスクトップウィンドウ]:num={0}", deskNum);
                for (int i = 0; i < deskNum; ++i)
                {
                    Console.WriteLine("  ({0:x}){1}", v5WinDesks.desktops[i].sysDesktopId, v5WinDesks.desktops[i].sysDesktopName);
                }
            }
        }

        private void SendInviteMail_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(InviteMailAddress.Text)) return;
            if (string.IsNullOrWhiteSpace(InviteMailAddress.Text)) return;

            V5CODE_tag ret = m_libv5lite.SendInviteMail(InviteMailAddress.Text);
            Console.WriteLine("[SendInviteMail_Click]ret={0}", ret);
        }
    }
}
