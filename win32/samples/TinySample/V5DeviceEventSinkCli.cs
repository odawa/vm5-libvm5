﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using libv5liteWrapper;
namespace V5Lite
{
    public class V5DeviceEventSinkCli : IV5DeviceEventSinkCli
    {
        public void OnAudioInDeviceListChanged(int index)
        {
            Console.WriteLine("[OnAudioInDeviceListChanged]{0}", index);
        }

        public void OnAudioOutDeviceListChanged(int index)
        {
            Console.WriteLine("[OnAudioInDeviceListChanged]{0}", index);
        }

        public void OnVideoDeviceListChanged(int index)
        {
            Console.WriteLine("[OnAudioInDeviceListChanged]{0}", index);
        }

        public void OnSelectedSystemAudioInDevicesChanged(int index)
        {
            Console.WriteLine("[OnAudioInDeviceListChanged]{0}", index);
        }

        public void OnSelectedSystemAudioOutDevicesChanged(int index)
        {
            Console.WriteLine("[OnAudioInDeviceListChanged]{0}", index);
        }
    }
}
