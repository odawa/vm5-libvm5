﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace V5Lite
{
    public interface IVisual
    {
        void CloseVisual();
        bool DoesContain(Point p);
    }
}
