﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace V5Lite
{
    static class UnixTimeHelperStatic
    {
        public readonly static DateTime UnixBaseTime = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        public static long ToUnixTime(DateTime dateTime)
        {
            dateTime = dateTime.ToUniversalTime();
            return (long)dateTime.Subtract(UnixBaseTime).TotalSeconds;
        }
        public static DateTime FromUnixTime(long unixTime)
        {
            DateTime localTime = System.TimeZoneInfo.ConvertTimeFromUtc(UnixBaseTime.AddSeconds(unixTime), System.TimeZoneInfo.Local);
            return localTime;
        }
    }
}
