﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.Windows.Input;
using System.Globalization;
using libv5liteWrapper;

namespace V5Lite
{
    class PcWindowShareVM : BindableBase
    {
        private LibV5Lite m_libv5lite = null;

        public event Action OnSharingStartedEvent = null;
        public event Action OnSharingStoppedEvent = null;

        private ObservableCollection<PcShareItem> pcWindows;
        public ObservableCollection<PcShareItem> PcWindows
        {
            get
            {
                return pcWindows;
            }
            private set{}
        }

        private PcShareItem selectedItem;
        public PcShareItem SelectedItem
        {
            get { return selectedItem; }
            set
            {
                SetProperty<PcShareItem>(ref selectedItem, value);
                ((DelegateCommand)PcWindowShareCommand).RaiseCanExecuteChanged();
            }
        }
        private bool prepareToStart;
        public ICommand PcWindowShareCommand { get; set; }

        private string buttonName;
        public string ButtonName
        {
            get { return buttonName; }
            set
            {
                SetProperty<string>(ref buttonName, value);
            }
        }

        public PcWindowShareVM(LibV5Lite libv5lite)
        {
            m_libv5lite = libv5lite;
            pcWindows = new ObservableCollection<PcShareItem>();
            PcWindowShareCommand = new DelegateCommand(PcWindowShare, CanPcWindowShare);

        }

        public void PrepareToStart()
        {
            prepareToStart = true;
            ButtonName = "共有を開始する";
            SelectedItem = null;

            pcWindows.Clear();
            V5WindowsAndDesktopsCli v5WinDesks = m_libv5lite.GetWindowsAndDesktops();
            int winNum = v5WinDesks.windows.Count();
            for (int i = 0; i < winNum; ++i)
            {
                var share = new PcShareItem(v5WinDesks.windows[i].appWindowAppName);
                IntPtr appWindowId = v5WinDesks.windows[i].appWindowId;
                share.StartShare = () => { m_libv5lite.ShareAppWindow(appWindowId); };
                pcWindows.Add(share);
            }

            //境界線用のダミー情報を挿入
            pcWindows.Add(new PcShareItem("-----"));

            CultureInfo cultureInfo = CultureInfo.CurrentCulture;
            TextInfo textInfo = cultureInfo.TextInfo;
            int deskNum = v5WinDesks.desktops.Count();
            for (int i = 0; i < deskNum; ++i)
            {
                string desktopName = string.Format("{0} ({1}x{2})",
                    textInfo.ToTitleCase(textInfo.ToLower(v5WinDesks.desktops[i].sysDesktopName)), v5WinDesks.desktops[i].sysDesktopRect.width, v5WinDesks.desktops[i].sysDesktopRect.height);
                var share = new PcShareItem(desktopName);
                uint sysDesktopId = v5WinDesks.desktops[i].sysDesktopId;
                share.StartShare = () => { m_libv5lite.ShareSysDesktop(sysDesktopId); };
                pcWindows.Add(share);
            }
            RaisePropertyChanged("PcWindows");
        }

        public void PrepareToStop()
        {
            prepareToStart = false;
            ButtonName = "共有を停止する";
        }

        
        private void PcWindowShare(object obj)
        {
            //共有開始
            if (prepareToStart)
            {
                Console.WriteLine("PcWindowShare SelectedItem:{0}", SelectedItem.Name);
                SelectedItem.StartShare();
                if (OnSharingStartedEvent != null) OnSharingStartedEvent();
            }
            //共有停止
            else
            {
                m_libv5lite.Unshare();
                if (OnSharingStoppedEvent != null) OnSharingStoppedEvent();
            }
        }

        private bool CanPcWindowShare(object obj)
        {
            bool ret = true;
            if (prepareToStart)
            {
                if (SelectedItem == null) ret = false;
            }
            return ret;
        }
    }
}
