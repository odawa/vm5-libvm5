﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace V5Lite
{
    public class Participant
    {
        public string Id { get; private set; }
        public string DisplayName { get; private set; }

        public Participant(string id, string displayName)
        {
            this.Id = id;
            this.DisplayName = displayName;
        }
    }
}

