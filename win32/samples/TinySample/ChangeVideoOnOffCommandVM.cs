﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace V5Lite
{
    class ChangeVideoOnOffCommandVM : BindableBase
    {
        private LayoutManager m_layoutManager;
        public ChangeVideoOnOffCommandVM(LayoutManager layoutManager)
        {
            m_layoutManager = layoutManager;
            Command = new DelegateCommand(ChangeVideo);
            isVideoOn = true;
            isFlat = true;
            SetVideoImage(isVideoOn, isFlat);
        }

        private bool isFlat;
        public bool IsFlat
        {
            get { return isFlat; }
            set
            {
                if (value == isFlat) return;
                isFlat = value;
                SetVideoImage(isVideoOn, isFlat);

                //ここで映像レイアウトの変更をライブラリに依頼する
                Console.WriteLine("Video isFlat:{0}", isFlat);
            }
        }

        private bool isVideoOn;
        public bool IsVideoOn
        {
            get { return isVideoOn; }
            set
            {
                if (value == isVideoOn) return;
                isVideoOn = value;
                SetVideoImage(isVideoOn, isFlat);

                Console.WriteLine("Video isVideoOn:{0}", isVideoOn);
            }
        }

        
        private string icomImageName;
        public string IcomImageName
        {
            get
            {
                return icomImageName;
            }
            set
            {
                SetProperty<string>(ref icomImageName, value);
            }
        }

        public ICommand Command { get; set; }

        public void InitializeVideoLayout(bool isFlat)
        {
            IsFlat = isFlat;
            //最初の映像レイアウト設定をライブラリに依頼する
        }

        private void ChangeVideo(object obj)
        {
            //変更不可の場合は何もしない
            if (!m_layoutManager.ChangeVideo()) return;
        }

        private void SetVideoImage(bool isVideoOn_, bool isFlat_)
        {
            if (isVideoOn_ && isFlat_)          IcomImageName = "/images/ButtonVideo-Flat_up.png";
            else if (!isVideoOn_ && isFlat_)   IcomImageName = "/images/ButtonVideo-Flat-OFF_up.png";
            else if (isVideoOn_ && !isFlat_) IcomImageName = "/images/ButtonVideo-AS_up.png";
            else if (!isVideoOn_ && !isFlat_) IcomImageName = "/images/ButtonVideo-AS-Off_up.png";
        }
    }
}
