/*
 *  LibWebRequestListener.h
 *  LibWebRequestListener
 *
 *  Created by V-0757 on 2015/03/09.
 *  Copyright (c) 2015年 V-cube, Inc. All rights reserved.
 *
 */

#ifndef RequestListener_
#define RequestListener_


#include <iostream>
#include <memory>

#include <V5Lite/IV5HttpdEventSink.h>
#include <V5Lite/RequestListenerCommon.h>

#ifdef __cplusplus

class HttpdRequest;

class RequestListener {
public:
    virtual ~RequestListener(){};
    
    typedef std::shared_ptr<RequestListener> instance_ptr_t;

    static instance_ptr_t createInstance();
    
    virtual void setHttpdEventSink( std::weak_ptr<IV5HttpdEventSink> sink ) = 0;
    virtual V5HTTPD_START HTTPDStart(unsigned short port) = 0;
    virtual void HTTPDStop() = 0;
    
};

#endif

#endif
