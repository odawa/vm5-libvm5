//
//  LibWebRequestListener.m
//  LibWebRequestListener
//

#import "RequestListener.h"
#import "LibWebRequestListener.hh"

class V5HttpdEventSink : public IV5HttpdEventSink {
public:
    V5HttpdEventSink( id<IV5HttpdRequestProtocol> protocol )
    {
        _protocol = protocol;
    }
    
    virtual ~V5HttpdEventSink()
    {
    
    };

    virtual unsigned int onAliveResponse() override;
    virtual bool onRequestJoinConfernce( const char* token, const char *potalURI ) override;
    
private:
    id<IV5HttpdRequestProtocol> _protocol;
    
};

unsigned int V5HttpdEventSink::onAliveResponse()
{
    return [_protocol onAliveResponse];
}

bool V5HttpdEventSink::onRequestJoinConfernce( const char* token, const char *potalURI )
{
    return [_protocol onRequestJoinConfernce:@(token) potalURI:@(potalURI)];
}

/****************************************************************************/
/* ここからobjective-Cメソッド */
/****************************************************************************/

@interface LibWebRequestListener()
{
    
}

@property std::shared_ptr<RequestListener> reqListener;


@end

@implementation LibWebRequestListener
{
    std::shared_ptr<IV5HttpdEventSink> _httpsink;
}

-(id)init {
    self = [super init];
    if (self) {
        self.reqListener = RequestListener::createInstance();
    }
    return self;
}

- (void)setHttpdEventSink:(id<IV5HttpdRequestProtocol>) protocol
{
    _httpsink = std::make_shared<V5HttpdEventSink>(protocol);
    self.reqListener->setHttpdEventSink(_httpsink);
}

- (V5HttpStart)HTTPDStart:(unsigned short) port
{
    return (V5HttpStart)(self.reqListener->HTTPDStart(port));
}

- (void)HTTPDStop
{
    self.reqListener->HTTPDStop();
}



@end
