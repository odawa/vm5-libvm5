
#include "RequestListener.h"

#include "HttpdRequest.h"

RequestListener::instance_ptr_t RequestListener::createInstance()
{
    return std::move(HttpdRequest::createInstance());
}
