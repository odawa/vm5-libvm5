//
//  LibWebRequestListener.h
//  LibWebRequestListener
//
//  Created by V-0757 on 2015/03/13.
//  Copyright (c) 2015年 V-cube, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <V5Lite/ObjcV5HttpdEventSink.hh>

typedef NS_ENUM(NSInteger, V5State) {
    V5State_Unknown = 0,
    V5State_Busy,
    V5State_Normal
};

typedef NS_ENUM(NSInteger, V5HttpStart) {
    V5HttpStart_Ok = 0,
    V5HttpStart_IPV4_Error,
    V5HttpStart_IPV6_Error
};

@interface LibWebRequestListener : NSObject
{
    
}

- (void)setHttpdEventSink:(id<IV5HttpdRequestProtocol>) protocol;
- (V5HttpStart)HTTPDStart:(unsigned short) port;
- (void)HTTPDStop;


@end
