//
//  V5ChatEventSinkImpl.swift
//  V5LiteSample
//
//  Created by V-0757 on 2015/02/04.
//  Copyright (c) 2015年 V-cube. All rights reserved.
//

import Cocoa

@objc class V5ChatEventSinkImpl : NSObject, IV5ChatEventSinkProtocol {
    
    var viewController:MainMenuViewController?
    
    override init() {
    }
    
    func onChatMessageAdded(message:ObjcMessage) {
        println("[IV5ChatEventSinkProtocol:onChatMessageAdded] In Swift");
        
        println("RecieveMessage");
        println("message.text           : \(message.text)");
        println("message.from           : \(message.from)");
        println("message.timestamp      : \(message.timestamp)");
        println("message.isSelfMessage  : \(message.isSelfMessage)");
        viewController?.onChatMessageAdded(message)
    }
    
    func onChatLogUpdated(messages:NSMutableArray, length:Int64) {
        
        viewController?.onChatLogUpdated(messages, length: length)
    }
}


