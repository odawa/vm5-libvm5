//
// Created by Hiromi on 2015/01/26.
// Copyright (c) 2015 V-cube. All rights reserved.
//

import Foundation

@objc class V5DeviceEventSinkImpl : NSObject, IV5DeviceEventSinkProtocol {
    override init() {
    }
    func onAudioInDeviceListChanged(list: V5DevList!) {
        println("[DeviceEvent]: onAudioInDeviceListChanged!");
    }
    func onAudioOutDeviceListChanged(list: V5DevList!) {
        println("[DeviceEvent]: onAudioOutDeviceListChanged!");
    }
    func onVideoDeviceListChanged(list:V5DevList!) {
        println("[DeviceEvent]: onVideoDeviceListChanged!");
    }
    func onSelectedSystemAudioInDevicesChanged(list:V5DevList!) {
        println("[DeviceEvent]: onSelectedSystemAudioInDevicesChanged!");
    }
    func onSelectedSystemAudioOutDevicesChanged(list:V5DevList!) {
        println("[DeviceEvent]: onSelectedSystemAudioOutDevicesChanged!");
    }
}
