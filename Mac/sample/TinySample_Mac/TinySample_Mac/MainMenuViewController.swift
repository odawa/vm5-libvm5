//
//  MainMenuController.swift
//  TinySample_Mac
//
//  Created by Hiromi on 2015/02/13.
//  Copyright (c) 2015年 Hiromi. All rights reserved.
//

import Cocoa

class MainMenuViewController : NSViewController
{
    var myParticipantList:Array<ObjcParticipant> = [];
    func getAppDelegate() -> AppDelegate? {
        return NSApplication.sharedApplication().delegate as? AppDelegate
    }
    
    @IBOutlet weak var userListView: NSTableView!
    @IBOutlet weak var url:NSTextField!
    @IBOutlet weak var idField: NSTextField!
    @IBOutlet weak var pwField: NSTextField!
    @IBOutlet weak var pinField: NSTextField!
    @IBOutlet weak var usePinButton: NSButton!
    @IBOutlet weak var userNameField: NSTextField!
    @IBOutlet weak var imageViewField: NSImageView!
    @IBOutlet weak var desktopShareViewField: NSImageView!
    @IBOutlet var ChatTextView: NSTextView!
    @IBOutlet var chatInputView: NSTextView!
    @IBOutlet var statusInfoView: NSTextView!
    @IBOutlet weak var participantListController: NSArrayController!
    @IBOutlet weak var window: NSWindow!
    @IBOutlet weak var subWindow: NSWindow!
    
    @IBOutlet weak var sharingWindowListButton: NSPopUpButton!
    @IBOutlet weak var roomSelectButton: NSPopUpButton!
    @IBOutlet weak var maxParticipantsStepper: NSStepper!
    @IBOutlet weak var maxParticipantsNumber: NSTextField!
    @IBOutlet weak var infoWindow: NSPanel!
    
    @IBOutlet weak var speakerListButton: NSPopUpButton!
    @IBOutlet weak var micListButton: NSPopUpButton!
    @IBOutlet weak var cameraListButton: NSPopUpButton!
    
    @IBOutlet weak var inputSendMaxkbpsField: NSTextField!
    @IBOutlet weak var setSendMaxKbps: NSButton!
    
    @IBOutlet weak var inputRecvMaxkbpsField: NSTextField!
    @IBOutlet weak var setRecvMaxKbps: NSButton!
    
    @IBOutlet weak var outputSendMaxkbpsField: NSTextField!
    @IBOutlet weak var getSendMaxKbps: NSButton!
    
    @IBOutlet weak var outputRecvMaxkbpsField: NSTextField!
    @IBOutlet weak var getRecvMaxKbps: NSButton!
    
    @IBOutlet weak var inputLayerField: NSTextField!
    @IBOutlet weak var inputSendBandWidthField: NSTextField!
    @IBOutlet weak var SendBandWidth: NSButton!
    
    @IBOutlet weak var newWidthValue: NSTextField!
    @IBOutlet weak var newHeighValue: NSTextField!
    @IBOutlet weak var newConfName: NSTextField!
    @IBOutlet var infoTextArea: NSTextView!
//    @IBOutlet weak var confStatistics: NSButton!
    
    var renderSelfView:Bool = false;
    @IBAction func clickEnter(sender: AnyObject) {
        NSUserDefaults.standardUserDefaults().setObject(url.stringValue, forKey: "url.stringValue")
        NSUserDefaults.standardUserDefaults().setObject(idField.stringValue, forKey: "idField.stringValue")
        NSUserDefaults.standardUserDefaults().setObject(userNameField.stringValue, forKey: "userNameField.stringValue")
        NSUserDefaults.standardUserDefaults().setObject(pwField.stringValue, forKey: "pwField.stringValue")
        NSUserDefaults.standardUserDefaults().setObject(roomSelectButton.indexOfSelectedItem, forKey: "roomSelectButton.indexOfSelectedItem")
        
        
        
        let delegate = getAppDelegate()
        let usePin = Bool(usePinButton.state)

        var index = roomSelectButton.indexOfSelectedItem

        delegate?.login(url: url.stringValue, id: idField.stringValue, password: pwField.stringValue, usePin: usePin, pinCode: pinField.stringValue, userName: userNameField.stringValue, roomIndex:index)
        
    }
    
    @IBAction func clickSetSendMaxKbps(sender: NSButton) {
        var kbps: Int32 = inputSendMaxkbpsField.intValue
        let delegate = getAppDelegate()
        delegate?.setSendMaxKbps(kbps)
    }
    
    @IBAction func clickSetRecvMaxKbps(sender: NSButton) {
        var kbps: Int32 = inputRecvMaxkbpsField.intValue
        let delegate = getAppDelegate()
        delegate?.setRecvMaxKbps(kbps)
    }
    
    @IBAction func clickGetSendMaxKbps(sender: NSButton) {
        let delegate = getAppDelegate()
        println("SendMaxKbps : \(delegate?.getSendMaxKbps())")
        //outputSendMaxkbpsField.setValue(delegate?.getSendMaxKbps())
    }
    
    @IBAction func clickGetRecvMaxKbps(sender: NSButton) {
        let delegate = getAppDelegate()
        println("RecvMaxKbps : \(delegate?.getRecvMaxKbps())")
        //outputRecvMaxkbpsField.setValue(delegate?.getRecvMaxKbps(), Int32() )
    }
    
    @IBAction func clickSetSendBandWidth(sender: NSButton) {
        var layer :UInt32 = UInt32(inputLayerField.intValue)
        var sendBandWidth: Int32 = inputSendBandWidthField.intValue
        let delegate = getAppDelegate()
        delegate?.setSendBandWidth(layer,sendBandWidth: sendBandWidth)
    }
    
    
    @IBAction func clickDesktopShare(sender: NSButton) {

        let delegate = getAppDelegate()
        if(sender.state != 0)
        {
            let index = sharingWindowListButton.indexOfSelectedItem
            // was not select window in list.
            if index == 0 {
                return
            }
            
            let title = sharingWindowListButton.titleOfSelectedItem
            var window:ObjcWindow?
            delegate?.getWindowsForSharing().iterate { win in
                if win.appWindowName == title {
                    window = win
                }
            }
            
            if let w = window {
                var screenId:UInt32 = w.appWindowId
                delegate?.startShareWindow(screenId as V5WindowCapturerWindowId, width:Int(w.width), height:Int(w.height))
            }
        }
        else
        {
            delegate?.stopShare()
        }
    }
    
    @IBAction func changeToActiveSpeakerLayout(sender: NSButton) {
        
        let delegate = getAppDelegate();
        if(sender.state != 0)
        {
            delegate?.changeToActiveSpeakerLayout(true);
        }
        else
        {
            delegate?.changeToActiveSpeakerLayout(false);
        }
        
    }
    @IBAction func onKickPressed(sender: NSButton) {
        if let delegate = getAppDelegate() {
        if userListView.selectedRow >= 0 {
            var rejectPID:String! = myParticipantList[userListView.selectedRow].id;
            delegate.rejectParticipant(rejectPID)
            }
        }
    }
    @IBAction func clickLogout(sender: AnyObject) {
        if let delegate = getAppDelegate() {
            delegate.logout()
            myParticipantList = [];
            if let list = participantListController {
                list.content = myParticipantList;
            }
            imageViewField.alphaValue = 0.0;
        }
    }
    
    @IBAction func onMuteSpeakerToggled(sender: NSButton) {
        getAppDelegate()?.speakerMuteToggle(Bool(sender.state));
    }
    
    @IBAction func onMuteMicToggled(sender: NSButton) {
        getAppDelegate()?.microphoneMuteToggle(Bool(sender.state));
    }
    
    @IBAction func onLockConfeToggled(sender: NSButton) {
        getAppDelegate()?.lockConference(Bool(sender.state));
    }
    
    @IBAction func onMuteVideoToggled(sender: NSButton) {
        getAppDelegate()?.cameraMuteToggle(Bool(sender.state))
    }
    @IBAction func ChangeVidyoRect(sender: NSButton) {
        if let newW = newWidthValue.stringValue.toInt() {
            if let newH = newHeighValue.stringValue.toInt() {
                
                getAppDelegate()?.setFrameDirection(UInt32(newW), h: UInt32(newH))
            }
        }
        

    }
    
    @IBAction func sendMessage(sender: NSButton) {
        
        if var sendText:String = chatInputView.string {
        
            chatInputView?.textStorage?.beginEditing()
            chatInputView?.textStorage?.setAttributedString( NSAttributedString(string: "") )
            chatInputView.textStorage?.endEditing()
            NSLog(sendText);
            getAppDelegate()?.sendChat(sendText);
        }
    }
    
    @IBAction func changeConferenceName(sender: NSButton) {
        getAppDelegate()?.updateConferenceName(newConfName.stringValue)
    }
    
    @IBAction func changeMaxParticipants(sender: NSStepper) {
        maxParticipantsNumber.stringValue = String(sender.integerValue)
        let current = getAppDelegate()?.getMaxParticipants()
        println("current maxParticipatns:\(current)")
        getAppDelegate()?.setMaxParticipants(sender.integerValue)
    }
    
    @IBAction func toggleSelfView(sender: AnyObject) {
        getAppDelegate()?.renderSelfView(renderSelfView)
        renderSelfView = !renderSelfView
    }
    
    @IBAction func debugMsgSend(sender: NSButton) {
        getAppDelegate()?.debugMsgSend()
    }

    func onAddShareCallback(uri:String)
    {
        if let delegate = getAppDelegate() {
            delegate.addShare(uri);
        }
    }
    
    func onRemoveShareCallback(uri:String)
    {
        if let delegate = getAppDelegate() {
            delegate.removeShare(uri);
        }
    }
    
    func dispatch_main(block:() ->())
    {
       dispatch_async(dispatch_get_main_queue(), block)
    }
    
    func dispatch_background(block:() ->()) { dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), block) }
    
    //handler from V5Sink
    func renderImageView(image:NSImage) {
        dispatch_main {
            self.imageViewField.image = image;
            self.imageViewField.setNeedsDisplay()
        }
    }
    
    func renderShareDesktopView(image:NSImage) {
        dispatch_main {
            self.desktopShareViewField.image = image
            self.desktopShareViewField.setNeedsDisplay()
        }
    }
    
    func onJoinedConference(info:ObjcConferenceInfo)
    {
        
        info.participantList.iterate { self.myParticipantList.append($0) }
        if let list = participantListController {
            list.content = myParticipantList;
        }
        onConferenceStatusUpdated(info.conferenceStatus)
        imageViewField.alphaValue = 1.0;
        
    }
    
    func onLeavedConference( errInfo:ObjcErrorInfo!) {
        
         dispatch_main {
            if errInfo.reason != 200 {
                var alert:NSAlert = NSAlert()
                alert.messageText = "onLeavedConference \n Category :" + errInfo.category.value.description + "\nReason: " + errInfo.reason.description
                alert.runModal()
            }
        }
    }
    
    func onJoinConferenceFailed( errInfo:ObjcErrorInfo ) {
         dispatch_main {
            var alert:NSAlert = NSAlert()
            alert.messageText = "onJoinConferenceFailed \n Category :" + errInfo.category.value.description + "\nReason: " + errInfo.reason.description
        
            alert.runModal()
        }
    }

    func onBeginSessionFailed(errInfo:ObjcErrorInfo)  {
        dispatch_main {
            var alert:NSAlert = NSAlert()
            alert.messageText = "onBeginSessionFailed \n Category :" + errInfo.category.value.description + "\nReason: " + errInfo.reason.description
            
            alert.runModal()
        }
    }

    func onSessionLinked(displayName:String) {
        
        if let delegate = getAppDelegate() {
            delegate.joinConference(userNameField.stringValue, roomPassword: nil)
        }
    }
    
    func onParticipantListUpdated(participantList: ObjcParticipantArray!) {
        dispatch_main {
            
            self.myParticipantList = [];
            participantList.iterate { self.myParticipantList.append($0) }
            if let list = self.participantListController {

                list.content = self.myParticipantList;
            }
        }
    }
    
    func onParticipantJoined(participant: ObjcParticipant) {}
    func onParticipantLeaved(participant: ObjcParticipant) {}
    
    
    func onReceiveJoinConferenceRequest(token:String, V5LiteURL:String){
        if let delegate = getAppDelegate() {
            delegate.beginSession(V5LiteURL, token:token)
        }
    }

    
    func onChatMessageAdded(message:ObjcMessage) {
        dispatch_main{
            self.ChatTextView?.textStorage?.beginEditing()
            var aLog = "----\n>" + message.displayName + "\n" + message.text + "\n";
            var attrString = NSAttributedString(string: aLog)
            self.ChatTextView?.textStorage?.appendAttributedString(attrString)
            self.ChatTextView?.textStorage?.endEditing()
        }
    }
    
    func onChatLogUpdated(messages:NSMutableArray, length:Int64) {
        
         dispatch_main{
            self.ChatTextView?.textStorage?.beginEditing()
            self.ChatTextView?.textStorage?.setAttributedString( NSAttributedString(string: "") )

            var attrString:NSAttributedString;
            var aLog:String = "";
            for var cnt = Int64(0); cnt < length; cnt++ {
                if let message:ObjcMessage = messages.objectAtIndex(Int(cnt)) as? ObjcMessage {
                    aLog += "----\n>" + message.displayName + "\n" + message.text + "\n"
                }
            }
            attrString = NSAttributedString(string: aLog)
            self.ChatTextView?.textStorage?.appendAttributedString(attrString)
            self.ChatTextView?.textStorage?.endEditing()
        }
        
    }
    
    func onConferenceStatusUpdated(status:ObjcConferenceStatus) {
    
        
        var confState:String = "Conference Name : " + status.conferenceName + "\n"
        confState += "Room Name : " + status.roomName + "\n"
        confState += "Conference URL : " + status.conferenceURL + "\n"
        confState += "Conference PinCode : " + status.conferencePinCode + "\n"
        confState += ( "Conference　Reserved　: " + (status.isReserved ? "YES\n":"NO\n"))
        confState += ( "Conference　Locked　: " + (status.isLocked ? "YES\n":"NO\n"))
        
        
        dispatch_main{
            
            self.statusInfoView?.textStorage?.beginEditing()
            self.statusInfoView?.textStorage?.setAttributedString( NSAttributedString(string: "") )
            
            var attrString:NSAttributedString;
            attrString = NSAttributedString(string: confState)
            self.statusInfoView?.textStorage?.appendAttributedString(attrString)
            self.statusInfoView?.textStorage?.endEditing()
        }
        
        NSLog(confState)
    }
    
    override func awakeFromNib(){
        super.awakeFromNib()
        if let delegate = getAppDelegate() {
            window.addChildWindow(subWindow, ordered: NSWindowOrderingMode.Above)
            
             let userDefault = NSUserDefaults.standardUserDefaults()
            
            if let url = userDefault.objectForKey("url.stringValue") as? String {
                self.url.stringValue = url
            }
            if let id = userDefault.objectForKey("idField.stringValue") as? String {
                idField.stringValue =  id
            }
            if let name = userDefault.objectForKey("userNameField.stringValue") as? String {
                userNameField.stringValue =  name
            }
            if let pw = userDefault.objectForKey("pwField.stringValue") as? String {
                pwField.stringValue =  pw
            }
            if let index = userDefault.objectForKey("roomSelectButton.selectItemAtIndex") as? Int {
                roomSelectButton.selectItemAtIndex(index)
            }
            
            
            participantListController.automaticallyRearrangesObjects = true;
            delegate.conferenceEventSink.viewController = self
            let width = UInt32(imageViewField.bounds.size.width);
            let height = UInt32(imageViewField.bounds.size.height);
            let rect = V5Rect(xPos: 0, yPos: 0, width: width, height:height);
            delegate.V5Init(imageViewField, rect:rect, viewContoller: self);
            let windows = delegate.getWindowsForSharing()
            var windowList = Array<String>()
            windows.iterate { win in
                windowList.append(win.appWindowName)
            }
            sharingWindowListButton.addItemsWithTitles(windowList)
        
            maxParticipantsStepper.minValue = 0
            maxParticipantsStepper.maxValue = 16
            maxParticipantsStepper.integerValue = 4
            maxParticipantsNumber.stringValue = "4"
        
            creteMenu( (delegate.getCameraList(),cameraListButton) )
            creteMenu( (delegate.getMicrophoneList(),micListButton) )
            creteMenu( (delegate.getSpeakerList(),speakerListButton) )
            
            window.addChildWindow(subWindow!, ordered: NSWindowOrderingMode.Above)
             infoWindow?.close()
        }
    }
    
    //デバイスリストの更新
    private func creteMenu( value:(V5DevList!,NSPopUpButton!) ) { 
        if let list = value.0 { if let selector = value.1 {
            
            selector.removeAllItems()
            let menu = NSMenu()
            
            for deviceName in list.name {
                let item = NSMenuItem()
                if let aName = deviceName as? String {
                    item.title = aName
                    menu.addItem(item)
                }
            }
            
            selector.menu = menu
            setSelectorIndex( ( selector, Int(list.index) ) )
            }}
    }
    private func setSelectorIndex( value:(NSPopUpButton!,Int) ){
        if let target = value.0 {  
            target.selectItemAtIndex(value.1)
        }
    }
    private var statistics:String = ""
    func renderStatistics(conferenceStatisticsAllInfo: ObjcConferenceStatisticsAllInfo!) {
        statistics = "";
        if let info = conferenceStatisticsAllInfo {
            
            
            statistics += ("VM Connected : \(info.communicationInfo.vmCommunicationStatus) / Use V-Proxy : \(info.communicationInfo.vmCommunicationViaWebProxy)  / Use Web Proxy : \(info.communicationInfo.vmCommunicationViaWebProxy)\n"  );
            statistics += ("VR Connected : \(info.communicationInfo.vrCommunicationStatus) / Use V-Proxy : \(info.communicationInfo.vrCommunicationViaVidyoProxy)  / Use Web Proxy : \(info.communicationInfo.vrCommunicationViaWebProxy)\n"  );
            
            statistics += ("endpointStatus \(info.endpointStatus.endPointStatus)\n");
            
            statistics += ("Audio : UP(Actual/Capacity) \(info.bandwidthInfo.actualSendBwAudio)/\(info.bandwidthInfo.availSendBwAudio) DOWN(Actual/Capacity) \(info.bandwidthInfo.actualRecvBwAudio)/\(info.bandwidthInfo.availRecvBwAudio)\n");
            
            statistics += ("Video : UP(Actual/Capacity) \(info.bandwidthInfo.actualSendBwVideo)/\(info.bandwidthInfo.availSendBwVideo) DOWN(Actual/Capacity) \(info.bandwidthInfo.actualRecvBwVideo)/ \(info.bandwidthInfo.availRecvBwVideo)\n");
            
            statistics += ("Sharing : UP(Actual/Capacity) \(info.bandwidthInfo.actualSendBwApplication)/\(info.bandwidthInfo.availSendBwApplication) DOWN(Actual/Capacity) \(info.bandwidthInfo.actualRecvBwApplication)/\(info.bandwidthInfo.availRecvBwApplication)\n");
            
            statistics += ("All : UP(Actual/Capacity) \(info.bandwidthInfo.actualSendBwMax)/\(info.bandwidthInfo.availSendBwMax) DOWN(Actual/Capacity) \(info.bandwidthInfo.actualRecvBwMax)/\(info.bandwidthInfo.availRecvBwMax)\n");
            
            statistics += ("[conferenceInfo] Rec: \(info.conferenceInfo.recording) / WebCast: \(info.conferenceInfo.webcast)\n" );
            statistics += ("VM info : \(info.connectivity.serverAddress):\(info.connectivity.serverPort) id( \(info.connectivity.vmIdentity) ) Enctipted(\(info.connectivity.serverSecured))\n");
            statistics += ("Proxy info : \(info.connectivity.vidyoProxyAddress):\(info.connectivity.vidyoProxyPort) revers( \(info.connectivity.reverseProxyAddress):\(info.connectivity.reverseProxyPort) ) type:\(info.connectivity.proxyType)\n");
            statistics += ("Portal info : \(info.connectivity.portalAddress) version( \(info.connectivity.portalVersion) )\n");
            statistics += ("External IP : \(info.connectivity.clientExternalIPAddress) location : \(info.connectivity.locationTag)\n");
            
            
            statistics += ("Total IFrames : \(info.mediaInfo.numIFrames) :: Total FIR : \(info.mediaInfo.numFirs) :: Total NACKs : \(info.mediaInfo.numNacks) :: Media Round trip time \(info.mediaInfo.mediaRTT)\n")
            
            statistics += ("VideoNomal Delay : \(info.rateShaperInfo.delayVideo)ms : Current queue: \(info.rateShaperInfo.numQueuedVideoPackets) / Total Drop: \(info.rateShaperInfo.numDroppedVideoPackets)\n")
            statistics += ("VideoHight Delay : \(info.rateShaperInfo.delayVideoHigh)ms : Current queue: \(info.rateShaperInfo.numQueuedVideoHighPackets)/ Total Drop: \(info.rateShaperInfo.numDroppedVideoHighPackets)\n")
            statistics += ("Sharing Nomal Delay : \(info.rateShaperInfo.delayApp)ms : Current queue: \(info.rateShaperInfo.numQueuedAppPackets)/ Total Drop: \(info.rateShaperInfo.numDroppedAppPackets)\n")
            statistics += ("Sharing High Delay : \(info.rateShaperInfo.delayAppHigh)ms : Current queue: \(info.rateShaperInfo.numQueuedAppHighPackets)// Total Drop: \(info.rateShaperInfo.numDroppedAppHighPackets) \n")
            
            statistics += ("Caputure FPS: \(info.frameRateInfo.captureFrameRate)fps / encode FPS: \(info.frameRateInfo.encodeFrameRate) / send FPS: \(info.frameRateInfo.sendFrameRate)\n")
            
            info.participantInfoCollection.iterate{ participantData in
                
                self.statistics += ("\(participantData.name) : \(participantData.receivedWidth)x\(participantData.receivedHeight) (\(participantData.receivedFrameRate)/\(participantData.decodedFrameRate)/\(participantData.displayedFrameRate) )fps reciveVideo \(participantData.receivedBytesVideo) reciveAudio \(participantData.receivedBytesAudio)\n" )
                return;
            }
            
            NSLog(statistics);
            dispatch_main{
                self.infoTextArea.string = self.statistics
            }
        }
    
    }
    
    @IBAction func selectCamera(sender: NSPopUpButton) { getAppDelegate()?.setCurrentCamera(sender.indexOfSelectedItem) }
    @IBAction func selectMic(sender: NSPopUpButton) {         
        getAppDelegate()?.setCurrentMicrophone(sender.indexOfSelectedItem)
    }
    @IBAction func selectSpeaker(sender: NSPopUpButton) {         
        getAppDelegate()?.setCurrentSpeaker(sender.indexOfSelectedItem)
    }
    @IBOutlet private weak var inviteMail: NSTextField!
    @IBAction func sendEmail( sender: AnyObject! ) {
        let origin = inviteMail.stringValue
        let address:[String] = origin.componentsSeparatedByString(",")
        getAppDelegate()?.sendInviteMail(address)
    }
    
    @IBAction func changePinPView(sender: AnyObject!) {
        getAppDelegate()?.changeSelfPreviewMode(V5_CLIENT_PREVIEW_MODE_PIP)
    }
    @IBAction func changeDocView(sender: AnyObject!) {
        getAppDelegate()?.changeSelfPreviewMode(V5_CLIENT_PREVIEW_MODE_DOCK)
    }

    @IBAction func changeNodeView(sender: AnyObject!) {
        getAppDelegate()?.changeSelfPreviewMode(V5_CLIENT_PREVIEW_MODE_NONE)
    }
    @IBAction func setSelfDisplayLabel( sender: AnyObject! ) {
        getAppDelegate()?.setSelfDisplayLabel(userNameField.stringValue)
    }
    
    var StatisticsInfoFlg:Bool = false;
    @IBAction func confStatistics(sender: NSButton) {
        
        if(StatisticsInfoFlg == false)
        {
            var waittime = 50000 as Int
            getAppDelegate()?.startStatisticsInfo()
            StatisticsInfoFlg = true
            infoWindow!.makeKeyAndOrderFront(self)
        }
        else
        {
            getAppDelegate()?.stopStatisticsInfo();
            StatisticsInfoFlg = false
            infoWindow!.close()
        }
        
    }


}