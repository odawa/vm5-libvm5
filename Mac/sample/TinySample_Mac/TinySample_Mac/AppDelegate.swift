//
//  AppDelegate.swift
//  TinySample_Mac
//
//  Created by Hiromi on 2015/02/13.
//  Copyright (c) 2015年 Hiromi. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate, NSWindowDelegate, VRMSLoginDelegate {

    @IBOutlet weak var window: NSWindow!


    func applicationDidFinishLaunching(aNotification: NSNotification) {
        // Insert code here to initialize your application
    }

    func applicationWillTerminate(aNotification: NSNotification) {
        // Insert code here to tear down your application
    }

// MARK: - LibV5Lite methods
    
    let lib:LibV5Lite = LibV5Lite()
    let login : VRMSLogin = VRMSLogin()
    let conferenceEventSink = V5ConferenceEventSinkImpl()
    let chatEventSink = V5ChatEventSinkImpl()
    let deviceEventSink = V5DeviceEventSinkImpl()
//    var roomIndex = 0
    var cameraIndex:Int32 = 1
    var userName:String?
    var url:String?
    
    func V5Init(view:NSView?, rect:V5Rect?,viewContoller:MainMenuViewController?) {
        
        var rectVariable = V5Rect(xPos:0, yPos:0, width:0, height:0)
        if let r = rect {
            rectVariable = r
        }
        
//        let logPrio = "warning info@AppGui info@App info@AppEmcpClient info@LmiApp";
        let logPrio = "warning info@AppGui info@App info@AppEmcpClient info@LmiApp";

        let fontpath = "";
        
        let cetifpath = "";
        
        var documentDirectory = ""
        LibV5Lite.libInit(documentDirectory, logPriority: logPrio);
        
        // VIDYO PROXY 使用設定
        lib.setProxyForce(true)
//        lib.setProxyForce(false)
        
        // WebProxy使用設定(手動でのアドレス、ポート、ID、パスワード設定)
        //lib.setProxySetting(String("https://172.16.100.1"), port: 8081, id: String("proxy"), password: String("proxy"))

        // VIDYOライブラリのプロキシ情報取得先フラグ設定
        //lib.setProxyIEOption( V5_PROXY_VIDYO_NO_SETTING );
        //lib.setProxyIEOption( V5_PROXY_VIDYO_IE );
        //lib.setProxyIEOption( V5_PROXY_VIDYO_IE_MANUAL );
        //lib.setProxyIEOption( V5_PROXY_VIDYO_IE_SCRIPT );
        //lib.setProxyIEOption( V5_PROXY_VISYO_IE_AUTO_DETECT );
        
        if var renderView = view {
            lib.startLogic(
                renderView,
                videoRect:&rectVariable,
                fontPath:"TinySample_Mac.app/Contents/Resources/System.vyf",
                certificatePath:"TinySample_Mac.app/Contents/Resources/");
        }else {
            lib.startLogic(nil, videoRect:nil, fontPath:"", certificatePath:"" /*&rect*/);
        }
        lib.startHTTPD(21920);
        
        conferenceEventSink.viewController = viewContoller;
        chatEventSink.viewController = viewContoller;
        lib.setConferenceEventSink(conferenceEventSink)
        lib.setChatEventSink(chatEventSink)
        lib.setDeviceEventSink(deviceEventSink, lib: lib)
//        lib.setSelfViewLoopbackPolicy(V5_CLIENT_LOOPBACK_POLICY_DISABLE)
        lib.setSelfViewLoopbackPolicy(V5_CLIENT_LOOPBACK_POLICY_ENABLE)
        
        //println(lib.getDeviceList(V5DeviceType(2)))
        //lib.selectDevice(V5DeviceType(3), index: Int32(0))
        //lib.selectDevice(V5DeviceType(2), index: Int32(0))
        //lib.selectDevice(V5DeviceType(1), index: Int32(0))
    }
    
    func deviceList() {
    }
    
    func login(#url:String, id:String, password:String, usePin:Bool, pinCode:String, userName:String,roomIndex:Int) {
        
        
        self.url = url
        self.userName = userName
        
//        while true {
//            if lib.userClientReady() {
//                break;
//            }
//            sleep(10000)
//        }
        
        if(usePin) {
            login.pinLogin(url, pinCode: pinCode, delegate: self)
        } else {
            login.login(url, id:id, pw:password, index:roomIndex, delegate:self)
        }
        
    }
    func windowDidChangeBackingProperties(notification: NSNotification) {

        self.setFrameDirection( self.vW,h:self.vH )
        dispatch_async(dispatch_get_main_queue()) {
            NSLog("resorution change");
            self.setFrameDirection( self.vW,h:self.vH )
            return;
        };
        

    }
    func dispatch_background(block:() ->()) { dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), block) }
    func getCameraList() -> V5DevList!
    {
        return lib.getDeviceList(V5_DEVICE_TYPE_VIDEO)
    }
    
    func setCurrentCamera( index: Int ) -> Void 
    {
        dispatch_background{ self.lib.selectDevice( V5_DEVICE_TYPE_VIDEO, index: Int32(index) ) }
    }
    
    func getSpeakerList() -> V5DevList!
    {
        return lib.getDeviceList( V5_DEVICE_TYPE_AUDIO_OUT )
    }
    
    func setCurrentSpeaker( index: Int ) -> Void 
    {
        dispatch_background{ self.lib.selectDevice( V5_DEVICE_TYPE_AUDIO_OUT, index: Int32(index) ) }
    }
    
    func getMicrophoneList() -> V5DevList!
    {
        return lib.getDeviceList(V5_DEVICE_TYPE_AUDIO_IN)
    }
    func setCurrentMicrophone( index: Int ) -> Void 
    {
        dispatch_background { self.lib.selectDevice( V5_DEVICE_TYPE_AUDIO_IN, index: Int32(index) ) }
    }
    
    
    func startWatchSharedRawFrame(uri:String)
    {
        lib.startWatchSharedRawFrame(uri);
    }
    func changeToActiveSpeakerLayout( isUseActiveSpeakerLayout:Bool )
    {
        lib.enableActiveSpeakerLayout(isUseActiveSpeakerLayout);
    }
    func stopWatchSharedRawFrame(uri:String)
    {
        
    }
    func addShare(uri:String)
    {
        lib.startWatchShared(uri);
    }
    
    func removeShare(uri:String)
    {
        lib.stopWatchShared(uri)
    }
    
    func startShareWindow(windowId:V5WindowCapturerWindowId, width:Int, height:Int)
    {
        lib.shareAppWindow(windowId, width: Int32(width), height: Int32(height))
    }
    
    func startShareDesktop(desktopId:uint, width:Int, height:Int)
    {
        lib.shareSysDesktop(desktopId, width: Int32(width), height: Int32(height))
    }
    
    func stopShare()
    {
        lib.unshare()
    }
    func sendInviteMail(mailAddress:[String]!) {
        lib.sendInviteMail(mailAddress)
    }
    func speakerMuteToggle(muted:Bool){
        lib.muteSpeaker(muted);
    }
    var vW:UInt32 = 400;
    var vH:UInt32 = 400;
    func setFrameDirection(w:UInt32, h:UInt32){
        vW = w
        vH = h;
        lib.setFrameDirection(0, y: 0, w: w, h: h)
    }
    
    func microphoneMuteToggle(muted:Bool){
        lib.muteMicrophone(muted);
    }
    func cameraMuteToggle(muted:Bool){
        lib.muteCamera(muted);
    }
    func lockConference( lock:Bool )
    {
        lib.lockConference(lock);
    }
    func rejectParticipant(rejectPID: String!){
        lib.rejectParticipant(rejectPID)
    }
    func sendChat( message:String! ){
        lib.sendChatMessage(message)
    }
    func logout()
    {
        lib.logout()
    }
    
    func getWindowsForSharing() -> ObjcWindowArray
    {
        let wad:ObjcWindowsAndDesktops = lib.getWindowsAndDesktops()
        return wad.windowList
    }
    func updateConferenceName(newName:String!){
        lib.updateConferenceName(newName)
    }
    func windowWillClose(notification: NSNotification) {

        lib.logout()
        // libFinal を呼ぶためにはログアウトが完了してから一回 UI スレッドを動かす必要があるらしい
        dispatch_async(dispatch_get_main_queue()) {
            self.lib.stopLogic();
            LibV5Lite.libFinal();
            return;
        };
    }
    func windowShouldClose(sender: AnyObject) -> Bool {
        
        return true;
    }
    
    func setMaxParticipants(max:Int) {
        lib.setMaxParticipants(Int32(max))
    }
    
    func getMaxParticipants() -> Int {
        return Int(lib.getMaxParticipants())
    }
    
    func renderSelfView(render:Bool) {
        lib.renderSelfView(render)
    }
    
    func startStatisticsInfoTime(millisecs:Int?) {
        // なんかコンパイルとおらないので、コメントアウト
        //lib.startStatisticsInfo(millisecs)
    }
    
    func startStatisticsInfo() {
        lib.startStatisticsInfo()
    }
    
    func stopStatisticsInfo() {
        lib.stopStatisticsInfo()
    }

    
    func beginSession(url:String, token:String)
    {
        var result:V5CODE = lib.beginSession(url + "/api/v5lite/client/", token: token, version: "5.1.0.0");
    }
    
    func joinConference(userName:String?, roomPassword:String?)
    {
        var passInfo:ObjcPasswdInfo? = nil;
        if (roomPassword != nil)
        {
            passInfo = ObjcPasswdInfo();
            passInfo!.passwd = roomPassword;
        
        }
        var result:V5CODE = lib.joinConference(userName, roomPasswdInfo: passInfo)
    }
    
    func changeSelfPreviewMode( previewMode:V5ClientPreviewMode ) {
        lib.changeSelfPreviewMode(previewMode)
    }
    
    func setSelfDisplayLabel( aLavel:String ) {
        lib.setSelfDisplayLabel(aLavel)
    }

    func setSendMaxKbps(kbps:Int32)
    {
        lib.setSendMaxKbps(kbps)
    }
    
    func setRecvMaxKbps(kbps:Int32)
    {
        lib.setRecvMaxKbps(kbps)
    }
    
    func getSendMaxKbps() -> Int32
    {
        var kbps :Int32 = lib.getSendMaxKbps()

        return kbps
    }
    
    func getRecvMaxKbps() -> Int32
    {
        var kbps :Int32 = lib.getRecvMaxKbps()
        return kbps
    }
    
    func setSendBandWidth(layer:UInt32 , sendBandWidth:Int32 )
    {
        lib.setSendBandWidth(layer, sendBandWidth: sendBandWidth)
    }
    
// MARK: - VRMSLoginDelegate

    func onGetToken(token:String) {
        let pw = "";
        var result:V5CODE = lib.beginSession(
            "https://" + url! + "/api/v5lite/client/",
            token: token,
            version: "5.1.0.0"
        );
    }

    func debugMsgSend()
    {
        lib.SendDebug()
    }
    
}

