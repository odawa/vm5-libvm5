//
//  VRMSLoginDelegate.swift
//  V5LiteSample
//
//  Created by shintaro on H27/02/11.
//  Copyright (c) 平成27年 V-cube. All rights reserved.
//

import Foundation
protocol VRMSLoginDelegate {
    func onGetToken(token:String)
}