//
//  VRMS4Login.swift
//  V5LiteSample
//
//  Created by shintaro on H27/02/11.
//  Copyright (c) 平成27年 V-cube. All rights reserved.
//

import Foundation


class VRMSLogin: NSObject,NSXMLParserDelegate {
 
    let net = Net();
    var _ParceKey: String = ""
    var session : String = ""
    var url:String = ""
    var roomIndex:Int = 0;
    var _delegate:VRMSLoginDelegate?;
    
    func login( baseUrl:String,id:String,pw:String,index:Int,delegate:VRMSLoginDelegate)
    {
       _delegate = delegate;
        roomIndex = index;
        url = "https://" + baseUrl + "/api/v5lite/user/"
        let params = ["action_login": 1, "id": id, "pw": pw,"output_type":"json"]
        net.GET(url, params: params, successHandler: { responseData in
            let result = responseData.json(error: nil)
                if var data = result["data"] as? NSDictionary {
                    if let _session = data["session"] as? String {
                        self.session = _session
                            self.getRoom()
                    }
            }
            
            
            }, failureHandler: { error in
                NSLog("Error")
        })
    }
    func pinLogin( baseUrl:String,pinCode:String,delegate:VRMSLoginDelegate )
    {
        _delegate = delegate;
        url = "https://" + baseUrl + "/api/v5lite/user/"
        let params = ["action_pin_login": 1, "pin_cd": pinCode, "output_type":"json"]
        net.GET(url, params: params, successHandler: { repsonceData in
            let result = repsonceData.json(error: nil)

            NSLog("\(result)");
            if var data = result["data"] as? NSDictionary {
                if let _session = data["session"] as? String {
                self.session = _session
                    if let _roomId = data["room_id"] as? String {
                        var roomId = _roomId
                        self.statConf(roomId);
                    }}}
            
        }, failureHandler: { error in
                NSLog("Error")
        })
        
    }
    func getRoom()
    {
//        NSLog(self.session)
        let paramsForRoom = ["action_get_room_list": 1, "n2my_session": session,"output_type":"json"]
        net.GET(url, params: paramsForRoom, successHandler: { roomData in
            let result = roomData.json(error: nil)
            if var data = result["data"] as? NSDictionary {
                
                if var rooms = data["rooms"] as? NSDictionary {
                    
                    if var room = rooms["room"] as? NSArray {
                        var i = 0;
                        if( room.count > self.roomIndex )
                        {
                            i = self.roomIndex;
                        }
                    
                        if var info = room[i] as? NSDictionary {
                    
                            if var room_info = info["room_info"] as? NSDictionary {
                                if var room_id = room_info["room_id"] as? String {
                                    self.statConf(room_id)
                                }

                            }
                        }
                    }

                }                
            }
            }, failureHandler: { error in
                NSLog("Error")
        })
    }
    func statConf(roomid:String)
    {
        let paramsForToken = ["action_start": 1, "n2my_session": session,"room_id":roomid]
        net.GET(url+"meeting/", params: paramsForToken, successHandler: { tokenData in
            let result = tokenData.json(error: nil)
            
            if var data = result["data"] as? NSDictionary { if var token = data["token"] as? String {
           
                self._delegate!.onGetToken( token ) }}

            }, failureHandler: { error in
                NSLog("Error")
        })
        
        
    }
}