//
//  ResponseSerialization.swift
//  Net
//
//  Created by Le Van Nghia on 7/31/14.
//  Copyright (c) 2014 Le Van Nghia. All rights reserved.
//

import Cocoa
//import UIKit

class ResponseData
{
    var urlResponse : NSURLResponse
    var data: NSData

    init(response: NSURLResponse, data: NSData) {
        self.urlResponse = response
        self.data = data
    }

    /**
    *  parse json with urlResponse
    *
    *  @param NSErrorPointer
    *
    *  @return json dictionary
    */
    func json(error: NSErrorPointer = nil) -> NSDictionary {
        if let httpResponse = urlResponse as? NSHTTPURLResponse {
            if httpResponse.statusCode == 200 {
                if let jsonData = NSJSONSerialization.JSONObjectWithData(data, options: .AllowFragments, error: error) as? NSDictionary {
                    return jsonData
                }
                else { return ["data":"nil"] }
            }
            else if error != nil {
                error.memory = NSError(domain: "HTTP_ERROR_CODE", code: httpResponse.statusCode, userInfo: nil)
            }
        }
        return ["data":"nil"]
    }

    /**
    *  convert urlResponse to image
    *
    *  @return UIImage
    */
    func image(error: NSErrorPointer?) -> NSImage? {
        if let httpResponse = urlResponse as? NSHTTPURLResponse {
            if httpResponse.statusCode == 200 && data.length > 0 {
                return NSImage(data: data)
                //return UIImage(data: data)
            }
            else if error != nil {
                error!.memory = NSError(domain: "HTTP_ERROR_CODE", code: httpResponse.statusCode, userInfo: nil)
            }
        }
        return nil
    }

    /**
    *  parse xml
    *
    *  @param NSXMLParserDelegate
    *
    *  @return
    */
    func parseXml(delegate: NSXMLParserDelegate, error: NSErrorPointer = nil) -> Bool {
        if let httpResponse = urlResponse as? NSHTTPURLResponse {
            if httpResponse.statusCode == 200 {
                let xmlParser = NSXMLParser(data: data)
                xmlParser.delegate = delegate
                xmlParser.parse()
                return true
            }
            else if error != nil {
                error.memory = NSError(domain: "HTTP_ERROR_CODE", code: httpResponse.statusCode, userInfo: nil)
            }
        }
        return false
    }
}
