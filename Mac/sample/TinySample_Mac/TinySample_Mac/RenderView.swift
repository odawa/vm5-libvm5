//
//  RenderView.swift
//  TinySample_Mac
//
//  Created by Hiromi on 2015/02/16.
//  Copyright (c) 2015年 Hiromi. All rights reserved.
//

import Cocoa

class RenderView : NSView
{
    var imageView:NSImageView?
    
    override init(frame frameRect: NSRect) {
        super.init(frame: frameRect)
        imageView = NSImageView(frame: frameRect)
    }

    required init?(coder: NSCoder) {
        super.init(coder:coder)
    }
    
    func setImageViewFrame(frame:NSRect) {
        if let view = imageView {
            view.frame = frame
        }else {
            imageView = NSImageView(frame: frame)
        }
        self.addSubview(imageView!)
    }
    
    func draw(image:NSImage) {
        imageView!.image = image
        if imageView!.image == nil {
            println("imageView image is null")
        }
    }
    
}
