//
// Created by Hiromi on 2015/01/26.
// Copyright (c) 2015 V-cube. All rights reserved.
//

import Cocoa

@objc class V5ConferenceEventSinkImpl : NSObject, IV5ConferenceEventSinkProtocol {

    
    /*

    -(void)onVersionUnmatched:(NSString *) requireVersion;
    -(void)onSessionLinked:(NSString *) displayName;
    -(void)onParticipantJoined:(ObjcParticipant *) participant;
    -(void)onParticipantLeaved:(ObjcParticipant *) participant;
    -(void)onReceiveJoinConferenceRequest:(NSString*)token v5LiteURL:(NSString*)V5LiteURL;
    -(void)onJoinConferenceFailed:(ObjcErrorInfo *) errInfo;
    -(void)onReceiveVideoFrame:(ObjcVideoRawFrame*) vieoRawFrame;
    -(void)onJoinedConference:(ObjcConferenceInfo *) conferenceInfo;
    -(void)onLeavedConference:(ObjcErrorInfo *) errInfo;
    -(void)onConferenceStatusUpdated:(ObjcConferenceStatus *) conferenceStatus;
    -(void)onRemoteSourceAdded:(ObjcRemoteSourceChanged*) added;        //VidyoClientAPI 3.3.0.00200 以降
    -(void)onRemoteSourceRemoved:(ObjcRemoteSourceChanged*) removed;    //VidyoClientAPI 3.3.0.00200 以降
    -(void)onParticipantListUpdated:(ObjcParticipantArray *)participantList;
    -(void)onSelectedParticipantsUpdated:(int)numberOfSelected selectedURI:(NSArray *)selectedURI;
    -(void)onReconnectConference:(ObjcConferenceInfo *) conferenceInfo;
    -(void)onAddShare:(NSString *)uri;
    -(void)onRemoveShare:(NSString *)uri;
    -(void)onRcvRejectParticipant:(ObjcParticipant *) participant;
    -(void)onAsyncError:(ObjcErrorInfo *) errInfo;
    -(void)onInviteMailIsSent:(ObjcErrorInfo*) errInfo status:(NSString *)status mailAddress:(NSString *)mailAddress;
    -(void)onPastTimeStart;
    -(void)onPastTimeStop;

    */
    var viewController:MainMenuViewController?
    
    override init() {
    }
    
    func onParticipantJoined(participant: ObjcParticipant) {
        viewController?.onParticipantJoined(participant);

    }
    
    
    func onParticipantLeaved(participant: ObjcParticipant) {
        viewController?.onParticipantLeaved(participant);
    }
    
    
    func onReceiveJoinConferenceRequest(token: String, v5LiteURL V5LiteURL: String){
        viewController?.onReceiveJoinConferenceRequest(token, V5LiteURL: V5LiteURL);
        // Mobile Project don't use this method.
    }

    func onSessionLinked(displayName: String!, logoURL LogoURL: [NSObject : AnyObject]!) {
        viewController?.onSessionLinked(displayName);
    }
//    -(void)onSessionLinked:(NSString *) displayName;
    
    func onBeginSessionFailed( errInfo:ObjcErrorInfo ) {
        viewController?.onBeginSessionFailed(errInfo)
    }
    

    func onJoinedConference(conferenceInfo: ObjcConferenceInfo!) {
        viewController?.onJoinedConference(conferenceInfo);
    }
    
    func onJoinConferenceFailed(errInfo: ObjcErrorInfo!) {
         viewController?.onJoinConferenceFailed(errInfo)
    }
    func onLeavedConference( errInfo:ObjcErrorInfo!) {
        viewController?.onLeavedConference(errInfo)
    }
    
    
    var writeFlag = false
    
    var writeCount = 0
    
    private func writeFileTest(#image:NSImage?, path:String) {
        let cgRef = image?.CGImageForProposedRect(nil, context: nil, hints: nil)
        let rep = NSBitmapImageRep(data:image!.TIFFRepresentation!)
        rep!.size = image!.size
        let data = rep!.representationUsingType(NSBitmapImageFileType.NSPNGFileType, properties: [:])
        data?.writeToFile(path, atomically: true)
    }
    
    func onReceiveVideoFrame(videoRawFrame:ObjcVideoRawFrame!) {
/*        if let view = viewController {
            let colorSpace = CGColorSpaceCreateDeviceRGB()
            if colorSpace == nil {
                return
            }
            
            var width = UInt(videoRawFrame.width)
            var height = UInt(videoRawFrame.height)
            var bitsPerComponent = UInt(8);
            var bytesPerRow = UInt(width * 4)
            
            let gtx:CGContextRef = CGBitmapContextCreate(videoRawFrame.data
                ,width
                ,height
                ,bitsPerComponent
                ,bytesPerRow
                ,colorSpace
                ,CGBitmapInfo(CGImageAlphaInfo.PremultipliedFirst.rawValue))
            let toCGImage:CGImageRef = CGBitmapContextCreateImage(gtx)
            let image:NSImage? = NSImage(CGImage:toCGImage, size:NSSize(width:Int(width), height:Int(height)))
//            if writeCount <= 100 {
//                writeCount++
//                writeFileTest(image: image, path: "/tmp/test\(writeCount).png")
//            }
            if videoRawFrame.mediaType == 1 {
                view.renderShareDesktopView(image!)
            }else {
                view.renderImageView(image!)
            }
        }*/
    }
    

    func onRemoteSourceAdded(added: ObjcRemoteSourceChanged!) {
        
    }
    func onRemoteSourceRemoved(removed:ObjcRemoteSourceChanged!) {
        
    }
    
    func onParticipantListUpdated(participantList: ObjcParticipantArray!) {
        viewController?.onParticipantListUpdated(participantList);
    }
    
    
    func onConferenceStatusUpdated(conferenceStatus: ObjcConferenceStatus!) {
        println("[CALLBACK] onConferenceStatusUpdated!")
        viewController?.onConferenceStatusUpdated(conferenceStatus)
    }
    
    func onAsyncError(errInfo: ObjcErrorInfo!) {
        println("[CALLBACK] onGuestNoPartmitCommand!")
        println("errInfo->category:\(errInfo.category)")
        println("errInfo->reason:\(errInfo.reason)")
    }
    
    
    func onAddShare(uri: String!, isSelfShare:Bool) {
        viewController!.onAddShareCallback(uri)
    }
    
    
    func onRemoveShare(uri: String!, isSelfShare:Bool) {
        viewController!.onRemoveShareCallback(uri)
    }
    
    

    func onSelectedParticipantsUpdated(numberOfSelected: Int32, selectedURI: [AnyObject]!) {
        
    }

    func onReconnectConference(conferenceInfo: ObjcConferenceInfo!) {
        println("[CALLBACK] onReconnectConference!")
    }
   
    func onRcvRejectParticipant(participant: ObjcParticipant!) {
        println("[CALLBACK] onRcvRejectParticipant!")
        println("displayName:\(participant.displayName)")
    } 
    func onPastTimeStart() {
        println("[CALLBACK] onPastTimeStart!")
        
    }
    func onPastTimeStop() {
        println("[CALLBACK] onPastTimeStop!")
    }
    func onInviteMailIsSent(errInfo: ObjcErrorInfo!, status: String!, mailAddress: String!) {
        
    }
    func onVersionUnmatched(requireVersion:String!) {
        
    }
    
    func onConferenceNameChangeFailed(nowConferenceName: String!) {
        println("[CALLBACK] onConferenceNameChangeFailed!")
        println("nowConferenceName : \(nowConferenceName)")
    }
    func onFloatingWindow(view: NSObject!) {
        NSLog("onFloatingWindow >>>")
        NSLog("\((view as? NSView)?.window?.title )")
    }
    
    func onNowMaintenance(maintenanceStartTime: Int64, maintenanceEndTime: Int64) {
        println("[CALLBACK] onNowMaintenance!")
        println("maintenanceStartTime : \(maintenanceStartTime)")
        println("maintenanceEndTime   : \(maintenanceEndTime)")
    }
    
    func onConferenceStatisticsNotice(conferenceStatisticsAllInfo: ObjcConferenceStatisticsAllInfo!) {
        viewController?.renderStatistics(conferenceStatisticsAllInfo)
    }
    
    func onConferenceEndTimeOverNotice(minutes: Int) {
        println("[CALLBACK] onConferenceEndTimeOverNotice in swift")

    }
    
    func onLibFatalError(errInfo: ObjcErrorInfo!) {
        println("[CALLBACK] onLibFatalError!")
        println("errInfo->category:\(errInfo.category)")
        println("errInfo->reason:\(errInfo.reason)")
    }
    
}


