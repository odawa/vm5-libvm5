﻿//#include "stdafx.h"

#include "precompile.h"

#include "V5LibTimer.h"
#include <thread>
#include <atomic>

///////////////////////////////////////////////////////////////////////
// TimerService
///////////////////////////////////////////////////////////////////////

class TimerService
: public boost::asio::io_service
{
public:
    typedef std::function<void()> timer_handler;
    
    TimerService()
    : work_handler(*this)
    , m_strand(*this)
    {
    }
    
    ~TimerService()
    {
        // IO停止処理実施
        stop();
        // IOスレッド処理中判定
        if (io_thread.joinable())
        {
            // IOスレッド処理終了待ち
            io_thread.join();
        }
    }
    
    strand& getStrand()
    {
        return m_strand;
    }
    
    void start()
    {
        if (!io_thread.joinable())
            io_thread = std::thread([this]{ run(); });
    }
    
private:
    work                                    work_handler;
    strand                                  m_strand;
    std::thread                             io_thread;
    
};

///////////////////////////////////////////////////////////////////////
// Timer
///////////////////////////////////////////////////////////////////////

Timer::Timer()
: service(getStaticService())
, deadlineTimer(*service)
{
    // クラスパラメータ初期化メソッドコール
    statusReset();
}

Timer::~Timer()
{
    deadlineTimer.cancel();
}

std::shared_ptr<TimerService> Timer::getStaticService()
{
    static std::shared_ptr<TimerService> s_service = std::make_shared<TimerService>();
    return std::move(std::atomic_load(&s_service));
}

void Timer::start( long millisec, timer_handler cb)
{
    // ループ回数1で起動
    this->start(millisec, cb, 1);
}

void Timer::start( long millisec, timer_handler cb, unsigned int repeat)
{

    // 実行中判定
    if (m_running)
    {
        // 実行中のため処理終了
        return;
    }
    
    // タイマー周期取得
    m_millisec = millisec;
    
    // ループ回数取得
    m_repeatCount = repeat;
    
    // ループ回数カウンタ初期化
    m_currentCount = 0;
    
    // リセットフラグ設定
    m_resetFlg = false;
    
    try
    {
        // IOスレッド実行開始
        service->start();
        
        // タイマー設定
        deadlineTimer.expires_from_now(boost::posix_time::milliseconds(millisec));
        
        // タイマー処理実施
        auto wapped = service->getStrand().wrap([this, cb](const error_code& err){ handler(err, cb); });
        deadlineTimer.async_wait(wapped);
        
        // 実行中フラグ設定
        m_running = true;
    }
    catch (...)
    {
        // 停止処理実施
        stop();
        throw;
    }
    
}

void Timer::stop()
{
    if( !m_running )
    {
        // タイマー非実行中でないため終了処理なし
        return;
    }
    
    // タイマーキャンセル実行
    deadlineTimer.cancel_one();
    
    // 実行中フラグ設定
    m_running = false;
}

void Timer::resetting(long millisec, timer_handler cb)
{
    // 実行回数1回指定でタイマーリセット
    this->resetting(millisec,cb,1);
}

void Timer::resetting(long millisec, timer_handler cb,unsigned int repeat)
{
    if( !m_running )
    {
        // タイマー実行中でないため終了処理なし
        return;
    }
    
    // 一応タイマーストップ
//    deadlineTimer.cancel_one();
    
    // タイマー待ち時間変更
    m_millisec = millisec;
    
    // ループ指定回数再設定
    m_repeatCount = repeat;
    
    // ループ実行回数再設定
    m_currentCount = 0;
    
    // リセットフラグ判定
    m_resetFlg = true;
    
    // タイマー待ち時間再設定
    deadlineTimer.expires_from_now(boost::posix_time::milliseconds(millisec));
    
    auto wapped = service->getStrand().wrap([this, cb](const error_code& err){ handler(err, cb); });
    deadlineTimer.async_wait(wapped);
    
}

void Timer::statusReset()
{
    // 実行中フラグ初期化
    m_running = false;
    
    // タイマー待ち時間初期化
    m_millisec = 1000;
        
    // ループ指定回数初期化
    // 0は無限ループなので、1を初期値にする。
    m_repeatCount = 1;
    
    // ループ実行回数初期化
    m_currentCount = 0;
    
    // リセットフラグ初期化
    m_resetFlg = false;
}


void Timer::handler(error_code const& err, timer_handler const& cb)
{
    // 実行中フラグ判定
    if( !m_running )
    {
        // タイマー処理終了状態なので、
        // 情報を初期化する。
        statusReset();
        return;
    }
    
    // エラー種別判定
    if (err != boost::asio::error::operation_aborted)
    {
        // コールバック起動
        cb();
    }
    
    // リセットフラグ判定
    if( m_resetFlg )
    {
        // リセット処理で設定しているので、なにもしない。
        return;
    }
    
    // ループカウントアップ
    m_currentCount++;
    
    // タイマーリピートカウント判定
    // 条件：
    // ループ指定回数 < ループ実行回数
    // かつ無限ループ(=ループ指定回数0)以外
    if( (m_repeatCount.load() <= m_currentCount.load() ) &&
       (m_repeatCount.load() != 0)  )
    {
        // 指定回数分タイマー処理を実施したので、
        // 次のタイマーを設定せず、各種情報を初期化する。
        statusReset();
        
        return;
    }
    
    try
    {
        // 次のタイマー設定時間計算
        // 現在時間 - タイマー処理開始時間
        auto diffTimePoint = steady_clock::now() - m_startTimePoint.load();
        
        // ミリ秒変換
        long long nextMilliSec = std::chrono::duration_cast<std::chrono::milliseconds>( diffTimePoint ).count();
        
        // 次の"5秒後"までの残りタイマー設定時間を計算
        nextMilliSec = m_millisec - ( nextMilliSec % m_millisec );
        
        // タイマー処理開始時間更新
        m_startTimePoint = steady_clock::now();
        
        // タイマー再設定
        deadlineTimer.expires_from_now(boost::posix_time::milliseconds(nextMilliSec));
        
        // タイマー処理実施
        auto wapped = service->getStrand().wrap([this, cb](const error_code& err){ handler(err, cb); });
        deadlineTimer.async_wait(wapped);
    }
    catch (...)
    {
        // 停止処理実施
        stop();
        throw;
    }

}

