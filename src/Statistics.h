﻿//
//  Statistics.h
//  LibV5Lite
//
//  Created by V-1094 on 2015/01/26.
//  Copyright (c) 2015 V-cube, Inc. All rights reserved.
//

#ifndef __LIBV5_STATISTICS_H
#define __LIBV5_STATISTICS_H

#include <iostream>
#include <Vidyo/VidyoClient.h>
#include <string>
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <IConferenceClientEventSink.h>

/*
 + #VIDYO_CLIENT_REQUEST_GET_COMMUNICATION_STATUS
 + #VIDYO_CLIENT_REQUEST_GET_ENDPOINT_STATUS

 + #VIDYO_CLIENT_REQUEST_GET_BANDWIDTH_INFO
 + #VIDYO_CLIENT_REQUEST_GET_CONFERENCE_INFO
 + #VIDYO_CLIENT_REQUEST_GET_CONNECTIVITY_INFO
 + #VIDYO_CLIENT_REQUEST_GET_CURRENT_SESSION_DISPLAY_INFO
 + #VIDYO_CLIENT_REQUEST_GET_MEDIA_INFO
 + #VIDYO_CLIENT_REQUEST_GET_PARTICIPANT_INFO
 + #VIDYO_CLIENT_REQUEST_GET_RATE_SHAPER_INFO
 + #VIDYO_CLIENT_REQUEST_GET_VIDEO_FRAME_RATE_INFO

 + #VIDYO_CLIENT_REQUEST_GET_PARTICIPANT_STATISTICS_LIST
 */

typedef struct V5BigStatistics_tag {
    VidyoClientRequestCommunicationStatus        comm_status;
    VidyoClientRequestGetEndpointStatus          endp_status;
    
    VidyoClientRequestBandwidthInfo              bw_info;
    VidyoClientRequestConferenceInfo             conf_info;
    VidyoClientRequestConnectivityInfo           conn_info;
    VidyoClientRequestSessionDisplayInfo         disp_info;
    VidyoClientRequestMediaInfo                  media_info;
    VidyoClientRequestParticipantInfo            par_info;
    VidyoClientRequestRateShaperInfo             rs_info;
    VidyoClientRequestFrameRateInfo              fr_info;

    VidyoClientRequestParticipantStatisticsList  pstat_list;
} V5BigStatistics;


class V5Stat {
    
public:
    V5Stat(IV5ConferenceEventSink& sk): V5Stat(10000, sk) {}
    V5Stat(int millisecs, IV5ConferenceEventSink& sk);
    void start();
    void cancel();
    void on_timer();
    ~V5Stat();
private:
    boost::asio::io_service     io_;
    boost::asio::deadline_timer timer_;
    int                         count_;
    int                         duration_in_ms_;
    IV5ConferenceEventSink      &sink_;
};

#endif // __LIBV5_STATISTICS_H

