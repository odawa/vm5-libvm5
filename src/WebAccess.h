﻿//
//  WebAccess.h
//  V5ClientExample
//
//  Created by V-1094 on 2014/12/11.
//  Copyright (c) 2015 V-cube, Inc. All rights reserved.
//

#ifndef V5ClientExample_WebAccess_h
#define V5ClientExample_WebAccess_h

#include <curl/curl.h>
#include <picojson/picojson.h>
#include "WebAPIObjects.h"
#include "URLLoader.h"
#include "utils.h"
#include "ProxySetting.h"

#include <V5Lite/V5Enumerations.h>

#define MAXURLLEN    (4096)
#define MAXERRORBUF  (4096)

struct MemoryStruct {
    char *memory;
    size_t size;
};

size_t
WriteMemoryCallback(void *contents, size_t size, size_t nmemb, void *userp);
void injectProxySetting(CURL* &curl, vrms5::ProxySetting const &proxySetting);

class V5ClientEnv
{
public:
    explicit V5ClientEnv(std::string const& prefix, std::shared_ptr<vrms5::ProxySetting> proxy);
    virtual ~V5ClientEnv();

    V5ClientHttpResult send(V5APIsObject& d);
    bool sendAsync(V5MapType& query, CALL_ASYNC_RESPONSE_CALLBACK func);
    bool dummySend(std::string const& json, V5APIsObject& d);
    bool sendDirect();
    std::string joinQueryParams(V5MapType& q);
    std::string prepareURL(V5MapType& q);
    std::string escape(std::string const& value);
    std::string urlEncode(std::string const & value) { return std::move(urlEncode(value.c_str())); }
    std::string urlEncode(const char *value);

private:
    std::string url_prefix;

    // for escape, urlEncode method.
    CURL        *curlForUtils;
    char errorBuf[V5_MAX_BUFFER];

    // proxy settings
    std::weak_ptr<vrms5::ProxySetting> proxySetting;

};

#endif
