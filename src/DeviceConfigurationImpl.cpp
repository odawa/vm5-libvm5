﻿

#include "precompile.h"

#include <iostream>
#include <string>
#include <Vidyo/VidyoClient.h>
#include "DeviceConfigurationImpl.h"
#include "V5Log.h"

V5DeviceConfigurationImpl::V5DeviceConfigurationImpl()
{
    audioDevice         = std::make_shared<DeviceCluster>();
    microphoneDevice    = std::make_shared<DeviceCluster>();
    cameraDevice        = std::make_shared<DeviceCluster>();
    videoPreferences    = std::make_shared<V5VideoPreferences>();
    (*videoPreferences) = V5_BEST_QUALITY;
    
}

#if 0
void V5DeviceConfigurationImpl::getConf()
{
    VidyoUint error;
    VidyoClientRequestConfiguration conf;
    
    if ((error = VidyoClientSendRequest(VIDYO_CLIENT_REQUEST_GET_CONFIGURATION, &conf, sizeof(VidyoClientRequestConfiguration))) != VIDYO_CLIENT_ERROR_OK)
    {
        V5LOG(V5_LOG_ERROR, "Failed to request configuration with error: %d\n", error);
    }
    else
    {
        deviceNum[V5_DEVICE_TYPE_AUDIO_IN]  = conf.numberMicrophones;
        deviceNum[V5_DEVICE_TYPE_AUDIO_OUT] = conf.numberSpeakers;
        deviceNum[V5_DEVICE_TYPE_VIDEO]     = conf.numberCameras;
        
        currentDev[V5_DEVICE_TYPE_AUDIO_IN]  = conf.currentMicrophone;
        currentDev[V5_DEVICE_TYPE_AUDIO_OUT] = conf.currentSpeaker;
        currentDev[V5_DEVICE_TYPE_VIDEO]     = conf.currentCamera;
        
        deviceMap[V5_DEVICE_TYPE_VIDEO].clear();
        for (VidyoUint i = 0 ; i < conf.numberCameras; i++) {
            unsigned char *uc = static_cast<unsigned char *>(conf.cameras[i]);
            void *v = static_cast<void *>(uc);
            char *p = static_cast<char *>(v);
            
            V5DeviceName* dn = new V5DeviceName();
            dn->name = std::string(p);
            dn->phyDevName = std::string(p);
            deviceMap[V5_DEVICE_TYPE_VIDEO].push_back(*dn);
        }
        
        deviceMap[V5_DEVICE_TYPE_AUDIO_IN].clear();
        for (VidyoUint i = 0; i < conf.numberMicrophones; i++) {
            unsigned char *uc = static_cast<unsigned char *>(conf.microphones[i]);
            void *v = static_cast<void *>(uc);
            char *p = static_cast<char *>(v);
            unsigned char *uc2 = static_cast<unsigned char *>(conf.microphonePhyDeviceName[i]);
            void *v2 = static_cast<void *>(uc2);
            char *p2 = static_cast<char *>(v2);
            
            V5DeviceName* dn = new V5DeviceName();
            dn->name = std::string(p);
            dn->phyDevName = std::string(p2);
            deviceMap[V5_DEVICE_TYPE_AUDIO_IN].push_back(*dn);
        }
        
        deviceMap[V5_DEVICE_TYPE_AUDIO_OUT].clear();
        for (VidyoUint i = 0; i < conf.numberSpeakers; i++) {
            unsigned char *uc = static_cast<unsigned char *>(conf.speakers[i]);
            void *v = static_cast<void *>(uc);
            char *p = static_cast<char *>(v);
            unsigned char *uc2 = static_cast<unsigned char *>(conf.speakerPhyDeviceName[i]);
            void *v2 = static_cast<void *>(uc2);
            char *p2 = static_cast<char *>(v2);
            
            V5DeviceName* dn = new V5DeviceName();
            dn->name = std::string(p);
            dn->phyDevName = std::string(p2);
            deviceMap[V5_DEVICE_TYPE_AUDIO_OUT].push_back(*dn);
        }
    }
}
#endif

void V5DeviceConfigurationImpl::injectDeviceConfiguration(VClientConfiguration const& clientConf )
{
    auto tmpCameraCluster = std::atomic_load( &cameraDevice );
    tmpCameraCluster->setNumber( clientConf.numberCameras );
    tmpCameraCluster->setCurrent( clientConf.currentCamera );
    for( VidyoUint i = 0; i < clientConf.numberCameras; i++ )
    {
        char tmpName[DEVICE_NAME_SIZE];
        memcpy( tmpName , clientConf.cameras[i] , DEVICE_NAME_SIZE );
        tmpCameraCluster->setItem( tmpName, tmpName );
    }
    
    auto tmpAudioCluster = std::atomic_load( &audioDevice );
    tmpAudioCluster->setNumber( clientConf.numberSpeakers );
    tmpAudioCluster->setCurrent( clientConf.currentSpeaker );
    for( VidyoUint i = 0; i < clientConf.numberSpeakers; i++ )
    {
        char tmpName[DEVICE_NAME_SIZE];
        memcpy( tmpName , clientConf.speakers[i] , DEVICE_NAME_SIZE );
        char tmpPhyName[DEVICE_NAME_SIZE];
        memcpy( tmpPhyName , clientConf.speakerPhyDeviceName[i] , DEVICE_NAME_SIZE );

        tmpAudioCluster->setItem( tmpName, tmpPhyName );
    }

    auto tmpMicCluster = std::atomic_load( &microphoneDevice );
    tmpMicCluster->setNumber( clientConf.numberMicrophones );
    tmpMicCluster->setCurrent( clientConf.currentMicrophone );
    for( VidyoUint i = 0; i < clientConf.numberMicrophones; i++ )
    {
        char tmpName[DEVICE_NAME_SIZE];
        memcpy( tmpName , clientConf.microphones[i] , DEVICE_NAME_SIZE );
        char tmpPhyName[DEVICE_NAME_SIZE];
        memcpy( tmpPhyName , clientConf.microphonePhyDeviceName[i] , DEVICE_NAME_SIZE );
        tmpMicCluster->setItem( tmpName, tmpPhyName );
    }
}

void V5DeviceConfigurationImpl::setConf(V5DeviceType type, unsigned int index)
{
    if( ( type != V5_DEVICE_TYPE_AUDIO_IN ) &&
        ( type != V5_DEVICE_TYPE_AUDIO_OUT ) &&
        ( type != V5_DEVICE_TYPE_VIDEO ) )
    {
        V5LOG(V5_LOG_ERROR,
              "[Device Unchanged]: illegal parameter(type=%d,index=%d).\n",
              type, index);
        // 設定不可能の種別が指定されたため、処理終了
        return;
    }
    
    VidyoUint error;
    VidyoClientRequestConfiguration conf;
    
    if ((error = VidyoClientSendRequest(VIDYO_CLIENT_REQUEST_GET_CONFIGURATION, &conf, sizeof(VidyoClientRequestConfiguration))) != VIDYO_CLIENT_ERROR_OK)
    {
        V5LOG(V5_LOG_ERROR, "Failed to request configuration with error: %d\n", error);
    } else {
        switch (type) {
            case V5_DEVICE_TYPE_AUDIO_IN:
            {
                if (index < conf.numberMicrophones) {
                    conf.currentMicrophone = index;
                    V5LOG(V5_LOG_INFO,
                          "[Device Change]: microphone changed (index = %d: Name = %s).\n",
                          index, conf.microphonePhyDeviceName[index]);
                }
                break;
            }
            case V5_DEVICE_TYPE_AUDIO_OUT:
            {
                if (index < conf.numberSpeakers) {
                    conf.currentSpeaker = index;
                    V5LOG(V5_LOG_INFO,
                          "[Device Change]: speaker changed (index = %d: Name = %s).\n",
                          index, conf.speakerPhyDeviceName[index]);
                }
                break;
            }
            case V5_DEVICE_TYPE_VIDEO:
            {
                if (index < conf.numberCameras) {
                    conf.currentCamera = index;
                    V5LOG(V5_LOG_INFO,
                          "[Device Change]: camera changed (index = %d: Name = %s).\n",
                          index, conf.cameras[index]);
                }
                break;
            }
            // 以降の処理は実行されることはないが、定義しないと警告がでる。
            case V5_DEVICE_TYPE_RING_TONE:
            default:
            {
                V5LOG(V5_LOG_ERROR,
                      "[Device Unchanged]: illegal parameter(enum=%d,index=%d).\n",
                      type, index);
                
                // 設定する必要がない、又は設定できない種別での呼び出しのため、処理終了。
                return;
            }
        }
        
        if (VidyoClientSendRequest(VIDYO_CLIENT_REQUEST_SET_CONFIGURATION, &conf, sizeof(VidyoClientRequestConfiguration)) != VIDYO_CLIENT_ERROR_OK)
        {
            V5LOG(V5_LOG_ERROR, "Failed to set configuration.\n");
        }
        else
        {
            V5LOG(V5_LOG_INFO, "Set configuration successfully for VIDYO_CLIENT_REQUEST_SET_CONFIGURATION\n");
            
            // 正常に更新イベントが成功した場合、デバイスごとのカレント番号を更新
            this->setCurrent(type, index);
        }
    }
}

unsigned int V5DeviceConfigurationImpl::getNumber(V5DeviceType type)
{
    switch (type) {
        case V5_DEVICE_TYPE_AUDIO_IN:
        {
            auto tmpCluster = std::atomic_load(&microphoneDevice);
            return tmpCluster->getNumber();
        }
        case V5_DEVICE_TYPE_AUDIO_OUT:
        {
            auto tmpCluster = std::atomic_load(&audioDevice);
            return tmpCluster->getNumber();
        }
        case V5_DEVICE_TYPE_VIDEO:
        {
            auto tmpCluster = std::atomic_load(&cameraDevice);
            return tmpCluster->getNumber();
        }
        case V5_DEVICE_TYPE_RING_TONE:
        {
            return 0;
        }
        default:
        {
            return 0;
        }
    }
}

unsigned int V5DeviceConfigurationImpl::getCurrent(V5DeviceType type)
{
    switch (type) {
        case V5_DEVICE_TYPE_AUDIO_IN:
        {
            auto tmpCluster = std::atomic_load(&microphoneDevice);
            return tmpCluster->getCurrent();
        }
        case V5_DEVICE_TYPE_AUDIO_OUT:
        {
            auto tmpCluster = std::atomic_load(&audioDevice);
            return tmpCluster->getCurrent();
        }
        case V5_DEVICE_TYPE_VIDEO:
        {
            auto tmpCluster = std::atomic_load(&cameraDevice);
            return tmpCluster->getCurrent();
        }
        case V5_DEVICE_TYPE_RING_TONE:
        default:
        {
            return -1; // didn't find it.
        }
    }
}

void V5DeviceConfigurationImpl::setCurrent(V5DeviceType type, unsigned int index)
{
    switch( type )
    {
        case V5_DEVICE_TYPE_AUDIO_IN:
        {
            auto tmpClusterPtr = std::make_shared<DeviceCluster>( *std::atomic_load( &this->microphoneDevice ) );
            tmpClusterPtr->setCurrent(index);
            std::atomic_store(&microphoneDevice, tmpClusterPtr);
            break;
        }
        case V5_DEVICE_TYPE_AUDIO_OUT:
        {
            auto tmpClusterPtr = std::make_shared<DeviceCluster>( *std::atomic_load( &this->audioDevice ) );
            tmpClusterPtr->setCurrent(index);
            std::atomic_store(&audioDevice, tmpClusterPtr);
            break;
        }
        case V5_DEVICE_TYPE_VIDEO:
        {
            auto tmpClusterPtr = std::make_shared<DeviceCluster>( *std::atomic_load( &this->cameraDevice ) );
            tmpClusterPtr->setCurrent(index);
            std::atomic_store(&cameraDevice, tmpClusterPtr);
            break;
        }
        case V5_DEVICE_TYPE_RING_TONE:
        default:
        {
            // didn't find it.
        }
    }
}

std::shared_ptr<V5DeviceName> V5DeviceConfigurationImpl::getMicrophoneItem(unsigned int index)
{
    auto tmpMicClusterPtr = std::atomic_load(&microphoneDevice);
    if (index >= tmpMicClusterPtr->getNumber()) {
        return nullptr;
    }
    return tmpMicClusterPtr->getItem(index);
}

std::shared_ptr<V5DeviceName> V5DeviceConfigurationImpl::getSpeakerItem(unsigned int index)
{
    auto tmpAudioClusterPtr = std::atomic_load(&audioDevice);
    if (index >= tmpAudioClusterPtr->getNumber()) {
        return nullptr;
    }
    return tmpAudioClusterPtr->getItem(index);
}

std::shared_ptr<V5DeviceName> V5DeviceConfigurationImpl::getCameraItem(unsigned int index)
{
    auto tmpCameraClusterPtr = std::atomic_load(&cameraDevice);
    if (index >= tmpCameraClusterPtr->getNumber()) {
        return nullptr;
    }
    return tmpCameraClusterPtr->getItem(index);
}

void V5DeviceConfigurationImpl::setVideoPreferences(V5VideoPreferences preferences)
{
    if( getVideoPreferences() == preferences )
    {
        // 既に反映されている設定と同じ内容で要求が来たので処理終了する。
        return;
    }
    
    VidyoUint error;
    VidyoClientRequestConfiguration conf;
    
    if (( error = VidyoClientSendRequest(VIDYO_CLIENT_REQUEST_GET_CONFIGURATION, &conf, sizeof(VidyoClientRequestConfiguration))) != VIDYO_CLIENT_ERROR_OK)
    {
        V5LOG(V5_LOG_ERROR, "Failed to request configuration with error: %d\n", error);
    }
    else
    {
        conf.videoPreferences = (VidyoClientVideoPreferences)preferences;
        
        if (VidyoClientSendRequest(VIDYO_CLIENT_REQUEST_SET_CONFIGURATION, &conf, sizeof(VidyoClientRequestConfiguration)) != VIDYO_CLIENT_ERROR_OK)
        {
            V5LOG(V5_LOG_ERROR, "Failed to set configuration for setVideoPreferences.\n");
        }
        else
        {
            V5LOG(V5_LOG_INFO, "Set configuration successfully for setVideoPreferences\n");
            
            // 設定に成功したらクラス変数に保持する
            auto videoPrefPtr = std::make_shared<V5VideoPreferences>();
            (*videoPrefPtr) = preferences;
            std::atomic_store(&videoPreferences, videoPrefPtr);
        }
    }
}

V5VideoPreferences V5DeviceConfigurationImpl::getVideoPreferences() {
    auto videoPrePtr = std::atomic_load(&videoPreferences);
    return (*videoPrePtr.get());
}

/*************************************************************************/
/* srtuct DeviceCluster                                                  */
/*************************************************************************/

DeviceCluster::DeviceCluster()
{
    deviceNumber    = -1;
    currentDevice   = -1;
    deviceNameList.reserve(V5_DEVICE_CONNECT_MAX);
    deviceNameList.clear();
}

DeviceCluster::DeviceCluster( DeviceCluster const &devCluster )
{
    deviceNumber    = devCluster.deviceNumber;
    currentDevice   = devCluster.currentDevice;
    deviceNameList.reserve(devCluster.deviceNameList.size());
    deviceNameList.clear();
    for( unsigned int i = 0; i < devCluster.deviceNameList.size() ; i++ )
    {
        auto tmpDevNamePtr = std::make_shared<V5DeviceName>();
        tmpDevNamePtr->name = devCluster.deviceNameList[i]->name;
        tmpDevNamePtr->phyDevName = devCluster.deviceNameList[i]->phyDevName;
        deviceNameList.push_back(tmpDevNamePtr);
    }
    
}

void DeviceCluster::setNumber( unsigned int number )
{
    deviceNumber = number;
}

void DeviceCluster::setCurrent( unsigned int index )
{
    currentDevice = index;
}

void DeviceCluster::setItem( const char *name, const char *phyName )
{
    std::shared_ptr<V5DeviceName> devPtr = std::make_shared<V5DeviceName>();
    devPtr->name        = name;
    devPtr->phyDevName  = phyName;
    deviceNameList.push_back(devPtr);
}


unsigned int DeviceCluster::getNumber() const
{
    return deviceNumber;
}

unsigned int DeviceCluster::getCurrent() const
{
    return currentDevice;
}

V5DeviceNamePtr DeviceCluster::getItem(unsigned int index ) const
{
    return deviceNameList[index];
}

std::ostream& operator<<(std::ostream& os, const DeviceCluster& cluster)
{
    os
    << "[DeviceClaster] DEBUG" << std::endl
    << "deviceNumber : " << cluster.getNumber()<< std::endl
    << "currentDevice : " << cluster.getCurrent()<< std::endl
    << "[DeviceList Data]" << std::endl
    << std::endl;
    
    for ( unsigned int count = 0; count < cluster.getNumber(); count++ )
    {
        os
        << "[DeviceList No." << count << "]" << std::endl
        << "name        : " << cluster.getItem(count)->name << std::endl
        << "phyDevName  : " << cluster.getItem(count)->phyDevName << std::endl
        ;
    }
    return os;
}


