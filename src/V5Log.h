﻿#pragma once
#ifndef __LibV5Lite__Log__
#define __LibV5Lite__Log__

#include <V5Lite/LibV5LiteCommon.h>

#include "VidyoClientAPI.h"

#define V5_NO_DEBUG_LOG (1)     // ログ出力用define

typedef enum {
    V5_LOG_FATAL,     /*!< Fatal */
    V5_LOG_ERROR,     /*!< Error */
    V5_LOG_WARNING,   /*!< Warning */
    V5_LOG_INFO,      /*!< Information */
    V5_LOG_DEBUG,     /*!< Debug */
    V5_LOG_SENT,      /*!< Sent */
    V5_LOG_RECEIVED,  /*!< Received */
} V5Level;

#ifdef V5_APPLE_CC
void CNSLog(const char * __restrict file,
            int line,
            const char * __restrict function,
            const char * __restrict format,
            ...);
#define V5_DEBUG_LOG_TO_NSLOG       // CNSLogコールログ出力用define
#endif


/********************************************************************/
// VidyoClientLogFormattedを使用してログ出力を行う場合、
// libInit(initialize)の引数に指定するlogPriorityの"@AppGui"のレベルを変更すれば
// Vidyoライブラリのログ出力レベルを変更せずに共通ライブラリのレベルのみの
// ログ出力レベルを変更できる。
/********************************************************************/

//#if !defined(V5_NO_DEBUG_LOG)
//#   define V5LOG(level,...) VidyoClientLogFormatted(static_cast<VidyoLogLevel>(level), __FILE__, __LINE__, __FUNCTION__, __VA_ARGS__)
//#elif defined(V5_DEBUG_LOG_TO_NSLOG)
//#   define V5LOG(level,...) CNSLog(__FILE__, __LINE__, __FUNCTION__, ##__VA_ARGS__)
//#else
//#   define V5LOG(level,...)
//#endif

//********************************************************************
// 以下の内容を実行すると、VidyoClientLogFormatted(VIDYO-API)と
// CNSLog(Objective-C)の2つでログ出力処理を実施する。
// VIDYOもしくはObjective-Cでのみログ出力を行う場合は上のコメントアウトされている処理の
// コメントアウトを外し、下のDefine定義をコメントアウトすること。
// ※CNSLog側はログの出力レベルを指定していないので、全部のログを出力している。
//
// 以下のdefineの組み合わせでログ出力用の処理を変更可能
// 1. V5_NO_DEBUG_LOG有効 + V5_DEBUG_LOG_TO_VIDYO有効 + V5_DEBUG_LOG_TO_NSLOG有効
// → VIDYO-API + CNSLog-API でログ出力
// 2. V5_NO_DEBUG_LOG有効 + V5_DEBUG_LOG_TO_VIDYO有効 + V5_DEBUG_LOG_TO_NSLOG無効
// → VIDYO-API でのみログ出力
// 3. V5_NO_DEBUG_LOG有効 + V5_DEBUG_LOG_TO_VIDYO無効 + V5_DEBUG_LOG_TO_NSLOG有効
// → CNSLog-APIでのみログ出力
// 4. V5_NO_DEBUG_LOG無効
// → ログ出力なし
//********************************************************************

#define V5_DEBUG_LOG                // DEBUG Log出力用deifne
#define V5_DEBUG_LOG_TO_VIDYO       // VIDYO-API ログ出力用deifne


//#undef V5_DEBUG_LOG               // Debug Log 出力用define削除
//#undef V5_DEBUG_LOG_TO_VIDYO      // VIDYO-API ログ出力用deifne削除
#if defined(DEBUG)
#undef V5_DEBUG_LOG_TO_NSLOG      // CNSLog-API ログ出力用define削除
#else
// リリースコンパイルの場合はCNSLogでのログ出力を抑止する。
#undef V5_DEBUG_LOG_TO_NSLOG      // CNSLog-API ログ出力用define削除
#endif

#if (defined(V5_DEBUG_LOG) && defined(V5_DEBUG_LOG_TO_VIDYO) )
#   define V5LOG_VIDYO(level,...) VidyoClientLogFormatted(static_cast<VidyoLogLevel>(level), __FILE__, __LINE__, __FUNCTION__, __VA_ARGS__)
#else
#   define V5LOG_VIDYO(level,...) 
#endif

#if ( defined(V5_DEBUG_LOG) && defined(V5_DEBUG_LOG_TO_NSLOG) )
#   define V5LOG_CNSLOG(level,...) CNSLog(__FILE__, __LINE__, __FUNCTION__, ##__VA_ARGS__)
#else
#   define V5LOG_CNSLOG(level,...)
#endif

#   define V5LOG(level,...) V5LOG_VIDYO(level,__VA_ARGS__); V5LOG_CNSLOG(level,##__VA_ARGS__)


#endif /* __LibV5Lite__Log__ */
