﻿#pragma once

#ifndef __LibV5Lite__DeviceConfigurationImpl__
#define __LibV5Lite__DeviceConfigurationImpl__

#include <V5Lite/DeviceConfiguration.h>


#ifdef __cplusplus
#include <vector>
#include <iostream>
#include <memory>

#define V5_DEVICE_CONNECT_MAX 20

typedef VidyoClientRequestConfiguration VClientConfiguration;

struct DeviceCluster;

typedef std::shared_ptr<DeviceCluster> DeviceClusterPtr;
typedef std::shared_ptr<V5DeviceName> V5DeviceNamePtr;



struct DeviceCluster
{
    explicit DeviceCluster();
    explicit DeviceCluster( DeviceCluster const& devCluster );
    
    DeviceCluster(DeviceCluster&&) = delete; // 禁止ではなく、未実装（おそらく default でも動作する）
    
    DeviceCluster const& operator=(DeviceCluster const&) = delete; // 禁止ではなく、未実装
    DeviceCluster const& operator=(DeviceCluster&&) = delete; // 禁止ではなく、未実装（おそらく default でも動作する）
    
    // Setter
    void setNumber( unsigned int number );
    void setCurrent( unsigned int index );
    void setItem( const char *name, const char *phyName );
    
    // Getter
    unsigned int    getNumber() const;
    unsigned int    getCurrent() const;
    V5DeviceNamePtr getItem(unsigned int index ) const;
    
private:
#ifdef V5_WIN32
#pragma warning(disable:4251)
#endif
    std::vector<V5DeviceNamePtr> deviceNameList;
#ifdef V5_WIN32
#pragma warning(default:4251)
#endif
    
    unsigned int deviceNumber;
    unsigned int currentDevice;
    
    friend std::ostream& operator<<(std::ostream& os, const DeviceClusterPtr& cluster);
};



class V5DeviceConfigurationImpl : public V5DeviceConfiguration
{
public:
    V5DeviceConfigurationImpl();
    
    virtual unsigned int getNumber(V5DeviceType type) override;
    virtual unsigned int getCurrent(V5DeviceType type) override;
    virtual void setCurrent(V5DeviceType type, unsigned int index) override;
    virtual std::shared_ptr<V5DeviceName> getMicrophoneItem(unsigned int index) override;
    virtual std::shared_ptr<V5DeviceName> getSpeakerItem(unsigned int index) override;
    virtual std::shared_ptr<V5DeviceName> getCameraItem(unsigned int index) override;
    virtual void setConf(V5DeviceType type, unsigned int index) override;
//    virtual void getConf() override;
    
    virtual void setVideoPreferences(V5VideoPreferences preferences) override;
    virtual V5VideoPreferences getVideoPreferences() override;
    
    void injectDeviceConfiguration(VClientConfiguration const& clientConf );

private:
    
    DeviceClusterPtr audioDevice;
    DeviceClusterPtr microphoneDevice;
    DeviceClusterPtr cameraDevice;
    std::shared_ptr<V5VideoPreferences> videoPreferences;
    
#if 0
#ifdef V5_WIN32
#pragma warning(disable:4251)
#endif
    std::vector<V5DeviceName> deviceMap[V5_DEVICE_TYPE_NUM];
#ifdef V5_WIN32
#pragma warning(default:4251)
#endif
    unsigned int deviceNum[V5_DEVICE_TYPE_NUM];
    unsigned int currentDev[V5_DEVICE_TYPE_NUM];
#endif
};

#endif






#endif /* defined(__LibV5Lite__DeviceConfigurationImpl__) */
