﻿#include "precompile.h"

#include <iostream>
#include <future>

#include "Statistics.h"
#include "V5Log.h"

V5Stat::V5Stat(int millisecs, IV5ConferenceEventSink &sk):
io_(),
duration_in_ms_(millisecs),
timer_(io_, boost::posix_time::milliseconds(millisecs)),
sink_(sk)
{
    count_ = 0;
    timer_.async_wait(boost::bind(&V5Stat::on_timer, this));
}

void V5Stat::start()
{
    auto result = std::async(std::launch::async, [this] {
        this->io_.run();
    });
}

void V5Stat::cancel()
{
    timer_.cancel();
}

V5Stat::~V5Stat()
{
    timer_.cancel();
}

void V5Stat::on_timer()
{
    
    V5BigStatistics stat = {0};
    // callback comes here!
    
    ++count_;
    timer_.expires_at(timer_.expires_at() + boost::posix_time::milliseconds(duration_in_ms_));
    timer_.async_wait(boost::bind(&V5Stat::on_timer,this));
}
