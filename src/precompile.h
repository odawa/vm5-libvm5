﻿//
//  precompile.h
//  LibV5Lite
//
//  Created by 穂積 崇 on 2015/02/03.
//  Copyright (c) 2015 V-cube, Inc. All rights reserved.
//
#pragma once
#ifndef LibV5Lite_precompile_h
#define LibV5Lite_precompile_h

#define LIBV5LITE_BUILD

#ifdef WIN32
#include <WinSock2.h>
#include <Windows.h>
#endif

#include <assert.h>

#if defined(DEBUG) || defined(_DEBUG)
#define V5DEBUG
#endif

#include <V5Lite/LibV5LiteCommon.h>

#endif

