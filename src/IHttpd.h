﻿#pragma once
#ifndef INCLUDE_IHTTPD_H
#define INCLUDE_IHTTPD_H

#include <V5Lite/RequestListenerCommon.h>

class IHttpd
{
public:
	virtual ~IHttpd() {}

	virtual V5HTTPD_START HTTPDStart(unsigned short port) = 0;
	virtual void HTTPDStop() = 0;
};

#endif // INCLUDE_IHTTPD_H
