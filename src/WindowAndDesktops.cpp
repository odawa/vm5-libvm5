﻿
#include "precompile.h"

#include <V5Lite/WindowsAndDesktops.h>

#include "utils.h"
#include "VidyoClientAPI.h"

V5WindowsAndDesktops::V5WindowsAndDesktops(VidyoClientRequestGetWindowsAndDesktops const& windowsAndDesktops)
{
    windows.resize(windowsAndDesktops.numApplicationWindows);
    for (VidyoUint i = 0; i < windows.size(); i++)
    {
        auto& info = windows[i];
        info.appWindowName = windowsAndDesktops.appWindowName[i];
        info.appWindowAppName = windowsAndDesktops.appWindowAppName[i];
        info.appWindowId = windowsAndDesktops.appWindowId[i];
        info.appWindowsRect.xPos = windowsAndDesktops.appWindowRect[i].xPos;
        info.appWindowsRect.yPos = windowsAndDesktops.appWindowRect[i].yPos;
        info.appWindowsRect.width = windowsAndDesktops.appWindowRect[i].width;
        info.appWindowsRect.height = windowsAndDesktops.appWindowRect[i].height;
    }

    desktops.resize(windowsAndDesktops.numSystemDesktops);
    for (VidyoUint i = 0; i < desktops.size(); i++)
    {
        auto& info = desktops[i];
        info.sysDesktopName = windowsAndDesktops.sysDesktopName[i];
        info.sysDesktopId = windowsAndDesktops.sysDesktopId[i];
        info.sysDesktopRect.xPos = windowsAndDesktops.sysDesktopRect[i].xPos;
        info.sysDesktopRect.yPos = windowsAndDesktops.sysDesktopRect[i].yPos;
        info.sysDesktopRect.width = windowsAndDesktops.sysDesktopRect[i].width;
        info.sysDesktopRect.height = windowsAndDesktops.sysDesktopRect[i].height;
#if VIDYO_HAS_NEXT_SHARE_API
        info.sysDesktopWorkArea.xPos = windowsAndDesktops.sysDesktopWorkArea[i].xPos;
        info.sysDesktopWorkArea.yPos = windowsAndDesktops.sysDesktopWorkArea[i].yPos;
        info.sysDesktopWorkArea.width = windowsAndDesktops.sysDesktopWorkArea[i].width;
        info.sysDesktopWorkArea.height = windowsAndDesktops.sysDesktopWorkArea[i].height;
        info.sysDesktopInfo.xDpi = windowsAndDesktops.sysDesktopInfo[i].xDpi;
        info.sysDesktopInfo.yDpi = windowsAndDesktops.sysDesktopInfo[i].yDpi;
        info.sysDesktopInfo.isPrimary = (windowsAndDesktops.sysDesktopInfo[i].isPrimary != VIDYO_FALSE);
#endif
    }

}
