﻿//
//  V5ConferenceStatistics.h
//  LibV5Lite_Mac
//
//  Created by V-0757 on 2015/04/27.
//  Copyright (c) 2015 V-cube, Inc. All rights reserved.
//

#ifndef __LibV5Lite_Mac__V5ConferenceStatistics__
#define __LibV5Lite_Mac__V5ConferenceStatistics__

#include <stdio.h>

#include <mutex>
#include <iostream>
#include <Vidyo/VidyoClient.h>
#include <string>
#include <atomic>

#include <V5Lite/LibV5LiteCommon.h>
#include <V5Lite/V5DataTypes.h>

#ifdef V5_CLANG
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wconversion"
#endif

#include <boost/thread.hpp>
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

#ifdef V5_CLANG
#pragma clang diagnostic pop
#endif


#include <V5Lite/IV5ConferenceEventSink.h>

#include "utils.h"

#include "V5Log.h"

// ●VIDYO_CLIENT_REQUEST_GET_COMMUNICATION_STATUS
// → 対VidyoManager,VidyoRouterの接続情報
//  1)
//   vmCommunicationStatus(VidyoBool)
//   → VidyoManagerへの接続状態ステータスフラグ？
//  2)
//   vmCommunicationViaVidyoProxy(VidyoBool)
//   → VidyoManagerへのVidyoProxy経由フラグ
//  3)
//   vmCommunicationViaWebProxy(VidyoBool)
//   → VidyoManagerへのVidyoWebProxy経由フラグ
//  4)
//   vrCommunicationStatus(VidyoBool)
//   → VidyoRouterへの接続状態ステータスフラグ？
//  5)
//   vrCommunicationViaVidyoProxy(VidyoBool)
//   → VidyoRouterへのVidyoProxy経由フラグ
//  6)
//   vrCommunicationViaWebProxy(VidyoBool)
//   → VidyoRouterへのVidyoWebProxy経由フラグ

// ●VIDYO_CLIENT_REQUEST_GET_ENDPOINT_STATUS
// → EndpointStatusの模様。
//  1)
//   endPointStatus(VidyoClientEndpointStatus)
//   → VIDYO_CLIENT_ENDPOINT_STATUS_INACTIVE
//     → サーバへのEndpoint未登録状態。
//   → VIDYO_CLIENT_ENDPOINT_STATUS_REGISTERED
//     → サーバ側への登録は完了しているが、ユーザ(クライアントサイド)への通知が来ていない。
//   → VIDYO_CLIENT_ENDPOINT_STATUS_ACTIVE
//     → サーバ/クライアント共に登録が完了し、使用可能となっている状態。

// ●VIDYO_CLIENT_REQUEST_GET_BANDWIDTH_INFO
// → 現在の会議の帯域幅の統計情報が取得できる。
//  1)
//   AvailSendBwVideo
//   → 映像データを送信するために利用可能な帯域幅。
//     単位はkbit / second.
//  2)
//   AvailSendBwAudio
//   → 音声データを送信するために利用可能な帯域幅。
//     単位はkbit / second.
//  3)
//   AvailSendBwApplication
//   → 画面共有Windowデータを送信するために利用可能な帯域幅。
//     単位はkbit / second.
//  4)
//   AvailSendBwMax
//   → 全リソース情報の送信に利用可能な最大帯域幅
//     単位はkbit / second.
//  5)
//   ActualSendBwVideo
//   → 映像データ送信する際に使用している帯域幅
//     単位はkbit / second.
//  6)
//   ActualSendBwAudio
//   → 音声データ送信する際に使用している帯域幅
//     単位はkbit / second.
//  7)
//   ActualSendBwApplication
//   → 画面共有Windowデータを送信する際に使用している帯域幅
//     単位はkbit / second.
//  8)
//   ActualSendBwMax
//   → 全リソース情報の送信に使用している最大帯域幅
//     単位はkbit / second.
//  9)
//   AvailRecvBwVideo
//   → 映像データを受信するために利用可能な帯域幅。
//     単位はkbit / second.
//  10)
//   AvailRecvBwAudio
//   → 音声データを受信するために利用可能な帯域幅。
//     単位はkbit / second.
//  11)
//   AvailRecvBwApplication
//   → 画面共有Windowデータを受信するために利用可能な帯域幅。
//     単位はkbit / second.
//  12)
//   AvailRecvBwMax
//   → 全リソース情報の送信に利用可能な最大帯域幅
//     単位はkbit / second.
//  13)
//   ActualRecvBwVideo
//   → 映像データ受信する際に使用している帯域幅
//     単位はkbit / second.
//  14)
//   ActualRecvBwAudio
//   → 音声データ受信する際に使用している帯域幅
//     単位はkbit / second.
//  15)
//   ActualRecvBwApplication
//   → 画面共有Windowデータを受信する際に使用している帯域幅
//     単位はkbit / second.
//  16)
//   ActualRecvBwMax
//   → 全リソース情報の受信に使用している最大帯域幅
//     単位はkbit / second.

// ●VIDYO_CLIENT_REQUEST_GET_CONFERENCE_INFO
// → 録画中及び動画配信中かどうかを示す情報
//  1)
//   recording(VidyoBool)
//   → 会議が録画中かどうかの情報。
//  2)
//   webcast(VidyoBool)
//   → Webでの動画配信がされているかどうかの情報

// ●VIDYO_CLIENT_REQUEST_GET_CONNECTIVITY_INFO
// → 現在使用さしているSign-in情報及びパラメータがかえってくる。
//  1)
//   serverAddress [SIGNIN_SERVERADDRESS_SIZE](char)
//   → VidyoManagerのアドレス。
//     IPアドレスもしくはHostName
//  2)
//   serverPort [SIGNIN_SERVERPORT_SIZE](char)
//   → VidyoManager Addressのポート番号
//  3)
//   serverSecured(VidyoBool)
//   → VidyoManagerへの接続にTLSを使用しているかどうか。
//     VIDYO_TRUEならTLSを使用。
//  4)
//   vmIdentity [VIDYO_MANAGER_ID_SIZE](char)
//   → VidyoManagerに割り当てられているEndpoint情報(かな？)
//  5)
//   userName [SIGNIN_USERNAME_SIZE](char)
//   → VidyoPortalへのSigninに使用しているUserName
//  6)
//   portalAddress [SIGNIN_PORTALADDRESS_SIZE](char)
//   → VidyoPortalのSOAPにアクセスするためのベースURL
//  7)
//   portalVersion [SIGNIN_PORTALVERSION_SIZE](char)
//   → VidyoPortalのバージョン
//  8)
//   locationTag [ENDPOINT_LOCATION_TAG_SIZE](char)
//   → Endpointに割り当てられたLocationTag
//  9)
//   vidyoProxyAddress [SIGNIN_VIDYO_PROXY_ADDRESS_SIZE](char)
//   → VidyoProxyのHostNameもしくはIPアドレス
//  10)
//   vidyoProxyPort [SIGNIN_VIDYO_PROXY_PORT_SIZE](char)
//   → VidyoPortalのポート番号
//  11)
//   guestLogin(VidyoBool)
//   → VIDYO_TRUEの場合、ユーザ名はゲストログイン用に先行的に与えられた一時的な名称。
//     VIDYO_FALSEの場合は非ゲストログインユーザ。
//  12)
//   clientExternalIPAddress [MAX_IPADDRESS_LEN](char)
//   → Webサーバから見たクライアントのグローバルIPアドレス。
//  13)
//   proxyType(VidyoClientProxyType)
//   → クライアントが利用可能なプロキシの種類
//  14)
//   reverseProxyAddress [SIGNIN_VIDYO_PROXY_ADDRESS_SIZE](char)
//   → リバースプロキシがVidyoManagerとの接続に使用するHostNameもしくはIPアドレス
//  15)
//   reverseProxyPort [SIGNIN_VIDYO_PROXY_PORT_SIZE]
//   → リバースプロキシがVidyoManagerとの接続に使用するポート番号

// ●VIDYO_CLIENT_REQUEST_GET_CURRENT_SESSION_DISPLAY_INFO
// → 現在の会議表示情報
//  1)
//   sessionDisplayContext
//   → 現在の会議情報
//  2)
//   sessionDisplayText [DISPLAY_CONFERENCE_SIZE](char)
//   → クライアントが会議に入室中の場合、現在のテキスト情報をWindowのキャプションに表示することができる。
//     それによってユーザが会議情報を識別できる。(?ちょっとなにいってるかわからない)

// ●VIDYO_CLIENT_REQUEST_GET_MEDIA_INFO
// → ネットワークに送信されたメディアに関連する情報を現在の会議のI-FRAME番号、FIR番号、NACKS番号、MEDIARTTを確認する
//  1)
//   numIFrames
//   → 現在の会議でクライアントから送信されたI-Frameの合計数。
//  2)
//   numFirs
//   → 現在の会議でクライアントが受信したフルイントラRequestの合計数。
//  3)
//   numNacks
//   → 現在の会議でクライアントが受信したNoACKnowlegementメッセージ合計数。
//  4)
//   mediaRTT
//   → ミリ秒単位のRTCP相互作用に基づくメディアパケットの往復推定時間。

// ●VIDYO_CLIENT_REQUEST_GET_PARTICIPANT_INFO
// →　会議参加中の全リモートユーザの各種統計情報を取得する。
//    書いてある情報以外は未実装か予約データ領域の模様。
//    ※MAX_PARTICIPANTS_NUMが24になっているが、
//    現在の最大参加可能人数はどうなっているのか・・・。
//    → 24という数値はVIDYO内でUnlimitedを示す数値らしい。
//  1)
//   Name[MAX_PARTICIPANTS_NUM]
//   → ユーザ名(DisplayNameか？)
//  2)
//   URI[MAX_PARTICIPANTS_NUM]
//   → 会議室に参加中ユーザのParticipantURI。
//     表示するユーザの選別はどうやる？
//     → 基本的に入ってきた人数分で全部だしかな。
//  3)
//   receivedFrameRate[MAX_PARTICIPANTS_NUM]
//   → ユーザごとの現在の受信フレームレート
//  4)
//   decodedFrameRate[MAX_PARTICIPANTS_NUM]
//   → ユーザごとのデコードフレームレート
//  5)
//   displayedFrameRate[MAX_PARTICIPANTS_NUM]
//   → ユーザごとの表示可能フレームレート
//  6)
//   receivedPacketRate[MAX_PARTICIPANTS_NUM]
//   → ユーザごとの受信可能パケットレート
//     未実装の模様。
//  7)
//   receivedBpsVideo[MAX_PARTICIPANTS_NUM]
//   → 映像受信量
//     bit / Second ?
//  8)
//   receivedWidth[MAX_PARTICIPANTS_NUM]
//   → Video Streamの幅サイズ
//     bit / Second ?
//  9)
//   receivedHeight[MAX_PARTICIPANTS_NUM]
//   → Video Streamの高さサイズ
//  10)
//   receivedBytesVideo[MAX_PARTICIPANTS_NUM]
//   → 映像の受信総量。byteデータの模様。
//     今までの映像情報の受信総量なのかどうなのか
//     どういう基準での情報なのかいまいちわからない。
//  11)
//   receivedBytesAudio[MAX_PARTICIPANTS_NUM]
//   → 音声データの受信総量。byteデータの模様。
//     どういう基準での情報なのかいまいちわからない。
//  12)
//   numberParticipants
//   → 会議参加人数かな。

// ●VIDYO_CLIENT_REQUEST_GET_RATE_SHAPER_INFO
// → 現在のレート整形に関連する統計情報を問い合わせる。
//  1)
//   delayVideoPriorytyNormal(VidyoUint)
//   → ミリ秒単位での通常プライオリティの映像送信データをシェイパーキューに流す際のパケット最大遅延量。
//  2)
//   numPacketsVideoPriorytyNormal(VidyoUint)
//   → 通常プライオリティレートの映像送信データをシェイパーキューに流しているパケットの現在の数
//  3)
//   numFramesVideoPriorytyNormal
//   → 予約パラメータ
//  4)
//   numDroppedVideoPriorytyNormal
//   → 現在の会議で送信した通常プライオリティレートの映像データの総パケットのうち、
//     シェイパーキューからドロップされたパケット数。
//  5)
//   delayVideoPriorutyRetransmit
//   → ミリ病単位の優先度が高いレートの映像送信データをシェイパーキューに流す際の実際の最大遅量。
//  6)
//   numPacketsVideoPriorutyRetransmit
//   → 優先度が高いレートの映像送信データをシェイパーキューに流しているパケットの現在数。
//  7)
//   numFramesVideoPriorutyRetransmit
//   → 予約パラメータ
//  8)
//   numDroppedVideoPriorutyRetransmit
//   → 現在の会議で送信した優先度が高いレートの映像送信データの内、
//     シェイパーキューからドロップされたパケット数。
//  9)
//   delayAppPriorityNormal
//   → ミリ秒単位での通常プライオリティレートのアプリケーション共有画面の映像送信データを
//     シェイパーキューに流す際のパケット最大遅延量。
//  10)
//   numPacketsAppPriorityNormal
//   → 通常プライオリティレートのアプリケーション画面共有の映像送信データを
//     シェイパーキューに流しているパケットの現在の数
//  11)
//   numFramesAppPriorityNormal
//   → 予約パラメータ
//  12)
//   numDroppedAppPriorityNormal
//   → 現在の会議で送信した通常プライオリティレートの映像送信データの内、
//     シェイパーキューからドロップされたパケット数。
//  13)
//   delayAppPriorityRetransmit
//   → ミリ病単位の優先度が高いレートのアプリケーション共有画面の映像送信データをシェイパーキューに流す際の実際の最大遅量。
//  14)
//   numPacketsAppPriorityRetransmit
//   → 優先度が高いレートのアプリケーション共有画面の映像送信データをシェイパーキューに流しているパケットの現在数。
//  15)
//   numFramesAppPriorityRetransmit
//   → 予約パラメータ
//  16)
//   numDroppedAppPriorityRetransmit
//   → 現在の会議で送信した優先度が高いレートのアプリケーション共有画面の映像送信データの内、
//     シェイパーキューからドロップされたパケット数。

// ●VIDYO_CLIENT_REQUEST_GET_VIDEO_FRAME_RATE_INFO
// → 参加会議に対する自端末からの以下のフレームレートを取得するAPI
//  1)
//   captureFrameRate
//   → カメラデバイスからの画像取得フレームレート。
//     Frames / Second.
//  2)
//   encodeFrameRate
//   → エンコードのフレームレート。
//     Frames / Second.
//  3)
//   sendFrameRate
//   → ネットワークを介して送信される映像のフレームレート。
//     Frames / Second.

// ●VIDYO_CLIENT_REQUEST_GET_PARTICIPANT_STATISTICS_LIST
// → 会議参加者の統計情報
//  1)
//   startIndex(VidyoInt)
//   → 統計情報を取得する会議参加者のIndexをしていする？
//     何を指定すればいいかわからない。
//  2)
//   numParticipants
//   → 会議参加人数
//  3)
//   statistics [MAX_GET_PARTICIPANT_STATISTICS](VidyoClientParticipantStatistics)
//   → 会議参加者の統計情報。
//     超デカい。


using boost::asio::deadline_timer;
using boost::system::error_code;
using boost::asio::io_service;

typedef boost::asio::io_service* io_service_ptr;

typedef std::shared_ptr<boost::asio::io_service::strand> strand_ptr;
typedef std::function<void( const error_code& error)> timer_callback_handler;
typedef std::shared_ptr<deadline_timer> statTimerPtr;
typedef std::shared_ptr<boost::thread> thread_ptr;

typedef std::shared_ptr<V5ConferenceStatisticsAllInfo> confStatAllInfoPtr;

class V5StatisticsInfo
{
    typedef vrms5::utils::ts_weak_ptr<IV5ConferenceEventSink> ConferenceCallbackStorageT;
    typedef std::lock_guard<std::recursive_mutex> lock_guard_t;
    
public:
    V5StatisticsInfo();
    V5StatisticsInfo( ConferenceCallbackStorageT sink );
    V5StatisticsInfo( int millisecs, ConferenceCallbackStorageT sink );
    
    ~V5StatisticsInfo();
    
    void setConferenceCallback(std::nullptr_t);
    void setConferenceCallback(ConferenceCallbackStorageT sink);
    
    void setWaitTime(int millisecs);
    
    bool startStatisticsInfo( int millisecs );
    bool startStatisticsInfo();
    bool stopStatisticsInfo();
    
private:
    void onGetStatisticsInfo( const error_code& error );
    bool getStatisticsInfoFuncCall();
    
    ConferenceCallbackStorageT  m_conferenceCallback;
    
    int m_millisecs;
    
    std::atomic_bool m_getStatisticsFlg;
    
    io_service_ptr      m_io_service;
    thread_ptr          m_io_thread;
    std::unique_ptr<boost::asio::io_service::work> m_work_handler;
    strand_ptr          m_strand;
    statTimerPtr        m_timerPtr;
    confStatAllInfoPtr  m_confStatInfo;
    
    mutable std::recursive_mutex            mtx;
};






#endif /* defined(__LibV5Lite_Mac__V5ConferenceStatistics__) */
