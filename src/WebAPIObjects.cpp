﻿//
//  WebAPIObjects.cpp
//  V5ClientExample
//
//  Created by V-1094 on 2014/12/12.
//  Copyright (c) 2014年 V-cube. All rights reserved.
//
#include "precompile.h"
#include <iostream>

#include "WebAccess.h"
#include "WebAPIObjects.h"

#include "V5Log.h"

using namespace std;
using namespace picojson;

void showErrorMessage(picojson::value const& json)
{
    auto& va2 = json.get<object>();
    if (va2.at("result").to_str() != "OK") {
        string code = va2.at("status").to_str();
        string message = va2.at("description").to_str();
        V5LOG(V5_LOG_INFO,"Error code = %s\n", code.c_str());
        V5LOG(V5_LOG_INFO,"Message    = %s\n", message.c_str());
    }
}

bool V5JoinConferenceWebServer::parse(picojson::value const& response)
{
    return true;
}

bool V5JoinConference::parse(picojson::value const& response)
{
    if( ( !response.contains("result") ) ||
        ( !response.contains("detail") ) )
    {
        isParsed = false;
        return false;
    }
    
    auto& detail = response["detail"];
    
    if( ( !detail.contains("status") ) ||
        ( !detail.contains("description") ) )
    {
        isParsed = false;
        return false;
    }
    
    result      = response["result"].to_str();
    
    status      = detail["status"].to_str();
    description = detail["description"].to_str();

    isParsed = true;
    return true;
}

bool V5PrepareJoinConference::parse(picojson::value const& response)
{
    if( ( !response.contains("result") ) ||
        ( !response.contains("detail") )
       )
    {
        isParsed = false;
        return false;
    }
    
    result = response["result"].to_str();
    
    auto& detail = response["detail"];
    
    if( ( !detail.contains("status") )      ||
        ( !detail.contains("description") )
       )
    {
        isParsed = false;
        return false;
    }
    
    status          = detail["status"].to_str();
    description     = detail["description"].to_str();
    
    if( status != "200")
    {
        // statusが200以外の場合は必要なパラメータがオプションで吹かされてくるので、
        // 個別に判定せず、データの取得を行う。
        conferencePassword = response["conferencePassword"].to_str();
        requireDisplayName = response["requireDisplayName"].to_str();
        
        isParsed = true;
        return true;
    }
    
    if( ( !response.contains("conferenceAddress") )     ||
        ( !response.contains("displayName") )           ||
        ( !response.contains("un") )                    ||
        ( !response.contains("pw") )                    ||
        ( !response.contains("portalAddress") )         ||
        ( !response.contains("conferenceID") )          ||
        ( !response.contains("locationTag") )           ||
        ( !response.contains("isGuest") )
        )
    {
        isParsed = false;
        return false;
    }
    
    conferenceAddress  = response["conferenceAddress"].to_str();
    displayName        = response["displayName"].to_str();
    un                 = response["un"].to_str();
    pw                 = response["pw"].to_str();
    portalAddress      = response["portalAddress"].to_str();
    conferenceID       = response["conferenceID"].to_str();
    locationTag        = response["locationTag"].to_str();
    isGuest            = response["isGuest"].to_str();

    isParsed = true;
    return true;
}

bool V5StartSConferenceSession::parse(picojson::value const& response)
{
    if( ( !response.contains("result") ) ||
        ( !response.contains("detail") )
       )
    {
        isParsed = false;
        return false;
    }
    
    auto& detail = response["detail"];

    if( ( !detail.contains("status") )      ||
        ( !detail.contains("description") )
       )
    {
        isParsed = false;
        return false;
    }
    
    result      = response["result"].to_str();
    status      = detail["status"].to_str();
    
    if( result != "OK" || status != "200" )
    {
        // 必須バージョンを満たしていない場合のみrequireVersionが設定されてくるため、
        // statusコードを判定してからrequireVersionの判定を行う。
        if( status == "308" )
        {
            if( !response.contains("requireVersion") )
            {
            }
            requireVersion = response["requireVersion"].to_str();
        }
        
        // メンテナンス中のに入室処理を実行した場合のみメンテナンスの開始時間と
        // 終了時間が設定されてくるので、statusコードを判定してからパラメータの判定を行う。
        if( status == "309" )
        {
            if( ( !response.contains("maintenanceStartTime") ) ||
                ( !response.contains("maintenanceEndTime") ) )
            {
                isParsed = false;
                return false;
            }
            maintenanceStartTime = response["maintenanceStartTime"].to_str();
            maintenanceEndTime = response["maintenanceEndTime"].to_str();
        }
        isParsed = true;
        return true;
    }
    
    if (response.contains("permission"))
    {
        auto& permission = response.get("permission");
    }

    if (response.contains("logoUrl"))
    {
        auto& logoUrl = response.get("logoUrl");
        this->logoUrl = logoUrl.get<picojson::object>();
    }

    if( ( !response.contains("displayName") ) ||
        ( !response.contains("token") )
       )
    {
        isParsed = false;
        return false;
    }
    
    displayName = response["displayName"].to_str();
    token       = response["token"].to_str();
    
    isParsed = true;
    return true;
}

bool V5SendInvitationMail::parse(picojson::value const& response)
{
    if( ( !response.contains("result") ) ||
        ( !response.contains("detail") )
       )
    {
        isParsed = false;
        return false;
    }
    
    auto& detail = response["detail"];
    
    if( ( !detail.contains("status") )       ||
        ( !detail.contains("description") )
       )
    {
        isParsed = false;
        return false;
    }

    result = response["result"].to_str();
    
    status      = detail["status"].to_str();
    description = detail["description"].to_str();
    
    isParsed = true;
    return true;
}

