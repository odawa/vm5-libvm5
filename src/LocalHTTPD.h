﻿#pragma once
#ifndef INCLUDE_LOCAL_HTTPD_H
#define INCLUDE_LOCAL_HTTPD_H

#include <istream>
#include <memory>
#include <functional>
#include <Poco/Net/HTTPRequestHandlerFactory.h>
#include <Poco/Net/HTTPServerParams.h>
#include <Poco/Util/ServerApplication.h>
#include <V5Lite/RequestListenerCommon.h>

namespace Poco {
	namespace Net {
		class HTTPServer;
		class HTTPServerRequest;
		class HTTPServerResponse;
	}
}

// -------------------------------------------------------------------------------------------
class LocalHTTPD : public Poco::Util::ServerApplication, public std::enable_shared_from_this<LocalHTTPD>{
	explicit LocalHTTPD(){}

public:
	typedef Poco::Net::HTTPServerResponse HTTPServerResponse;
	typedef Poco::Net::HTTPServerRequest HTTPServerRequest;

	typedef std::map<std::string, std::string> requestQuery; // RequestQueryのKey, Valueを格納
	typedef bool(*httpResponseFunc)(std::shared_ptr<requestQuery> query, HTTPServerResponse& resp);
	typedef std::map<std::string, std::function<bool(std::shared_ptr<requestQuery>, HTTPServerResponse &)>> PathCallback;

	struct HTTPRequestParseResult{
		std::string method;
		std::string path;
		std::shared_ptr<requestQuery> query;
	};

	~LocalHTTPD() {}
	static std::shared_ptr<LocalHTTPD> createInstance() {
		return std::shared_ptr<LocalHTTPD>(new LocalHTTPD());
	}

    LocalHTTPD(const LocalHTTPD& rhs) {}    // コピーコンストラクタ利用禁止
    LocalHTTPD(LocalHTTPD&& rhs) {}         // ムーブコンストラクタ利用禁止
    LocalHTTPD& operator=(const LocalHTTPD& rhs) = delete;  // コピー代入演算子利用禁止
    LocalHTTPD& operator=(LocalHTTPD&& rhs) = delete;       // ムーブ代入演算子利用禁止
    
    // HTTPD起動
	V5HTTPD_START start(int listenPort);
    
    // HTTPD終了
    void stop();
    
    // コールバック登録
	bool addResouce(std::string, std::function<bool(std::shared_ptr<requestQuery>, HTTPServerResponse &)>);

    // コールバック削除
	bool removeResouce(std::string path);
    
    // コールバック呼び出し
	bool invokeCallback(std::shared_ptr<HTTPRequestParseResult> result, HTTPServerResponse& resp);

    // HTTPリクエストのパース
	bool paerseHTTPRequest(HTTPServerRequest& req, std::shared_ptr<HTTPRequestParseResult> result);
    
private:
	std::shared_ptr<Poco::Net::HTTPServer> httpServer;
	std::shared_ptr<Poco::Net::HTTPServer> httpServerV6;
    std::shared_ptr<Poco::ThreadPool> threadPool;
    Poco::Net::HTTPRequestHandlerFactory::Ptr requestHandlerFactory;
    Poco::Net::HTTPServerParams::Ptr serverParams;
    
	PathCallback httpResponse; // HTTP Response生成コールバックを格納
    
    void initialize();
    bool startV4(int listenPort);
    bool startV6(int listenPort);
};

#endif /* defined(INCLUDE_LOCAL_HTTPD_H) */
