﻿#include "precompile.h"
#include "ServiceLocator.h"

#ifdef USE_HTTPD
#include "V5HTTPD.h"
#endif

class StubHttpd : public IHttpd {
public:
	typedef std::weak_ptr<IV5HTTPDListener> listener_wp;
	explicit StubHttpd(listener_wp listener) {}
	virtual ~StubHttpd() {}

	V5HTTPD_START HTTPDStart(unsigned short port) { return V5HTTPD_START::OK; };
	void HTTPDStop() {};
};

std::shared_ptr<AbstractServiceLocator> AbstractServiceLocator::createInstance()
{
#ifdef USE_HTTPD
	return std::make_shared<ServiceLocator<V5HTTPD>>();
#else
	return std::make_shared<ServiceLocator<StubHttpd>>();
#endif 
}


