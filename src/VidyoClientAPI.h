﻿#pragma once

#ifndef VIDYO_CLIENT_H_INCLUDED
#define VIDYO_CLIENT_H_INCLUDED

#include <stddef.h>
#include <Vidyo/VidyoClient.h>
#include <string>
#include "DefaultFrameSize.h"

#ifdef VIDYO_MAX_EMAIL_ADDRESS_SIZE
#define VIDYO_HAS_RMOTE_SOURCE_CHANGED_EVENT 1
#define VIDYO_HAS_NEXT_SHARE_API 1
#else
#define VIDYO_HAS_RMOTE_SOURCE_CHANGED_EVENT 0
#define VIDYO_HAS_NEXT_SHARE_API 0
#endif


#define VIDYO_URL_MAX           URI_LEN
#define VIDYO_SERVER_PORT_MAX   SERVERPORT_SIZE
#define VIDYO_PROXY_UN_MAX      FIELD_SIZE
#define VIDYO_PROXY_PW_MAX      FIELD_SIZE


namespace vrms5
{
    namespace vidyo
    {
#ifdef V5DEBUG
        std::string toString(::VidyoClientOutEvent);
#else
        inline std::string const& toString(::VidyoClientOutEvent) { static std::string s; return s; }
#endif

        class VidyoClient
        {
        public:
            typedef ::VidyoClientCallState CallState;
            typedef ::VidyoClientInEvent InEvent;
            typedef ::VidyoClientRequest Request;
            typedef std::string const& arg_string;

            static bool isStarted()
            {
                return (VidyoClientIsStarted() == VIDYO_TRUE);
            }

            static CallState getCallStatus();
            static bool isIdle()
            {
                return (getCallStatus() == VIDYO_CLIENT_CALL_STATE_IDLE);
            }

            static bool login(arg_string portalAddress, arg_string un, arg_string pw);
            static bool loginCancel();
            static bool leave();
            static bool signOff();

            static bool getVideoMuted();
            static bool getAudioInMuted();
            static bool getAudioOutMuted();
            static bool setVideoMute(bool muted);
            static bool setAudioInMute(bool muted);
            static bool setAudioOutMute(bool muted);
            static bool setMaxParticipants(unsigned int maxParticipants);
            static bool setRendererVisible(bool visible);
            static bool startDynamicWatchVideoSource(unsigned int numParticipants, DefaultFrameSize &videoSize);
            static bool stopDynamicWatchVideoSource();
            
            static bool setBandWidth(unsigned int sLayer, int bandWidth );
            
            static VidyoUint getParticipants(VidyoClientRequestParticipants* participants);
            static VidyoUint getSelectedParticipants(VidyoClientRequestParticipants* participants);
            static VidyoUint getRemoteShares(VidyoClientRequestWindowShares* shares);

            static VidyoUint getConfiguration(VidyoClientRequestConfiguration* conf);
            static VidyoUint setConfiguration(VidyoClientRequestConfiguration& conf);
            
            static VidyoUint getMicrophoneVolume(VidyoClientRequestVolume* volume);
            static VidyoUint setMicrophoneVolume(VidyoClientRequestVolume& volume);
            static VidyoUint getSpeakerVolume(VidyoClientRequestVolume* volume);
            static VidyoUint setSpeakerVolume(VidyoClientRequestVolume& volume);
            
            static VidyoUint webProxySettingValid(VidyoClientRequestWebProxySettingValid *valid);
            
            static VidyoUint setSendMaxKBPS(int kbps);
            static VidyoUint getSendMaxKBPS(int *kbps);
            static VidyoUint setRecvMaxKBPS(int kbps);
            static VidyoUint getRecvMaxKBPS(int *kbps);

            template <typename params_t>
            static bool sendEvent(InEvent event, params_t& param)
            {
                return (::VidyoClientSendEvent(event, &param, sizeof(param)) != VIDYO_FALSE);
            }

            static bool sendEvent(InEvent event)
            {
                return (::VidyoClientSendEvent(event, NULL, 0) != VIDYO_FALSE);
            }

            template <typename params_t>
            static VidyoUint sendRequest(Request request, params_t& param)
            {
                return ::VidyoClientSendRequest(request, &param, sizeof(param));
            }

            template <typename params_t>
            static VidyoUint sendRequest(Request request, params_t* param)
            {
                return ::VidyoClientSendRequest(request, param, sizeof(*param));
            }
        };

    }
}

#endif
