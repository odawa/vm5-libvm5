﻿//
//  LibV5Lite.h
//  LibV5Lite
//
//  Created by V-1094 on 2015/01/14.
//  Copyright (c) 2015 V-cube, Inc. All rights reserved.
//
#pragma once
#ifndef INCLUDE_V5LITE_IMPL_H
#define INCLUDE_V5LITE_IMPL_H

#include <V5Lite/V5Client.h>

#include "V5WebsocketConnection.h"
#include "IConferenceClientEventSink.h"
#include "utils.h"
#include "EventFlag.h"
#include "DefaultFrameSize.h"

#include "VidyoClientAPI.h"
#include "V5LibTimer.h"
#include "ProxySetting.h"

#include "DeviceConfigurationImpl.h"

#include "V5ConferenceStatistics.h"


#include <boost/optional.hpp>

#include <atomic>
#include <cstddef>
#include <type_traits>
#include <future>

#include <chrono>

struct V5UData;

#ifdef __cplusplus

class ConferenceClient;
class IHttpd;
class HTTPDListener;
class AbstractServiceLocator;
class V5DeviceConfiguration;
class LibTimer;

class V5LibEventSink;

static_assert(std::is_same<V5Uint, VidyoUint>::value, "typedef error.");

class V5ClientImpl
    : public vrms5::V5Client
    , public vrms5::IConferenceClientEventSink
    , public std::enable_shared_from_this<V5ClientImpl>
    , vrms5::utils::noncopyable
{
	friend class HTTPDListener;

    struct VidyoParticipantInfo;

    typedef std::shared_ptr<std::string>            SharedString;
    typedef boost::optional<std::string>            OptionalString;
    typedef std::atomic_bool                        atomic_bool;
    typedef std::atomic_uint                        atomic_uint;

    typedef std::recursive_mutex                    mutex_t;
    typedef std::lock_guard<mutex_t>                lock_guard;
    
    typedef std::shared_ptr<VidyoParticipantInfo>   VidyoParticipantInfoPtrT;
    
    typedef std::chrono::steady_clock               steady_clock;
    
    enum V5ClientStatus
    {
        CLIENT_IDLE,
        START_CONFERENCE_SESSION,
        CLIENT_LINKED_SESSION,
        PREPARE_JOIN,
        SIGNIN_TO_MEDIA_SERVER,
        CONNECTING_TO_MEDIA_SERVER,
        CONNECTING_TO_CONFERENCE_SERVER,
        PROCESSING_REJECT_JOIN_CONFERENCE,
        PROCESSING_LEAVE_CONFERENCE,
        IN_CONFERENCE,
        TERMINATING,
    };

public:
    typedef std::shared_ptr<V5ClientImpl> self_ptr_t;

public:
    static V5CODE initialize(const char *logpath, const char *log_priority);
    static V5CODE finalize();
    static self_ptr_t createInstance();
    
private:
    V5ClientImpl();
    
public:
    virtual ~V5ClientImpl();

    bool isLogicStarted() const;

private:
    virtual bool isBusy() const override;
    
    virtual V5CODE startLogic(V5WindowId parentWindow, V5Rect *videoRect, const char *fontpath, const char *certificatepath) override;
    virtual V5CODE stopLogic() override;
    
    virtual V5CODE beginSession(const char *vcubeURL, const char *token, const char *version) override;
    virtual V5CODE joinConference(const char *displayName, const PasswdInfoPtrT& psswd) override;
    virtual void leaveConferenceSync() override;
    virtual AsyncResult leaveConferenceAsync() override;
    virtual V5CODE sendInviteMail(const char* mailAddress) override;

    virtual void onReceiveJoinConferenceRequest(const char *token, const char *V5LiteURL) override;
    
    virtual bool isGuest() override;
    virtual bool isCameraMuted() override;
    virtual bool isMicrophoneMuted() override;
    virtual bool isSpeakerMuted() override;
    
    virtual void muteCamera(bool muted, bool force = false) override;
    virtual void muteMicrophone(bool muted, bool force = false) override;
    virtual void muteSpeaker(bool muted, bool force = false) override;

    virtual V5CODE setFrameDirection(V5Rect rect) override;
    virtual V5CODE setOrientation(V5Orientation orie) override;
    
    virtual void setConferenceEventSink(std::weak_ptr<IV5ConferenceEventSink> const& sink) override;
    virtual void setConferenceEventSink(std::nullptr_t) override;
    virtual void setChatEventSink(std::weak_ptr<IV5ChatEventSink> const& chatSink) override;
    virtual void setChatEventSink(std::nullptr_t) override;
    virtual void setDeviceEventSink(std::weak_ptr<IV5DeviceEventSink> const& deviceSink) override;
    virtual void setDeviceEventSink(std::nullptr_t) override;
    virtual void sendChatMessage(const char* SendMessage) override;

    virtual bool startHTTPD(unsigned short port = 21920) override;
    virtual bool stopHTTPD() override;
    
    virtual std::shared_ptr<V5DeviceConfiguration> getDeviceConfiguration() override;
    
    virtual void lockConference(bool LockStatus) override;
    virtual void rejectParticipant(const char *RejectPID) override;
    virtual void updateConferenceName(const char *ConferenceName) override;
    
    virtual std::shared_ptr<const V5ConferenceStatus> getConferenceStatus() const override;
    virtual V5ParticipantList const& getParticipants() override;
    virtual std::shared_ptr<const V5Participant> getSelfParticipant() override;
    
    virtual void startWatchSharedRawFrame(const char * uri) override;
    virtual void stopWatchSharedRawFrame(const char * uri) override;
    
    virtual void enableActiveSpeakerLayout(bool showActiveSpeaker) override;
    virtual void changeSelfPreviewMode(V5ClientPreviewMode previewMode) override;
    virtual void setSelfViewLoopbackPolicy(V5ClientSelfViewLoopbackPolicy viewPolicy) override;

    virtual void shareAppWindow(V5WindowCapturerWindowId windowId, int width, int height) override;
    virtual void shareSysDesktop(V5Uint windowId, int width, int height) override;
    virtual void unshare() override;

    virtual void startWatchShared(const char * uri) override;
    virtual void stopWatchShared(const char * uri) override;
    
    virtual V5CODE changeParticipantVideoFrame(const char* participantURI, int width, int height, int frameRate, int minFrameInterval) override;

    virtual void DebugFuncCall() override;

    virtual bool isRawFrameMode() override;
    virtual void SendWorkerCrash() override;
    virtual WindowAndDesktopsPtrT getWindowsAndDesktops() override;
    virtual void setMaxParticipants(unsigned int maxParticipants) override;
    virtual unsigned int getMaxParticipants() override;

    // act only on raw frame rendering mode.
    virtual void forceRendering(std::shared_ptr<std::string> const& pid) override;
    virtual void removeForceRendering() override;

    void renderSelfView(bool render);
    
    virtual void setVideoFrameSize(int width, int height, int frameRate, int minFrameInterval) override;
    virtual void setShareFrameSize(int width, int height, int frameRate, int minFrameInterval) override;

    virtual void setSelectDeviceReserve(unsigned int type, unsigned int index ) override;
    virtual bool isCameraMuteChangeState() override ;
    
    virtual unsigned int getMicrophoneVolume() override ;
    virtual void setMicrophoneVolume( unsigned int volume ) override ;

    virtual unsigned int getSpeakerVolume() override ;
    virtual void setSpeakerVolume( unsigned int volume ) override ;

    virtual void setVideoPreferences(V5VideoPreferences preferences);
    virtual V5VideoPreferences getVideoPreferences();
    
    virtual void setSelfDisplayLabel(const char *aLavel) override;
    
    virtual void startStatisticsInfo( int millisecs ) override;
    virtual void startStatisticsInfo() override;
    virtual void stopStatisticsInfo() override;

    virtual bool setSendMaxKBPS(int kbps) override;
    virtual int getSendMaxKBPS() override;
    virtual bool setRecvMaxKBPS(int kbps) override;
    virtual int getRecvMaxKBPS() override;
    virtual bool setSendBandWidth( V5SLayer layer, V5SendBandWidth bandWidth ) override;
    
private:
    // conference 接続からのエラーハンドリング
    virtual void onParticipantJoined(V5Participant const& participant) override;
    virtual void onParticipantLeaved(V5Participant const& participant) override;

    virtual void onJoinedConferenceFail(V5ErrorInfo const& errInfo) override;
    virtual void onJoinedConference(V5ConferenceInfo const& conferenceInfo) override;
    virtual void onLeavedConference(V5ErrorInfo const& errInfo) override;
    virtual void onParticipantListUpdated(const V5Participant * const participants[], unsigned long length) override;
    virtual void onReconnectConference(V5ConferenceInfo const& conferenceInfo) override;
    
    virtual void onConferenceStatusUpdated(V5ConferenceStatus const& conferenceStatus) override;
    virtual void onAsyncError(V5ErrorInfo const& errInfo) override;
    virtual void onInviteMailIsSent(V5ErrorInfo const& errInfo, const char* status, const char* mailAddress) override;

    virtual void onRcvRejectParticipant(V5Participant const& participant) override;
    virtual void onPastTimeStart() override;
    virtual void onPastTimeStop() override;
    
    virtual void setFrameSize(DefaultFrameSize& sizeStruct, int width, int height, int frameRate, int minFrameInterval);
    
    virtual void setProxyForce( bool force ) override;
    virtual void setProxyIEOption( V5ProxyVidyoIEType option ) override;
    
    virtual void onConferenceNameChangeFailed( const char* nowConferenceName ) override;
    
    virtual void onPeriodicStatisticsChanged(V5BigStatistics const& stat) override;

    virtual void onSetOverTimeCountTimer( unsigned long startTime ) override;
    virtual void onCancelOverTimeCountTimer() override;
    
private:
    void rejectPrepareJoinConference(V5ErrorCode code, V5Component component, V5ErrorReason reason);
    void invokeAsyncCallBack(V5ErrorCode code, V5Component component, V5ErrorReason reason);
    void invokeInviteMailCallBack(V5ErrorCode code, V5Component component, V5ErrorReason reason,
                                  const char* status, const char* mailAddress);
    CONFCNCT_INT connectToConferenceServer(
        const std::string& uri,
        const std::string& TOKEN,
        const std::string& conferenceID,
        const std::string& FromPID,
        const std::string& DName);
    AsyncResult terminateConference(V5ErrorCode code,
                                    V5Component component,
                                    V5ErrorReason reason,
                                    std::string const& status);

    AsyncResult terminateConference(V5ErrorCode code, V5Component component, V5ErrorReason reason);
    AsyncResult terminateConference(V5ErrorInfo const& reason);
    AsyncResult disconnect();
    
    void transitionStatus(V5ClientStatus to);
    bool transitionStatus(V5ClientStatus from, V5ClientStatus to);

    AsyncResult disconnectCheck();
    
    void rejectBeginSession(V5ErrorCode code,
                            V5Component component,
                            V5ErrorReason reason,
                            std::string const& status);
    void rejectBeginSession(V5ErrorCode code, V5Component component, V5ErrorReason reason);
    void rejectBeginSession(V5ErrorInfo const& errInfo);

    void VersionUnmatched( const char *version );
    void NowMaintenance( std::string const& maintenanceStartTime, std::string const& maintenanceEndTime );
    
    void libFatalError( V5ErrorCode code, V5Component component, V5ErrorReason reason );
    
    void vidyoCallback(VidyoClientOutEvent actionType,
                       VidyoVoidPtr param,
                       VidyoUint paramSize);
    static void V5_CALLBACK s_callback(VidyoClientOutEvent actionType,
                                       VidyoVoidPtr param,
                                       VidyoUint paramSize,
                                       VidyoVoidPtr data);
    
    void onJoinConferenceTimeout();
    void onSelectedParticipantsUpdated(VidyoClientRequestParticipants const& selected);
    void notifyCurrentSelectedParticipants();
    void notifyAllAddedShare();

    void startWatchVideoSource(std::string &uri);
    void stopWatchVideoSource(std::string &uri);

    void setProxySetting(std::string const &address, long port, std::string const &id, std::string const &password);
    void setVidyoConfiguration();
    void updateDeviceConfiguration( VidyoClientRequestConfiguration const& conf );

    bool isUriSelf(std::string const& uri) const { return isUriSelf(uri.c_str()); }
    bool isUriSelf(const char * uri) const;
    
    void reservedParameterSettings();
    
    void onConferenceEndTimeOverNotice();
        
private:
    static atomic_bool initalized;
    static atomic_bool started;
    static VidyoClientLogParams logParams;
    
    V5ErrorInfo terminateReason;

    std::future<void> beginSessionResult;
    std::future<void> stopLogicResult;
    
    vrms5::utils::timer joinConfTime;
    std::future<void> joinConferenceResult;
    std::atomic<V5ClientStatus> currentStatus;

    std::shared_ptr<ConferenceClient> conferenceClient;
    std::shared_ptr<V5DeviceConfigurationImpl> devConf;
    
	std::shared_ptr<IHttpd> httpd;
	std::shared_ptr<HTTPDListener> httpdListener;
	std::shared_ptr<AbstractServiceLocator> serviceLocator;
    std::shared_ptr<Timer> joinConferenceTimer;
    std::shared_ptr<Timer> confEndTimeOverNoticeTimer;
    
    std::shared_ptr<V5StatisticsInfo> statInfo;
    
    vrms5::utils::ts_weak_ptr<IV5ConferenceEventSink> eventSink;
    vrms5::utils::ts_weak_ptr<IV5ChatEventSink> chatEventSink;
    vrms5::utils::ts_weak_ptr<IV5DeviceEventSink> deviceSink;

    vrms5::utils::EventFlag vidyoSignoutFlag;
    vrms5::utils::EventFlag vidyoIdleFlag;
    vrms5::utils::EventFlag confDisconnectedFlag;

    std::map<std::string, VidyoParticipantInfoPtrT> vidyoParticipantSpecificInfos;

    std::string sessionToken;
    std::string vcubeURL;
    V5UData *ud;
    
    char conferenceURL[V5_MAX_URL];
    char conferenceID[V5_MAX_ID];
    char displayName[V5_MAX_DNAME];
    std::atomic_bool GuestState;
    
    std::atomic_bool selectDeviceReserve;
    std::atomic_int  deviceIndexVideo;
    
    std::atomic_bool showActiveSpeakerReserve;
    std::atomic_bool nextShowActiveSpeaker;
    
    std::atomic_bool selfPreviewModeReserve;
    std::atomic<V5ClientPreviewMode> nextPreviewMode;
    
    bool isRetrieveRawFrame;

    // client selected peripheral states.
    atomic_bool speakerMuteState;
    atomic_bool speakerMuteRequesting;
    atomic_bool microphoneMuteState;
    atomic_bool microphoneMuteRequesting;
    atomic_bool cameraMuteState;
    atomic_bool cameraMuteChangeMtoUM;
    atomic_bool cameraMuteRequesting;
    // set mute if before conference start.

    // Vidyo ProxySetting Parameters.
    std::atomic_bool proxyForce;
    std::atomic_bool proxyForceChanged;
    std::atomic_bool proxyAddressChanged;
    std::atomic<V5ProxyVidyoIEType> proxyIEOption;
    std::atomic_bool proxyIEOptionChanged;
    // set proxyForce if before StartLogic API Call
    
    // set Send/Recv MaxKbps
    std::atomic_bool sendMaxKbpsReserve;
    std::atomic<int> sendMaxKbps;
    
    std::atomic_bool recvMaxKbpsReserve;
    std::atomic<int> recvMaxKbps;
    // set Send/Recv MaxKbps
    
    // ConferenceEndTime Over Timer
    std::atomic_bool confEndTimeOverTimerRunning;
    std::atomic<long long> confEndTimeOverNoticeStart;
    std::atomic<long> defaultNoticeTimeSpan;
    // ConferenceEndTime Over Timer
    
    atomic_uint maxParticipants;
    atomic_bool maxParticipantsRequesting;
    std::size_t currentNumberOfParticipants;

    std::shared_ptr<std::string> targetingPid;
    std::atomic_bool selfViewRenderingState;
    std::shared_ptr<std::vector<std::string>> activeParticipantUriList;
    std::mutex activeParticipantUriListMtx;
    std::atomic_bool renderingTargetPidOnlyMode;
    std::shared_ptr<vrms5::ProxySetting> proxySetting;
    V5VideoPreferences videoPreferences;
    
    DefaultFrameSize videoSize;
    DefaultFrameSize shareSize;

    mutable mutex_t mtx;

};

#endif // __cplusplus

#ifdef V5_WIN32
#define V5_CALLBACK WINAPI
#else
#define V5_CALLBACK
#endif

#endif // INCLUDE_V5LITE_IMPL_H
