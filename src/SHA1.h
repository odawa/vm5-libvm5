﻿//
//  SHA1.h
//  LibV5Lite
//
//  Created by V-1094 on 2015/01/26.
//  Copyright (c) 2015 V-cube, Inc. All rights reserved.
//

#ifndef __LIBV5_LITE_SHA1_H_
#define __LIBV5_LITE_SHA1_H_

#include <string>

std::string getSHA1String(const char * seed, std::size_t length);

inline std::string getSHA1String(std::string const & seed)
{
    return getSHA1String(seed.c_str(), seed.length());
}



#endif // __LIBV5_LITE_SHA1_H_
