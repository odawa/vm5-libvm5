﻿//
//  V5WebsocketConnection.cpp
//  LibV5Lite
//

#include "precompile.h"
#include "V5WebsocketConnection.h"
#include "IConferenceClientEventSink.h"

ConferenceClient::ConferenceClient()
{
    ClientInitialize();
    
    m_errInfoSet = false;
    
    proxyStorage = std::make_shared<ProxyStorage>();

    SetErrorReason();
    
    m_client.clear_access_channels( websocketpp::log::alevel::all );
    m_client.clear_error_channels( websocketpp::log::elevel::all );
    m_client.init_asio();
    m_client.start_perpetual();
    m_thread = make_shared<thread>( &client::run, &m_client );

    m_client.set_open_handler(bind(&ConferenceClient::onOpen,
                                   this,
                                   websocketpp::lib::placeholders::_1
                                   )
                              );
    
    m_client.set_fail_handler(bind(&ConferenceClient::onFail,
                                   this,
                                   websocketpp::lib::placeholders::_1
                                   )
                              );
    
    m_client.set_close_handler(bind(&ConferenceClient::onClose,
                                    this,
                                    websocketpp::lib::placeholders::_1
                                    )
                               );
    
    m_client.set_message_handler(bind(&ConferenceClient::onMessage,
                                      this,
                                      websocketpp::lib::placeholders::_1,
                                      websocketpp::lib::placeholders::_2
                                      )
                                 );
    
    m_client_tls.clear_access_channels( websocketpp::log::alevel::all );
    m_client_tls.clear_error_channels( websocketpp::log::elevel::all );
    m_client_tls.init_asio();
    m_client_tls.start_perpetual();
    m_thread_tls = make_shared<thread>( &client_tls::run, &m_client_tls );

    m_client_tls.set_open_handler(bind(&ConferenceClient::onOpen,
                                       this,
                                       websocketpp::lib::placeholders::_1
                                       )
                                  );
    
    m_client_tls.set_fail_handler(bind(&ConferenceClient::onFail,
                                       this,
                                       websocketpp::lib::placeholders::_1
                                       )
                                  );
    
    m_client_tls.set_close_handler(bind(&ConferenceClient::onClose,
                                        this,
                                        websocketpp::lib::placeholders::_1
                                        )
                                   );
    
    m_client_tls.set_message_handler(bind(&ConferenceClient::onMessage,
                                          this,
                                          websocketpp::lib::placeholders::_1,
                                          websocketpp::lib::placeholders::_2
                                          )
                                     );
    
    m_client_tls.set_tls_init_handler(bind(&ConferenceClient::onInitTLS,
                                           this,
                                           websocketpp::lib::placeholders::_1
                                           )
                                      );
    
}

ConferenceClient::~ConferenceClient()
{
    m_client.stop_perpetual();
    if( m_thread != nullptr )
    {
        m_thread->join();
    }

    m_client_tls.stop_perpetual();
    if( m_thread_tls != nullptr )
    {
        m_thread_tls->join();
    }
}

#if 0
void ConferenceClient::SetConferenceCallback( std::weak_ptr<IV5ConferenceEventSink> const& ConferenceEventSink_P )
{
    m_ConfCallbacks = ConferenceEventSink_P;
}
#endif

void ConferenceClient::SetChatCallback(std::nullptr_t)
{
	m_ChatCallbacks.reset();
}

void ConferenceClient::SetChatCallback(ChatCallbackArgT sink)
{
    m_ChatCallbacks = sink;
}

void ConferenceClient::SetLibCallback(std::nullptr_t)
{
	m_libCallbacks.reset();
}

void ConferenceClient::SetLibCallback(CallbackArgT sink)
{
    m_libCallbacks = sink;
}

void ConferenceClient::SetProxyInfo(std::string const& address,
                                    long const& port,
                                    std::string const& id,
                                    std::string const& pw )
{
    lock_guard_t lock(pxymtx);
    proxyStorage->update( address, port, id, pw );
}

CONFCNCT_INT ConferenceClient::Connect( const std::string& uri,
                                        const std::string& TOKEN,
                                        const std::string& conferenceID,
                                        const std::string& FromPID,
                                        const std::string& DName
                                      )
{
    V5LOG(V5_LOG_INFO,"[Websocket \"Connect\"] ---->");

    if( ( uri.length()   == 0 ) ||
        ( TOKEN.length() == 0 ) )
    {
        V5LOG(V5_LOG_ERROR,"IN Parameter Error.");
        V5LOG(V5_LOG_INFO,"[Websocket \"Connect\"] <----");
        return CCLIENT_CON_NON_PRM;
    }
    
    if( m_status != INITIALIZE )
    {
        V5LOG(V5_LOG_ERROR,"Already Connected To Conference Server.");
        V5LOG(V5_LOG_INFO,"[Websocket \"Connect\"] <----");
        return CCLIENT_CON_CONNECTED;
    }
    
#if 0
    V5LOG(V5_LOG_INFO,"URI = %s.",uri.c_str());
    V5LOG(V5_LOG_INFO,"TOKEN =  %s.",TOKEN.c_str());
    V5LOG(V5_LOG_INFO,"conferenceID = %s.",conferenceID.c_str());
    V5LOG(V5_LOG_INFO,"FromPID = %s.",FromPID.c_str());
    V5LOG(V5_LOG_INFO,"DisplayName = %s.",DName.c_str());
#endif
    const char *uri_buf = uri.c_str();
    
    if( strncmp( uri_buf , CONNECT_URI_FIRST_WORD_WSS, 3 ) == 0 )
    {
        m_connect_words = CCLIENT_CON_WORDS_WSS;
    }
    else if( strncmp( uri_buf , CONNECT_URI_FIRST_WORD_WS, 2 ) == 0 )
    {
        m_connect_words = CCLIENT_CON_WORDS_WS;
    }
    else
    {
        V5LOG(V5_LOG_ERROR,"Connect URI Error.");
//        V5LOG(V5_LOG_ERROR,"IN Parameter uri : %s.",uri.c_str());
        V5LOG(V5_LOG_INFO,"[Websocket \"Connect\"] <----");
        return CCLIENT_CON_WORD_NG;
    }
    
    std::string uri_query = uri;
    
    if( uri.at(uri.length() - 1) != '/' )
    {
        uri_query += "/";
    }
    
    userConURI  = uri_query;
    userTOKEN   = TOKEN;
    userConfID  = conferenceID;
    userPID     = FromPID;
    userDName   = DName;
    
    uri_query += conferenceID;
    uri_query += "/?method=connect&pid=";
    uri_query += FromPID;
    uri_query += "&token=";
    uri_query += TOKEN;
    uri_query += "&displayName=";
    uri_query += DName;
    
//    V5LOG(V5_LOG_INFO,"Connecting URI QUERY : %s",uri_query.c_str());
    
    CONFCNCT_INT ret = CCLIENT_ILLEGAL_ERROR;
    
    if(m_connect_words == CCLIENT_CON_WORDS_WS)
    {
        ret = Connect_WS( uri_query );
    }
    else if( m_connect_words == CCLIENT_CON_WORDS_WSS )
    {
        ret = Connect_WSS( uri_query );
    }
        
    if( ret == CCLIENT_OK )
    {
        m_ConfEndCallbackCalled = false;
        m_onJoinConfFailCall    = false;
        m_errInfoSet            = false;
        m_status                = CONNECTING;
    }
    else
    {
        ClientInitialize();
        V5LOG(V5_LOG_ERROR,"Connect Failed To Conference Server.");
    }
    
    V5LOG(V5_LOG_INFO,"[Websocket \"Connect\"] <----");

    return ret;
}

CONFCNCT_INT ConferenceClient::Reconnect()
{
    V5LOG(V5_LOG_INFO,"[Websocket \"Reconnect\"] ---->");
    
    if( ( m_status != INITIALIZE ) &&
        ( m_status != CLOSING ) )
    {
        V5LOG(V5_LOG_ERROR,"m_status : %d",m_status.load());
        V5LOG(V5_LOG_ERROR,"Already Connected To Conference Server.");
        V5LOG(V5_LOG_INFO,"[Websocket \"Reconnect\"] <----");
        return CCLIENT_RCN_CONNECT_ERR;
    }
    
    std::string recon_uri;
    recon_uri += userConURI;
    recon_uri += userConfID;
    recon_uri += "/?method=reconnect&pid=";
    recon_uri += userPID;
    recon_uri += "&token=";
    recon_uri += userTOKEN;
    recon_uri += "&displayName=";
    recon_uri += userDName;
    
//    V5LOG(V5_LOG_INFO,"Reconnecting uri : %s",recon_uri.c_str());
    
    CONFCNCT_INT ret = CCLIENT_ILLEGAL_ERROR;
    
    if( m_connect_words == CCLIENT_CON_WORDS_WS )
    {
        ret = Connect_WS( recon_uri );
    }
    else if( m_connect_words == CCLIENT_CON_WORDS_WSS )
    {
        ret = Connect_WSS( recon_uri );
    }
    
    if( ret == CCLIENT_OK )
    {
        m_ConfEndCallbackCalled = false;
        m_status = RECONNECTING;
    }
    else
    {
        V5LOG(V5_LOG_ERROR,"Reconnect Failed To Conference Server.");
        return CCLIENT_RCN_CONNECT_ERR;
    }
    
    V5LOG(V5_LOG_INFO,"[Websocket \"Reconnect\"] <----");
    
    return CCLIENT_OK;
}

CONFCNCT_INT ConferenceClient::SendMessage( const std::string& DestPID, const std::string& Message )
{
    V5LOG(V5_LOG_INFO,"[Websocket \"SendMessage\"] ---->");
    
    if( ( DestPID.length() == 0 ) ||
        ( Message.length() == 0 ) )
    {
        return CCLIENT_MSG_NON_PRM;
    }
    
    if( m_connect_words == CCLIENT_CON_WORDS_WS )
    {
        if( m_connection == nullptr )
        {
            V5LOG(V5_LOG_ERROR,"No Connect \"ws\" To Conference Server.");
            V5LOG(V5_LOG_INFO,"[Websocket \"SendMessage\"] <----");
            return CCLIENT_MSG_NON_CONNECT;
        }
    }
    else
    {
        if( m_connection_tls == nullptr )
        {
            V5LOG(V5_LOG_ERROR,"No Connect \"wss\" To Conference Server.");
            V5LOG(V5_LOG_INFO,"[Websocket \"SendMessage\"] <----");
            return CCLIENT_MSG_NON_CONNECT;
        }
    }
    
    if( m_status != SENDMSG_OK )
    {
        return CCLIENT_MSG_SEND_STS_NG;
    }
    
//    V5LOG(V5_LOG_INFO,"IN Param Message : %s",Message.c_str());

    std::string EncodeMsg;
    vrms5::utils::base64Encode( Message, &EncodeMsg );

//    V5LOG(V5_LOG_INFO,"Message to Base 64 Encode!");
//    V5LOG(V5_LOG_INFO,"Encode Message : %s",EncodeMsg.c_str());
    
    picojson::object obj;
    obj["event"] = std::string(CHAT_EV_SND_SNDMSG);
    obj["from"] = std::string(userPID);
    obj["dest"] = std::string(DestPID);
    obj["conference"] = std::string(userConfID);
//    obj["data"] = std::string(EncodeMsg);
    obj["data"] = std::string(Message);
    picojson::value val(obj);
    
//    V5LOG(V5_LOG_INFO, "Message Data : %s",val.serialize().c_str());
    
    error_code ec;
    
    if( m_connect_words == CCLIENT_CON_WORDS_WS )
    {
        m_client.send( m_connection->get_handle() ,
                       val.serialize() ,
                       websocketpp::frame::opcode::text ,
                       ec
                      );
    }
    else
    {
        m_client_tls.send( m_connection_tls->get_handle() ,
                           val.serialize() ,
                           websocketpp::frame::opcode::text ,
                           ec
                          );
    }
    
    if( ec )
    {
        V5LOG(V5_LOG_INFO,"SendMessage Send Error.");
        
        /* 以降、メッセージ送信の正常が見込めないため、 */
        /* 強制切断して、全情報を解放後、エラーの返り値を返却する。 */
        if( m_errInfoSet != true )
        {
            m_errInfoSet = true;
            auto errorInfo = std::make_shared<V5ErrorInfo>();
            errorInfo->code_deprecated = V5_ECODE_WEBSOCKET_SENDMESSAGE;
            errorInfo->category  = V5_COMPONENT_CONFERENCE;
            errorInfo->reason    = V5_CONFERENCE_CLIENT_EVENT_TYPE_SEND_MESSAGE_SENDING_ERROR;
            std::atomic_store( &m_errInfo, errorInfo );
        }
        
        CloseConnection();
        ClientInitialize();
        
        auto callback = m_libCallbacks.lock();
        if( callback )
        {
            if( m_ConfEndCallbackCalled == false )
            {
                m_ConfEndCallbackCalled = true;
                if( m_status <= EST_RCV_STANDBY )
                {
                    V5LOG(V5_LOG_INFO,"onJoinedConferenceFail Called.");
                    callback->onJoinedConferenceFail( *std::atomic_load( &m_errInfo ) );
                }
                else
                {
                    V5LOG(V5_LOG_INFO,"onLeavedConference Called.");
                    callback->onLeavedConference( *std::atomic_load( &m_errInfo ) );
                }
            }
        }
        
        V5LOG(V5_LOG_INFO,"[Websocket \"SendMessage\"] <----");
        return CCLIENT_MSG_SEND_ERR;
    }
    
    V5LOG(V5_LOG_INFO,"[Websocket \"SendMessage\"] <----");
    return CCLIENT_OK;
}

CONFCNCT_INT ConferenceClient::Close()
{
    V5LOG(V5_LOG_INFO,"[Websocket \"Close\"] ---->");
    
    if( ( m_status == CONNECTING ) ||
        ( m_status == RECONNECTING ) )
    {
        m_close_try = true;
        V5LOG(V5_LOG_INFO,"[Websocket \"Close\"] <----");
        return CCLIENT_CLS_RESERVE;
    }
    
    if( ( m_status == INITIALIZE ) ||
        ( m_status == CLOSING ) )
    {
        V5LOG(V5_LOG_INFO,"No Connection To Conference Server.");
        V5LOG(V5_LOG_INFO,"[Websocket \"Close\"] <----");
        return CCLIENT_OK;
    }
    
    if( m_connect_words == CCLIENT_CON_WORDS_WS )
    {
        if( m_connection == nullptr )
        {
            V5LOG(V5_LOG_ERROR,"No Connect \"ws\" To Conference Server.");
            V5LOG(V5_LOG_INFO,"[Websocket \"Close\"] <----");
            return CCLIENT_CLS_NON_CONNECT;
        }
    }
    else
    {
        if( m_connection_tls == nullptr )
        {
            V5LOG(V5_LOG_ERROR,"No Connect \"wss\" To Conference Server.");
            V5LOG(V5_LOG_INFO,"[Websocket \"Close\"] <----");
            return CCLIENT_CLS_NON_CONNECT;
        }
    }
    
    m_pongflg = false;
    
    picojson::object obj;
    obj["event"] = std::string(CHAT_EV_SND_RMVFMCONF);
    obj["from"] = userPID;
    obj["dest"] = std::string("");
    obj["conference"] = userConfID;
    obj["data"] = userTOKEN;
    picojson::value val(obj);
    
//    V5LOG(V5_LOG_INFO, "Message Data : %s",val.serialize().c_str());
    
    error_code ec;
    
    if( m_connect_words == CCLIENT_CON_WORDS_WS )
    {
        m_client.send( m_connection->get_handle() ,
                       val.serialize() ,
                       websocketpp::frame::opcode::text ,
                       ec
                      );
    }
    else
    {
        m_client_tls.send( m_connection_tls->get_handle() ,
                           val.serialize() ,
                           websocketpp::frame::opcode::text ,
                           ec
                          );
    }
    
    if( ec )
    {        
        V5LOG(V5_LOG_ERROR,"Close Message Send Error.");
        /* 以降、メッセージ送信の正常が見込めないため、 */
        /* 強制切断して、全情報を解放後、エラーの返り値を返却する。 */
        if( m_errInfoSet != true )
        {
            m_errInfoSet = true;
            auto errorInfo = std::make_shared<V5ErrorInfo>();
            errorInfo->code_deprecated = V5_ECODE_WEBSOCKET_CLOSE;
            errorInfo->category  = V5_COMPONENT_CONFERENCE;
            errorInfo->reason    = V5_CONFERENCE_CLIENT_EVENT_TYPE_REMOVE_FROM_CONFERENCE_SEND_ERROR;
            std::atomic_store( &m_errInfo, errorInfo );
        }
        
        CloseConnection();
        ClientInitialize();
        
        auto callback = m_libCallbacks.lock();
        if( callback )
        {
            if( m_ConfEndCallbackCalled == false )
            {
                m_ConfEndCallbackCalled = true;
                if( m_status <= EST_RCV_STANDBY )
                {
                    V5LOG(V5_LOG_INFO,"onJoinedConferenceFail Called.");
                    callback->onJoinedConferenceFail( *std::atomic_load( &m_errInfo ) );
                }
                else
                {
                    V5LOG(V5_LOG_INFO,"onLeavedConference Called.");
                    callback->onLeavedConference( *std::atomic_load( &m_errInfo ) );
                }
            }
        }
        
        V5LOG(V5_LOG_INFO,"[Websocket \"Close\"] <----");
        return CCLIENT_CLS_SEND_ERR;
    }
    
    SetMsgTimer(CLOSEFROMSERVER_RCV_WAITTIME, WEBSOCKET_TO_EV_CLOSEFROMSERVER );
    
    m_status = CLOSING;
    
    V5LOG(V5_LOG_INFO,"[Websocket \"Close\"] <----");
    return CCLIENT_OK;
}

CONFCNCT_INT ConferenceClient::RejectParticipant( const std::string& tgtPID )
{
    V5LOG(V5_LOG_INFO,"[Websocket \"RejectParticipant\"] ---->");
    
    if( tgtPID.length() == 0 )
    {
        return CCLIENT_REJ_NON_PRM;
    }
    
    if( m_status != SENDMSG_OK )
    {
        V5LOG(V5_LOG_ERROR,"Message Send Status NG.");
        V5LOG(V5_LOG_INFO,"[Websocket \"RejectParticipant\"] <----");
        return CCLIENT_REJ_SEND_STS_NG;
    }

    if( m_connect_words == CCLIENT_CON_WORDS_WS )
    {
        if( m_connection == nullptr )
        {
            V5LOG(V5_LOG_ERROR,"No Connect \"ws\" To Conference Server.");
            V5LOG(V5_LOG_INFO,"[Websocket \"SendMessage\"] <----");
            return CCLIENT_REJ_NON_CONNECT;
        }
    }
    else
    {
        if( m_connection_tls == nullptr )
        {
            V5LOG(V5_LOG_ERROR,"No Connect \"wss\" To Conference Server.");
            V5LOG(V5_LOG_INFO,"[Websocket \"SendMessage\"] <----");
            return CCLIENT_REJ_NON_CONNECT;
        }
    }
    
    picojson::object obj;
    obj["event"] = std::string(CHAT_EV_SND_REJCTPTCPNT);
    obj["from"] = userPID;
    obj["dest"] = tgtPID;
    obj["conference"] = userConfID;
    picojson::value val(obj);
    
//    V5LOG(V5_LOG_INFO, "Message Data : %s",val.serialize().c_str());
    
    error_code ec;
    
    if( m_connect_words == CCLIENT_CON_WORDS_WS )
    {
        m_client.send( m_connection->get_handle() ,
                       val.serialize() ,
                       websocketpp::frame::opcode::text ,
                       ec
                      );
    }
    else
    {
        m_client_tls.send( m_connection_tls->get_handle() ,
                           val.serialize() ,
                           websocketpp::frame::opcode::text ,
                           ec
                          );
    }
    
    if( ec )
    {
        V5LOG(V5_LOG_ERROR,"RejectParticipant Message Send Error.");
        /* 以降、メッセージ送信の正常が見込めないため、 */
        /* 強制切断して、全情報を解放後、エラーの返り値を返却する。 */
        if( m_errInfoSet != true )
        {
            m_errInfoSet = true;
            auto errorInfo = std::make_shared<V5ErrorInfo>();
            errorInfo->code_deprecated = V5_ECODE_WEBSOCKET_REJECTPARTICIPANT;
            errorInfo->category  = V5_COMPONENT_CONFERENCE;
            errorInfo->reason    = V5_CONFERENCE_CLIENT_EVENT_TYPE_REJECT_PARTICIPANT_SEND_ERROR;
            std::atomic_store( &m_errInfo, errorInfo );
        }
        
        CloseConnection();
        ClientInitialize();
        
        auto callback = m_libCallbacks.lock();
        if( callback )
        {
            if( m_ConfEndCallbackCalled == false )
            {
                m_ConfEndCallbackCalled = true;
                if( m_status <= EST_RCV_STANDBY )
                {
                    V5LOG(V5_LOG_INFO,"onJoinedConferenceFail Called.");
                    callback->onJoinedConferenceFail( *std::atomic_load( &m_errInfo ) );
                }
                else
                {
                    V5LOG(V5_LOG_INFO,"onLeavedConference Called.");
                    callback->onLeavedConference( *std::atomic_load( &m_errInfo ) );
                }
            }
        }
        
        V5LOG(V5_LOG_INFO,"[Websocket \"RejectParticipant\"] <----");
        return CCLIENT_REJ_SEND_ERR;
    }
    
    V5LOG(V5_LOG_INFO,"[Websocket \"RejectParticipant\"] <----");
    return CCLIENT_OK;
}

CONFCNCT_INT ConferenceClient::LockConference( bool LockStatus )
{
    V5LOG(V5_LOG_INFO,"[Websocket \"LockConference\"] ---->");
    
    if( m_status != SENDMSG_OK )
    {
        V5LOG(V5_LOG_ERROR,"Message Send Status NG.");
        V5LOG(V5_LOG_INFO,"[Websocket \"LockConference\"] <----");
        return CCLIENT_LOC_SEND_STS_NG;
    }

    if( m_connect_words == CCLIENT_CON_WORDS_WS )
    {
        if( m_connection == nullptr )
        {
            V5LOG(V5_LOG_ERROR,"No Connect \"ws\" To Conference Server.");
            V5LOG(V5_LOG_INFO,"[Websocket \"LockConference\"] <----");
            return CCLIENT_LOC_NON_CONNECT;
        }
    }
    else
    {
        if( m_connection_tls == nullptr )
        {
            V5LOG(V5_LOG_ERROR,"No Connect \"wss\" To Conference Server.");
            V5LOG(V5_LOG_INFO,"[Websocket \"LockConference\"] <----");
            return CCLIENT_LOC_NON_CONNECT;
        }
    }
    
    picojson::object obj;
    obj["event"] = std::string(CHAT_EV_SND_LOCKCONF);
    obj["from"] = userPID;
    obj["dest"] = std::string("");
    obj["conference"] = userConfID;
    obj["data"] = std::string((LockStatus?"true":"false"));
    picojson::value val(obj);
    
//    V5LOG(V5_LOG_INFO, "Message Data : %s",val.serialize().c_str());
    
    error_code ec;
    
    if( m_connect_words == CCLIENT_CON_WORDS_WS )
    {
        m_client.send( m_connection->get_handle() ,
                       val.serialize() ,
                       websocketpp::frame::opcode::text ,
                       ec
                      );
    }
    else
    {
        m_client_tls.send( m_connection_tls->get_handle() ,
                           val.serialize() ,
                           websocketpp::frame::opcode::text ,
                           ec
                          );
    }
    
    if( ec )
    {
        V5LOG(V5_LOG_ERROR,"LockConference Message Send Error.");
        /* 以降、メッセージ送信の正常が見込めないため、 */
        /* 強制切断して、全情報を解放後、エラーの返り値を返却する。 */
        if( m_errInfoSet != true )
        {
            m_errInfoSet = true;
            auto errorInfo = std::make_shared<V5ErrorInfo>();
            errorInfo->code_deprecated = V5_ECODE_WEBSOCKET_LOCKCONFERENCE;
            errorInfo->category  = V5_COMPONENT_CONFERENCE;
            errorInfo->reason    = V5_CONFERENCE_CLIENT_EVENT_TYPE_LOCK_CONFERENCE_SENDING_ERROR;
            std::atomic_store( &m_errInfo, errorInfo );
        }
        
        CloseConnection();
        ClientInitialize();
        
        auto callback = m_libCallbacks.lock();
        if( callback )
        {
            if( m_ConfEndCallbackCalled == false )
            {
                m_ConfEndCallbackCalled = true;
                if( m_status <= EST_RCV_STANDBY )
                {
                    V5LOG(V5_LOG_INFO,"onJoinedConferenceFail Called.");
                    callback->onJoinedConferenceFail( *std::atomic_load( &m_errInfo ) );
                }
                else
                {
                    V5LOG(V5_LOG_INFO,"onLeavedConference Called.");
                    callback->onLeavedConference( *std::atomic_load( &m_errInfo ) );
                }
            }
        }
        
        V5LOG(V5_LOG_INFO,"[Websocket \"LockConference\"] <----");
        return CCLIENT_REJ_SEND_ERR;
    }
    
    V5LOG(V5_LOG_INFO,"[Websocket \"LockConference\"] <----");
    return CCLIENT_OK;
}

CONFCNCT_INT ConferenceClient::UpdateConferenceName( const std::string& NewConfName )
{
    V5LOG(V5_LOG_INFO,"[Websocket \"UpdateConferenceName\"] ---->");
    
    if( m_status != SENDMSG_OK )
    {
        V5LOG(V5_LOG_ERROR,"Message Send Status NG.");
        V5LOG(V5_LOG_INFO,"[Websocket \"LockConference\"] <----");
        return CCLIENT_LOC_SEND_STS_NG;
    }
    
    if( m_connect_words == CCLIENT_CON_WORDS_WS )
    {
        if( m_connection == nullptr )
        {
            V5LOG(V5_LOG_ERROR,"No Connect \"ws\" To Conference Server.");
            V5LOG(V5_LOG_INFO,"[Websocket \"LockConference\"] <----");
            return CCLIENT_LOC_NON_CONNECT;
        }
    }
    else
    {
        if( m_connection_tls == nullptr )
        {
            V5LOG(V5_LOG_ERROR,"No Connect \"wss\" To Conference Server.");
            V5LOG(V5_LOG_INFO,"[Websocket \"LockConference\"] <----");
            return CCLIENT_LOC_NON_CONNECT;
        }
    }
    
    picojson::object obj;
    obj["event"] = std::string(CHAT_EV_SND_UPDATECNAME);
    obj["from"] = userPID;
    obj["dest"] = std::string("");
    obj["conference"] = userConfID;
    obj["data"] = std::string(NewConfName);
    picojson::value val(obj);
    
//    V5LOG(V5_LOG_INFO, "Message Data : %s",val.serialize().c_str());
    
    error_code ec;
    
    if( m_connect_words == CCLIENT_CON_WORDS_WS )
    {
        m_client.send( m_connection->get_handle() ,
                       val.serialize() ,
                       websocketpp::frame::opcode::text ,
                       ec
                      );
    }
    else
    {
        m_client_tls.send( m_connection_tls->get_handle() ,
                           val.serialize() ,
                           websocketpp::frame::opcode::text ,
                           ec
                          );
    }
    
    if( ec )
    {
        V5LOG(V5_LOG_ERROR,"LockConference Message Send Error.");
        /* 以降、メッセージ送信の正常が見込めないため、 */
        /* 強制切断して、全情報を解放後、エラーの返り値を返却する。 */
        if( m_errInfoSet != true )
        {
            m_errInfoSet = true;
            auto errorInfo = std::make_shared<V5ErrorInfo>();
            errorInfo->code_deprecated = V5_ECODE_WEBSOCKET_UPDATECONFERENCENAME;
            errorInfo->category  = V5_COMPONENT_CONFERENCE;
            errorInfo->reason    = V5_CONFERENCE_CLIENT_EVENT_TYPE_UPDATE_CONFERENCE_NAME_SENDING_ERROR;
            std::atomic_store( &m_errInfo, errorInfo );
        }
        
        CloseConnection();
        ClientInitialize();
        
        auto callback = m_libCallbacks.lock();
        if( callback )
        {
            if( m_ConfEndCallbackCalled == false )
            {
                m_ConfEndCallbackCalled = true;
                if( m_status <= EST_RCV_STANDBY )
                {
                    V5LOG(V5_LOG_INFO,"onJoinedConferenceFail Called.");
                    callback->onJoinedConferenceFail( *std::atomic_load( &m_errInfo ) );
                }
                else
                {
                    V5LOG(V5_LOG_INFO,"onLeavedConference Called.");
                    callback->onLeavedConference( *std::atomic_load( &m_errInfo ) );
                }
            }
        }
        
        V5LOG(V5_LOG_INFO,"[Websocket \"UpdateConferenceName\"] <----");
        return CCLIENT_REJ_SEND_ERR;
    }
    
    V5LOG(V5_LOG_INFO,"[Websocket \"UpdateConferenceName\"] <----");
    return CCLIENT_OK;
}


ParticipantPtrT ConferenceClient::GetSelfParticipant() const
{
    V5LOG(V5_LOG_INFO,"[Websocket \"GetSelfParticipant\"] ---->");
    lock_guard_t lock(mtx);
    return participantListStorage->getSelfCluster();
}

V5ConferenceStatusPtrT ConferenceClient::GetConferenceStatus() const
{
    V5LOG(V5_LOG_INFO,"[Websocket \"GetConferenceStatus\"] ---->");
    return std::move(std::atomic_load(&ConfStsCluster));
}


/************************************************************************************************/
/* Private Method                                                                               */
/************************************************************************************************/
CONFCNCT_INT ConferenceClient::Connect_WS( const std::string& uri )
{
    V5LOG(V5_LOG_INFO,"[Websocket \"Connect_WS\"] ---->");

    error_code ec;
    m_connection = m_client.get_connection( uri, ec );
    if ( ec )
    {
        V5LOG(V5_LOG_ERROR,"get_connection Failed.");
        V5LOG(V5_LOG_ERROR,"Error Code : %d",ec.value());
        V5LOG(V5_LOG_ERROR,"Error Reason : %s",ec.message().c_str());
        V5LOG(V5_LOG_INFO,"[Websocket \"Connect_WS\"] <----");
        return CCLIENT_CON_CONNECT_NG;
    }
    
    m_connection->set_termination_handler(bind(&ConferenceClient::onTermination,
                                               this,
                                               websocketpp::lib::placeholders::_1
                                               )
                                          );
    
    lock_guard_t lock(pxymtx);
    if( proxyStorage->isProxy() )
    {
        std::string proxyAddress = proxyStorage->getAddress();
        proxyAddress += ":";
        proxyAddress += proxyStorage->getPort();
        m_connection->set_proxy( proxyAddress, ec );
        if( ec )
        {
            V5LOG(V5_LOG_ERROR,"Proxy URI Setting Failed.");
            V5LOG(V5_LOG_ERROR,"Error Reason : %s",ec.message().c_str());
            V5LOG(V5_LOG_INFO,"[Websocket \"Connect_WS\"] <----");
            return CCLIENT_CON_PXY_URI_SET_NG;
        }
        
        V5LOG(V5_LOG_INFO,"Proxy Set OK.");
        
        if( proxyStorage->getID().c_str() != 0 )
        {
            m_connection->set_proxy_basic_auth( proxyStorage->getID(), proxyStorage->getPassword(), ec );
            if( ec )
            {
                V5LOG(V5_LOG_ERROR,"Proxy AUTH Setting Failed.");
                V5LOG(V5_LOG_ERROR,"Error Reason : %s",ec.message().c_str());
                V5LOG(V5_LOG_INFO,"[Websocket \"Connect_WS\"] <----");
                return CCLIENT_CON_PXY_AUTH_SET_NG;
            }
            
            V5LOG(V5_LOG_ERROR,"ProxyAddress : %s",proxyAddress.c_str());
            V5LOG(V5_LOG_ERROR,"ProxyID      : %s",proxyStorage->getID().c_str());
            V5LOG(V5_LOG_ERROR,"ProxyPassword: %s",proxyStorage->getPassword().c_str());
            
            V5LOG(V5_LOG_INFO,"Proxy Auth Set OK.");
        }

    }
    else
    {
        V5LOG(V5_LOG_ERROR,"Proxy Non Setting.");
    }
    
    m_client.connect(m_connection);

    V5LOG(V5_LOG_INFO,"[Websocket \"Connect_WS\"] <----");
    return CCLIENT_OK;
}

CONFCNCT_INT ConferenceClient::Connect_WSS( const std::string& uri )
{
    V5LOG(V5_LOG_INFO,"[Websocket \"Connect_WSS\"] ---->");
    
    error_code ec;
    m_connection_tls = m_client_tls.get_connection( uri, ec );
    if ( ec )
    {
        V5LOG(V5_LOG_ERROR,"get_connection Failed.");
        V5LOG(V5_LOG_ERROR,"Error Code : %d",ec.value());
        V5LOG(V5_LOG_ERROR,"Error Reason : %s",ec.message().c_str());
        V5LOG(V5_LOG_INFO,"[Websocket \"Connect_WSS\"] <----");
        return CCLIENT_CON_CONNECT_NG;
    }
    
    m_connection_tls->set_termination_handler(bind(&ConferenceClient::onTermination,
                                                   this,
                                                   websocketpp::lib::placeholders::_1
                                                   )
                                              );
    
    lock_guard_t lock(pxymtx);
    if( proxyStorage->isProxy() )
    {
        std::string proxyAddress = proxyStorage->getAddress();
        proxyAddress += ":";
        proxyAddress += proxyStorage->getPort();
        m_connection_tls->set_proxy( proxyAddress, ec );
        if( ec )
        {
            V5LOG(V5_LOG_ERROR,"Proxy Setting Failed.");
            V5LOG(V5_LOG_ERROR,"Error Reason : %s",ec.message().c_str());
            V5LOG(V5_LOG_INFO,"[Websocket \"Connect_WSS\"] <----");
            return CCLIENT_CON_PXY_URI_SET_NG;
        }
        
        V5LOG(V5_LOG_INFO,"Proxy Set OK.");
        
        if( proxyStorage->getID().c_str() != 0 )
        {
            m_connection_tls->set_proxy_basic_auth( proxyStorage->getID(), proxyStorage->getPassword(), ec );
            if( ec )
            {
                V5LOG(V5_LOG_ERROR,"Proxy AUTH Setting Failed.");
                V5LOG(V5_LOG_ERROR,"Error Reason : %s",ec.message().c_str());
                V5LOG(V5_LOG_INFO,"[Websocket \"Connect_WS\"] <----");
                return CCLIENT_CON_PXY_AUTH_SET_NG;
            }
            
            V5LOG(V5_LOG_ERROR,"ProxyAddress : %s",proxyAddress.c_str());
            V5LOG(V5_LOG_ERROR,"ProxyID      : %s",proxyStorage->getID().c_str());
            V5LOG(V5_LOG_ERROR,"ProxyPassword: %s",proxyStorage->getPassword().c_str());
            
            V5LOG(V5_LOG_INFO,"Proxy Auth Set OK.");
        }
    }
    else
    {
        V5LOG(V5_LOG_INFO,"Proxy Non Setting.");
    }
    
    m_client_tls.connect(m_connection_tls);

    V5LOG(V5_LOG_INFO,"[Websocket \"Connect_WSS\"] <----");
    return CCLIENT_OK;
}

void ConferenceClient::ClientInitialize(  )
{
    V5LOG(V5_LOG_INFO,"[Websocket \"ClientInitialize\"] ---->");
    
    m_status        = INITIALIZE;
    
    m_connect_words = CCLIENT_CON_WORDS_INIT;
    
    userConURI      = "";
    userTOKEN       = "";
    userPID         = "";
    userDName       = "";
    
    m_pongflg               = false;
    
    m_pastTimeSts           = false;
    m_close_try             = false;
    m_reconnect_try         = false;
    m_confEndTimeOverTimerStatus = false;
//    m_connection        = nullptr;
//    m_connection_tls    = nullptr;
    {
        lock_guard_t lock(mtx);
        participantListStorage.reset();
        chatLogStorage.reset();
    }
    std::atomic_store(&ConfStsCluster, decltype(ConfStsCluster)(nullptr));
    m_est_timer_ptr     = nullptr;
    m_cfs_timer_ptr     = nullptr;
    
//    m_ConfCallbacks = nullptr;
//    m_ChatCallbacks = nullptr;
    
    V5LOG(V5_LOG_INFO,"[Websocket \"ClientInitialize\"] <----");

    return;
}

void ConferenceClient::CloseConnection()
{
    V5LOG(V5_LOG_ERROR,"[Websocket \"CloseConnection\"] ---->");
    
    m_pongflg = false;
    
    error_code ec;
    std::string reason = "CloseFromServer Recive Timeout.";
    
    if( m_connect_words == CCLIENT_CON_WORDS_WS )
    {
        if( m_connection != nullptr )
        {
            m_client.close( m_connection->get_handle(),
                            websocketpp::close::status::normal,
                            reason,
                            ec
                           );
            
        }
    }
    else
    {
        if( m_connection_tls != nullptr )
        {
            m_client_tls.close( m_connection_tls->get_handle(),
                                websocketpp::close::status::normal,
                                reason,
                                ec
                               );
        }
    }
    
    if (ec) {
        V5LOG(V5_LOG_ERROR,"Connection Close Error.");
        V5LOG(V5_LOG_ERROR,"Error Message : %s",ec.message().c_str());
    }
    
    V5LOG(V5_LOG_ERROR,"[Websocket \"CloseConnection\"] <----");
}

void ConferenceClient::SetMsgTimer( long latency, V5TIMEOUT_EVENT_LIST Event )
{
    V5LOG(V5_LOG_INFO,"[Websocket \"SetMsgTimer\"] ---->");
    
    V5LOG(V5_LOG_INFO,"Event : %d",Event);
    V5LOG(V5_LOG_INFO,"latency : %d",latency);
    
    timer_ptr tmp_timer_ptr;
    
    if (m_connect_words == CCLIENT_CON_WORDS_WS )
    {
        if( m_connection != nullptr )
        {
            tmp_timer_ptr =
            m_client.set_timer(latency,
                               bind(&ConferenceClient::onTimeout,
                                    this,
                                    (unsigned int)Event,
                                    websocketpp::lib::placeholders::_1
                                    )
                               );

        }
    }
    else
    {
        if( m_connection_tls != nullptr )
        {
            tmp_timer_ptr =
            m_client_tls.set_timer(latency,
                                   bind(&ConferenceClient::onTimeout,
                                        this,
                                        (unsigned int)Event,
                                        websocketpp::lib::placeholders::_1
                                        )
                                   );

        }
    }
    
    switch( Event )
    {
        case WEBSOCKET_TO_EV_ESTABLISHED:
        {
            m_est_timer_ptr = tmp_timer_ptr;
        }
        break;
            
        case WEBSOCKET_TO_EV_CLOSEFROMSERVER:
        {
            m_cfs_timer_ptr = tmp_timer_ptr;
        }
        break;
        default:
        {
            V5LOG(V5_LOG_INFO,"Timer Event Type Unexpected.");
        }
        break;
    }
    
    V5LOG(V5_LOG_INFO,"[Websocket \"SetMsgTimer\"] <----");
    return;
}

/*****************************/
/* Websocket Callback Method */
/*****************************/

void ConferenceClient::onOpen( websocketpp::connection_hdl hdl )
{
    V5LOG(V5_LOG_INFO,"[Websocket \"onOpen\"] ---->");

    if( m_status == CONNECTING )
    {
        m_status = EST_RCV_STANDBY;
    }
    else
    {
        m_status = RECON_EST_RCV_STANDBY;
    }

    if( m_close_try != true )
    {
        m_reconnect_try = true;
        m_pongflg = true;
        SetMsgTimer( ESTABLISHED_RCV_WAITTIME , WEBSOCKET_TO_EV_ESTABLISHED);
        V5LOG(V5_LOG_INFO,"[Websocket \"onOpen\"] <----");
        return;
    }
    
    CONFCNCT_INT ret = Close();
    if( ret == CCLIENT_OK )
    {
        V5LOG(V5_LOG_INFO,"Close Call OK");
        V5LOG(V5_LOG_INFO,"[Websocket \"onOpen\"] <----");
        return;
    }
    
    if( m_errInfoSet != true )
    {
        m_errInfoSet = true;
        auto errorInfo = std::make_shared<V5ErrorInfo>();
        errorInfo->code_deprecated = V5_ECODE_WEBSCOKET_CALLBACK_ONOPEN;
        errorInfo->category  = V5_COMPONENT_CONFERENCE;
        
        if( m_status <= EST_RCV_STANDBY )
        {
            errorInfo->reason = V5_CONFERENCE_CLIENT_CONNECTING_CLOSE_FAILED;
            m_onJoinConfFailCall = true;
        }
        else
        {
            errorInfo->reason = V5_CONFERENCE_CLIENT_RECONNECTING_CLOSE_FAILED;
            m_onJoinConfFailCall = false;
        }
        std::atomic_store( &m_errInfo , errorInfo );
    }
    else
    {
        if( m_status <= EST_RCV_STANDBY )
        {
            m_onJoinConfFailCall = true;
        }
        else
        {
            m_onJoinConfFailCall = false;
        }
    }
    
    
    /* RemoveFromConference送信に失敗してどうしようもないので強制切断実施 */
    CloseConnection();
    
    /* CloseConnectionで強制切断を実施したためこのあとonTarimnationがコールされる */
    /* そのためここでは会議終了系のコールバックは呼ばない */
//    auto callback = m_libCallbacks.lock();
//    if( callback )
//    {
//        if( m_ConfEndCallbackCalled == false )
//        {
//            if( m_status == CONNECTING )
//            {
//                callback->onJoinedConferenceFail( errorInfo );
//                m_ConfEndCallbackCalled = true;
//                V5LOG(V5_LOG_INFO,"onJoinedConferenceFail Called.");
//            }
//            else
//            {
//                callback->onLeavedConference( errorInfo );
//                m_ConfEndCallbackCalled = true;
//                V5LOG(V5_LOG_INFO,"onLeavedConference Called.");
//            }
//        }
//    }

    V5LOG(V5_LOG_INFO,"[Websocket \"onOpen\"] <----");
}

void ConferenceClient::onFail( websocketpp::connection_hdl hdl )
{
    V5LOG(V5_LOG_ERROR,"[Websocket \"onFail\"] ---->");

    if( m_connect_words == CCLIENT_CON_WORDS_WS )
    {
        V5LOG(V5_LOG_ERROR,"Client_e_code : %d",m_connection->get_ec().value());
        V5LOG(V5_LOG_ERROR,"Client_e_reason : %s",m_connection->get_ec().message().c_str());
        V5LOG(V5_LOG_ERROR,"Remote_e_code : %d",m_connection->get_remote_close_code());
        V5LOG(V5_LOG_ERROR,"Remote_e_reason : %s",m_connection->get_remote_close_reason().c_str());
    }
    else
    {
        V5LOG(V5_LOG_ERROR,"Client_e_code : %d",m_connection_tls->get_ec().value());
        V5LOG(V5_LOG_ERROR,"Client_e_reason : %s",m_connection_tls->get_ec().message().c_str());
        V5LOG(V5_LOG_ERROR,"Remote_e_code : %d",m_connection_tls->get_remote_close_code());
        V5LOG(V5_LOG_ERROR,"Remote_e_reason : %s",m_connection_tls->get_remote_close_reason().c_str());
    }
    
    if( m_errInfoSet != true )
    {
        m_errInfoSet = true;
        auto errorInfo = std::make_shared<V5ErrorInfo>();
        errorInfo->code_deprecated = V5_ECODE_WEBSCOKET_CALLBACK_ONFAIL;
        errorInfo->category  = V5_COMPONENT_CONFERENCE;
        
        if( m_status <= EST_RCV_STANDBY )
        {
            errorInfo->reason    = V5_CONFERENCE_CLIENT_CONNECT_FAILED;
            m_onJoinConfFailCall = true;
        }
        else
        {
            errorInfo->reason    = V5_CONFERENCE_CLIENT_RECONNECT_FAILED;
            m_onJoinConfFailCall = false;
        }
        
        std::atomic_store( &m_errInfo , errorInfo );
    }
    else
    {
        if( m_status <= EST_RCV_STANDBY )
        {
            m_onJoinConfFailCall = true;
        }
        else
        {
            m_onJoinConfFailCall = false;
        }
    }
    
    /* Socketレベルで接続に失敗した場合に呼ばれるがこの後onTerminationも呼ばれるので */
    /* ここでは会議終了系コールバックは呼ばない */
//    auto callback = m_libCallbacks.lock();
//    if( callback )
//    {
//        if( m_ConfEndCallbackCalled == false )
//        {
//            if( m_status == CONNECTING )
//            {
//                callback->onJoinedConferenceFail( errorInfo );
//                m_ConfEndCallbackCalled = true;
//                V5LOG(V5_LOG_INFO,"onJoinedConferenceFail Called.");
//            }
//            else
//            {
//                callback->onLeavedConference( errorInfo );
//                m_ConfEndCallbackCalled = true;
//                V5LOG(V5_LOG_INFO,"onLeavedConference Called.");
//            }
//        }
//    }
    
    m_reconnect_try = false;
    
    V5LOG(V5_LOG_ERROR,"[Websocket \"onFail\"] <----");
}

void ConferenceClient::onClose( websocketpp::connection_hdl hdl )
{
    V5LOG(V5_LOG_INFO,"[Websocket \"onClose\"] ---->");
    if( m_connect_words == CCLIENT_CON_WORDS_WS )
    {
        V5LOG(V5_LOG_INFO,"Client_e_code : %d",m_connection->get_ec().value());
        V5LOG(V5_LOG_INFO,"Client_e_reason : %s",m_connection->get_ec().message().c_str());
        V5LOG(V5_LOG_INFO,"Remote_e_code : %d",m_connection->get_remote_close_code());
        V5LOG(V5_LOG_INFO,"Remote_e_reason : %s",m_connection->get_remote_close_reason().c_str());
    }
    else
    {
        V5LOG(V5_LOG_INFO,"Client_e_code : %d",m_connection_tls->get_ec().value());
        V5LOG(V5_LOG_INFO,"Client_e_reason : %s",m_connection_tls->get_ec().message().c_str());
        V5LOG(V5_LOG_INFO,"Remote_e_code : %d",m_connection_tls->get_remote_close_code());
        V5LOG(V5_LOG_INFO,"Remote_e_reason : %s",m_connection_tls->get_remote_close_reason().c_str());
    }
    
    /* SocketがCloseした場合に呼ばれるが、このあとonTerminationが呼ばれるため、 */
    /* ここでは特に何もしない。 */
    m_status = CLOSING;
    
    V5LOG(V5_LOG_INFO,"[Websocket \"onClose\"] <----");
}

void ConferenceClient::onMessage( websocketpp::connection_hdl hdl, message_ptr msg)
{
    V5LOG(V5_LOG_INFO,"[Websocket \"onMessage\"] ---->");
//    V5LOG(V5_LOG_INFO,"msg->get_payload().length() = \n%d",msg->get_payload().length() );
//    V5LOG(V5_LOG_INFO,"msg->get_payload() = \n%s",msg->get_payload().c_str() );
    JsonParseEvent( msg->get_payload() );
    V5LOG(V5_LOG_INFO,"[Websocket \"onMessage\"] <----");
}

void ConferenceClient::onTermination( websocketpp::connection_hdl hdl )
{
    V5LOG(V5_LOG_INFO,"[Websocket \"onTermination\"] ---->");
    if( m_connect_words == CCLIENT_CON_WORDS_WS )
    {
        V5LOG(V5_LOG_INFO,"Client_e_code : %d",m_connection->get_ec().value());
        V5LOG(V5_LOG_INFO,"Client_e_reason : %s",m_connection->get_ec().message().c_str());
        V5LOG(V5_LOG_INFO,"Remote_e_code : %d",m_connection->get_remote_close_code());
        V5LOG(V5_LOG_INFO,"Remote_e_reason : %s",m_connection->get_remote_close_reason().c_str());
    }
    else
    {
        V5LOG(V5_LOG_INFO,"Client_e_code : %d",m_connection_tls->get_ec().value());
        V5LOG(V5_LOG_INFO,"Client_e_reason : %s",m_connection_tls->get_ec().message().c_str());
        V5LOG(V5_LOG_INFO,"Remote_e_code : %d",m_connection_tls->get_remote_close_code());
        V5LOG(V5_LOG_INFO,"Remote_e_reason : %s",m_connection_tls->get_remote_close_reason().c_str());
    }
    
    m_pongflg = false;
    V5ErrorReason leave_err_code = V5_CONFERENCE_CLOSE_FROM_SERVER_CLOSE_REASON_SUCCESS;
    int e_code = 0;
    if( m_connect_words == CCLIENT_CON_WORDS_WS )
    {
        if( m_connection != nullptr )
        {
            e_code = m_connection->get_remote_close_code();
        }
    }
    else
    {
        if( m_connection_tls != nullptr )
        {
            e_code = m_connection_tls->get_remote_close_code();
        }
    }
    
    
    std::string status;
    /* onTerminationが呼ばれるより先にClientInitializeが起動されていると */
    /* エラーコードを取得できない。取得できなかったときは、とりあえずreconnectしとくか・・・。 */
    if( e_code != 1000 )
    {
        if( m_reconnect_try == true )
        {
            m_reconnect_try = false;
            CONFCNCT_INT ret = Reconnect();
            if( ret == CCLIENT_OK )
            {
                V5LOG(V5_LOG_INFO,"Recoonect Success to Conference Server.");
                V5LOG(V5_LOG_INFO,"[Websocket \"onTermination\"] <----");
                return;
            }
            
            V5LOG(V5_LOG_ERROR,"Reconnect Failed.");
        }
        
        leave_err_code = V5_CONFERENCE_CLOSE_FROM_SERVER_CLOSE_REASON_SERVER_ERROR;
        status = std::to_string(m_status.load());
    }
    
    ClientInitialize();

    if( m_errInfoSet != true )
    {
        m_errInfoSet = true;
        auto errorInfo = std::make_shared<V5ErrorInfo>();
        errorInfo->code_deprecated = V5_ECODE_WEBSCOKET_CALLBACK_ONTERM;
        errorInfo->category     = V5_COMPONENT_CONFERENCE;
        errorInfo->reason       = leave_err_code;
        errorInfo->status       = status;
        std::atomic_store( &m_errInfo , errorInfo );
    }
    
    auto callback = m_libCallbacks.lock();
    if( callback )
    {
        if( m_ConfEndCallbackCalled == false )
        {
            m_ConfEndCallbackCalled = true;
            
            if( m_onJoinConfFailCall )
            {
                V5LOG(V5_LOG_INFO,"onJoinedConferenceFail Called.");
                callback->onJoinedConferenceFail( *std::atomic_load( &m_errInfo ) );
            }
            else
            {
                V5LOG(V5_LOG_INFO,"onLeavedConference Called.");
                callback->onLeavedConference( *std::atomic_load( &m_errInfo ) );
            }
        }
    }
    
    V5LOG(V5_LOG_INFO,"[Websocket \"onTermination\"] <----");
    return;
}

void ConferenceClient::onTimeout( unsigned int Event, websocketpp::lib::error_code const & ec )
{
    V5LOG(V5_LOG_INFO,"[Websocket \"onTimeout\"] ---->");
    
    V5LOG(V5_LOG_INFO,"Event : %d",Event);

    if( ec )
    {
        if( ec.value() == websocketpp::transport::error::operation_aborted )
        {
            V5LOG(V5_LOG_INFO,"Error Code   : %d",ec.value());
            V5LOG(V5_LOG_INFO,"Error Reason : %s",ec.message().c_str());
            /* タイマーに対応したメッセージを受信してタイマーをCancelした場合も */
            /* onTimeoutが起動されてここのルートを通るので、ここで終了する。 */
            return;
        }
        V5LOG(V5_LOG_ERROR,"Error Code   : %d",ec.value());
        V5LOG(V5_LOG_ERROR,"Error Reason : %s",ec.message().c_str());
    }

    switch( Event )
    {
        case WEBSOCKET_TO_EV_ESTABLISHED:
        {
            V5LOG(V5_LOG_ERROR,"Established Recieve Timeout.");
            m_est_timer_ptr = nullptr;

            CONFCNCT_INT ret = Close();
            if( ret == CCLIENT_OK )
            {
                V5LOG(V5_LOG_ERROR,"Close Call OK");
                
                if( m_status <= EST_RCV_STANDBY )
                {
                    m_status = EST_TIMEOUT;
                }
                else
                {
                    m_status = RECON_EST_TIMEOUT;
                }
                break;
            }
            else
            {
                V5LOG(V5_LOG_ERROR,"Close Call NG");
                if( m_status <= EST_RCV_STANDBY )
                {
                    V5LOG(V5_LOG_ERROR,"Connect Timing Established Timeout.");
                    
                    if( m_errInfoSet != true )
                    {
                        m_errInfoSet = true;
                        auto errorInfo = std::make_shared<V5ErrorInfo>();
                        errorInfo->code_deprecated = V5_ECODE_WEBSCOKET_ONTIMEOUT;
                        errorInfo->category = V5_COMPONENT_CONFERENCE;
                        errorInfo->reason   = V5_CONFERENCE_CLIENT_ESTABLISHED_TIMEOUT_CONNECTION_CLOSE_FAILED;
                        std::atomic_store( &m_errInfo , errorInfo );
                    }
                }
                else
                {
                    V5LOG(V5_LOG_ERROR,"Reconnect Timing Established Timeout.");
                    
                    if( m_errInfoSet != true )
                    {
                        m_errInfoSet = true;
                        auto errorInfo = std::make_shared<V5ErrorInfo>();
                        errorInfo->code_deprecated = V5_ECODE_WEBSCOKET_ONTIMEOUT;
                        errorInfo->category = V5_COMPONENT_CONFERENCE;
                        errorInfo->reason   = V5_CONFERENCE_CLIENT_ESTABLISHED_TIMEOUT_CONNECTION_CLOSE_FAILED_STATUS_RECONNECTING;
                        std::atomic_store( &m_errInfo , errorInfo );
                    }
                }
                
                /* カンファレンスサーバから応答がなく、以降のメッセージも期待できないため強制切断 */
                CloseConnection();
            }
        }
        break;
        case WEBSOCKET_TO_EV_CLOSEFROMSERVER:
        {
            V5LOG(V5_LOG_ERROR,"CloseFromServer Recieve Timeout.");
            
            if( m_errInfoSet != true )
            {
                m_errInfoSet = true;
                auto errorInfo = std::make_shared<V5ErrorInfo>();
                errorInfo->code_deprecated = V5_ECODE_WEBSCOKET_ONTIMEOUT;
                errorInfo->category = V5_COMPONENT_CONFERENCE;
                errorInfo->reason   = V5_CONFERENCE_CLIENT_CLOSE_FROM_SERVER_TIMEOUT;
                std::atomic_store( &m_errInfo , errorInfo );
            }
            
            /* カンファレンスサーバから応答がなく、以降のメッセージも期待できないため強制切断 */
            CloseConnection();
        }
        break;
        default:
        {
            V5LOG(V5_LOG_ERROR,"Unknown Event Type.");
            // 想定外のイベント種別を通知された。
            // 解除するタイマーが判別できないため何もしない。
        }
        break;
    }
    
    V5LOG(V5_LOG_INFO,"[Websocket \"onTimeout\"] <----");
    
    return;
}

context_ptr ConferenceClient::onInitTLS( websocketpp::connection_hdl hdl )
{
    context_ptr ctx = websocketpp::lib::make_shared<boost::asio::ssl::context>(boost::asio::ssl::context::sslv23);
    
    try {
        ctx->set_options(boost::asio::ssl::context::default_workarounds |
                         boost::asio::ssl::context::no_sslv2 |
                         boost::asio::ssl::context::no_sslv3 |
                         boost::asio::ssl::context::single_dh_use);
        
    }
    catch (std::exception& e)
    {
        V5LOG(V5_LOG_INFO,"exception : %s",e.what());
    }
    return ctx;
}

void ConferenceClient::RcvPing(picojson::value const&)
{
    V5LOG(V5_LOG_RECEIVED, "[Websocket \"RcvPing\"] ---->");

    if( m_pongflg != true )
    {
        V5LOG(V5_LOG_RECEIVED, "[Websocket \"RcvPing\"] <----");
        return;
    }

    picojson::object obj;
    obj["event"] = std::string(CHAT_EV_SND_PONG);
    obj["from"] = userPID;
    obj["dest"] = std::string("");
    obj["conference"] = userConfID;
    obj["data"] = std::string("");
    picojson::value val(obj);
    
    if( m_connect_words == CCLIENT_CON_WORDS_WS )
    {
        if( m_connection != nullptr )
        {
            m_client.send( m_connection->get_handle() ,
                           val.serialize() ,
                           websocketpp::frame::opcode::text
                          );
        }
    }
    else
    {
        if( m_connection_tls != nullptr )
        {
            m_client_tls.send( m_connection_tls->get_handle() ,
                               val.serialize() ,
                               websocketpp::frame::opcode::text
                              );
        }
    }
    
    V5LOG(V5_LOG_RECEIVED, "[Websocket \"RcvPing\"] <----");
    return;
}

void ConferenceClient::RcvSendMessage(picojson::value const& json)
{
    V5LOG(V5_LOG_INFO, "[Websocket \"RcvSendMessage\"] ---->");

    ChatLogClusterPtrT cluster;
    {
        lock_guard_t lock(mtx);
        cluster = chatLogStorage->addMessage(json, userPID);
    }

    auto callback = m_ChatCallbacks.lock();
    if (callback)
    {
        callback->onChatMessageAdded(*cluster);
        V5LOG(V5_LOG_INFO,"onChatMessageAdded Called.");
    }
    
#if WEBSOCKET_FUNC_DEBUG
    Debuglog( DEBUG_EV_CHATLOG );
#endif
    V5LOG(V5_LOG_INFO,"[Websocket \"RcvSendMessage\"] <----");

    return;
}

void ConferenceClient::RcvParticipantJoin(picojson::value const& json)
{
    V5LOG(V5_LOG_INFO,"[Websocket \"RcvParticipantJoin\"] ---->");
    
    ParticipantCluster target(json["data"]);
    auto callback = m_libCallbacks.lock();
    
    {
        lock_guard_t lock(mtx);     // 人数変化による時間再開までをアトミックにする必要がある
        auto added = participantListStorage->add(json);
#if WEBSOCKET_FUNC_DEBUG
        if (added) Debuglog(DEBUG_EV_PLIST);
#endif
        if (callback)
        {
            auto list = participantListStorage->getList();
            auto count = participantListStorage->count();
            if (added)
            {
                callback->onParticipantListUpdated(list, count);
                V5LOG(V5_LOG_INFO,"onParticipantListUpdated Called.");
            }
            callback->onParticipantJoined(target);

            if ((m_pastTimeSts == false) &&
                (count >= 1))
            {
                callback->onPastTimeStart();
                m_pastTimeSts = true;
                V5LOG(V5_LOG_INFO,"onPastTimeStart Called.");
            }
        }
    }
    
    V5LOG(V5_LOG_INFO,"[Websocket \"RcvParticipantJoin\"] <----");
    return;
}

void ConferenceClient::RcvParticipantLeave(picojson::value const& json)
{
    V5LOG(V5_LOG_INFO,"[Websocket \"RcvParticipantLeave\"] ---->");

    ParticipantCluster target(json["data"]);
    auto callback = m_libCallbacks.lock();

    {
        lock_guard_t lock(mtx);     // 人数変化による時間停止までをアトミックにする必要がある
        auto removed = participantListStorage->remove(json);
#if WEBSOCKET_FUNC_DEBUG
        if (removed) Debuglog(DEBUG_EV_PLIST);
#endif
        if (callback)
        {
            auto list = participantListStorage->getList();
            auto count = participantListStorage->count();
            if (removed)
            {
                callback->onParticipantListUpdated(list, count);
            }

            callback->onParticipantLeaved(target);

            if ((m_pastTimeSts == true) &&
                (count == 0))
            {
                callback->onPastTimeStop();
                m_pastTimeSts = false;
                V5LOG(V5_LOG_INFO,"onPastTimeStop Called.");
            }
        }
    }
    
    V5LOG(V5_LOG_INFO,"[Websocket \"RcvParticipantLeave\"] <----");

    return;
}

void ConferenceClient::RcvConferenceStatusUpdate(picojson::value const& obj)
{
    V5LOG(V5_LOG_INFO,"[Websocket \"RcvConferenceStatusUpdate\"] ---->");

    // ConfStsCluster は getter が用意されているため、 *ConfStsCluster に対しては破壊的な変更を加えてはならない

    // ディープコピーを作成する
    auto copy = std::make_shared<ConferenceStatusStorage>(*std::atomic_load(&ConfStsCluster));

    // ディープコピーに対して変更値を更新する
    copy->update(obj);

    ConfClusterPtr ptr = copy;
    std::atomic_store(&ConfStsCluster, ptr);
    
    if( (*copy.get()).getConfEndTimeCountFlg() )
    {
        unsigned long CountStartTime = 0;
        if( (*copy.get()).RemainTime < 0 )
        {
            CountStartTime = (*copy.get()).RemainTime * (-1);
        }
        
        auto libCallback = m_libCallbacks.lock();
        if (libCallback)
        {
            // 会議終了時間超過タイマー起動コールバック
            libCallback->onSetOverTimeCountTimer( CountStartTime );
        }
    }
    
#if WEBSOCKET_FUNC_DEBUG
    Debuglog(DEBUG_EV_CONFSTS);
#endif
    
    auto callback = m_libCallbacks.lock();
    if( callback )
    {
        callback->onConferenceStatusUpdated(*ptr);
    }
    
    V5LOG(V5_LOG_INFO,"[Websocket \"RcvConferenceStatusUpdate\"] <----");

    return;
}

void ConferenceClient::RcvMethodNotAllowed(picojson::value const& obj)
{
    V5LOG(V5_LOG_INFO,"[Websocket \"RcvMethodNotAllowed\"] ---->");

    std::string NAEventName = obj["data"].get<std::string>();
    
    V5LOG(V5_LOG_INFO,"NG Event : %d",NAEventName.c_str());
    
    V5ErrorInfo errorInfo;
    errorInfo.code_deprecated = V5_ECODE_WEBSCOKET_RCV_METHODNA;
    errorInfo.category = V5_COMPONENT_CONFERENCE;
    
    if( NAEventName == CHAT_EV_SND_REJCTPTCPNT )
    {
        errorInfo.reason = V5_CONFERENCE_CLIENT_METHOD_NOT_ALLOWED_TO_REJECT_PARTICIPANT;
    }
    else if( NAEventName == CHAT_EV_SND_LOCKCONF )
    {
        errorInfo.reason = V5_CONFERENCE_CLIENT_METHOD_NOT_ALLOWED_TO_LOCK_CONFERENCE;
    }
    else if( NAEventName == CHAT_EV_SND_UPDATECNAME )
    {
        errorInfo.reason = V5_CONFERENCE_CLIENT_METHOD_NOT_ALLOWED_TO_UPDATE_CONFERENCE_NAME;
    }
    else
    {
        errorInfo.reason = V5_CONFERENCE_CLIENT_METHOD_NOT_ALLOWED_TO_UNKNOWN_EVENT;
        errorInfo.status = NAEventName;
    }

    auto callback = m_libCallbacks.lock();
    if( callback )
    {
        callback->onAsyncError(errorInfo);
        V5LOG(V5_LOG_INFO,"onAsyncError Called.");
    }
    
    V5LOG(V5_LOG_INFO,"[Websocket \"RcvMethodNotAllowed\"] <----");

    return;
}

void ConferenceClient::RcvEstablished(picojson::value const& obj)
{
    V5LOG(V5_LOG_INFO,"[Websocket \"RcvEstablished\"] ---->");
    
    auto data = obj["data"].get<picojson::object>();

    if( m_est_timer_ptr != nullptr )
    {
        V5LOG(V5_LOG_INFO,"Established Recive Wait Timer Cancel.");
        m_est_timer_ptr->cancel();
        m_est_timer_ptr = nullptr;
    }
    else
    {
        V5LOG(V5_LOG_ERROR,"(RcvEstablished) Disconnected to Conference Server.");
        V5LOG(V5_LOG_INFO,"[Websocket \"RcvEstablished\"] <----");
        return;
    }
    
    // 会議情報を更新する
    ConfClusterPtr tmpConfSts = std::make_shared<ConferenceStatusStorage>(data["conferenceStatus"]);
    std::atomic_store(&ConfStsCluster, tmpConfSts);

    // ユーザーリストとチャットデータを更新する
    {
        decltype(participantListStorage) plist = std::make_shared<ParticipantListStorage>(data["participantList"], userPID);
        decltype(chatLogStorage) chatLog = std::make_shared<ChatLogStorage>(data["chatData"], userPID);
        lock_guard_t lock(mtx);
        std::swap(participantListStorage, plist);    // 旧データのデストラクタをmtxロック外へ遅延させるためにコピーではなく swap する
        std::swap(chatLogStorage, chatLog);          // 旧データのデストラクタをmtxロック外へ遅延させるためにコピーではなく swap する
    }

    // 会議情報とユーザーリストのコールバックを行う
    {
        auto libCallback = m_libCallbacks.lock();
        if (libCallback)
        {
            V5ConferenceInfo info;
            info.ConfSts = tmpConfSts.get();

            lock_guard_t lock(mtx);     // 参加者リストは破壊的な編集を行うため、コールバック中はロックし続けなければならない

            info.participants = participantListStorage->getList();
            info.length = participantListStorage->count();
            info.selfParticipant = participantListStorage->getSelf();

            if (m_status == EST_RCV_STANDBY)
            {
                libCallback->onJoinedConference(info);
                V5LOG(V5_LOG_INFO,"onJoinedConference Called.");
            }
            else
            {
                libCallback->onReconnectConference(info);
                V5LOG(V5_LOG_INFO,"onReconnectConference Called.");
            }
            
            if ((m_pastTimeSts == false) && (info.length >= 1))
            {
                libCallback->onPastTimeStart();
                m_pastTimeSts = true;
                V5LOG(V5_LOG_INFO,"onPastTimeStart Called.");
            }
            
            if( (m_pastTimeSts == true) && (info.length == 0) )
            {
                libCallback->onPastTimeStop();
                m_pastTimeSts = false;
                V5LOG(V5_LOG_INFO,"onPastTimeStop Called.");
            }
            
            if( ( (*tmpConfSts.get()).getConfEndTimeCountFlg() == true ) &&
                ( m_confEndTimeOverTimerStatus.load() == false ) )
            {
                m_confEndTimeOverTimerStatus = true;
                
                unsigned long CountStartTime = 0;
                if( (*tmpConfSts.get()).RemainTime < 0 )
                {
                    CountStartTime = (*tmpConfSts.get()).RemainTime * (-1);
                }
                
                // 会議終了時間超過タイマー起動コールバック
                libCallback->onSetOverTimeCountTimer( CountStartTime );
            }
            
            if( ( (*tmpConfSts.get()).getConfEndTimeCountFlg() == false ) &&
               ( m_confEndTimeOverTimerStatus.load() == true ) )
            {
                m_confEndTimeOverTimerStatus = false;
                                
                // 会議終了時間超過タイマー起動コールバック
                libCallback->onCancelOverTimeCountTimer();
            }

            
        }
    }

    // チャットデータ更新をコールバック通知する
    {
        auto chatCallback = m_ChatCallbacks.lock();
        if (chatCallback)
        {
            lock_guard_t lock(mtx);   // チャットは破壊的な編集を行うため、コールバック中はロックし続けなければならない
            auto const& list = chatLogStorage->getList();
            chatCallback->onChatLogUpdated(list.data(), list.size());
            V5LOG(V5_LOG_INFO,"onChatLogUpdated Called");
        }
    }

#if WEBSOCKET_FUNC_DEBUG
    Debuglog(DEBUG_EV_PLIST);
    Debuglog(DEBUG_EV_CHATLOG);
    Debuglog(DEBUG_EV_CONFSTS);
#endif
    
    m_status = SENDMSG_OK;
    V5LOG(V5_LOG_INFO,"[Websocket \"RcvEstablished\"] <----");

    return;
}

void ConferenceClient::RcvRejectParticipant(picojson::value const& obj)
{
    V5LOG(V5_LOG_INFO,"[Websocket \"RcvRejectParticipant\"] ---->");
    auto callback = m_libCallbacks.lock();
    if( callback )
    {
        lock_guard_t lock(mtx);
//        auto& id = obj["from"].get<std::string>();
//        auto* fromParticipant = participantListStorage->find(id);
//        callback->onRcvRejectParticipant(*fromParticipant);  // TODO:見つからなかった場合の備えがない！
        
        struct ParticipantCluster fromParticipant( obj["from"].get<std::string>() ,
                                                   obj["data"]["display_name"].get<std::string>()
                                                  );
        callback->onRcvRejectParticipant(fromParticipant);
        V5LOG(V5_LOG_ERROR,"onRcvRejectParticipant Called.");
    }
    
    V5LOG(V5_LOG_INFO,"[Websocket \"RcvRejectParticipant\"] <----");
    return;
}

void ConferenceClient::RcvCloseFromServer(picojson::value const& message)
{
    V5LOG(V5_LOG_INFO,"[Websocket \"RcvCloseFromServer\"] ---->");
    auto& Reason = message["data"].get<std::string>();

    if( m_cfs_timer_ptr != nullptr )
    {
        m_cfs_timer_ptr->cancel();
        m_cfs_timer_ptr = nullptr;
    }

    /* Establishedを受信する前にCloseFromServerを受信するパターンがあるので */
    /* ここでEstablishedのタイマーキャンセルを実施 */
    if( m_est_timer_ptr != nullptr )
    {
        m_est_timer_ptr->cancel();
        m_est_timer_ptr = nullptr;
    }
    
    V5ErrorReason reason_int = (V5ErrorReason)atoi( Reason.c_str() );
    
#if WEBSOCKET_FUNC_DEBUG
    std::map<int,std::string>::iterator it = ErrReason.find(reason_int);
    const char *EReason = it->second.c_str();
    V5LOG(V5_LOG_INFO,"Error Reason : %s",EReason);
#endif
    
    m_reconnect_try = false;
    
    if( m_errInfoSet != true )
    {
        m_errInfoSet = true;
        auto errorInfo = std::make_shared<V5ErrorInfo>();
        errorInfo->code_deprecated = V5_ECODE_WEBSCOKET_RCV_CLOSEFSERVER;
        errorInfo->category = V5_COMPONENT_CONFERENCE;
        
        if( m_status == EST_TIMEOUT  )
        {
            errorInfo->reason = V5_CONFERENCE_CLIENT_CLOSE_CONNECTION_TO_ESTABLISHED_TIMEOUT;
            m_onJoinConfFailCall = true;
        }
        else if( m_status == RECON_EST_TIMEOUT )
        {
            errorInfo->reason = V5_CONFERENCE_CLIENT_CLOSE_CONNECTION_TO_ESTABLISHED_TIMEOUT_RECONNECT;
            m_onJoinConfFailCall = false;
        }
        else
        {
            errorInfo->reason = reason_int;
            switch( reason_int )
            {
                case V5_CONFERENCE_CLOSE_FROM_SERVER_CLOSE_REASON_TOKEN_ERROR:
                case V5_CONFERENCE_CLOSE_FROM_SERVER_CLOSE_REASON_NON_CONFERENCE_ID:
                case V5_CONFERENCE_CLOSE_FROM_SERVER_CLOSE_REASON_SUCCESS:
                case V5_CONFERENCE_CLOSE_FROM_SERVER_CLOSE_REASON_CONFERENCE_TRANSRATION:
                case V5_CONFERENCE_CLOSE_FROM_SERVER_CLOSE_REASON_CONFERENCE_LOCKED:
                case V5_CONFERENCE_CLOSE_FROM_SERVER_CLOSE_REASON_CONNECT_TIMEOUT:
                case V5_CONFERENCE_CLOSE_FROM_SERVER_CLOSE_REASON_PONG_TIMEOUT:
                case V5_CONFERENCE_CLOSE_FROM_SERVER_CLOSE_REASON_NO_CONNECT_PREPARE:
                case V5_CONFERENCE_CLOSE_FROM_SERVER_CLOSE_REASON_NO_VACANCY:
                case V5_CONFERENCE_CLOSE_FROM_SERVER_CLOSE_REASON_SERVER_ERROR:
                case V5_CONFERENCE_CLOSE_FROM_SERVER_CLOSE_REASON_PORTAL_ERROR:
                case V5_CONFERENCE_CLOSE_FROM_SERVER_CLOSE_REASON_RESERVE_CONFERENCE_END:
                case V5_CONFERENCE_CLOSE_FROM_SERVER_CLOSE_REASON_FORCED_TERMINATION_CONFERENCE:
                case V5_CONFERENCE_CLOSE_FROM_SERVER_CLOSE_REASON_CONFERENCE_SYSTEM_REJECT:
                {
                    if( m_status <= EST_TIMEOUT )
                    {
                        m_onJoinConfFailCall = true;
                    }
                    else
                    {
                        m_onJoinConfFailCall = false;
                    }
                }
                break;

                default:
                {
                    errorInfo->reason = V5_CONFERENCE_CLIENT_CLOSE_FROM_SERVER_REASON_UNKNOWN;
                    errorInfo->status = Reason.c_str();
                    if( m_status <= EST_TIMEOUT )
                    {
                        m_onJoinConfFailCall = true;
                    }
                    else
                    {
                        m_onJoinConfFailCall = false;
                    }
                }
                break;
                    
            }
        }
        
        std::atomic_store( &m_errInfo , errorInfo );
    }
    
    V5LOG(V5_LOG_INFO,"[Websocket \"RcvCloseFromServer\"] <----");

    return;
}

void ConferenceClient::RcvNotificationFailed(picojson::value const& message)
{
    V5LOG(V5_LOG_INFO,"[Websocket \"RcvNotificationFailed\"] ---->");
    
    auto& data = message["data"];
    if( !data.contains("event") )
    {
        V5LOG(V5_LOG_ERROR,"Event Paramater Nothing");
        return;
    }
    
    auto failEvent = data["event"].get<std::string>();
    
    V5LOG(V5_LOG_ERROR,"Recieve Failed Event : %s.",failEvent.c_str());
    
    if( failEvent == CHAT_EV_SND_UPDATECNAME )
    {
        if( data.contains("detail"))
        {
            auto detail = data["detail"].get<std::string>();
            
            V5LOG(V5_LOG_INFO,"detail = %s",detail.c_str());
            
            auto callback = m_libCallbacks.lock();
            if( callback )
            {
                auto tmpConfCluster = std::atomic_load(&ConfStsCluster);
                std::string confNamePtr = tmpConfCluster->ConferenceName;
                callback->onConferenceNameChangeFailed(confNamePtr.c_str());
                V5LOG(V5_LOG_INFO,"onRcvRejectParticipant Called.");
            }
            
            V5LOG(V5_LOG_INFO,"[Websocket \"RcvNotificationFailed\"] <----");
            return;
        }
    }

    V5LOG(V5_LOG_INFO,"Unknown Failed Event!");
    V5LOG(V5_LOG_INFO,"[Websocket \"RcvNotificationFailed\"] <----");
}

void ConferenceClient::JsonParseEvent(const std::string& JSONParseMsg)
{
    static const std::map<std::string, void (ConferenceClient::*)(picojson::value const&)> HANDLER_MAP{
        { CHAT_EV_RCV_PING,        &ConferenceClient::RcvPing                   },
        { CHAT_EV_RCV_SNDMSG,      &ConferenceClient::RcvSendMessage            },
        { CHAT_EV_RCV_PJOIN,       &ConferenceClient::RcvParticipantJoin        },
        { CHAT_EV_RCV_PLEAVE,      &ConferenceClient::RcvParticipantLeave       },
        { CHAT_EV_RCV_CONFSTSUPD,  &ConferenceClient::RcvConferenceStatusUpdate },
        { CHAT_EV_RCV_METHODNALLW, &ConferenceClient::RcvMethodNotAllowed       },
        { CHAT_EV_RCV_ESTABLISHED, &ConferenceClient::RcvEstablished            },
        { CHAT_EV_RCV_REJCTPTCPNT, &ConferenceClient::RcvRejectParticipant      },
        { CHAT_EV_RCV_CLSFMSRV,    &ConferenceClient::RcvCloseFromServer        },
        { CHAT_EV_RCV_DEBUG,       &ConferenceClient::RcvDebug                  },
        { CHAT_EV_RCV_NOTIFFAIL,   &ConferenceClient::RcvNotificationFailed     },
    };
    const char *cnvData = JSONParseMsg.c_str();
    picojson::value message;
    std::string err;
    
    picojson::parse(message, cnvData, cnvData + strlen(cnvData), &err);
    
    if( !err.empty() )
    {
        return;
    }
    
    auto& event = message.get("event").get<std::string>();
    
//    V5LOG(V5_LOG_INFO, "Recieve Message : %s", JSONParseMsg.c_str());
    
    V5LOG(V5_LOG_INFO,"[Websocket \"JsonParseEvent\"]");
    V5LOG(V5_LOG_INFO, "Event : %s", event.c_str());
    
    auto ite = HANDLER_MAP.find(event);
    if (ite == HANDLER_MAP.end())
    {
        return;
    }
    return (this->*ite->second)(message);
}

void ConferenceClient::SetErrorReason()
{
    ErrReason[V5_CONFERENCE_CLOSE_FROM_SERVER_CLOSE_REASON_SUCCESS]                       = "Success Connect Close";
    ErrReason[V5_CONFERENCE_CLOSE_FROM_SERVER_CLOSE_REASON_CONFERENCE_TRANSRATION]        = "Conference Transration";
    ErrReason[V5_CONFERENCE_CLOSE_FROM_SERVER_CLOSE_REASON_TOKEN_ERROR]                   = "Connect Token Error";
    ErrReason[V5_CONFERENCE_CLOSE_FROM_SERVER_CLOSE_REASON_CONFERENCE_LOCKED]             = "Conference Login Locked";
    ErrReason[V5_CONFERENCE_CLOSE_FROM_SERVER_CLOSE_REASON_NON_CONFERENCE_ID]             = "Connect Conference is No Destination";
    ErrReason[V5_CONFERENCE_CLOSE_FROM_SERVER_CLOSE_REASON_CONNECT_TIMEOUT]               = "Conference Connect Sequence Timeout";
    ErrReason[V5_CONFERENCE_CLOSE_FROM_SERVER_CLOSE_REASON_PONG_TIMEOUT]                  = "Ping Responce Timeout";
    ErrReason[V5_CONFERENCE_CLOSE_FROM_SERVER_CLOSE_REASON_NO_CONNECT_PREPARE]            = "";
    ErrReason[V5_CONFERENCE_CLOSE_FROM_SERVER_CLOSE_REASON_NO_VACANCY]                    = "Conference is Full";
    ErrReason[V5_CONFERENCE_CLOSE_FROM_SERVER_CLOSE_REASON_SERVER_ERROR]                  = "Conference Server Error";
    ErrReason[V5_CONFERENCE_CLOSE_FROM_SERVER_CLOSE_REASON_PORTAL_ERROR]                    = "Portal Server Error";
    ErrReason[V5_CONFERENCE_CLOSE_FROM_SERVER_CLOSE_REASON_RESERVE_CONFERENCE_END]        = "Conference Reserve End Time Over";
    ErrReason[V5_CONFERENCE_CLOSE_FROM_SERVER_CLOSE_REASON_FORCED_TERMINATION_CONFERENCE] = "Conference Exit";
    ErrReason[V5_CONFERENCE_CLOSE_FROM_SERVER_CLOSE_REASON_CONFERENCE_SYSTEM_REJECT]      = "System Reject";

    return;
}

void ConferenceClient::Debuglog(V5DEBUG_EVENT Event) const
{
    switch( Event )
    {
        case DEBUG_EV_PLIST:
        {
            lock_guard_t lock(mtx);
            std::cout << *participantListStorage << std::endl;
        }
        break;
            
        case DEBUG_EV_CHATLOG:
        {
            lock_guard_t lock(mtx);
            std::cout << *chatLogStorage << std::endl;
        }
        break;
            
        case DEBUG_EV_CONFSTS:
        {
            std::cout << *std::atomic_load(&ConfStsCluster) << std::endl;
        }
        break;
    }
    
    return;
}

void ConferenceClient::DebugSend()
{
    V5LOG(V5_LOG_INFO, "[Websocket \"Debug\"] ----> In");

#if WEBSOCKET_FUNC_DEBUG
    
    picojson::object AllJsonData;

    // ここからParticipantList
    
    picojson::object PList;
    PList["numParticipants"] = std::string("2");
    PList["conferenceID"] = userConfID;
    
    picojson::array Parray;

    picojson::object PData1;
    PData1["display_name"] = std::string("dummyUserName1");
    PData1["participant_id"] = std::string("dummyUserParticipantID1");
    picojson::value vPdata1(PData1);
    
    Parray.push_back( vPdata1 );
    
    picojson::object PData2;
    PData2["display_name"] = std::string("dummyUserName2");
    PData2["participant_id"] = std::string("dummyUserParticipantID2");
    picojson::value vPdata2(PData2);
    
    Parray.push_back( vPdata2 );
    picojson::value vPayyay(Parray);
    
    PList["participants"] = vPayyay;
    
    picojson::value vPList(PList);
    
    AllJsonData["participantList"] = vPList;
    
    // ここまでParticipantList
    // ここからChatLog
    
    picojson::object ChatLog;
    ChatLog["length"] = std::string("2");
    
    picojson::array LogDataArray;
    
    picojson::object LogData1;
    LogData1["from"] = std::string("dummyUserName1");
    LogData1["conference"] = std::string(userConfID);
    picojson::object LogMsg1;
    LogMsg1["message"] = std::string("abcdefg");
    LogMsg1["displayname"] = std::string("dummyUserName1");
    LogMsg1["timestamp"] = std::string("9876543210");
    picojson::value vLogMsg1(LogMsg1);
    LogData1["data"] = vLogMsg1;
    picojson::value vLogData1(LogData1);
    
    LogDataArray.push_back( vLogData1 );
    
    picojson::object LogData2;
    LogData2["from"] = std::string("dummyUserName2");
    LogData2["conference"] = std::string(userConfID);
    picojson::object LogMsg2;
    LogMsg2["message"] = std::string("hijklmn");
    LogMsg2["displayname"] = std::string("dummyUserName2");
    LogMsg2["timestamp"] = std::string("9876543210");
    picojson::value vLogMsg2(LogMsg1);
    LogData2["data"] = vLogMsg2;
    picojson::value vLogData2(LogData2);
    
    LogDataArray.push_back( vLogData2 );
    picojson::value vLogDataArray(LogDataArray);

    ChatLog["data"] = vLogDataArray;
    
    picojson::value vChatLog(ChatLog);
    
    AllJsonData["chatData"] = vChatLog;

    // ここからChatLog
    // ここからConferenceStatus
    
    picojson::object ConfSts;
    
    ConfSts["pastTime"]                 = std::string("-101010");
    ConfSts["conferenceStartTime"]      = std::string("3256987410");
    ConfSts["conferenceEndTime"]        = std::string("1145144545");
    ConfSts["conferencePinCode"]        = std::string("9876543210");
    
    ConfSts["remainTime"]               = std::string("10000");  // 更新ありPRM
    ConfSts["maintenanceStartTime"]     = std::string("20000");  // 更新ありPRM
    ConfSts["maintenanceEndTime"]       = std::string("30000");  // 更新ありPRM
    ConfSts["maintenanceRemainTime"]    = std::string("50000");   // 更新ありPRM
    
    ConfSts["roomName"]         = std::string("room1");
    ConfSts["conferenceURL"]    = std::string("testConferenceURL");
    ConfSts["documentSender"]   = std::string("It's you");              // 更新ありPRM
    ConfSts["conferenceName"]   = std::string("NewtestConferenceName"); // 更新ありPRM
    
    ConfSts["isReserved"]       = std::string("true");
    ConfSts["isRecoading"]      = std::string("true");  // 更新ありPRM
    ConfSts["isLocked"]         = std::string("true");  // 更新ありPRM

    picojson::value vConfSts(ConfSts);
    
    AllJsonData["conferenceStatus"] = vConfSts;
    
    // ここからConferenceStatus
    // ここからConferenceStatus(更新確認用)
    
    picojson::object newConfSts;
    
    newConfSts["pastTime"]                 = std::string("asdfk");
    newConfSts["conferenceStartTime"]      = std::string("1456riojvgoier");
    newConfSts["conferenceEndTime"]        = std::string("vneuibr4584577");
    newConfSts["conferencePinCode"]        = std::string("9876543210");

    newConfSts["remainTime"]               = std::string("vaer748frae");  // 更新ありPRM
    newConfSts["maintenanceStartTime"]     = std::string("b.ewtlrb,[");  // 更新ありPRM
    newConfSts["maintenanceEndTime"]       = std::string(":[/.;rere");  // 更新ありPRM
    newConfSts["maintenanceRemainTime"]    = std::string("5a5a5a5a");   // 更新ありPRM
    
    newConfSts["roomName"]         = std::string("room1");
    newConfSts["conferenceURL"]    = std::string("testConferenceURL");
    newConfSts["documentSender"]   = std::string("It's you");   // 更新ありPRM
    newConfSts["conferenceName"]   = std::string("NewtestConferenceName");  // 更新ありPRM
    
    newConfSts["isReserved"]       = std::string("true");
    newConfSts["isRecoading"]      = std::string("true");  // 更新ありPRM
    newConfSts["isLocked"]         = std::string("true");  // 更新ありPRM
    
    picojson::value vNewConfSts(newConfSts);

    picojson::object dataWrapper;
    dataWrapper["data"] = vNewConfSts;
    
    picojson::value vNewConfStsData(newConfSts);

    AllJsonData["newConferenceStatus"] = dataWrapper;

    // ここまでConferenceStatus(更新確認用)
    
    picojson::value vAllData(AllJsonData);
    V5LOG(V5_LOG_INFO, "Debug ALL Data =\n%s",vAllData.serialize().c_str());

    picojson::object obj;
    obj["event"] = std::string(CHAT_EV_SND_DEBUG);
    obj["from"] = std::string(userPID);
    obj["dest"] = std::string("");
    obj["conference"] = std::string(userConfID);
    obj["data"] = vAllData;
    picojson::value val(obj);
    
    V5LOG(V5_LOG_INFO, "Message Data : %s",val.serialize().c_str());
    
    error_code ec;
    
    if( m_connect_words == CCLIENT_CON_WORDS_WS )
    {
        m_client.send( m_connection->get_handle() ,
                       val.serialize() ,
                       websocketpp::frame::opcode::text ,
                       ec
                      );
    }
    else
    {
        m_client_tls.send( m_connection_tls->get_handle() ,
                           val.serialize() ,
                           websocketpp::frame::opcode::text ,
                           ec
                          );
    }
    
    if( ec )
    {
        V5LOG(V5_LOG_INFO, "Send Error.");
        V5LOG(V5_LOG_INFO, "[Websocket \"Debug\"] <---- Out");
        return;
    }
    
#endif
    V5LOG(V5_LOG_INFO, "[Websocket \"Debug\"] <---- Out");
    
    return;
}

void ConferenceClient::WorkerCrashSend()
{
    V5LOG(V5_LOG_INFO, "[Websocket \"WorkerCrashSend\"] ----> In");

#if WEBSOCKET_FUNC_DEBUG
    picojson::object obj;
    obj["event"] = std::string(CHAT_EV_SND_CRASH);
    obj["from"] = std::string("");
    obj["dest"] = std::string("");
    obj["conference"] = std::string("");
    obj["data"] = std::string("");
    picojson::value val(obj);
    
    V5LOG(V5_LOG_INFO, "Message Data : %s",val.serialize().c_str());
    
    error_code ec;
    
    if( m_connect_words == CCLIENT_CON_WORDS_WS )
    {
        m_client.send( m_connection->get_handle() ,
                       val.serialize() ,
                       websocketpp::frame::opcode::text ,
                       ec
                      );
    }
    else
    {
        m_client_tls.send( m_connection_tls->get_handle() ,
                           val.serialize() ,
                           websocketpp::frame::opcode::text ,
                           ec
                          );
    }
    
    if( ec )
    {
        V5LOG(V5_LOG_INFO, "Send Error.");
        V5LOG(V5_LOG_INFO, "[Websocket \"WorkerClashSend\"] <---- Out");
        return;
    }
    
#endif
    
    V5LOG(V5_LOG_INFO, "[Websocket \"WorkerClashSend\"] <---- Out");
    
    return;
}

void ConferenceClient::RcvDebug(picojson::value const& message)
{
    V5LOG(V5_LOG_INFO, "[Websocket \"RcvDebug\"] ----> In");
    
#if WEBSOCKET_FUNC_DEBUG
    auto data = message["data"].get<picojson::object>();

    ParticipantListStorage pls(data["participantList"], userPID);
    ChatLogStorage cls(data["chatData"], userPID);
    ConferenceStatusStorage css(data["conferenceStatus"]);
    
    std::cout << "Before Data" << std::endl;
    std::cout << pls << std::endl;
    std::cout << cls << std::endl;
    std::cout << css << std::endl;

    
    css.update(data["newConferenceStatus"]);
    
    std::cout << "After Data" << std::endl;
    std::cout << pls << std::endl;
    std::cout << cls << std::endl;
    std::cout << css << std::endl;
#endif
    V5LOG(V5_LOG_INFO, "[Websocket \"RcvDebug\"] <---- Out");
    
    return;
}

// ログ出力処理統合のためコメントアウト
//void ConferenceClient::DebugLogPrint( WEBSOCKET_LOG_LEVEL logLv,const char *fmt,...) const
//{
//    if( logLv >= WEBSOCKET_LOG_OUTPUT_LV )
//    {
//        va_list list;
//        va_start(list, fmt);
//       vprintf(fmt, list);
//        printf("\n");
//        va_end(list);
//    }
//}

/*********************************************************************************/
/* 各種会議データ設定メソッド群                                                       */
/*********************************************************************************/

// 会議情報
ConferenceStatusStorage::ConferenceStatusStorage(picojson::value const& json)
    : conferenceNameStorage(json["conferenceName"].get<std::string>())
    , roomNameStorage(json["roomName"].get<std::string>())
    , conferenceURLStorage(json["conferenceURL"].get<std::string>())
    , conferencePinCodeStorage(json["conferencePinCode"].get<std::string>())
    , documentSenderStorage(json["documentSender"].get<std::string>())
    , oldConferenceNameStorage("")
    , oldDocumentSenderStorage("")

{
    V5ConferenceStatus::OldConferenceEndTime = 0;
    V5ConferenceStatus::OldRemainTime = 0;
    V5ConferenceStatus::OldMaintenanceStartTime = 0;
    V5ConferenceStatus::OldMaintenanceRemainTime = 0;
    V5ConferenceStatus::OldMaintenanceEndTime = 0;

    // データの内容をチェックして問題なければデータを通知
    // 問題があれば0を初期値で登録する。
    if( json.contains("pastTime") )
    {
        auto pastTime = json["pastTime"].get<std::string>();
        if( std::all_of(pastTime.cbegin(), pastTime.cend(), isdigit) )
        {
            V5ConferenceStatus::PastTime = std::stoll(pastTime);
        }
        else
        {
            // 受信したデータの内容が異常なので、0を設定
            V5ConferenceStatus::PastTime = 0;
        }
    }
    else
    {
        V5ConferenceStatus::PastTime = 0;
    }
    
    if( json.contains("remainTime") )
    {
        auto remainTime = json["remainTime"].get<std::string>();
        // 数値(±)の範囲
        if( remainTime.find_first_of("-0123456789") != std::string::npos )
        {
            V5ConferenceStatus::RemainTime = std::stoll(json["remainTime"].get<std::string>());
            if( V5ConferenceStatus::RemainTime <= 0)
            {
                confEndTimeCountFlg = true;
            }
            else
            {
                confEndTimeCountFlg = false;
            }
        }
        // 予約会議がなくなったときの通知
        else if( remainTime.length() == 0 )
        {
            confEndTimeCountFlg = false;
        }
        // 数値(±)でも""(空文字)でもない異常パターン
        else
        {
            // 受信したデータの内容が異常なので、0を設定
            V5ConferenceStatus::RemainTime = 0;
        }
    }
    else
    {
        V5ConferenceStatus::RemainTime = 0;
    }
    
    if( json.contains("conferenceStartTime") )
    {
        auto conferenceStartTime = json["conferenceStartTime"].get<std::string>();
        if( std::all_of(conferenceStartTime.cbegin(), conferenceStartTime.cend(), isdigit) )
        {
            V5ConferenceStatus::ConferenceStartTime = std::stoll(json["conferenceStartTime"].get<std::string>());
        }
        else
        {
            // 受信したデータの内容が異常なので、0を設定
            V5ConferenceStatus::ConferenceStartTime = 0;
        }
    }
    else
    {
        V5ConferenceStatus::ConferenceStartTime = 0;
    }

    if( json.contains("conferenceEndTime") )
    {
        auto conferenceEndTime = json["conferenceEndTime"].get<std::string>();
        if( std::all_of(conferenceEndTime.cbegin(), conferenceEndTime.cend(), isdigit) )
        {
            V5ConferenceStatus::ConferenceEndTime = std::stoll(json["conferenceEndTime"].get<std::string>());
        }
        else
        {
            // 受信したデータの内容が異常なので、0を設定
            V5ConferenceStatus::ConferenceEndTime = 0;
        }
    }
    else
    {
        V5ConferenceStatus::ConferenceEndTime = 0;
    }
    
    if( json.contains("maintenanceStartTime") )
    {
        auto maintenanceStartTime = json["maintenanceStartTime"].get<std::string>();
        if( std::all_of(maintenanceStartTime.cbegin(), maintenanceStartTime.cend(), isdigit) )
        {
            V5ConferenceStatus::MaintenanceStartTime = std::stoll(json["maintenanceStartTime"].get<std::string>());
        }
        else
        {
            // 受信したデータの内容が異常なので、0を設定
            V5ConferenceStatus::MaintenanceStartTime = 0;
        }
    }
    else
    {
        V5ConferenceStatus::MaintenanceStartTime = 0;
    }
    
    if( json.contains("maintenanceEndTime") )
    {
        auto maintenanceEndTime = json["maintenanceEndTime"].get<std::string>();
        if( std::all_of(maintenanceEndTime.cbegin(), maintenanceEndTime.cend(), isdigit) )
        {
            V5ConferenceStatus::MaintenanceEndTime = std::stoll(json["maintenanceEndTime"].get<std::string>());
        }
        else
        {
            // 受信したデータの内容が異常なので、0を設定
            V5ConferenceStatus::MaintenanceEndTime = 0;
        }
    }
    else
    {
        V5ConferenceStatus::MaintenanceEndTime = 0;
    }
    
    if( json.contains("maintenanceRemainTime") )
    {
        auto maintenanceRemainTime = json["maintenanceRemainTime"].get<std::string>();
        if( std::all_of(maintenanceRemainTime.cbegin(), maintenanceRemainTime.cend(), isdigit) )
        {
            V5ConferenceStatus::MaintenanceRemainTime = std::stoll(json["maintenanceRemainTime"].get<std::string>());
        }
        else
        {
            V5ConferenceStatus::MaintenanceRemainTime = 0;
        }
    }
    else
    {
        V5ConferenceStatus::MaintenanceRemainTime = 0;
    }
    
    V5ConferenceStatus::isLocked = false;
    V5ConferenceStatus::isRecoading = (json["isRecoading"] == "true");
    V5ConferenceStatus::isReserved = (json["isReserved"] == "true");

    V5ConferenceStatus::ConferenceName = conferenceNameStorage.c_str();
    V5ConferenceStatus::RoomName = roomNameStorage.c_str();
    V5ConferenceStatus::ConferenceURL = conferenceURLStorage.c_str();
    V5ConferenceStatus::ConferencePinCode = conferencePinCodeStorage.c_str();
    V5ConferenceStatus::DocumentSender = documentSenderStorage.c_str();
    
    V5ConferenceStatus::OldConferenceName = oldConferenceNameStorage.c_str();
    V5ConferenceStatus::OldDocumentSender = oldDocumentSenderStorage.c_str();
    
    // 全パラメータのチェックフラグを設定する。
    V5ConferenceStatus::ConferenceNameChanged = true;
    V5ConferenceStatus::DocumentSenderChanged = true;
    V5ConferenceStatus::RemainTimeChanged = true;
    V5ConferenceStatus::ConferenceEndTimeChanged = true;
    V5ConferenceStatus::MaintenanceStartTimeChanged = true;
    V5ConferenceStatus::MaintenanceRemainTimeChanged = true;
    V5ConferenceStatus::MaintenanceEndTimeChanged = true;
    V5ConferenceStatus::isRecoadingChanged = true;
    V5ConferenceStatus::isLockedChanged = true;
    
    // 予約会議判定
    if( V5ConferenceStatus::isReserved == true )
    {
        // 予約会議ルート
        if( V5ConferenceStatus::RemainTime <= 0 )
        {
            confEndTimeCountFlg = true;
        }
        else
        {
            confEndTimeCountFlg = false;
        }
    }
    else
    {
        // 非予約(通常)会議ルート
        if(V5ConferenceStatus::ConferenceEndTime == 0 )
        {
            // 後発予約会議なし
            // 会議超過時間通知停止
            confEndTimeCountFlg = false;
        }
        else
        {
            // 後発予約あり
            if( V5ConferenceStatus::RemainTime <= 0 ){
                // 会議終了時間ありかつ残り時間が0以下
                // 会議超過時間通知開始
                confEndTimeCountFlg = true;
            }
            else
            {
                // 会議終了時間ありかつ残り時間が1以上
                // = UI側でハンドリングするので放置
                // 会議超過時間通知停止
                confEndTimeCountFlg = false;
            }
        }
    }

}

ConferenceStatusStorage::ConferenceStatusStorage(ConferenceStatusStorage const& rhs)
    : V5ConferenceStatus(rhs)
    , conferenceNameStorage(rhs.conferenceNameStorage)
    , roomNameStorage(rhs.roomNameStorage)
    , conferenceURLStorage(rhs.conferenceURLStorage)
    , conferencePinCodeStorage(rhs.conferencePinCodeStorage)
    , documentSenderStorage(rhs.documentSenderStorage)

    , oldConferenceNameStorage(rhs.conferenceNameStorage)
    , oldDocumentSenderStorage(rhs.documentSenderStorage)

    , confEndTimeCountFlg(rhs.confEndTimeCountFlg)
{
    V5ConferenceStatus::ConferenceName = conferenceNameStorage.c_str();
    V5ConferenceStatus::RoomName = roomNameStorage.c_str();
    V5ConferenceStatus::ConferenceURL = conferenceURLStorage.c_str();
    V5ConferenceStatus::ConferencePinCode = conferencePinCodeStorage.c_str();
    V5ConferenceStatus::DocumentSender = documentSenderStorage.c_str();
    
    V5ConferenceStatus::OldConferenceName = oldConferenceNameStorage.c_str();
    V5ConferenceStatus::OldDocumentSender = oldDocumentSenderStorage.c_str();
    
}

void ConferenceStatusStorage::update(picojson::value const& json)
{
    auto& data = json["data"];
    
    if( data.contains("conferenceEndTime") )
    {
        auto conferenceEndTime = data["conferenceEndTime"].get<std::string>();
        if( conferenceEndTime.find_first_of("-0123456789") != std::string::npos )
        {
            V5ConferenceStatus::OldConferenceEndTime = V5ConferenceStatus::ConferenceEndTime;
            V5ConferenceStatus::ConferenceEndTime = std::stoll(data["conferenceEndTime"].get<std::string>());
            if( V5ConferenceStatus::OldConferenceEndTime != V5ConferenceStatus::ConferenceEndTime )
            {
                V5ConferenceStatus::ConferenceEndTimeChanged = true;
            }
            else
            {
                V5ConferenceStatus::ConferenceEndTimeChanged = false;
            }
        }
        else
        {
            // 受信したデータの内容が異常なので、情報を更新しない
            V5ConferenceStatus::ConferenceEndTimeChanged = false;
        }
    }
    else
    {
        V5ConferenceStatus::ConferenceEndTimeChanged = false;
    }
    
    if (data.contains("remainTime") )
    {
        auto remainTime = data["remainTime"].get<std::string>();
        if( remainTime.find_first_of("-0123456789") != std::string::npos )
        {
            V5ConferenceStatus::OldRemainTime = V5ConferenceStatus::RemainTime;
            V5ConferenceStatus::RemainTime = std::stoll(data["remainTime"].get<std::string>());
            if( V5ConferenceStatus::OldRemainTime != V5ConferenceStatus::RemainTime )
            {
                V5ConferenceStatus::RemainTimeChanged = true;
            }
            else
            {
                V5ConferenceStatus::RemainTimeChanged = false;
            }
        }
        else
        {
            // パラメータの内容がおかしいので更新しない。
            // 変更フラグもfalseにする。
            V5ConferenceStatus::RemainTimeChanged = false;
        }
    }
    else
    {
        // パラメータ変更がないため、変更フラグをfalseにする。
        V5ConferenceStatus::RemainTimeChanged = false;
    }
    
    if (data.contains("conferenceName"))
    {
        oldConferenceNameStorage = conferenceNameStorage;
        V5ConferenceStatus::OldConferenceName = oldConferenceNameStorage.c_str();
        conferenceNameStorage = data["conferenceName"].get<std::string>();
        V5ConferenceStatus::ConferenceName = conferenceNameStorage.c_str();
        if( oldConferenceNameStorage != conferenceNameStorage )
        {
            V5ConferenceStatus::ConferenceNameChanged = true;
        }
        else
        {
            V5ConferenceStatus::ConferenceNameChanged = false;
        }
    }
    else
    {
        // パラメータ変更がないため、変更フラグをfalseにする。
        V5ConferenceStatus::ConferenceNameChanged = false;
    }
    
    if (data.contains("documentSender") && data["documentSender"] != "")
    {
        oldDocumentSenderStorage = documentSenderStorage;
        V5ConferenceStatus::OldDocumentSender = oldDocumentSenderStorage.c_str();
        documentSenderStorage = data["documentSender"].get<std::string>();
        V5ConferenceStatus::DocumentSender = documentSenderStorage.c_str();
        if( oldDocumentSenderStorage != documentSenderStorage )
        {
            V5ConferenceStatus::DocumentSenderChanged = true;
        }
        else
        {
            V5ConferenceStatus::DocumentSenderChanged = false;
        }
    }
    else
    {
        // パラメータ変更がないため、変更フラグをfalseにする。
        V5ConferenceStatus::DocumentSenderChanged = false;
    }
    
    if (data.contains("isRecoading") && data["isRecoading"] != "")
    {
        V5ConferenceStatus::OldisRecoading = V5ConferenceStatus::isRecoading;
        V5ConferenceStatus::isRecoading = (data["isRecoading"] == "true");
        if( V5ConferenceStatus::OldisRecoading != V5ConferenceStatus::isRecoading )
        {
            V5ConferenceStatus::isRecoadingChanged = true;
        }
        else
        {
            V5ConferenceStatus::isRecoadingChanged = false;
        }
    }
    else
    {
        // パラメータ変更がないため、変更フラグをfalseにする。
        V5ConferenceStatus::isRecoadingChanged = false;
    }
    
    if (data.contains("isLocked") && data["isLocked"] != "")
    {
        V5ConferenceStatus::OldisLocked = V5ConferenceStatus::isLocked;
        V5ConferenceStatus::isLocked = (data["isLocked"] == "true");
        if( V5ConferenceStatus::OldisLocked != V5ConferenceStatus::isLocked )
        {
            V5ConferenceStatus::isLockedChanged = true;
        }
        else
        {
            V5ConferenceStatus::isLockedChanged = false;
        }
    }
    else
    {
        // パラメータ変更がないため、変更フラグをfalseにする。
        V5ConferenceStatus::isLockedChanged = false;
    }
    
    if (data.contains("maintenanceStartTime"))
    {
        auto maintenanceStartTime = data["maintenanceStartTime"].get<std::string>();
        if( std::all_of(maintenanceStartTime.cbegin(), maintenanceStartTime.cend(), isdigit) )
        {
            V5ConferenceStatus::OldMaintenanceStartTime = V5ConferenceStatus::MaintenanceStartTime;
            V5ConferenceStatus::MaintenanceStartTime = std::stoll(data["maintenanceStartTime"].get<std::string>());
            if( V5ConferenceStatus::OldMaintenanceStartTime != V5ConferenceStatus::MaintenanceStartTime )
            {
                V5ConferenceStatus::MaintenanceStartTimeChanged = true;
            }
            else
            {
                V5ConferenceStatus::MaintenanceStartTimeChanged = false;
            }
        }
        else
        {
            // パラメータの内容がおかしいので更新しない。
            // 変更フラグもfalseにする。
            V5ConferenceStatus::MaintenanceStartTimeChanged = false;
        }
    }
    else
    {
        // パラメータ変更がないため、変更フラグをfalseにする。
        V5ConferenceStatus::MaintenanceStartTimeChanged = false;
    }
    
    if (data.contains("maintenanceEndTime") )
    {
        auto maintenanceEndTime = data["maintenanceEndTime"].get<std::string>();
        if( std::all_of(maintenanceEndTime.cbegin(), maintenanceEndTime.cend(), isdigit) )
        {
            V5ConferenceStatus::OldMaintenanceEndTime = V5ConferenceStatus::MaintenanceEndTime;
            V5ConferenceStatus::MaintenanceEndTime = std::stoll(data["maintenanceEndTime"].get<std::string>());
            if( V5ConferenceStatus::OldMaintenanceEndTime != V5ConferenceStatus::MaintenanceEndTime )
            {
                V5ConferenceStatus::MaintenanceEndTimeChanged = true;
            }
            else
            {
                V5ConferenceStatus::MaintenanceEndTimeChanged = false;
            }
        }
        else
        {
            // パラメータの内容がおかしいので更新しない。
            // 変更フラグもfalseにする。
            V5ConferenceStatus::MaintenanceEndTimeChanged = false;
        }
    }
    else
    {
        // パラメータ変更がないため、変更フラグをfalseにする。
        V5ConferenceStatus::MaintenanceEndTimeChanged = false;
    }
    
    if (data.contains("maintenanceRemainTime"))
    {
        auto maintenanceRemainTime = data["maintenanceRemainTime"].get<std::string>();
        if( std::all_of(maintenanceRemainTime.cbegin(), maintenanceRemainTime.cend(), isdigit) )
        {
            V5ConferenceStatus::OldMaintenanceRemainTime = V5ConferenceStatus::MaintenanceRemainTime;
            V5ConferenceStatus::MaintenanceRemainTime = std::stoll(data["maintenanceRemainTime"].get<std::string>());
            if( V5ConferenceStatus::OldMaintenanceRemainTime != V5ConferenceStatus::MaintenanceRemainTime )
            {
                V5ConferenceStatus::MaintenanceRemainTimeChanged = true;
            }
            else
            {
                V5ConferenceStatus::MaintenanceRemainTimeChanged = false;
            }
        }
        else
        {
            // パラメータの内容がおかしいので更新しない。
            // 変更フラグもfalseにする。
            V5ConferenceStatus::MaintenanceRemainTimeChanged = false;
        }
        
    }
    else
    {
        // パラメータ変更がないため、変更フラグをfalseにする。
        V5ConferenceStatus::MaintenanceRemainTimeChanged = false;
    }
    
    // 会議超過時間通知処理開始判定
    
    // 予約会議判定
    if( V5ConferenceStatus::isReserved == true )
    {
        // 予約会議ルート
        if( V5ConferenceStatus::RemainTime <= 0 )
        {
            confEndTimeCountFlg = true;
        }
        else
        {
            confEndTimeCountFlg = false;
        }
    }
    else
    {
        // 非予約(通常)会議ルート
        if(V5ConferenceStatus::ConferenceEndTime == 0 )
        {
            // 後発予約会議なし
            // 会議超過時間通知停止
            confEndTimeCountFlg = false;
        }
        else
        {
            // 後発予約あり
            if( V5ConferenceStatus::RemainTime <= 0 ){
                // 会議終了時間ありかつ残り時間が0以下
                // 会議超過時間通知開始
                confEndTimeCountFlg = true;
            }
            else
            {
                // 会議終了時間ありかつ残り時間が1以上
                // = UI側でハンドリングするので放置
                // 会議超過時間通知停止
                confEndTimeCountFlg = false;
            }
        }
    }
    
}

bool ConferenceStatusStorage::getConfEndTimeCountFlg() const
{
    return confEndTimeCountFlg;
}



std::ostream& operator<<(std::ostream& os, const ConferenceStatusStorage& cluster)
{
    return os
        << "--------------------------------------------------------" << std::endl
        << "[ConferenceStatus Data]" << std::endl
        << "conferenceNameStorage       : " << cluster.conferenceNameStorage << std::endl
        << "roomNameStorage             : " << cluster.roomNameStorage << std::endl
        << "conferenceURLStorage        : " << cluster.conferenceURLStorage << std::endl
        << "conferencePinCodeStorage    : " << cluster.conferencePinCodeStorage << std::endl
        << "documentSenderStorage       : " << cluster.documentSenderStorage << std::endl
        << "ConferenceName              : " << cluster.V5ConferenceStatus::ConferenceName << std::endl
        << "RoomName                    : " << cluster.V5ConferenceStatus::RoomName << std::endl
        << "ConferenceURL               : " << cluster.V5ConferenceStatus::ConferenceURL << std::endl
        << "ConferencePinCode           : " << cluster.V5ConferenceStatus::ConferencePinCode << std::endl
        << "DocumentSender              : " << cluster.V5ConferenceStatus::DocumentSender << std::endl
        << "PastTime                    : " << cluster.V5ConferenceStatus::PastTime << std::endl
        << "RemainTime                  : " << cluster.V5ConferenceStatus::RemainTime << std::endl
        << "MaintenanceStartTime        : " << cluster.V5ConferenceStatus::MaintenanceStartTime << std::endl
        << "MaintenanceEndTime          : " << cluster.V5ConferenceStatus::MaintenanceEndTime << std::endl
        << "MaintenanceRemainTime       : " << cluster.V5ConferenceStatus::MaintenanceRemainTime << std::endl
        << "ConferenceStartTime         : " << cluster.V5ConferenceStatus::ConferenceStartTime << std::endl
        << "ConferenceEndTime           : " << cluster.V5ConferenceStatus::ConferenceEndTime << std::endl
        << "isRecoading                 : " << cluster.V5ConferenceStatus::isRecoading << std::endl
        << "isReserved                  : " << cluster.V5ConferenceStatus::isReserved << std::endl
        << "isLocked                    : " << cluster.V5ConferenceStatus::isLocked << std::endl
    
        << "oldConferenceNameStorage    : " << cluster.oldConferenceNameStorage << std::endl
        << "oldConferenceName           : " << cluster.V5ConferenceStatus::OldConferenceName << std::endl
        << "oldDocumentSenderStorage    : " << cluster.oldDocumentSenderStorage << std::endl
        << "oldDocumentSender           : " << cluster.V5ConferenceStatus::OldDocumentSender << std::endl
        << "oldConferenceEndTime        : " << cluster.V5ConferenceStatus::OldConferenceEndTime << std::endl
        << "OldRemainTime               : " << cluster.V5ConferenceStatus::OldRemainTime << std::endl
        << "OldMaintenanceStartTime     : " << cluster.V5ConferenceStatus::OldMaintenanceStartTime << std::endl
        << "OldMaintenanceEndTime       : " << cluster.V5ConferenceStatus::OldMaintenanceEndTime << std::endl
        << "OldMaintenanceRemainTime    : " << cluster.V5ConferenceStatus::OldMaintenanceRemainTime << std::endl
        << "OldisRecoading              : " << cluster.V5ConferenceStatus::OldisRecoading << std::endl
        << "OldisLocked                 : " << cluster.V5ConferenceStatus::OldisLocked << std::endl
    
        << "ConferenceNameChanged           : " << cluster.V5ConferenceStatus::ConferenceNameChanged << std::endl
        << "DocumentSenderChanged           : " << cluster.V5ConferenceStatus::DocumentSenderChanged << std::endl
        << "ConferenceEndTimeChanged        : " << cluster.V5ConferenceStatus::ConferenceEndTimeChanged << std::endl
        << "RemainTimeChanged               : " << cluster.V5ConferenceStatus::RemainTimeChanged << std::endl
        << "MaintenanceStartTimeChanged     : " << cluster.V5ConferenceStatus::MaintenanceStartTimeChanged << std::endl
        << "MaintenanceEndTimeChanged       : " << cluster.V5ConferenceStatus::MaintenanceEndTimeChanged << std::endl
        << "MaintenanceRemainTimeChanged    : " << cluster.V5ConferenceStatus::MaintenanceRemainTimeChanged << std::endl
        << "isRecoadingChanged              : " << cluster.V5ConferenceStatus::isRecoadingChanged << std::endl
        << "isLockedChanged                 : " << cluster.V5ConferenceStatus::isLockedChanged << std::endl
        << "--------------------------------------------------------";
}

// チャットログ
ChatLogStorage::ChatLogStorage(picojson::value const& json, std::string const& selfPID)
{
    auto& logDataArray = json["data"].get<picojson::array>();

    messageList.reserve(logDataArray.size() * 2 + 100);

    /* ChatLog Original Data Save */
    for (auto& element : logDataArray)
    {
        auto cluster = std::make_shared<ChatLogCluster>(element);
        cluster->isSelfMessage = (cluster->from == selfPID);

        messageList.push_back(cluster.get());
        clusterStorage.push_back(cluster);
    }
}

ChatLogClusterPtrT ChatLogStorage::addMessage(picojson::value const& json, std::string const& selfPID)
{
    auto cluster = std::make_shared<ChatLogCluster>(json);
    cluster->isSelfMessage = (cluster->from == selfPID);
    
    if (messageList.capacity() == messageList.size())
        messageList.reserve(messageList.size() * 2 + 100);
    messageList.push_back(cluster.get());
    clusterStorage.push_back(cluster);
    return std::move(cluster);
}

std::ostream& operator<<(std::ostream& os, const ChatLogCluster& cluster)
{
    return os
        << "from                : " << cluster.fromStorage << std::endl
        << "text                : " << cluster.textStorage << std::endl
        << "displayName         : " << cluster.displayNameStorage << std::endl
        << "msg.from            : " << cluster.V5ChatMessage::from << std::endl
        << "msg.text            : " << cluster.V5ChatMessage::text << std::endl
        << "msg.displayName     : " << cluster.V5ChatMessage::displayName << std::endl
        << "msg.timestamp       : " << cluster.V5ChatMessage::timestamp << std::endl
        << "msg.isSelfMessage   : " << cluster.V5ChatMessage::isSelfMessage;
}

ChatLogCluster::ChatLogCluster(picojson::value const& json)
    : textStorage(json["data"]["message"].get<std::string>())
    , fromStorage(json["from"].get<std::string>())
    , displayNameStorage(json["data"]["displayname"].get<std::string>())

{
    auto& data = json["data"];

    V5ChatMessage::from = fromStorage.c_str();
    V5ChatMessage::text = textStorage.c_str();
    V5ChatMessage::displayName = displayNameStorage.c_str();
    V5ChatMessage::timestamp = std::stoll(data["timestamp"].get<std::string>());
    V5ChatMessage::isSelfMessage = false;
}

std::ostream& operator<<(std::ostream& os, const ChatLogStorage& storage)
{
    int cnt = 0;
    os  << "[JsonParseChatLog] DEBUG" << std::endl
        << "[ChatLogBuffer Data]" << std::endl
        << "" << std::endl;
    for (auto& element : storage.clusterStorage)
    {
        os  << "[ChatLogBuffer No." << cnt++ << "]" << std::endl
            << "------------------------------------" << std::endl
            << "address             : " << element << std::endl
            << *element << std::endl
            << std::endl;
    }

    cnt = 0;
    os << "[ChatLog Data]" << std::endl
        << "" << std::endl;
    for (auto& element : storage.messageList)
    {
        os << "[ChatLog No." << cnt++ << "]" << std::endl
            << "------------------------------------" << std::endl
            << "address       : " << element << std::endl
            << "from          : " << element->from << std::endl
            << "text          : " << element->text << std::endl
            << "displayName   : " << element->displayName << std::endl
            << "timestamp     : " << element->timestamp << std::endl
            << "isSelfMessage : " << element->isSelfMessage << std::endl
            << std::endl;
    }
    return os;
}

// 会議参加者情報
ParticipantCluster::ParticipantCluster()
{
    V5Participant::displayName = displayNameStorage.c_str();
    V5Participant::id = participantIDStorage.c_str();
}

ParticipantCluster::ParticipantCluster(picojson::value const& json)
    : displayNameStorage(json["display_name"].get<std::string>())
    , participantIDStorage(json["participant_id"].get<std::string>())
{
    V5Participant::displayName = displayNameStorage.c_str();
    V5Participant::id = participantIDStorage.c_str();
}

ParticipantCluster::ParticipantCluster( std::string const& pid, std::string const& displayName )
    : displayNameStorage( displayName )
    , participantIDStorage( pid )
{
    V5Participant::displayName = displayNameStorage.c_str();
    V5Participant::id = participantIDStorage.c_str();
}


std::ostream& operator<<(std::ostream& os, const ParticipantCluster& cluster)
{
    return os
        << "displayNameStorage          : " << cluster.displayNameStorage << std::endl
        << "participantIDStorage        : " << cluster.participantIDStorage << std::endl
        << "V5Participant::displayName  : " << cluster.V5Participant::displayName << std::endl
        << "V5Participant::id           : " << cluster.V5Participant::id << std::endl;
}

ParticipantListStorage::ParticipantListStorage(picojson::value const& json, std::string const& selfPID)
{
    auto& jsonArray = json["participants"].get<picojson::array>();

    participantList.reserve(jsonArray.size() + 20);

    for (auto& element : jsonArray)
    {
        auto cluster = std::make_shared<ParticipantCluster>(element);
        participantMap[cluster->getID()] = cluster;

        if (cluster->getID() == selfPID)
        {
            self = cluster;
        }
        else
        {
            participantList.push_back(cluster.get());
        }
    }
}

ParticipantClusterPtr ParticipantListStorage::add(picojson::value const& json)
{
    auto& data = json["data"];
    auto& id = data["participant_id"].get<std::string>();

    auto ite = participantMap.find(id);
    if (ite != participantMap.end())
        return nullptr;

    auto added = std::make_shared<ParticipantCluster>(data);
    participantMap[id] = added;
    participantList.push_back(added.get());
    return added;
}

ParticipantClusterPtr ParticipantListStorage::remove(picojson::value const& json)
{
    auto& data = json["data"];
    auto& id = data["participant_id"].get<std::string>();

    ParticipantClusterPtr find;
    auto ite = participantMap.find(id);
    if (ite == participantMap.end())
        return std::move(find);

    find = ite->second;
    participantMap.erase(ite);
    for (auto ite = participantList.begin(); ite != participantList.end(); ite++)
    {
        if ((*ite)->id == id)
        {
            participantList.erase(ite);
            break;
        }
    }
    return std::move(find);
}

std::ostream& operator<<(std::ostream& os, const ParticipantListStorage& storage)
{
    int count = 0;
    os
        << "[JsonParseParticipantList] DEBUG" << std::endl
        << "[Plist_map Data]" << std::endl
        << std::endl;
    for (auto& element : storage.participantMap)
    {
        os
            << "[UserNo." << count++ << "]" << std::endl
            << "key                         : " << element.first << std::endl
            << "address                     : " << element.second << std::endl
            << *element.second << std::endl;
    }

    count = 0;
    os
        << "[Plist_vec Data]" << std::endl
        << std::endl;
    for (auto& element : storage.participantList)
    {
        os
            << "[UserNo." << count++ << "]" << std::endl
            << "displayName : " << element->displayName << std::endl
            << "id          : " << element->id << std::endl;
    }
    return os;
}

// Proxy 関連情報
ProxyStorage::ProxyStorage()
: proxyAddress( "" )
, proxyPort( "" )
, proxyID( "" )
, proxyPW( "" )
{
    isProxySet = false;
}

ProxyStorage::ProxyStorage(std::string const& address,
                           long const& port,
                           std::string const& id,
                           std::string const& pw )
: proxyAddress( address )
, proxyPort( std::to_string( port ) )
, proxyID( id )
, proxyPW( pw )
{
    if( ( proxyAddress.length() != 0 ) &&
        ( proxyPort.length() != 0 ) &&
        ( proxyID.length() != 0 ) &&
        ( proxyPW.length() != 0 ) )
    {
        isProxySet = true;
    }
    else
    {
        isProxySet = false;
    }
    
}

void ProxyStorage::update(std::string const& address,
                          long const& port,
                          std::string const& id,
                          std::string const& pw)
{
    proxyAddress = address;
    proxyPort = std::to_string( port );
    proxyID = id;
    proxyPW = pw;
    
    if( ( proxyAddress.length() != 0 ) &&
       ( proxyPort.length() != 0 ) )
    {
        isProxySet = true;
    }
    else
    {
        isProxySet = false;
    }
}

void ProxyStorage::erase()
{
    isProxySet = false;
    proxyAddress = "";
    proxyPort = "";
    proxyID = "";
    proxyPW = "";
}

std::ostream& operator<<(std::ostream& os, const ProxyStorage& Proxy)
{
    return os
    << "address : " << Proxy.proxyAddress << std::endl
    << "port    : " << Proxy.proxyPort << std::endl
    << "id      : " << Proxy.proxyID << std::endl
    << "pw      : " << Proxy.proxyPW << std::endl;
}



