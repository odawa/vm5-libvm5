﻿#pragma once
#ifndef EVENT_FLAG_H_INCLUDED
#define EVENT_FLAG_H_INCLUDED

#include <mutex>
#include <condition_variable>

namespace vrms5
{
    namespace utils
    {
        class EventFlag
        {
        public:
            enum SignalState : bool {
                NON_SIGNALED = false,
                SIGNALED = true,
            };

            explicit EventFlag(SignalState initial)
                : signal(initial)
            {
            }

            EventFlag(EventFlag const&) = delete;
            EventFlag(EventFlag &&) = delete;
            EventFlag const & operator=(EventFlag const&) = delete;
            EventFlag const & operator=(EventFlag&&) = delete;

            virtual ~EventFlag()
            {
            }

            SignalState isSignal() const
            {
                std::lock_guard<std::mutex> lock(mtx);
                return signal;
            }

            bool setSignal()
            {
                std::lock_guard<std::mutex> lock(mtx);
                if (signal)
                    return false;
                signal = SIGNALED;
                cv.notify_all();
                return true;
            }

            bool resetSignal()
            {
                std::lock_guard<std::mutex> lock(mtx);
                if (!signal)
                    return false;
                signal = NON_SIGNALED;
                return true;
            }

            void wait() const
            {
                std::unique_lock<std::mutex> uni_lk(mtx);
                cv.wait(uni_lk, [this]{ return signal;  });
            }

            template<class Rep, class Period>
            bool wait_for(const std::chrono::duration<Rep, Period>& Rel_time) const
            {
                std::unique_lock<std::mutex> uni_lk(mtx);
                return cv.wait_for(uni_lk, Rel_time, [this]{ return signal; });
            }

            template<class Clock, class Duration>
            bool wait_until(const std::chrono::time_point<Clock, Duration>& abs_time) const
            {
                std::unique_lock<std::mutex> uni_lk(mtx);
                return cv.wait_until(uni_lk, abs_time, [this]{ return signal; });
            }

        private:
            mutable std::mutex mtx;
            mutable std::condition_variable cv;
            SignalState signal;

        };
    }
}

#endif // EVENT_FLAG_H_INCLUDED