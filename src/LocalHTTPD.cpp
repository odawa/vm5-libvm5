﻿#include "precompile.h"
#include "LocalHTTPD.h"

#include <Poco/Net/HTTPRequestHandler.h>
#include <Poco/Net/HTTPResponse.h>
#include <Poco/Net/HTTPServer.h>
#include <Poco/Net/HTTPServerRequest.h>
#include <Poco/Net/HTTPServerResponse.h>
#include <Poco/Net/ServerSocket.h>
#include <Poco/Net/HTMLForm.h>
#include <Poco/Util/ServerApplication.h>
#include <Poco/ThreadPool.h>
#include <Poco/Environment.h>
#include <Poco/URI.h>
#include <Poco/Net/SocketAddress.h>
#include <Poco/Net/Socket.h>

using Poco::Net::NameValueCollection;
using Poco::Net::HTTPRequestHandler;
using Poco::Net::HTTPRequestHandlerFactory;
using Poco::Net::HTTPResponse;
using Poco::Net::HTTPServer;
using Poco::Net::HTTPServerParams;
using Poco::Net::HTTPServerRequest;
using Poco::Net::HTTPServerResponse;
using Poco::Net::ServerSocket;
using Poco::Net::HTMLForm;
using Poco::Net::SocketAddress;
using Poco::Net::Socket;
using Poco::Util::ServerApplication;
using Poco::ThreadPool;
using Poco::Environment;
using Poco::IOException;
using Poco::URI;

// -------------------------------------------------------------------------------------------
class LocalHTTPDRequestHandler : public HTTPRequestHandler {
public:
	typedef LocalHTTPD::HTTPRequestParseResult HTTPRequestParseResult;
	typedef LocalHTTPD::requestQuery requestQuery;

	LocalHTTPDRequestHandler(const std::weak_ptr<LocalHTTPD>& in_httpd) {
        httpd_wp = in_httpd;
    }
    virtual void handleRequest(HTTPServerRequest& req, HTTPServerResponse& resp);
private:
	std::weak_ptr<LocalHTTPD> httpd_wp;
};

// -------------------------------------------------------------------------------------------
class LocalHTTPDRequestHandlerFactry : public HTTPRequestHandlerFactory {
public:
	LocalHTTPDRequestHandlerFactry(const std::weak_ptr<LocalHTTPD>& in_httpd) {
        httpd_wp = in_httpd;
    }
    
    virtual HTTPRequestHandler* createRequestHandler(const HTTPServerRequest&){
        return new LocalHTTPDRequestHandler(httpd_wp);
    }
private:
	std::weak_ptr<LocalHTTPD> httpd_wp;
};

// -------------------------------------------------------------------------------------------
// 受信したHTTPリクエストの内容によりコールバック呼び出しやレスポンスの生成を行う。
void LocalHTTPDRequestHandler::handleRequest(HTTPServerRequest& req, HTTPServerResponse& resp){
    
    if (httpd_wp.expired()) {
        // 503 Response
        resp.setStatus(HTTPResponse::HTTP_SERVICE_UNAVAILABLE);
        resp.setReason(HTTPResponse::HTTP_REASON_SERVICE_UNAVAILABLE);
		std::ostream& respMsg = resp.send();
        respMsg.flush();
        return;
    }
	std::shared_ptr<LocalHTTPD> httpd = httpd_wp.lock(); // メソッド利用のためshared_ptr取り出し。
	std::shared_ptr<HTTPRequestParseResult> parsedRequest(new HTTPRequestParseResult);
	std::shared_ptr<requestQuery> query(new requestQuery);
    parsedRequest->query = query;

    // HTTP Request解析
    if ( !httpd->paerseHTTPRequest(req, parsedRequest) ) {
        
        // 503 Response
        resp.setStatus(HTTPResponse::HTTP_SERVICE_UNAVAILABLE);
        resp.setReason(HTTPResponse::HTTP_REASON_SERVICE_UNAVAILABLE);
		std::ostream& respMsg = resp.send();
        respMsg.flush();
        return;
    }

    // コールバック呼び出し
    if ( !httpd->invokeCallback(parsedRequest, resp) ) {
        
        // 404 Response
        resp.setStatus(HTTPResponse::HTTP_NOT_FOUND);
        resp.setReason(HTTPResponse::HTTP_REASON_NOT_FOUND);
		std::ostream& respMsg = resp.send();
        respMsg.flush();
        return;
    }
}

// 既にpathが同じものが登録されていた場合はreturn false
bool LocalHTTPD::addResouce(std::string path, std::function<bool(std::shared_ptr<requestQuery>, HTTPServerResponse &)> func_obj) {
	std::pair<PathCallback::iterator, bool> ret;
	ret = httpResponse.insert(PathCallback::value_type(path, func_obj));

	return ret.second;
}

// 関数ポインタ削除
// 一致するpathが無ければreturn false
bool LocalHTTPD::removeResouce(std::string path){
    PathCallback::iterator it = httpResponse.find(path);
    if ( it == httpResponse.end() ) { return false; } // 該当なし
    httpResponse.erase(it);
    return true;
}

bool LocalHTTPD::invokeCallback(std::shared_ptr<HTTPRequestParseResult> result, HTTPServerResponse& resp) {
    PathCallback::iterator it = httpResponse.find(result->path);
    if ( it == httpResponse.end() ) {
        return false;
    }
	it->second(result->query, resp); // コールバック呼出
	return true;
}

bool LocalHTTPD::paerseHTTPRequest(HTTPServerRequest& req, std::shared_ptr<HTTPRequestParseResult> result) {
    URI uri(req.getURI());
    result->method  = req.getMethod();
    result->path    = uri.getPath();
    
    // GET POST 以外のメソッドによるリクエストは受け付けない
    if ( result->method.compare("GET") != 0 && result->method.compare("POST") != 0 ) {
        return false;
    }

    // URIのクエリ部とHTTP Body部のID,Valueを連想配列に格納。
    HTMLForm form(req, req.stream());
    if ( !form.empty() ) {
        NameValueCollection::ConstIterator itr = form.begin();
        while ( itr != form.end() ) {
            result->query->insert(requestQuery::value_type(itr->first, itr->second));
            itr++;
        }
    }

    return true;
}

void LocalHTTPD::initialize() {
    // 端末のCore数をスレッド数の下限/上限とする
    int threadMin = Environment::processorCount();
    int threadMax = Environment::processorCount();
    
    threadPool = std::make_shared<ThreadPool>(threadMin, threadMax);
    requestHandlerFactory = LocalHTTPDRequestHandlerFactry::Ptr(new LocalHTTPDRequestHandlerFactry(shared_from_this()));
    serverParams = HTTPServerParams::Ptr(new Poco::Net::HTTPServerParams);
}

bool LocalHTTPD::startV4(int listenPort) {
    ServerSocket sock;
    try {
        SocketAddress sockAddress("127.0.0.1", listenPort);
        sock.bind(sockAddress, true);
        sock.listen();
    }
    catch(IOException ex) {
        return false;
    }
    
	std::shared_ptr<HTTPServer> server(new HTTPServer(requestHandlerFactory, *threadPool, sock, serverParams));
    httpServer = server;
    httpServer->start();
    
    return true;
}

bool LocalHTTPD::startV6(int listenPort) {
    ServerSocket sock;
    try {
        SocketAddress sockAddress("::1", listenPort);
        sock.bind6(sockAddress, true, true);
        sock.listen();
    }
    catch(IOException ex) {
        return false;
    }
    
	std::shared_ptr<HTTPServer> server(new HTTPServer(requestHandlerFactory, *threadPool, sock, serverParams));
    httpServerV6 = server;
    httpServerV6->start();
    
    return true;
}

V5HTTPD_START LocalHTTPD::start(int listenPort) {
    initialize();
    
    if(Socket::supportsIPv4()) {
        if(!startV4(listenPort)) {
            return V5HTTPD_START::IPV4_ERROR;
        }
    }

    if(Socket::supportsIPv6()) {
        if(!startV6(listenPort)) {
            return V5HTTPD_START::IPV6_ERROR;
        }
    }

    return V5HTTPD_START::OK;
}

void LocalHTTPD::stop() {
    if(Socket::supportsIPv6() && httpServerV6) {
        httpServerV6->stopAll();
    }
    if(Socket::supportsIPv4() && httpServer) {
        httpServer->stopAll();
    }
    return;
}