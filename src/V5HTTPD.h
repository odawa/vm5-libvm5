﻿#pragma once
#ifndef __V5HTTPD__
#define __V5HTTPD__

#include "LocalHTTPD.h"
#include "LocalHTTPD_IF.h"
#include "IHttpd.h"
#include <cassert>

class V5HTTPD : public IHttpd {
public:
	typedef LocalHTTPD::HTTPServerResponse HTTPServerResponse;
	typedef LocalHTTPD::requestQuery requestQuery;


	typedef std::weak_ptr<IV5HTTPDListener> listener_wp;
	explicit V5HTTPD(listener_wp listener);
	virtual ~V5HTTPD();

	V5HTTPD_START HTTPDStart(unsigned short port) {
		return httpd->start(port);
	}

	void HTTPDStop() {
		httpd->stop();
	}

private:
	bool request_crossdomain(std::shared_ptr<requestQuery> query, HTTPServerResponse& resp);
	bool request_alive(std::shared_ptr<requestQuery> query, HTTPServerResponse& resp);
	bool request_joinConference(std::shared_ptr<requestQuery> query, HTTPServerResponse& resp);

	listener_wp listener_;
	std::shared_ptr<LocalHTTPD> httpd;
};

#endif /* defined(__V5HTTPD__) */
