﻿//
//  DefaultFrameSize.h
//  LibV5Lite
//
//  Created by V-1094 on 2015/01/26.
//  Copyright (c) 2015 V-cube, Inc. All rights reserved.
//

#ifndef __LIBV5_LITE_DEFAULT_FRAME_SIZE_H_
#define __LIBV5_LITE_DEFAULT_FRAME_SIZE_H_

struct DefaultFrameSize {
    int width;
    int height;
    int frameRate;
    int minFrameInterval;
    DefaultFrameSize() {}
    DefaultFrameSize(int width, int height, int frameRate, int minFrameInterval) :
    width(width), height(height), frameRate(frameRate), minFrameInterval(minFrameInterval) {}
};

#endif // __LIBV5_LITE_DEFAULT_FRAME_SIZE_H_
