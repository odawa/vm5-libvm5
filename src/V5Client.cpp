﻿#include "precompile.h"
#include <V5Lite/V5Client.h>
#include "V5ClientImpl.h"

namespace vrms5
{
	V5Client::~V5Client()
	{

	}

    V5CODE V5Client::initialize(const char *logpath, const char *log_priority)
    {
        return V5ClientImpl::initialize(logpath, log_priority);
    }
    
    V5CODE V5Client::finalize()
    {
        return V5ClientImpl::finalize();
    }
    
    V5Client::instance_ptr_t V5Client::createInstance()
    {
        return std::move(V5ClientImpl::createInstance());
    }
    

}
