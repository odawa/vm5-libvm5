﻿#include <iostream>

#ifndef _PROXYSETTING_H_
#define _PROXYSETTING_H_

namespace vrms5
{
    class ProxySetting
    {
    public:
        ProxySetting() {
            proxyAddress  = std::make_shared<std::string>("");
            proxyPort     = 0;
            proxyId       = std::make_shared<std::string>("");
            proxyPassword = std::make_shared<std::string>("");
        }

        std::shared_ptr<std::string> const getProxyAddress() const {
            return std::make_shared<std::string>(*proxyAddress);
        }

        void setProxyAddress(std::string const &proxyAddress) {
            ProxySetting::proxyAddress = std::make_shared<std::string>(proxyAddress);
        }

        long getProxyPort() const {
            return proxyPort;
        }

        void setProxyPort(long proxyPort) {
            ProxySetting::proxyPort = proxyPort;
        }

        std::shared_ptr<std::string> const getProxyId() const {
            return std::make_shared<std::string>(*proxyId);
        }

        void setProxyId(std::string const &proxyId) {
            ProxySetting::proxyId = std::make_shared<std::string>(proxyId);
        }

        std::shared_ptr<std::string> const getProxyPassword() const {
            return std::make_shared<std::string>(*proxyPassword);
        }

        void setProxyPassword(std::string const &proxyPassword) {
            ProxySetting::proxyPassword = std::make_shared<std::string>(proxyPassword);
        }

    private:
        std::shared_ptr<std::string> proxyAddress;
        long                         proxyPort;
        std::shared_ptr<std::string> proxyId;
        std::shared_ptr<std::string> proxyPassword;

    };
}

#endif //_PROXYSETTING_H_
