//
//  ObjcV5Params.m
//  LibV5Lite
//
//  Created by Hiromi on 2015/02/18.
//  Copyright (c) 2015 V-cube, Inc. All rights reserved.
//

#import "ObjcParams.h"
#import "V5DataTypes.h"

@implementation ObjcConferenceInfo

- (instancetype)init {
    self = [super init];
    if (self) {
        self.participantList = [[ObjcParticipantArray alloc] init];

        self.selfParticipant = [[ObjcParticipant alloc] init];
        self.selfParticipant.displayName          = @"";
        self.selfParticipant.id                   = @"";

        self.conferenceStatus = [[ObjcConferenceStatus alloc] init];
    }

    return self;
}

- (void)setParticipantArray:(const V5Participant * const*) participantArray length:(unsigned long) length
{
    NSMutableArray *mutableArray = [[NSMutableArray alloc] init];
    for(unsigned long i = 0; i < length; i++) {
        ObjcParticipant *part = [[ObjcParticipant alloc] init];
        [part setParticipant:participantArray[i]];
        [mutableArray addObject:part];
    }
    self.participantList = [[ObjcParticipantArray alloc] init];
    [self.participantList setParticipants:[mutableArray copy]];
}

- (void)setConferenceStatusData:(const V5ConferenceStatus*) status
{
    self.conferenceStatus = [[ObjcConferenceStatus alloc] init];
    [self.conferenceStatus setConferenceStatus:status];
}

- (void)setSelfParticipantData:(const V5Participant*)participant {
    _selfParticipant = [[ObjcParticipant alloc] init];
    [_selfParticipant setParticipant:participant];
}

@end

@implementation ObjcConferenceStatus

- (instancetype)init {
    self = [super init];
    if (self) {
        _conferenceName      = @"";
        _roomName            = @"";
        _conferenceURL       = @"";
        _documentSender      = @"";
        _conferenceStartTime = 0;
        _conferenceEndTime   = 0;
        _maintenanceStartTime = 0;
        _maintenanceEndTime = 0;
        _maintenanceRemainTime = 0;
        _remainTime          = 0;
        _pastTime            = 0;
        _isReserved          = false;
        _isRecording         = false;
        _isLocked            = false;
        _oldConferenceName   = @"";
        _oldDocumentSender   = @"";
        _oldisLocked         = false;
        _oldisRecoading      = false;
        _oldMaintenanceRemainTime = 0;
        _oldMaintenanceStartTime = 0;
        _oldMaintenanceEndTime = 0;
        _oldRemainTime       = 0;
        _conferenceNameChanged = false;
        _documentSenderChanged = false;
        _remainTimeChanged = false;
        _maintenanceStartTimeChanged = false;
        _maintenanceRemainTimeChanged = false;
        _maintenanceEndTimeChanged = false;
        _isRecoadingChanged = false;
        _isLockedChanged = false;
    }

    return self;
}

- (void)setConferenceStatus:(const V5ConferenceStatus*) status {
    _conferenceName      = [NSString stringWithCString:status->ConferenceName encoding:NSUTF8StringEncoding];
    _roomName            = [NSString stringWithCString:status->RoomName encoding:NSUTF8StringEncoding];
    _conferenceURL       = [NSString stringWithCString:status->ConferenceURL encoding:NSUTF8StringEncoding];
    _conferencePinCode   = [NSString stringWithCString:status->ConferencePinCode encoding:NSUTF8StringEncoding];
    _documentSender      = [NSString stringWithCString:status->DocumentSender encoding:NSUTF8StringEncoding];
    _conferenceStartTime = status->ConferenceStartTime;
    _conferenceEndTime   = status->ConferenceEndTime;
    _remainTime          = status->RemainTime;
    _pastTime            = status->PastTime;
    _maintenanceStartTime = status->MaintenanceStartTime;
    _maintenanceEndTime = status->MaintenanceEndTime;
    _maintenanceRemainTime = status->MaintenanceRemainTime;
    _isReserved          = status->isReserved;
    _isRecording         = status->isRecoading;
    _isLocked            = status->isLocked;
    _oldConferenceName   = [NSString stringWithCString:status->OldConferenceName encoding:NSUTF8StringEncoding];
    _oldDocumentSender   = [NSString stringWithCString:status->OldDocumentSender encoding:NSUTF8StringEncoding];;
    _oldisLocked         = status->OldisLocked;
    _oldisRecoading      = status->OldisRecoading;
    _oldMaintenanceRemainTime = status->OldMaintenanceRemainTime;
    _oldMaintenanceStartTime = status->OldMaintenanceStartTime;
    _oldMaintenanceEndTime = status->OldMaintenanceEndTime;
    _oldRemainTime       = status->OldRemainTime;
    _conferenceNameChanged = status->ConferenceNameChanged;
    _documentSenderChanged = status->DocumentSenderChanged;
    _remainTimeChanged = status->RemainTimeChanged;
    _maintenanceStartTimeChanged = status->MaintenanceStartTimeChanged;
    _maintenanceRemainTimeChanged = status->MaintenanceRemainTimeChanged;
    _maintenanceEndTimeChanged = status->MaintenanceEndTimeChanged;
    _isRecoadingChanged = status->isRecoadingChanged;
    _isLockedChanged = status->isLockedChanged;
}

@end

@implementation ObjcV5StatisticsConnectivity
- (instancetype)init {
    self = [super init];
    if (self) {
        _serverAddress = @"";
        _serverPort = @"";
        _serverSecured = false;
        _vmIdentity = @"";
        _userName = @"";
        _portalAddress = @"";
        _portalVersion = @"";
        _locationTag = @"";
        _vidyoProxyAddress = @"";
        _vidyoProxyPort = @"";
        _guestLogin = false;
        _clientExternalIPAddress = @"";
        _proxyType = 0;
        _reverseProxyAddress = @"";
        _reverseProxyPort = @"";
    }
    return self;
}
@end


@implementation ObjcV5StatisticsSessionDisplayInfo
- (instancetype)init {
    self = [super init];
    if (self) {
        _sessionDisplayContext = 0;
        _sessionDisplayText = @"";
    }
    return self;
}
@end

@implementation ObjcV5StatisticsParticipantInfo
- (instancetype)init {
    self = [super init];
    if (self) {
        _name = @"";
        _URI = @"";
        _bytesRcvd = 0;
        _decodedFrameRate = 0;
        _displayedFrameRate = 0;
        _numNacksSent = 0;
        _numDistinctNacksSent = 0;
        _receivedFrameRate = 0;
        _receivedBpsAudio = 0;
        _receivedBpsVideo = 0;
        _receivedBytesAudio = 0;
        _receivedBytesVideo = 0;
        _receivedFrameRate = 0;
        _receivedHeight = 0;
        _receivedPacketRate = 0;
        _receivedWidth = 0;
    }
    return self;
}
@end

@implementation ObjcV5StatisticsParticipantData
- (instancetype)init {
    self = [super init];
    if (self) {
        _URI = @"";
        _name = @"";
        ObjcV5VRect rect;
        _videoResolution = rect;
        _videoKBitsPerSecRecv = 0;
        _audioKBitsPerSecRecv = 0;
        _firs = 0;
        _nacks = 0;
        _videoFrameRate = 0;
        _videoDecodedFrameRate = 0;
        _videoDisplayedFrameRate = 0;
    }
    return self;
}
@end

@implementation ObjcV5StatisticsParticipantStatisticsList
- (instancetype)init {
    self = [super init];
    if (self) {
        _startIndex = 0;
        _numParticipants = 0;
        _statistics = [[ObjcStatisticsParticipantDataCollection alloc] init];
    }
    return self;
}
@end


@implementation ObjcStatisticsParticipantDataCollection {
    __strong NSArray *_array;
}

- (instancetype)init {
    self = [super init];
    if (self) {
    }
    
    return self;
}


- (NSUInteger)count {
    if(_array == nil) {
        return 0;
    }
    return _array.count;
}

- (ObjcParticipant *)objectAtIndexedSubscript:(NSUInteger)index{
    return _array[index];
    
}
- (void)setParticipants:(NSArray *)array {
    _array = array;
}

- (void)iterate:(void (^)(ObjcV5StatisticsParticipantData *))participantData {
    if(_array == nil) {
        return;
    }
    for(ObjcV5StatisticsParticipantData* part in _array) {
        participantData(part);
    }
}
@end

@implementation ObjcV5StatisticsParticipantInfoCollection {
    __strong NSArray *_array;
}

- (instancetype)init {
    self = [super init];
    if (self) {
    }
    
    return self;
}


- (NSUInteger)count {
    if(_array == nil) {
        return 0;
    }
    return _array.count;
}

- (ObjcParticipant *)objectAtIndexedSubscript:(NSUInteger)index{
    return _array[index];
    
}
- (void)setParticipants:(NSArray *)array {
    _array = array;
}

- (void)iterate:(void (^)(ObjcV5StatisticsParticipantInfo *))participantInfo {
    if(_array == nil) {
        return;
    }
    for(ObjcV5StatisticsParticipantInfo* info in _array) {
        participantInfo(info);
    }
}
@end



@implementation ObjcConferenceStatisticsAllInfo
- (instancetype)init {
    self = [super init];
    if (self) {
        ObjcV5StatisticsCommunicationInfo communicationInfo;
        _communicationInfo = communicationInfo;

        ObjcV5StatisticsEndpointStatus endpointStatus;
        _endpointStatus = endpointStatus;
        
        ObjcV5StatisticsBandwidthInfo bandwidthInfo;
        _bandwidthInfo = bandwidthInfo;
        
        ObjcV5StatisticsConferenceInfo conferenceInfo;
        _conferenceInfo = conferenceInfo;
        
        _connectivity = [[ObjcV5StatisticsConnectivity alloc]init];
        _sessionDisplayInfo = [[ObjcV5StatisticsSessionDisplayInfo alloc] init];
        
        ObjcV5StatisticsMediaInfo mediaInfo;
        _mediaInfo = mediaInfo;
        
        _participantInfoCollection = [[ObjcV5StatisticsParticipantInfoCollection alloc] init];
        
        ObjcV5StatisticsRateShaperInfo rateShaperInfo;
        _rateShaperInfo = rateShaperInfo;
        
        ObjcV5StatisticsFrameRateInfo frameRateInfo;
        _frameRateInfo = frameRateInfo;
        
        _participantStatisticsCollection = [[ObjcStatisticsParticipantDataCollection alloc] init];
    }
    return self;
}
@end

@implementation ObjcParticipant

- (instancetype)init {
    self = [super init];
    if (self) {
        _displayName = @"";
        _id          = @"";
    }
    return self;
}

- (void)setParticipant:(const V5Participant*) participant {
    _displayName = [NSString stringWithCString:participant->displayName encoding:NSUTF8StringEncoding];
    _id          = [NSString stringWithCString:participant->id encoding:NSUTF8StringEncoding];
}

@end

@implementation ObjcParticipantArray {
    __strong NSArray *_array;
}

- (instancetype)init {
    self = [super init];
    if (self) {
    }

    return self;
}


- (NSUInteger)count {
    if(_array == nil) {
        return 0;
    }
    return _array.count;
}

- (ObjcParticipant *)objectAtIndexedSubscript:(NSUInteger)index{
    return _array[index];

}
- (void)setParticipants:(NSArray *)array {
    _array = array;
}

- (void)iterate:(void (^)(ObjcParticipant *))participant {
    if(_array == nil) {
        return;
    }
    for(ObjcParticipant* part in _array) {
        participant(part);
    }
}

@end


@implementation ObjcRemoteSourceChanged : NSObject
+ (ObjcRemoteSourceChanged*)createFrom:(const V5RemoteSourceChanged*) data
{
    ObjcRemoteSourceChanged *obj = [[ObjcRemoteSourceChanged alloc] init];
    
    obj->_sourceType = data->sourceType;
    obj->_participantURI = [NSString stringWithCString:data->participantURI encoding:NSUTF8StringEncoding];
    obj->_displayName  = [NSString stringWithCString:data->displayName encoding:NSUTF8StringEncoding];
    obj->_sourceName = [NSString stringWithCString:data->sourceName encoding:NSUTF8StringEncoding];

    return obj;
}

@end

@implementation ObjcErrorInfo
- (instancetype)init {
    self = [super init];
    if (self) {
        _code       = (V5ErrorCode)0;
        _category   = (V5Component)0;
        _reason     = 0;
        _status     = @"";
    }
    
    return self;
}

- (void)setErrorInfo:(const V5ErrorInfo*) errorInfo {
    _code       = errorInfo->code_deprecated;
    _category   = errorInfo->category;
    _reason     = errorInfo->reason;
    _status     = [NSString stringWithCString:errorInfo->status.c_str() encoding:NSUTF8StringEncoding];
}


@end

@implementation ObjcPasswdInfo
- (instancetype)init {
    self = [super init];
    if (self) {
        _passwd = @"";
        _hashCount = 0;
        _hashType = V5_HASH_TYPE_RAW;
        _salt = nil;
    }
    return self;
}
@end


