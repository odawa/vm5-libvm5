﻿//
//  WebAccess.cpp
//  V5ClientExample
//
//  Created by V-1094 on 2014/12/11.
//  Copyright (c) 2015 V-cube, Inc. All rights reserved.
//
#include "precompile.h"
#include "WebAccess.h"

#include <curl/curl.h>
#include <iostream>
#include <functional>
#include <Vidyo/VidyoClient.h>
#include "V5Log.h"

using namespace std;
using namespace picojson;

#define V5ShowJSON (1)
#undef V5ShowJSON

V5ClientEnv::V5ClientEnv(std::string const& prefix, std::shared_ptr<vrms5::ProxySetting> proxy)
{
    url_prefix = prefix;
    curl_global_init(CURL_GLOBAL_ALL);
    curlForUtils = curl_easy_init();
    proxySetting = proxy;

    //curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);
    //curl_easy_setopt(curl, CURLOPT_HEADER, 1L);

//    curl_easy_setopt(curl, CURLOPT_ERRORBUFFER, errorBuf);
//    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteMemoryCallback);
}

V5ClientEnv::~V5ClientEnv()
{
    curl_easy_cleanup(curlForUtils);
    curl_global_cleanup();
}


size_t
WriteMemoryCallback(void *contents, size_t size, size_t nmemb, void *userp)
{
    size_t realsize = size * nmemb;
    struct MemoryStruct *mem = (struct MemoryStruct *)userp;
    
    //mem->memory = reinterpret_cast<char *>(realloc(mem->memory, mem->size + realsize + 1));
    
    char *tmpMemory = reinterpret_cast<char *>(realloc(mem->memory, mem->size + realsize + 1));
    
//    if(mem->memory == NULL)
    if( tmpMemory == NULL )
    {
        /* out of memory! */
        V5LOG(V5_LOG_FATAL,"not enough memory (realloc returned NULL)\n");
        return 0;
    }
    else
    {
        mem->memory = tmpMemory;
    }
    
    memcpy(&(mem->memory[mem->size]), contents, realsize);
    mem->size += realsize;
    mem->memory[mem->size] = 0;
    
    return realsize;
}

void injectProxySetting(CURL* &curl, vrms5::ProxySetting const &proxySetting) {
    std::string proxyAddress = *(proxySetting.getProxyAddress());
    long proxyPort = proxySetting.getProxyPort();
    std::string proxyId = *(proxySetting.getProxyId());
    std::string proxyPassword = *(proxySetting.getProxyPassword());

    if(proxyAddress != "") {
        curl_easy_setopt(curl, CURLOPT_PROXY, proxyAddress.c_str());
        curl_easy_setopt(curl, CURLOPT_PROXYPORT, proxyPort);
        if(proxyId != "") {
            curl_easy_setopt(curl, CURLOPT_PROXYUSERNAME, proxyId.c_str());
        }
        if(proxyPassword != "") {
            curl_easy_setopt(curl, CURLOPT_PROXYPASSWORD, proxyPassword.c_str());
        }
    }
}

bool V5ClientEnv::dummySend(string const& json, V5APIsObject& d)
{
    bool ret = true;
    string ebufjson;
    picojson::value response;

    MemoryStruct chunk;
    
    chunk.memory = reinterpret_cast<char *>(malloc(1));  /* will be grown as needed by the realloc above */
    chunk.size = 0;    /* no data at this point */
    
    // WriteMemoryCallback関数の中でreallocしているが、
    // realloc時に再取得したメモリアドレスが再取得前のメモリアドレスと違う場合かつ
    // reallocに失敗したときに再取得前のallocで取得したメモリを解放する必要があるので、
    // メモリアドレスを退避しておく。
    char *memoryAddrBuckup = chunk.memory;
    
    size_t sz = WriteMemoryCallback((void *)json.c_str(), 1, json.length(), &chunk);
    if( sz != 0 )
    {
        picojson::parse(response, chunk.memory, reinterpret_cast<char *>(chunk.memory) + chunk.size, &ebufjson);
        if (ebufjson.empty()) {
            ret = d.parse(response);
        } else {
            V5LOG(V5_LOG_ERROR,"%s\n",ebufjson.c_str());
            ret = false;
        }
    }
    else
    {
        // mallocに成功していて、reallocに失敗した場合、chunk.memoryはNULLになっている。
        // mallocで捕捉したメモリはrealloc失敗時は解放されないため、
        // mallocで捕捉したアドレスのメモリを解放する必要がある。
        // WriteMemoryCallbackの返却値が0(=realloc失敗)の場合、
        // mallocのアドレスを解放する必要がある。
        if( ( memoryAddrBuckup != NULL ) &&
            ( memoryAddrBuckup != chunk.memory ))
        {
            free(memoryAddrBuckup);
        }
        ret = false;
    }
    
    if(chunk.memory)
    {
        free(chunk.memory);
    }
    
    return ret;
}

V5ClientHttpResult V5ClientEnv::send(V5APIsObject& d) {
    V5ClientHttpResult ret = V5_HTTP_OK;
    string ebufjson;
    picojson::value response;

    MemoryStruct chunk;
    chunk.memory = reinterpret_cast<char *>(malloc(1));  /* will be grown as needed by the realloc above */
    chunk.size = 0;    /* no data at this point */

    CURL *curl = curl_easy_init();

    //curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);
    //curl_easy_setopt(curl, CURLOPT_HEADER, 1L);

    curl_easy_setopt(curl, CURLOPT_ERRORBUFFER, errorBuf);
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteMemoryCallback);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, static_cast<void *>(&chunk));

    d.url = prepareURL(d.query);
    std::string queryString = joinQueryParams(d.query);

#if V5ShowJSON
    V5LOG(V5_LOG_INFO,"[XXXX] url=%s\n", d.url.c_str());
#endif
    if(auto setting = proxySetting.lock()) {
        //std::shared_ptr<vrms5::ProxySetting> setting = proxySetting.lock();
        injectProxySetting(curl, *setting);
    }

	curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, false);
    curl_easy_setopt(curl, CURLOPT_URL, url_prefix.c_str());
    curl_easy_setopt(curl, CURLOPT_POSTFIELDS, queryString.c_str());
    if (curl_easy_perform(curl) != CURLE_OK) {
        V5LOG(V5_LOG_ERROR,"request failed...%s\n", errorBuf);
        ret = V5_HTTP_REQUEST_SEND_NG;
    } else {
        
#ifdef V5ShowJSON
        V5LOG(V5_LOG_INFO,"[YYYY] JSON:\n");
        V5LOG(V5_LOG_INFO,"%s\n", chunk.memory);
#endif
        picojson::parse(response, chunk.memory, reinterpret_cast<char *>(chunk.memory) + chunk.size, &ebufjson);
        if (ebufjson.empty()) {
            ret = V5_HTTP_OK;
            (void)d.parse(response);
        } else {
            V5LOG(V5_LOG_ERROR,"%s\n", ebufjson.c_str());
            ret = V5_HTTP_RESPONSE_PARSE_NG;
        }
    }
    if(chunk.memory)
    {
        free(chunk.memory);
    }

    if(curl) {
        curl_easy_cleanup(curl);
    }
    return ret;
}

// returns JSON object! (not concrete objects!)
bool V5ClientEnv::sendAsync(V5MapType& query, CALL_ASYNC_RESPONSE_CALLBACK func) {
    URLLoader loader;
    loader.setProxySetting(proxySetting);

    std::string URL = prepareURL(query);
    std::string queryString = joinQueryParams(query);
//    V5LOG(V5_LOG_INFO,"[XXXX][A] url=%s\n", URL.c_str());
    loader.setResponseCallback([=](bool result, char* response, size_t responseSize)->void{
        V5ClientHttpResult ret = V5_HTTP_OK;
        picojson::value response_value;
        string ebufjson;

#ifdef V5ShowJSON
        V5LOG(V5_LOG_INFO,"[YYYY] JSON:\n");
        V5LOG(V5_LOG_INFO,"%s\n", response);
#endif
        if (result) {
            picojson::parse(response_value, response, response + responseSize, &ebufjson);
            if (ebufjson.empty()) {
                V5LOG(V5_LOG_INFO, "PicoJson Parse succeeded!\n");
            } else {
                V5LOG(V5_LOG_ERROR, "PicoJson Parse Error: %s\n", ebufjson.c_str());
                ret = V5_HTTP_RESPONSE_PARSE_NG;
            }
        }
        else
        {
            V5LOG(V5_LOG_ERROR, "http request Send NG: %s\n", ebufjson.c_str());
            ret = V5_HTTP_REQUEST_SEND_NG;
        }
        if (func != NULL) {
            func(response_value, ret);
        }
    });

    loader.load(url_prefix.c_str(), queryString.c_str());

    return true;
}

bool V5ClientEnv::sendDirect() {
    bool ret = true;
    string ebufjson;
    picojson::value response;

    MemoryStruct chunk;
    chunk.memory = reinterpret_cast<char *>(malloc(1));  /* will be grown as needed by the realloc above */
    chunk.size = 0;    /* no data at this point */

    CURL *curl = curl_easy_init();

    //curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);
    //curl_easy_setopt(curl, CURLOPT_HEADER, 1L);

    curl_easy_setopt(curl, CURLOPT_ERRORBUFFER, errorBuf);
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteMemoryCallback);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, static_cast<void *>(&chunk));
    
#if V5ShowJSON
    V5LOG(V5_LOG_INFO,"[XXXX] url=%s\n", this->url_prefix.c_str());
#endif
	curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, false);
    curl_easy_setopt(curl, CURLOPT_URL, url_prefix.c_str());
    if (curl_easy_perform(curl) != CURLE_OK) {
        V5LOG(V5_LOG_ERROR,"request failed...: %s\n", errorBuf);
        ret = false;
    } else {
        
#ifdef V5ShowJSON
        V5LOG(V5_LOG_INFO,"[YYYY] JSON:\n");
        V5LOG(V5_LOG_INFO,"%s\n", chunk.memory);
#endif
    }
    if(chunk.memory)
    {
        free(chunk.memory);
    }

    if(curl) {
        curl_easy_cleanup(curl);
    }

    return ret;
}

string V5ClientEnv::escape(string const& value)
{
    char* data = curl_easy_escape(curlForUtils,
                                  value.c_str(),
                                  static_cast<int>(value.length())
                                  );
    std::string ret_val = data;
    curl_free(data);
    return std::move( ret_val );
}

std::string V5ClientEnv::urlEncode(const char* value)
{
    char* data = curl_easy_escape(curlForUtils,
                                  value,
                                  static_cast<int>(strlen(value))
                             );
    std::string ret_val = data;
    curl_free(data);
    return std::move( ret_val );
}

string V5ClientEnv::joinQueryParams(V5MapType& query) {
    static const std::string EQ("=");
    string url = "";
    bool isFirst = true;
    for (const auto& entry : query) {
        if (!isFirst) {
            url += "&";
        } else {
            isFirst = false;
        }
        url += entry.first;
        if (entry.second != "") {
            url += EQ + urlEncode(entry.second);
        }
    }

    return std::move(url);
}

string V5ClientEnv::prepareURL(V5MapType& query)
{
    string url = url_prefix + "?" + joinQueryParams(query);
    return std::move(url);
}

//void V5ClientEnv::setProxySetting(const std::shared_ptr<vrms5::ProxySetting> &proxySetting) {
//    this->proxySetting = proxySetting;
//}
