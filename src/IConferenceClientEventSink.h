﻿#pragma once
#ifndef ICONFERENCE_EVENT_SINK_INCLUDED
#define ICONFERENCE_EVENT_SINK_INCLUDED

#include <memory>

typedef struct V5Participant_tag V5Participant;
typedef struct V5ErrorInfo_tag V5ErrorInfo;
typedef struct V5ConferenceInfo_tag V5ConferenceInfo;
typedef struct V5ConferenceStatus_tag V5ConferenceStatus;
typedef struct V5BigStatistics_tag V5BigStatistics;

class IV5ConferenceEventSink;

namespace vrms5
{

    class IConferenceClientEventSink
    {
    public:
        virtual ~IConferenceClientEventSink() {}

        virtual void onParticipantJoined(V5Participant const& participant) = 0;
        virtual void onParticipantLeaved(V5Participant const& participant) = 0;

        virtual void onJoinedConferenceFail(V5ErrorInfo const& errInfo) = 0;
        virtual void onJoinedConference(V5ConferenceInfo const& conferenceInfo) = 0;
        virtual void onLeavedConference(V5ErrorInfo const& errInfo) = 0;
        virtual void onParticipantListUpdated(const V5Participant * const participants[], unsigned long length) = 0;
        virtual void onConferenceStatusUpdated(V5ConferenceStatus const& conferenceStatus) = 0;
        virtual void onReconnectConference(V5ConferenceInfo const& conferenceInfo) = 0;
        virtual void onAsyncError(V5ErrorInfo const& errInfo) = 0;
        virtual void onInviteMailIsSent(V5ErrorInfo const& errInfo, const char* status, const char* mailAddress) = 0;

        virtual void onRcvRejectParticipant(V5Participant const& participant) = 0;
        virtual void onPastTimeStart() = 0;
        virtual void onPastTimeStop() = 0;

        virtual void onConferenceNameChangeFailed( const char* nowConferenceName ) = 0;
        
        virtual void onPeriodicStatisticsChanged(V5BigStatistics const& stat) = 0;
        
        virtual void onSetOverTimeCountTimer( unsigned long startTime ) = 0;
        virtual void onCancelOverTimeCountTimer() = 0;

    };

}

#endif // ICONFERENCE_EVENT_SINK_INCLUDED
