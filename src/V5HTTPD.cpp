﻿#include "precompile.h"
#include "V5HTTPD.h"

#include <Poco/Net/HTTPRequestHandler.h>
#include <Poco/Net/HTTPRequestHandlerFactory.h>
#include <Poco/Net/HTTPResponse.h>
#include <Poco/Net/HTTPServer.h>
#include <Poco/Net/HTTPServerParams.h>
#include <Poco/Net/HTTPServerRequest.h>
#include <Poco/Net/HTTPServerResponse.h>
#include <Poco/Net/ServerSocket.h>
#include <Poco/Net/HTMLForm.h>
#include <Poco/Util/ServerApplication.h>
#include <Poco/ThreadPool.h>
#include <Poco/Environment.h>
#include <Poco/URI.h>

using Poco::Net::NameValueCollection;
using Poco::Net::HTTPRequestHandler;
using Poco::Net::HTTPRequestHandlerFactory;
using Poco::Net::HTTPResponse;
using Poco::Net::HTTPServer;
using Poco::Net::HTTPServerParams;
using Poco::Net::HTTPServerRequest;
using Poco::Net::HTTPServerResponse;
using Poco::Net::ServerSocket;
using Poco::Net::HTMLForm;
using Poco::Util::ServerApplication;
using Poco::ThreadPool;
using Poco::Environment;
using Poco::IOException;
using Poco::URI;


V5HTTPD::V5HTTPD(listener_wp listener) : listener_(listener) {
	httpd = LocalHTTPD::createInstance();

	using namespace std::placeholders;
	httpd->addResouce("/crossdomain.xml", std::bind(&V5HTTPD::request_crossdomain, this, _1, _2));
	httpd->addResouce("/api/alive/", std::bind(&V5HTTPD::request_alive, this, _1, _2));
	httpd->addResouce("/api/joinConference/", std::bind(&V5HTTPD::request_joinConference, this, _1, _2));
}
V5HTTPD::~V5HTTPD() {
	httpd->removeResouce("/crossdomain.xml");
	httpd->removeResouce("/api/alive/");
	httpd->removeResouce("/api/joinConference/");
}

bool V5HTTPD::request_crossdomain(std::shared_ptr<requestQuery> query, V5HTTPD::HTTPServerResponse& resp) {

	// クロスドメインファイル
	std::string crossDomainXML;
	crossDomainXML += "<?xml version=\"1.0\"?>\n";
	crossDomainXML += "  <cross-domain-policy>\n";
	crossDomainXML += "  <site-control permitted-cross-domain-policies=\"all\" />\n";
	crossDomainXML += "  <allow-access-from domain=\"*\" />\n";
	crossDomainXML += "  <allow-http-request-headers-from domain=\"*\" headers=\"*\"/>\n";
	crossDomainXML += "</cross-domain-policy>\n";

	// レスポンス送信
	resp.setStatus(HTTPResponse::HTTP_OK);
	resp.setReason(HTTPResponse::HTTP_REASON_OK);
	resp.setContentType("text/xml");
	std::ostream& respMsg = resp.send();
	respMsg << crossDomainXML;
	respMsg.flush();

	return true;
}

// /api/alive/リクエストを受けた際に呼び出し、
bool V5HTTPD::request_alive(std::shared_ptr<requestQuery> query, HTTPServerResponse& resp){

	resp.setStatus(HTTPResponse::HTTP_OK);
	resp.setReason(HTTPResponse::HTTP_REASON_OK);
	resp.setContentType("application/json");
	std::ostream& respMsg = resp.send();

	auto listener = listener_.lock();
	switch (listener->getStatus()){
	case IV5HTTPDListener::V5State::Busy:
			respMsg << "{\"message\":\"busy\"}";
			break;
	case IV5HTTPDListener::V5State::Idle:
			respMsg << "{\"message\":\"idle\"}";
			break;
	case IV5HTTPDListener::V5State::Unknown:
		default:
			respMsg << "{\"message\":\"unknown\"}";
	}
	respMsg.flush();
	return true;
}

bool V5HTTPD::request_joinConference(std::shared_ptr<V5HTTPD::requestQuery> query, V5HTTPD::HTTPServerResponse& resp){

	// Token取り出し
	std::string token;
	requestQuery::iterator it = query->find("token");
	if ( it != query->end() ) {
		token = it->second.c_str();
	}

	// portalURL取り出し
	std::string portal;
	it = query->find("portal");
	if ( it != query->end() ) {
		portal = it->second.c_str();
	}

	if ( token.empty() || portal.empty()) {
		resp.setStatus(HTTPResponse::HTTP_BAD_REQUEST);
		resp.setReason(HTTPResponse::HTTP_REASON_BAD_REQUEST);
		resp.setContentType("plane/text");
		std::ostream& respMsg = resp.send();
		respMsg.flush();
		return true;
	}

	auto listener = listener_.lock();
	if ( listener->joinConference(token, portal) ) {
		resp.setStatus(HTTPResponse::HTTP_OK);
		resp.setReason(HTTPResponse::HTTP_REASON_OK);
		resp.setContentType("plane/text");
	} else {
		resp.setStatus(HTTPResponse::HTTP_BAD_REQUEST);
		resp.setReason(HTTPResponse::HTTP_REASON_BAD_REQUEST );
		resp.setContentType("plane/text");
	}


	std::ostream& respMsg = resp.send();
	respMsg.flush();
	return true;
}
