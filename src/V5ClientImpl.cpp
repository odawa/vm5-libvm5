﻿//
//  LibV5Lite.cpp
//  LibV5Lite
//
//  Created by V-1094 on 2015/01/14.
//  Copyright (c) 2015蟷ｴ V-cube. All rights reserved.
//

#include "precompile.h"
#include "V5ClientImpl.h"

#include <V5Lite/DeviceConfiguration.h>
#include "WebAccess.h"
#include "WebAPIObjects.h"
#include <V5Lite/WindowsAndDesktops.h>
#include "utils.h"
#include "V5Log.h"
#include "SHA1.h"
#include "ServiceLocator.h"
#include "EventFlag.h"
#include "VidyoClientAPI.h"

#include <stdio.h>
#include <string.h>
#include <iostream>
#include <string>
#include <future>
#include <exception>
#include <algorithm>
#include <cctype>
#include <regex>

using namespace vrms5::vidyo;
using namespace vrms5::utils;

#ifdef USE_HTTPD
#include "IHttpd.h"
#include "LocalHTTPD_IF.h"
#endif

#if defined(V5_IOS) || defined(V5_ANDROID)
#define ENABLE_DYNAMIC_WATCH    1
#else
#define ENABLE_DYNAMIC_WATCH    0
#endif

// VC++ で constexpr がサポートされるまでは define で対処する
#define V5_VIDYO_PROFILE_CONFIG_KEY_PATH	"SOFTWARE\\VCube\\vrms\\vrms5lite"

class  V5ClientEnv;
struct V5GenerateAuthToken;
struct V5PrepareJoinConference;
struct V5JoinConference;
struct V5SendInvitationMail;

struct V5UData {
	char token[V5_MAX_TOKEN];
	char vcubeURL[V5_MAX_URL];
	char portalURL[V5_MAX_URL];
	char userName[V5_MAX_UN];
	char passWord[V5_MAX_PASS];
	char screenName[V5_MAX_SCREEN];
	std::string PID;
	std::shared_ptr<V5ClientEnv> env;
	std::shared_ptr<V5JoinConference> joinCon;
    std::shared_ptr<V5SendInvitationMail> sendMail;
};

struct V5ClientImpl::VidyoParticipantInfo
{
    std::unique_ptr<VidyoClientInEventStartWatchVideoSource> videoSorceSetting;
};


std::atomic_bool V5ClientImpl::initalized;
VidyoClientLogParams V5ClientImpl::logParams = {0};

/*
 * [API] V5ClientImpl::V5Initialize
 * Initialize V5UData with two const char * and V5Rect * arguments.
 * so please pass this API with empty V5UData reference.
 * V5WindowId is platform specific Hardware Graphics Handle
 * (HWND in windows, others are void *).
 */
V5CODE V5ClientImpl::initialize(const char *logpath, const char *log_priority)
{
    assert(initalized == false);
    
    logParams.logBaseFileName = "V5Lite_";
    logParams.pathToLogDir = logpath;
    logParams.logLevelsAndCategories = log_priority;
    
    VidyoClientConsoleLogConfigure(VIDYO_CLIENT_CONSOLE_LOG_CONFIGURATION_ALL);
        
    if (VidyoClientInitialize(&V5ClientImpl::s_callback, nullptr, &logParams) == VIDYO_FALSE)
    {
        // このタイミングではコールバックを起動できないので、コメントアウト
        // 失敗時は返り値をエラーで返すので問題ない。
        // I should call another event sink.
//        invokeErrorCallBack(V5_ECODE_INITIALIZE, V5_COMPONENT_VIDYO, V5_VIDYO_FAILED_TO_INITIALIZE);
        V5LOG(V5_LOG_ERROR, "Client Initialize failed.\n");
        return V5Error;
    }
    
    initalized = true;
    
    return V5OK;
}

V5CODE V5ClientImpl::finalize()
{
    VidyoClientUninitialize();
    initalized = false;
    return V5OK;
}

V5ClientImpl::V5ClientImpl()
: ud(new V5UData())
, vidyoIdleFlag(EventFlag::SIGNALED)
, vidyoSignoutFlag(EventFlag::SIGNALED)
, confDisconnectedFlag(EventFlag::SIGNALED)
, conferenceClient(new ConferenceClient)
, serviceLocator(AbstractServiceLocator::createInstance())
, targetingPid(nullptr)
, videoSize(200,200,10,100)
, shareSize(200,200,10,100)
, statInfo( new V5StatisticsInfo )
{
    httpdListener = std::make_shared<HTTPDListener> (this);
    activeParticipantUriList = std::make_shared<std::vector<std::string>>();
    proxySetting = std::make_shared<vrms5::ProxySetting>();

    devConf = std::make_shared<V5DeviceConfigurationImpl>();

    // Windows では std::atomic がパラメーター付きコンストラクタを持っていないため
    currentStatus = CLIENT_IDLE;
    GuestState = false;
    speakerMuteState = false;
    speakerMuteRequesting = false;
    microphoneMuteState = false;
    microphoneMuteRequesting = false;
    cameraMuteState = false;
    cameraMuteRequesting = false;
    
    sendMaxKbpsReserve = false;
    sendMaxKbps = 0;
    recvMaxKbpsReserve = false;
    recvMaxKbps = 0;
    
    proxyForce = true;
    proxyForceChanged = false;
    proxyAddressChanged = false;
    proxyIEOption = V5_PROXY_VIDYO_NO_SETTING;
    proxyIEOptionChanged = false;
    
    confEndTimeOverTimerRunning = false;
    defaultNoticeTimeSpan = 300000;
    
    maxParticipants = V5_DEFAULT_MAX_PARTICIPANTS;
    maxParticipantsRequesting = false;
    selfViewRenderingState = true;
    renderingTargetPidOnlyMode = false;
    videoPreferences = (V5VideoPreferences) VIDYO_CLIENT_VIDEO_PREFERENCES_BEST_RESOLUTION;
    selectDeviceReserve = false;
    
    selfPreviewModeReserve = false;
    showActiveSpeakerReserve = false;
    
    joinConferenceTimer = std::make_shared<Timer>();
    
}

V5ClientImpl::~V5ClientImpl()
{
    VidyoClientStop();
}

V5ClientImpl::self_ptr_t V5ClientImpl::createInstance()
{
    self_ptr_t instancePtr(new V5ClientImpl());     // インスタンスサイズが大きいため、make_shared は使用しない
    instancePtr->conferenceClient->SetLibCallback(instancePtr);
    return std::move(instancePtr);
}

V5CODE V5ClientImpl::startLogic(V5WindowId parentWindow, V5Rect *videoRect, const char *fontpath, const char *certificatepath)
{
    VidyoClientProfileParams profileParams = { 0 };
    profileParams.CONFIG_KEY_PATH = V5_VIDYO_PROFILE_CONFIG_KEY_PATH;
    if (certificatepath != NULL)
    {
        profileParams.CERT_FILE_DIR = certificatepath;
    }
    
    VidyoRect vrect = { 0, 0, 0, 0 };
    if (videoRect)
    {
        vrect.xPos = videoRect->xPos;
        vrect.yPos = videoRect->yPos;
        vrect.width = videoRect->width;
        vrect.height = videoRect->height;
    }
    isRetrieveRawFrame = (videoRect == nullptr);

#if ENABLE_DYNAMIC_WATCH
    if(isRetrieveRawFrame) {
        maxParticipants = V5_MOBILE_DEFAULT_MAX_PARTICIPANTS;
    }
#endif
    
    // startup VidyoClient library
    VidyoBool retv = VidyoClientStart(&V5ClientImpl::s_callback,
                                      this,
                                      &logParams,
                                      (VidyoWindowId)parentWindow,
                                      &vrect,
                                      nullptr,
                                      &profileParams,
                                      VIDYO_FALSE);
    if (retv == VIDYO_FALSE) {
        // I should call another event sink.
        terminateConference(V5_ECODE_INITIALIZE,
                            V5_COMPONENT_VIDYO,
                            V5_VIDYO_CLIENT_START_FAILED);
        V5LOG(V5_LOG_ERROR, "Client Start failed.\n");
        return V5Error;
    }
    
    char versionBuf[VIDYO_CLIENT_VERSION_SIZE];
    VidyoClientGetVersion(versionBuf, VIDYO_CLIENT_VERSION_SIZE);
    
    V5LOG(V5_LOG_INFO,"[Vidyo Version]: %s\n", versionBuf);
    
    // VIDYO_CLIENT_OUT_EVENT_LOGIC_STARTED は VidyoClientStart 内で同期的に完了しているはず
    if (fontpath != NULL)
    {
        VidyoClientInEventSetFontFile fontfile = {0};
        vrms5::utils::utf8cpy(fontfile.fontFileName, fontpath);
        
        if(VidyoClientSendEvent(VIDYO_CLIENT_IN_EVENT_SET_FONT_FILE , &fontfile, sizeof(VidyoClientInEventSetFontFile) ) != VIDYO_CLIENT_ERROR_OK)
        {
            // should call another callback
            terminateConference(V5_ECODE_CALLBACK_LOGIC_STARTED,
                                V5_COMPONENT_VIDYO,
                                V5_VIDYO_CLIENT_IN_EVENT_SET_FONT_FILE_REQUEST_FAILED);
            V5LOG(V5_LOG_ERROR, "in event set Font File failed.\n");
            return V5Error;
        }
        
    }
    return V5OK;
}

V5CODE V5ClientImpl::stopLogic()
{
    if (stopLogicResult.valid())
    {
        auto status = stopLogicResult.wait_for(std::chrono::seconds::zero());
        if (status == std::future_status::timeout)
            return V5OK;
    }
    
    stopLogicResult = std::async(std::launch::async, [this]
    {
        leaveConferenceSync();
        auto result = VidyoClientStop();
        assert(result != VIDYO_FALSE);
        result = result;
    });
    
    return V5OK;
}

void V5ClientImpl::invokeAsyncCallBack(V5ErrorCode code, V5Component component, V5ErrorReason reason)
{
    auto p = this->eventSink.lock();
    
    if (p != nullptr) {
        V5ErrorInfo info;
        info.code_deprecated = code;
        info.category = component;
        info.reason   = reason;
        
        p->onAsyncError(info);
    }
}

void V5ClientImpl::invokeInviteMailCallBack(V5ErrorCode code, V5Component component, V5ErrorReason reason,
                                            const char* status, const char* mailAddress)
{
    auto p = this->eventSink.lock();
    
    if (p != nullptr) {
        V5ErrorInfo info;
        info.code_deprecated = code;
        info.category   = component;
        info.reason     = reason;
        info.status     = status;
        p->onInviteMailIsSent(info, status, mailAddress);
    }
}

void V5ClientImpl::updateDeviceConfiguration( VidyoClientRequestConfiguration const& conf )
{
    std::shared_ptr<V5DeviceConfigurationImpl> p = std::make_shared<V5DeviceConfigurationImpl>();
    p->injectDeviceConfiguration( conf );
    
//    p->getConf();
    
    // should lock it
    std::atomic_store(&(this->devConf),p);
    
    V5LOG(V5_LOG_INFO, "[updateDeviceConfiguration]\n");
}

std::shared_ptr<V5DeviceConfiguration> V5ClientImpl::getDeviceConfiguration()
{
    return std::atomic_load(&(this->devConf));
}

bool V5ClientImpl::isRawFrameMode()
{
    return isRetrieveRawFrame;
}

bool V5ClientImpl::isBusy() const
{
    if (VidyoClient::isStarted() && !VidyoClient::isIdle())
        return true;

    if (!vidyoSignoutFlag.isSignal())
        return true;

    if (!confDisconnectedFlag.isSignal())
        return true;

    if (!vidyoIdleFlag.isSignal())
        return true;

    return (currentStatus != CLIENT_IDLE);
}

bool V5ClientImpl::isLogicStarted() const
{
    return (initalized && VidyoClient::isStarted());
}

// getter Participnat Gest Status
bool V5ClientImpl::isGuest()
{
    return GuestState.load();
}

// getters and setters of Mic,Spk,Cam

bool V5ClientImpl::isCameraMuted()
{
    return cameraMuteState;
}

bool V5ClientImpl::isMicrophoneMuted()
{
    return microphoneMuteState;
}

bool V5ClientImpl::isSpeakerMuted()
{
    return speakerMuteState;
}

void V5ClientImpl::muteCamera(bool muted, bool force)
{
    bool needRequest;
    bool old_cameraMuteState;
    
    // save client selection.
    {
        lock_guard l(mtx);
        old_cameraMuteState = cameraMuteState;
        cameraMuteState = muted;
        needRequest = (currentStatus == IN_CONFERENCE) && !cameraMuteRequesting;
        cameraMuteRequesting = needRequest;
        
        if( ( muted == false ) &&
            ( old_cameraMuteState != muted ) )
        {
            cameraMuteChangeMtoUM = true;
        }
        else
        {
            cameraMuteChangeMtoUM = false;
        }
    }

    if (needRequest)
    {
        V5LOG(V5_LOG_INFO, "send Camera Mute Request = %s\n", ((muted) ? "true" : "false"));
    }
    
    if (needRequest && !VidyoClient::setVideoMute(muted)) // in the middle of meeting
    {
        // エラー
        cameraMuteRequesting = false;
        invokeAsyncCallBack(V5_ECODE_MUTE_CAMERA,
                            V5_COMPONENT_VIDYO,
                            V5_VIDYO_CLIENT_IN_EVENT_MUTE_VIDEO_EVENT_REQUEST_FAILED);
    }
    else
    {
        if( old_cameraMuteState == muted && muted == false )
        {
//            printf("muteCamera selectDeviceReserve root.\n");
            if( selectDeviceReserve )
            {
//                printf("muteCamera selectDeviceReserve function Run.\n");
                (getDeviceConfiguration())->setConf(V5_DEVICE_TYPE_VIDEO,deviceIndexVideo);
                
                /* ここの処理も変更が必要 */
                // デバイスのカレント番号を変更する必要がある。
                // カレント番号はsetConf内で設定しているので、ここでは更新しない。
                //updateDeviceConfiguration();
                auto devsink = deviceSink.lock();
                if(devsink)
                {
                    devsink->onVideoDeviceListChanged(deviceIndexVideo);
                }
                selectDeviceReserve = false;
            }
        }
    }
}

void V5ClientImpl::muteMicrophone(bool muted, bool force)
{
    bool needRequest;

    // save client selection.
    {
        lock_guard l(mtx);
        microphoneMuteState = muted;
        needRequest = (currentStatus == IN_CONFERENCE) && !microphoneMuteRequesting;
        microphoneMuteRequesting = needRequest;
    }

    if (needRequest && !VidyoClient::setAudioInMute(muted)) // in the middle of meeting
    {
        // エラー
        microphoneMuteRequesting = false;
        invokeAsyncCallBack(V5_ECODE_MUTE_MICROPHONE,
                            V5_COMPONENT_VIDYO,
                            V5_VIDYO_CLIENT_IN_EVENT_MUTE_AUDIO_IN_EVENT_REQUEST_FAILED);
    }
}

void V5ClientImpl::muteSpeaker(bool muted, bool force)
{
    bool needRequest;

    // save client selection.
    {
        lock_guard l(mtx);
        speakerMuteState = muted;
        needRequest = (currentStatus == IN_CONFERENCE) && !speakerMuteRequesting;
        speakerMuteRequesting = needRequest;
    }

    if (needRequest && !VidyoClient::setAudioOutMute(muted)) // in the middle of meeting
    {
        // エラー
        speakerMuteRequesting = false;
        invokeAsyncCallBack(V5_ECODE_MUTE_SPEAKER,
                            V5_COMPONENT_VIDYO,
                            V5_VIDYO_CLIENT_IN_EVENT_MUTE_AUDIO_OUT_EVENT_REQUEST_FAILED);
    }
}

unsigned int V5ClientImpl::getMaxParticipants()
{
    return this->maxParticipants;
}

void V5ClientImpl::setMaxParticipants(unsigned int count)
{
    bool needRequest;

    // save client selection.
    {
        lock_guard l(mtx);
        maxParticipants = count;
        needRequest = (currentStatus == IN_CONFERENCE) && !maxParticipantsRequesting;
        maxParticipantsRequesting = needRequest;
    }

    if (needRequest && !VidyoClient::setMaxParticipants(count)) // in the middle of meeting
    {
        // モバイルでは必ずエラー値が返却されるのでエラー制御しない
        maxParticipantsRequesting = false;
    }

#if ENABLE_DYNAMIC_WATCH
    if(isRetrieveRawFrame && needRequest && VidyoClient::startDynamicWatchVideoSource(maxParticipants.load(), videoSize))
    {
        maxParticipantsRequesting = false;
    }
#endif
}

void V5ClientImpl::setFrameSize(DefaultFrameSize& sizeStruct, int width, int height, int frameRate, int minFrameInterval)
{
    sizeStruct.width            = width;
    sizeStruct.height           = height;
    sizeStruct.frameRate        = frameRate;
    sizeStruct.minFrameInterval = minFrameInterval;
}

void V5ClientImpl::setVideoFrameSize(int width, int height, int frameRate, int minFrameInterval)
{
    setFrameSize(videoSize, width, height, frameRate, minFrameInterval);
}

void V5ClientImpl::setShareFrameSize(int width, int height, int frameRate, int minFrameInterval)
{
    setFrameSize(shareSize, width, height, frameRate, minFrameInterval);
}

#define V5_FORCE_SECURE_ACCESS (1)
#undef  V5_FORCE_SECURE_ACCESS

V5CODE V5ClientImpl::joinConference(
    const char *displayName_,
    const PasswdInfoPtrT& psswd)
{
    //currentStatus = CLIENT_IDLE;        // コードが安定するまで IDLE にしてしまう
    if (!transitionStatus(CLIENT_LINKED_SESSION, PREPARE_JOIN))
    {
        return V5Busy;
    }
    
    joinConfTime.reset();
    
    // 1. prepare environment and call PrepareJoinConference web APIs.
    this->ud->env = std::make_shared<V5ClientEnv>(vcubeURL, proxySetting);
    this->selfViewRenderingState = true;
    this->renderingTargetPidOnlyMode = false;

//    this->updateDeviceConfiguration();

#if 0
    V5LOG(V5_LOG_INFO, "displayName=%s\n", (displayName_) ? displayName_ : "[nullptr]" );
    //V5LOG(V5_LOG_INFO, "password=%s\n", (password_) ? password_ : "[nullptr]");
#endif
    //set selfview lavbel
    
    if( displayName_ )
        this->setSelfDisplayLabel( displayName_ );
    
    SharedString dn = (displayName_) ? std::make_shared<std::string>(displayName_) : SharedString();
    joinConferenceResult = std::async(std::launch::async, [this, dn, psswd] {
        using namespace std::chrono;
        assert(currentStatus == PREPARE_JOIN);
        
        V5ClientEnv env(this->vcubeURL.c_str(), proxySetting);
        V5PrepareJoinConference prepareJoin(this->sessionToken, dn, psswd);
        
        vrms5::utils::timer sw;
        V5LOG(V5_LOG_INFO, "%1.3lf[sec]: request prepareJoin.\n", joinConfTime.elapsed());
        V5ClientHttpResult ret2 = env.send(prepareJoin);
        V5LOG(V5_LOG_INFO, "%1.3lf[sec]: response prepareJoin. (%1.3lf[sec])\n", joinConfTime.elapsed(), sw.elapsed());
        
        if( ret2 == V5_HTTP_REQUEST_SEND_NG )
        {
            V5LOG(V5_LOG_ERROR, "Prepare Join Conference HTTP Request Send failed.\n");
            terminateConference(V5_ECODE_JOIN_IMPL_PREPARE,
                                V5_COMPONENT_VCUBE_PORTAL,
                                V5_PORTAL_PREPARE_JOIN_CONF_HTTP_REQUEST_SEND_NG);
            return;
        }
        
        if ( ret2 == V5_HTTP_RESPONSE_PARSE_NG )
        {
            V5LOG(V5_LOG_ERROR, "Prepare Join Conference HTTP Response Data Json Parse failed.\n");
            terminateConference(V5_ECODE_JOIN_IMPL_PREPARE,
                                V5_COMPONENT_VCUBE_PORTAL,
                                V5_PORTAL_PREPARE_JOIN_CONF_HTTP_RESPONSE_PARSE_NG);
            return;
        }
        
        if (!prepareJoin.isParsed)
        {
            V5LOG(V5_LOG_ERROR, "Prepare Join Conference: failed (not filled with data).\n");
            terminateConference(V5_ECODE_JOIN_IMPL_PREPARE,
                                V5_COMPONENT_VCUBE_PORTAL,
                                V5_PORTAL_PREPARE_JOIN_CONF_HTTP_RESPONSE_NOT_FILLED_WITH_DATA);
            return;
        }
        
        if (!((prepareJoin.result == "OK") && (prepareJoin.status == "200"))) {
            if (prepareJoin.status == "100")
            {
                // パラメーターエラー
                std::string requireDisplayName = prepareJoin.requireDisplayName;
                std::string conferencePassword = prepareJoin.conferencePassword;
                std::transform(requireDisplayName.cbegin(), requireDisplayName.cend(), requireDisplayName.begin(), toupper);
                std::transform(conferencePassword.cbegin(), conferencePassword.cend(), conferencePassword.begin(), toupper);
                
                if (requireDisplayName == "REQUIRED" && conferencePassword == "REQUIRED")
                {
                    return rejectPrepareJoinConference(V5_ECODE_JOIN_IMPL_PREPARE,
                                                       V5_COMPONENT_VCUBE_PORTAL,
                                                       V5_PORTAL_PREPARE_JOIN_CONF_HTTP_RESPONSE_REQUIRED_DISPLAY_NAME_AND_CONFERENCE_PW);
                    // この場合は idle へ遷移してはならない
                }
                else if (requireDisplayName == "REQUIRED")
                {
                    return rejectPrepareJoinConference(V5_ECODE_JOIN_IMPL_PREPARE,
                                                       V5_COMPONENT_VCUBE_PORTAL,
                                                       V5_PORTAL_PREPARE_JOIN_CONF_HTTP_RESPONSE_REQUIRED_DISPLAY_NAME);
                    // この場合は idle へ遷移してはならない
                }
                else if (conferencePassword == "REQUIRED")
                {
                    return rejectPrepareJoinConference(V5_ECODE_JOIN_IMPL_PREPARE,
                                                       V5_COMPONENT_VCUBE_PORTAL,
                                                       V5_PORTAL_PREPARE_JOIN_CONF_HTTP_RESPONSE_REQUIRED_CONFERENCE_PW);
                    // この場合は idle へ遷移してはならない
                }
                
                if (requireDisplayName == "INVALID" && conferencePassword == "INVALID")
                {
                    return rejectPrepareJoinConference(V5_ECODE_JOIN_IMPL_PREPARE,
                                                       V5_COMPONENT_VCUBE_PORTAL,
                                                       V5_PORTAL_PREPARE_JOIN_CONF_HTTP_RESPONSE_INVALID_DISPLAY_NAME_AND_CONFERENCE_PW);
                    // この場合は idle へ遷移してはならない
                }
                else if (requireDisplayName == "INVALID")
                {
                    return rejectPrepareJoinConference(V5_ECODE_JOIN_IMPL_PREPARE,
                                                       V5_COMPONENT_VCUBE_PORTAL,
                                                       V5_PORTAL_PREPARE_JOIN_CONF_HTTP_RESPONSE_INVALID_DISPLAY_NAME);
                    // この場合は idle へ遷移してはならない
                }
                else if (conferencePassword == "INVALID")
                {
                    return rejectPrepareJoinConference(V5_ECODE_JOIN_IMPL_PREPARE,
                                                       V5_COMPONENT_VCUBE_PORTAL,
                                                       V5_PORTAL_PREPARE_JOIN_CONF_HTTP_RESPONSE_INVALID_CONFERENCE_PW);
                    // この場合は idle へ遷移してはならない
                }
            }
            V5LOG(V5_LOG_ERROR, "Prepare Join Conference: response error status(%s).\n", prepareJoin.status.c_str());
            V5LOG(V5_LOG_ERROR, "Prepare Join Conference: response error description(%s).\n", prepareJoin.description.c_str());
            
            // 個別にエラーを判定していない(判定できない)場合は第4引数にエラーのStatusを通知するように変更
            terminateConference(V5_ECODE_JOIN_IMPL_PREPARE,
                                V5_COMPONENT_VCUBE_PORTAL,
                                V5_PORTAL_PREPARE_JOIN_CONF_HTTP_RESPONSE_RESULT_NG,
                                prepareJoin.status);
            return;
        }
        
#if 0
        V5LOG(V5_LOG_WARNING, "portalAddress: %s\n", prepareJoin.portalAddress.c_str());
        V5LOG(V5_LOG_WARNING, "conferenceAddress: %s\n", prepareJoin.conferenceAddress.c_str());
        V5LOG(V5_LOG_WARNING, "isGUEST: %s\n", prepareJoin.isGuest.c_str());
#endif
        
        GuestState = (prepareJoin.isGuest == "true");
        
        vrms5::utils::utf8cpy(this->conferenceURL, prepareJoin.conferenceAddress.c_str());
        vrms5::utils::utf8cpy(this->conferenceID, prepareJoin.conferenceID.c_str());
        vrms5::utils::utf8cpy(this->displayName, env.urlEncode(prepareJoin.displayName).c_str());
        //    vrms5::utils::utf8cpy(this->displayName,prepareJoin->displayName.c_str());
        
        // 3. send LOGIN event to Vidyo Client APIs.
#ifdef V5_FORCE_SECURE_ACCESS
        prepareJoin.portalAddress = prepareJoin.portalAddress.replace(0, 4, "https");
        V5LOG(V5_LOG_INFO, "Secure portal: %s\n", prepareJoin.portalAddress.c_str());
#endif
        
#if 0
        V5LOG(V5_LOG_INFO, "vidyoPortal=%s\n", prepareJoin.portalAddress.c_str());
        V5LOG(V5_LOG_INFO, "userName   =%s\n", prepareJoin.un.c_str());
        V5LOG(V5_LOG_INFO, "password   =%s\n", prepareJoin.pw.c_str());
#endif
        
        if (!transitionStatus(PREPARE_JOIN, SIGNIN_TO_MEDIA_SERVER))
        {
            // 既に退室もしくは終了シーケンスに入っていると思われる
            // TODO: シーケンスを確認し、切断処理に移行するべき
            terminateConference(V5_ECODE_JOIN_IMPL_PREPARE,
                                V5_COMPONENT_VCUBE_PORTAL,
                                V5_INTERNAL_STATUS_CHANGE_FAILED_SIGNIN_TO_MEDIA_SERVER);
            return;
        }
        
        // Vidyoに対してProxy設定反映
        setVidyoConfiguration();
        
        V5LOG(V5_LOG_INFO, "%1.3lf[sec]: try sign-in to Media-Server.\n", joinConfTime.elapsed());
        if (!VidyoClient::login(prepareJoin.portalAddress, prepareJoin.un, prepareJoin.pw))
        {
            terminateConference(V5_ECODE_JOIN_IMPL_LOGIN_EVENT,
                                V5_COMPONENT_VIDYO,
                                V5_VIDYO_CLIENT_IN_EVENT_LOGIN_EVENT_REQUEST_FAILED);
            return;
        }
    });
    //future.get();
    
    /* JoinConference Timer Set (60 seconds) */
    joinConferenceTimer->start(60000,std::bind(&V5ClientImpl::onJoinConferenceTimeout, this));

    return V5OK;
}

void V5ClientImpl::rejectPrepareJoinConference(V5ErrorCode code, V5Component component, V5ErrorReason reason)
{
    this->joinConferenceTimer->stop();

    auto p = this->eventSink.lock();
    
    if (!transitionStatus(PREPARE_JOIN, CLIENT_LINKED_SESSION))
    {
        terminateConference(code,
                            V5_COMPONENT_INTERNAL,
                            V5_INTERNAL_STATUS_CHANGE_FAILED_CLIENT_LINKED_SESSION_IN_REJECT_PREPARE_JOIN_CONFERENCE);
        return;
    }
    
    if (p != nullptr) {
        V5ErrorInfo info;
        info.code_deprecated = code;
        info.category = component;
        info.reason = reason;

        p->onJoinConferenceFailed(info);
    }
}

void V5_CALLBACK V5ClientImpl::s_callback(VidyoClientOutEvent actionType,
                                          VidyoVoidPtr param,
                                          VidyoUint paramSize,
                                          VidyoVoidPtr data)
{
    if (actionType != VIDYO_CLIENT_OUT_EVENT_VIDEO_FRAME_RECEIVED) {
#ifdef V5DEBUG
        auto eventName = toString(actionType);
        V5LOG(V5_LOG_INFO, "[OutEvent] Received event = %s\n", eventName.c_str());
#else
        V5LOG(V5_LOG_INFO, "[OutEvent] Received event = %d\n", static_cast<int>(actionType));
#endif
        
    }
    if (!data)
        return;
    
    return static_cast<V5ClientImpl*>(data)->vidyoCallback(actionType, param, paramSize);
}


void V5ClientImpl::vidyoCallback(VidyoClientOutEvent actionType,
                                 VidyoVoidPtr param,
                                 VidyoUint paramSize)
{
    if (actionType >= VIDYO_CLIENT_OUT_EVENT_MIN
        && actionType <= VIDYO_CLIENT_OUT_EVENT_MAX)
    {
        switch (actionType) {
            case VIDYO_CLIENT_OUT_EVENT_LOGIC_STARTED:
            {
                V5LOG(V5_LOG_ERROR, "Logic started!!\n"); /*E*/
                
                VidyoClientRequestConfiguration conf = { 0 };
                auto error = VidyoClient::getConfiguration(&conf);
                if (error != VIDYO_CLIENT_ERROR_OK)
                {
                    // should call another callback
                    V5LOG(V5_LOG_ERROR, "Failed to request get configuration with error: %d\n", error); /*E*/
                    terminateConference(V5_ECODE_CALLBACK_LOGIC_STARTED,
                                        V5_COMPONENT_VIDYO,
                                        V5_VIDYO_CLIENT_REQUEST_GET_CONFIGURATION_REQUEST_FAILED);
                    return;
                }
                else {
                    /* Default configuration */
                    conf.enableShowConfParticipantName = VIDYO_TRUE;
                    conf.enableBackgrounding = VIDYO_TRUE;

                    /* Disable autologin */
                    conf.userID[0] = '\0';
                    conf.portalAddress[0] = '\0';
                    conf.serverAddress[0] = '\0';
                    conf.password[0] = '\0';
                    conf.selfViewLoopbackPolicy = 0;
                    conf.enableMuteMicrophoneOnJoin = VIDYO_TRUE;
                    conf.enableMuteSpeakerOnJoin = VIDYO_TRUE;
                    conf.enableHideCameraOnJoin = VIDYO_TRUE;
                    conf.videoPreferences = (VidyoClientVideoPreferences) videoPreferences;
                    
                    if( proxyForce )
                    {
                        V5LOG(V5_LOG_INFO, "Set PROXY_VIDYO_FORCE = ON.\n");
                        // 過去の設定が生き残っている可能性があるので、
                        // PROXY_VIDYO_FORCE 以外のフラグを落とすため上書き
                        conf.proxySettings = PROXY_VIDYO_FORCE;
                        
                    }
                    else
                    {
                        V5LOG(V5_LOG_INFO, "Set PROXY_VIDYO_FORCE = OFF.\n");
                        // 過去の設定が残存してしまっている可能性があるので全フラグ初期化
                        conf.proxySettings = 0;
                        
                    }
                    
                    auto tmpProxyIEOption = proxyIEOption.load();
                    switch( tmpProxyIEOption )
                    {
                        case V5_PROXY_VIDYO_IE:
                        {
                            conf.proxySettings |= PROXY_WEB_IE;
                            V5LOG(V5_LOG_INFO, "[VIDYO WEB PROXY SETTING] PROXY_WEB_IE On.\n");
                            break;
                        }
                            
                        case V5_PROXY_VISYO_IE_AUTO_DETECT:
                        {
                            conf.proxySettings |= PROXY_WEB_IE_AUTO_DETECT;
                            V5LOG(V5_LOG_INFO, "[VIDYO WEB PROXY SETTING] PROXY_WEB_IE_AUTO_DETECT On.\n");
                            break;
                        }
                            
                        case V5_PROXY_VIDYO_IE_SCRIPT:
                        {
                            conf.proxySettings |= PROXY_WEB_IE_AUTO_CONFIG_SCRIPT;
                            V5LOG(V5_LOG_INFO, "[VIDYO WEB PROXY SETTING] PROXY_WEB_IE_AUTO_CONFIG_SCRIPT On.\n");
                            break;
                        }
                        case V5_PROXY_VIDYO_IE_MANUAL:
                        {
                            conf.proxySettings |= PROXY_WEB_IE_MANUAL;
                            V5LOG(V5_LOG_INFO, "[VIDYO WEB PROXY SETTING] PROXY_WEB_IE_MANUAL On.\n");
                            break;
                        }
                            
                        case V5_PROXY_VIDYO_NO_SETTING:
                        default:
                        {
                            // 特定のIEオプションを指定されていないため設定しない。
                            V5LOG(V5_LOG_INFO, "[VIDYO WEB PROXY SETTING] V5_PROXY_VIDYO_NO_SETTING or default.\n");
                            break;
                        }
                    }
                    
                    V5LOG(V5_LOG_INFO, "conf.proxySettings  : %x\n",conf.proxySettings);
                    
                    // ここは初期設定箇所のため、Proxyアドレスを判定して、
                    // 設定されていなかった場合、設定しない。
                    if( ((proxySetting->getProxyAddress())->length() != 0) &&
                        (tmpProxyIEOption == V5_PROXY_VIDYO_IE_MANUAL))
                    {
                        V5LOG(V5_LOG_INFO, "[VIDYO WEB PROXY SETTING] Before.\n");
                        V5LOG(V5_LOG_INFO, "conf.webProxyAddress  : %s\n",conf.webProxyAddress);
                        V5LOG(V5_LOG_INFO, "conf.webProxyPort     : %s\n",conf.webProxyPort);
                        V5LOG(V5_LOG_INFO, "conf.webProxyUsername : %s\n",conf.webProxyUsername);
                        V5LOG(V5_LOG_INFO, "conf.webProxyPassword : %s\n",conf.webProxyPassword);
                        
                        utf8cpy(conf.webProxyAddress, *proxySetting->getProxyAddress());
                        utf8cpy(conf.webProxyPort, std::to_string(proxySetting->getProxyPort()));
                        utf8cpy(conf.webProxyUsername, *proxySetting->getProxyId());
                        utf8cpy(conf.webProxyPassword, *proxySetting->getProxyPassword());
                        
                        V5LOG(V5_LOG_INFO, "[VIDYO WEB PROXY SETTING] After.\n");
                        V5LOG(V5_LOG_INFO, "conf.webProxyAddress  : %s\n",conf.webProxyAddress);
                        V5LOG(V5_LOG_INFO, "conf.webProxyPort     : %s\n",conf.webProxyPort);
                        V5LOG(V5_LOG_INFO, "conf.webProxyUsername : %s\n",conf.webProxyUsername);
                        V5LOG(V5_LOG_INFO, "conf.webProxyPassword : %s\n",conf.webProxyPassword);
                    }
                    else
                    {
                        V5LOG(V5_LOG_INFO, "[VIDYO WEB PROXY SETTING] VIDYO WebProxy Setting No Changed.\n");
                    }
                    
                    error = VidyoClient::setConfiguration(conf);
                    if (error != VIDYO_CLIENT_ERROR_OK)
                    {
                        // should call another callback
                        terminateConference(V5_ECODE_CALLBACK_LOGIC_STARTED,
                                            V5_COMPONENT_VIDYO,
                                            V5_VIDYO_CLIENT_REQUEST_SET_CONFIGURATION_REQUEST_FAILED);
                        V5LOG(V5_LOG_ERROR, "Failed to request set configuration with error: %d\n",error); /*E*/
                        return;
                    }
                    
                    // 初期設定完了のため、事前入力されていた場合のProxy関連更新フラグを更新する。
                    proxyForceChanged = false;
                    proxyAddressChanged = false;
                    proxyIEOptionChanged = false;
                    
                    // デバイス情報更新
                    updateDeviceConfiguration( conf );
                }
                break;
            }
                
            case VIDYO_CLIENT_OUT_EVENT_CALL_STATE:
            {
                auto const& data = *static_cast<VidyoClientOutEventCallState*>(param);

                {
                    lock_guard l(mtx);
                    if (data.callState == VIDYO_CLIENT_CALL_STATE_IDLE)
                    {
                        if (vidyoIdleFlag.setSignal())
                        {
                            terminateConference(V5_ECODE_CALLBACK_CALLSTATE,
                                                V5_COMPONENT_VIDYO,
                                                V5_VIDYO_CLIENT_OUT_EVENT_CALL_STATE_RECIEVED_STATUS_IDLE);
                            return;
                        }
                    }
                    else
                    {
                        vidyoIdleFlag.resetSignal();
                    }
                }

                //if (data.callState == VIDYO_CLIENT_CALL_STATE_IDLE)
                //    terminateConference(V5_ECODE_UNEXPECTED_MEDIA_STREAM_ENDED, V5_COMPONENT_VIDYO, 0);
                break;
            }
                
            case VIDYO_CLIENT_OUT_EVENT_LICENSE:
            {
                /*
                 * If there are any issues with Licenses, this event will be sent
                 * by the VidyoClient library
                 */
                auto eventLicense = static_cast<VidyoClientOutEventLicense*>(param);
                VidyoUint error = eventLicense->error;
//                VidyoUint vmConnectionPath = eventLicense->vmConnectionPath;
//                VidyoBool OutOfLicenses = eventLicense->OutOfLicenses;
                
                // 正常時は以下の値が帰ってくる。
                // eventLicense->error = 0
                // eventLicense->vmConnectionPath = 1
                // eventLicense->OutOfLicenses = 0
                
                V5LOG(V5_LOG_INFO,
                      "License Error: errorid=%d  vmConnectionPath=%d OutOfLicense=%d\n",
                      error,
                      eventLicense->vmConnectionPath,
                      eventLicense->OutOfLicenses
                      );
                
                // 設定値のDefineがわからないので、
                // 正常時に設定される0で判定する。
                if( error != VIDYO_CLIENT_ERROR_OK )
                {
                    // Licenseでエラーの場合は入室処理継続不可のため、
                    // 終了処理を起動する。
                    terminateConference(V5_ECODE_CALLBACK_LICENSE,
                                        V5_COMPONENT_VIDYO,
                                        V5_VIDYO_CLIENT_OUT_EVENT_LICENSE_ERROR_CODE_ABNORMAL);
                    return;
                }
                
                break;
            }
            case VIDYO_CLIENT_OUT_EVENT_SIGN_IN:
            {
                vidyoSignoutFlag.resetSignal();

                auto eventSignIn = static_cast<VidyoClientOutEventSignIn*>(param);
                
                VidyoUint activeEid = eventSignIn->activeEid;
//                VidyoBool signinSecured = eventSignIn->signinSecured;
                
                V5LOG(V5_LOG_INFO,
                      "activeEid=%d signinSecured=%s\n",
                      activeEid,
                      (eventSignIn->signinSecured? "Yes": "No")
                      );
                
                /*
                 * If the EID is not setup, it will resturn activeEid = 0
                 * in this case, we invoke the license request using below event
                 */
                if(!activeEid) {
                    (void)VidyoClientSendEvent(VIDYO_CLIENT_IN_EVENT_LICENSE, NULL, 0);
                }
                break;
            }
                
            case VIDYO_CLIENT_OUT_EVENT_SIGNED_IN:
            {
                V5LOG(V5_LOG_INFO, "%1.3lf[sec]: Complete sign-in to Media-Server.\n", joinConfTime.elapsed());
                
                vidyoSignoutFlag.resetSignal();
               
                // 4. wait SIGNED_IN and call Join Conference web APIs.
                if (!transitionStatus(SIGNIN_TO_MEDIA_SERVER, CONNECTING_TO_MEDIA_SERVER))
                {
                    // 既に退室もしくは終了シーケンスに入っていると思われる
                    // TODO: シーケンスを確認し、切断処理に移行するべき
                    terminateConference(V5_ECODE_CALLBACK_SIGNED_IN,
                                        V5_COMPONENT_VCUBE_PORTAL,
                                        V5_INTERNAL_STATUS_CHANGE_FAILED_CONNECTING_TO_MEDIA_SERVER);
                    return;

                }
                ud->joinCon = std::make_shared<V5JoinConference>(sessionToken);
                
                vrms5::utils::timer sw;
                V5LOG(V5_LOG_INFO, "%1.3lf[sec]: request joinConference\n", joinConfTime.elapsed());
                
                bool ret3 = ud->env->sendAsync(ud->joinCon->query, [=](picojson::value& jsonObj, V5ClientHttpResult result)->void {
                    V5LOG(V5_LOG_INFO, "%1.3lf[sec]: response joinConference (%1.3lf[sec]).\n", joinConfTime.elapsed(), sw.elapsed());
                    
                    if( result == V5_HTTP_REQUEST_SEND_NG )
                    {
                        V5LOG(V5_LOG_ERROR, "JoinConference http Request Send NG.\n");
                        terminateConference(V5_ECODE_CALLBACK_SIGNED_IN,
                                            V5_COMPONENT_VCUBE_PORTAL,
                                            V5_PORTAL_JOIN_CONF_HTTP_REQUEST_SEND_NG);
                        return; /*E*/
                    }
                    
                    if ( result == V5_HTTP_RESPONSE_PARSE_NG )
                    {
                        V5LOG(V5_LOG_ERROR, "JoinConference http Response Data JsonParse Failed.\n");
                        terminateConference(V5_ECODE_CALLBACK_SIGNED_IN,
                                            V5_COMPONENT_VCUBE_PORTAL,
                                            V5_PORTAL_JOIN_CONF_HTTP_RESPONSE_DATA_PARSE_NG);
                        return; /*E*/
                    }
                    
                    ud->joinCon->parse(jsonObj);
                    if (! ud->joinCon->isParsed)
                    {
                        V5LOG(V5_LOG_ERROR, "Join Conference: failed (not filled with data).\n");
                        terminateConference(V5_ECODE_CALLBACK_SIGNED_IN,
                                            V5_COMPONENT_VCUBE_PORTAL,
                                            V5_PORTAL_JOIN_CONF_HTTP_RESPONSE_NOT_FILLED_WITH_DATA);
                        return; /*E*/
                    }
                    
                    if (! ((ud->joinCon->result == "OK")&&(ud->joinCon->status == "200")))
                    {
                        V5LOG(V5_LOG_ERROR, "Join Conference: response error. status(%s).\n", ud->joinCon->status.c_str());
                        V5LOG(V5_LOG_ERROR, "Prepare Join Conference: response error. status(%s).\n", ud->joinCon->status.c_str());
                        V5LOG(V5_LOG_ERROR, "Prepare Join Conference: response error. description(%s).\n", ud->joinCon->description.c_str());

                        // 個別にエラーを判定していない(判定できない)場合は第4引数にエラーのStatusを通知するように変更
                        terminateConference(V5_ECODE_CALLBACK_SIGNED_IN,
                                            V5_COMPONENT_VCUBE_PORTAL,
                                            V5_PORTAL_JOIN_CONF_HTTP_RESPONSE_RESULT_NG,
                                            ud->joinCon->status);
                        return; /*E*/
                    }
                });
                if (!ret3) {
                    V5LOG(V5_LOG_ERROR, "Join Conference: sendAsync Function Call failure.\n");
                    terminateConference(V5_ECODE_CALLBACK_SIGNED_IN,
                                        V5_COMPONENT_VCUBE_PORTAL,
                                        V5_PORTAL_JOIN_CONF_HTTP_SENDASYNC_FUNCCALL_NG);
                    return;
                }
                break;
            }

            case VIDYO_CLIENT_OUT_EVENT_SIGN_OUT:
            {
                // オフラインでは来ない
                break;
            }
            case VIDYO_CLIENT_OUT_EVENT_SIGNED_OUT:
            {
                // SIGNED_IN が来なくてもくる
                auto callStatus = VIDYO_CLIENT_CALL_STATE_IDLE;
                if (VidyoClient::isStarted())
                    callStatus = VidyoClient::getCallStatus();
                
                if (vidyoSignoutFlag.setSignal())
                {
                    lock_guard l(mtx);
                    // JOINING から IDLE への遷移が発生しないことがあるため
                    if (callStatus == VIDYO_CLIENT_CALL_STATE_IDLE ||
                        callStatus == VIDYO_CLIENT_CALL_STATE_JOINING)
                    {
                        vidyoIdleFlag.setSignal();
                    }
                    terminateConference(V5_ECODE_CALLBACK_SIGNED_OUT,
                                        V5_COMPONENT_VIDYO,
                                        V5_VIDYO_CLIENT_OUT_EVENT_SIGNED_OUT_RECIEVED);
                    return;
                }
                break;
            }
            case VIDYO_CLIENT_OUT_EVENT_LOGIN:
            {
                vidyoSignoutFlag.resetSignal();
                break;
            }
            
            case VIDYO_CLIENT_OUT_EVENT_JOINING:
            {
                // enableMuteMicrophoneOnJoin などが効かないプラットフォームがあるため
                VidyoClient::setAudioInMute(true);
                VidyoClient::setAudioOutMute(true);
                VidyoClient::setVideoMute(true);
                VidyoClient::setMaxParticipants(0); //必ずエラー値が返却されるのでエラー制御しない
                break;
            }

            case VIDYO_CLIENT_OUT_EVENT_JOIN_PROGRESS:
            {
                //V5LOG(V5_LOG_INFO, "joining ...\n");
                break;
            }
            
            case VIDYO_CLIENT_OUT_EVENT_CONFERENCE_ACTIVE:
            {
                V5LOG(V5_LOG_INFO, "%1.3lf[sec]: start streaming to Media-Server.\n", joinConfTime.elapsed());

                //sendInviteMail("takeshi-kimura@vcube.co.jp,hozumi@vcube.co.jp,daisuke-sako@vcube.co.jp");
                //sendInviteMail(""); // will return in 701.
                
                // 強制的に自映像表示モードになっているので、
                // ここでのrenderSelfViewは実行しないように変更する。
                //renderSelfView(true);
                
                
#if ENABLE_DYNAMIC_WATCH
                VidyoClient::startDynamicWatchVideoSource(maxParticipants.load(), videoSize);
#endif
                
                // 6. get PID.
                VidyoClientRequestGetPid pid = { 0 };
                
                if (VidyoClientSendRequest(VIDYO_CLIENT_REQUEST_GET_PID, &pid, sizeof(VidyoClientRequestGetPid)) != VIDYO_CLIENT_ERROR_OK)
                {
                    V5LOG(V5_LOG_ERROR, "GET PID failed.\n"); /*E*/
                    terminateConference(V5_ECODE_CALLBACK_CONFERENCE_ACTIVE,
                                        V5_COMPONENT_VIDYO,
                                        V5_VIDYO_CLIENT_REQUEST_GET_PID_REQUEST_NG);
                    return;
                }
                ud->PID = pid.PID;
                V5LOG(V5_LOG_INFO, "[PID]: My PID: %s\n", ud->PID.c_str());

                if (!transitionStatus(CONNECTING_TO_MEDIA_SERVER, CONNECTING_TO_CONFERENCE_SERVER))
                {
                    // 既に退室もしくは終了シーケンスに入っていると思われる
                    // TODO: シーケンスを確認し、切断処理に移行するべき
                    terminateConference(V5_ECODE_CALLBACK_CONFERENCE_ACTIVE,
                                        V5_COMPONENT_VIDYO,
                                        V5_INTERNAL_STATUS_CHANGE_FAILED_CONNECTING_TO_CONFERENCE_SERVER);
                    return;
                }
                
                // 予約パラメータ反映処理
                reservedParameterSettings();
                
                V5LOG(V5_LOG_INFO, "%1.3lf[sec]: connect to Conference-Server.\n", joinConfTime.elapsed());
                // X. Call Conference Client "Connect" APIs
                int retWS = connectToConferenceServer(this->conferenceURL,
                                                      this->sessionToken,
                                                      this->conferenceID,
                                                      pid.PID,
                                                      this->displayName);
                if( retWS != CCLIENT_OK ){
                    V5ErrorReason reason;
                    switch( retWS ) {
                        case CCLIENT_CON_NON_PRM:
                        {
                            V5LOG(V5_LOG_ERROR, "Websocket Connect : failed (In Parameters error).\n");
                            reason = V5_CONFERENCE_CLIENT_CONNECT_PARAMETER_ERROR;
                            break;
                        }
                        case CCLIENT_CON_CONNECT_NG:
                        {
                            V5LOG(V5_LOG_ERROR, "Websocket Connect : failed (Connection error).\n");
                            reason = V5_CONFERENCE_CLIENT_SOCKET_CONNECTION_NG;
                            break;
                        }
                        case CCLIENT_CON_CONNECTED:
                        {
                            V5LOG(V5_LOG_ERROR, "Websocket Connect : failed (Connected error).\n");
                            reason = V5_CONFERENCE_CLIENT_TO_CONFERENCE_SERVER_ALREADY_CONNECTED;
                            break;
                        }
                        case CCLIENT_CON_WORD_NG:
                        {
                            V5LOG(V5_LOG_ERROR, "Websocket Connect : failed (words error).\n");
                            reason = V5_CONFERENCE_CLIENT_SPECIFIED_CONNECTION_WORDS_NOT_WS_AND_NOT_WSS;
                            break;
                        }
                        case CCLIENT_CON_PXY_URI_SET_NG:
                        {
                            V5LOG(V5_LOG_ERROR, "Websocket Connect : failed (Proxy Data Set NG).\n");
                            reason = V5_CONFERENCE_CLIENT_PROXY_DATA_SET_FAILED;
                            break;
                        }
                        default:
                        {
                            V5LOG(V5_LOG_ERROR, "Websocket Connect : failed (Unkown error).\n");
                            reason = V5_CONFERENCE_CLIENT_CONNECT_UNKNOWN_ERR;
                            break;
                        }
                    }
                    
                    // VIDYO SIGNOFF CALL
                    V5LOG(V5_LOG_INFO, "VIDYO SIGNOFF EVENT CALL" );
                    terminateConference(V5_ECODE_CALLBACK_CONFERENCE_ACTIVE, V5_COMPONENT_CONFERENCE, reason);
                    return;
                }
                
                // JoinConference Timer Cancel
                this->joinConferenceTimer->stop();
                
                break;
            }
			case VIDYO_CLIENT_OUT_EVENT_CONFERENCE_ENDED:
			{
                terminateConference(V5_ECODE_UNEXPECTED_MEDIA_STREAM_ENDED,
                                    V5_COMPONENT_VIDYO,
                                    V5_VIDYO_CLIENT_OUT_EVENT_CONFERENCE_ENDED_RECIEVED);
                break;
			}
            case VIDYO_CLIENT_OUT_EVENT_SELECTED_DEVICE_ON_OS_CHANGED:
            {
                VidyoClientOutEventSelectedDeviceOnOsChanged* changed = (VidyoClientOutEventSelectedDeviceOnOsChanged*)param;
                
                // DeviceConfiguration内でGET_CONFIGURATIONのRequestをしない構造にしたので、
                // ここで実施する。
                VidyoUint error;
                VidyoClientRequestConfiguration conf;
                
                if ((error = VidyoClientSendRequest(VIDYO_CLIENT_REQUEST_GET_CONFIGURATION, &conf, sizeof(VidyoClientRequestConfiguration))) != VIDYO_CLIENT_ERROR_OK)
                {
                    V5LOG(V5_LOG_ERROR, "Failed to request configuration with error: %d\n", error);
                }
                else
                {
                    // デバイス情報更新
                    updateDeviceConfiguration( conf );
                }

                int index = 0;

                auto callback = deviceSink.lock();
                switch(changed->deviceType) {
                    case VIDYO_CLIENT_DEVICE_TYPE_AUDIO_IN:
                        index = devConf->getCurrent(V5_DEVICE_TYPE_AUDIO_IN);
                        if (callback) callback->onSelectedSystemAudioInDevicesChanged(index);
                        break;
                    case VIDYO_CLIENT_DEVICE_TYPE_AUDIO_OUT:
                        index = devConf->getCurrent(V5_DEVICE_TYPE_AUDIO_OUT);
                        if (callback) callback->onSelectedSystemAudioOutDevicesChanged(index);
                        break;
                    default:
                        V5LOG(V5_LOG_ERROR, "[DeviceSink]: SYSTEM DEVICE CHANGED: illegal device type [%d]\n", static_cast<int>(changed->deviceType)); /*E*/
                        invokeAsyncCallBack(V5_ECODE_CALLBACK_SYSTEM_DEVICE_CHANGED,
                                            V5_COMPONENT_VIDYO,
                                            V5_VIDYO_CLIENT_OUT_EVENT_SELECTED_DEVICE_ON_OS_CHANGED_IN_ILLEGAL_DEVICE_TYPE);
                        break;
                }
                break;
            }
                
            case VIDYO_CLIENT_OUT_EVENT_DEVICE_SELECT:
            {
                V5LOG(V5_LOG_INFO, "[VIDYO_CLIENT_OUT_EVENT_DEVICE_SELECT!]\n");
                
                break;
            }
                
            // we do not use this event because of differencies between MacOSX and iOS (SelectionMethod is only for iOS).
            case VIDYO_CLIENT_OUT_EVENT_DEVICE_SELECTION_CHANGED:
            {
				auto selectionC = static_cast<VidyoClientOutEventDeviceSelectionChanged*>(param);
                selectionC = selectionC;
                V5LOG(V5_LOG_INFO, "[DEVICE SELECTION CHANGED!]\n");
                V5LOG(V5_LOG_INFO, "changeType = %d\n", selectionC->changeType);
                V5LOG(V5_LOG_INFO, "  prev=%s\n", selectionC->prevDeviceName);
                V5LOG(V5_LOG_INFO, "  curr=%s\n", selectionC->newDeviceName);
                V5LOG(V5_LOG_INFO, "  changeType=%d\n", selectionC->changeType);
                break;
            }
            case VIDYO_CLIENT_OUT_EVENT_DEVICES_CHANGED:
            {
                VidyoClientOutEventDevicesChanged* changed = (VidyoClientOutEventDevicesChanged*)param;
                
                // DeviceConfiguration内でGET_CONFIGURATIONのRequestをしない構造にしたので、
                // ここで実施する。
                VidyoUint error;
                VidyoClientRequestConfiguration conf;
                
                if ((error = VidyoClientSendRequest(VIDYO_CLIENT_REQUEST_GET_CONFIGURATION, &conf, sizeof(VidyoClientRequestConfiguration))) != VIDYO_CLIENT_ERROR_OK)
                {
                    V5LOG(V5_LOG_ERROR, "Failed to request configuration with error: %d\n", error);
                }
                else
                {
                    // デバイス情報更新
                    updateDeviceConfiguration( conf );
                }

                int index;
                
                auto callback = deviceSink.lock();
                switch(changed->deviceType) {
                    case VIDYO_CLIENT_DEVICE_TYPE_AUDIO_IN:
                        index = devConf->getCurrent(V5_DEVICE_TYPE_AUDIO_IN);
                        if (callback) callback->onAudioInDeviceListChanged(index);
                        break;
                    case VIDYO_CLIENT_DEVICE_TYPE_AUDIO_OUT:
                        index = devConf->getCurrent(V5_DEVICE_TYPE_AUDIO_OUT);
                        if (callback) callback->onAudioOutDeviceListChanged(index);
                        break;
                    case VIDYO_CLIENT_DEVICE_TYPE_VIDEO:
                        index = devConf->getCurrent(V5_DEVICE_TYPE_VIDEO);
                        if (callback) callback->onVideoDeviceListChanged(index);
                        break;
                    default:
                        // do nothing
                        V5LOG(V5_LOG_ERROR, "[DeviceSink]: DEVICE CHANGED: illegal device type [%d]\n", static_cast<int>(changed->deviceType)); /*E*/
                        invokeAsyncCallBack(V5_ECODE_CALLBACK_DEVICES_CHANGE,
                                            V5_COMPONENT_VIDYO,
                                            V5_VIDYO_CLIENT_OUT_EVENT_DEVICES_CHANGED_IN_ILLEGAL_DEVICE_TYPE);
                        break;
                }
                break;
            }
#if VIDYO_HAS_RMOTE_SOURCE_CHANGED_EVENT
            case VIDYO_CLIENT_OUT_EVENT_REMOTE_SOURCE_ADDED:
            {
                auto const& changed = *static_cast<VidyoClientOutEventRemoteSourceChanged*>(param);
                auto callback = eventSink.lock();
                // IN_CONFERENCE でなければとりあえず投げないようにしてしまうが、
                // onJoinedConference で正しい RemoteSource を通知することはできない
                if (callback && currentStatus == IN_CONFERENCE)
                {
                    V5RemoteSourceChanged data;
                    switch(changed.sourceType)
                    {
                        case VIDYO_CLIENT_SOURCE_TYPE_VIDEO:
                            data.sourceType = V5_MEDIA_SOURCE_TYPE_VIDEO;
                            break;
                        case VIDYO_CLIENT_SOURCE_TYPE_APPLICATION:
                            data.sourceType = V5_MEDIA_SOURCE_TYPE_APPLICATION;
                            break;
                        case VIDYO_CLIENT_SOURCE_TYPE_AUDIO:
                            data.sourceType = V5_MEDIA_SOURCE_TYPE_AUDIO;
                            break;
                        default:
                            data.sourceType = V5_MEDIA_SOURCE_TYPE_UNKNOWN;
                            break;
                    }
                    data.participantURI = changed.participantURI;
                    data.displayName = changed.displayName;
                    data.sourceName = changed.sourceName;
                    callback->onRemoteSourceAdded(data);
                }
                break;
            }
            case VIDYO_CLIENT_OUT_EVENT_REMOTE_SOURCE_REMOVED:
            {
                auto const& changed = *static_cast<VidyoClientOutEventRemoteSourceChanged*>(param);
                auto callback = eventSink.lock();
                // IN_CONFERENCE でなければとりあえず投げないようにしてしまうが、
                // onJoinedConference で正しい RemoteSource を通知することはできない
                if (callback && currentStatus == IN_CONFERENCE)
                {
                    V5RemoteSourceChanged data;
                    switch(changed.sourceType)
                    {
                        case VIDYO_CLIENT_SOURCE_TYPE_VIDEO:
                            data.sourceType = V5_MEDIA_SOURCE_TYPE_VIDEO;
                            break;
                        case VIDYO_CLIENT_SOURCE_TYPE_APPLICATION:
                            data.sourceType = V5_MEDIA_SOURCE_TYPE_APPLICATION;
                            break;
                        case VIDYO_CLIENT_SOURCE_TYPE_AUDIO:
                            data.sourceType = V5_MEDIA_SOURCE_TYPE_AUDIO;
                            break;
                        default:
                            data.sourceType = V5_MEDIA_SOURCE_TYPE_UNKNOWN;
                            break;
                    }
                    data.participantURI = changed.participantURI;
                    data.displayName = changed.displayName;
                    data.sourceName = changed.sourceName;
                    callback->onRemoteSourceRemoved(data);
                }
                break;
            }
#endif
            case VIDYO_CLIENT_OUT_EVENT_SELECTED_PARTICIPANTS_CHANGED:
            {
                if (currentStatus == IN_CONFERENCE)
                {
                    auto const& selected = *static_cast<VidyoClientRequestParticipants*>(param);
                    onSelectedParticipantsUpdated(selected);
                }
                break;
            }
            case VIDYO_CLIENT_OUT_EVENT_PARTICIPANTS_CHANGED:
            {
                decltype(vidyoParticipantSpecificInfos) vidyoParticipantSpecificInfosNext;

                auto const& data = *static_cast<VidyoClientOutEventParticipantsChanged*>(param);
                if (data.participantCount)
                {
                    VidyoClientRequestParticipants participants;
                    if (VidyoClient::getParticipants(&participants) != VIDYO_CLIENT_ERROR_OK)
                    {
                        // TODO: エラー
                    }

                    for (VidyoUint i = 0; i < participants.numberParticipants; i++)
                    {
                        auto& uri = participants.URI[i];
                        vidyoParticipantSpecificInfosNext[uri] = vidyoParticipantSpecificInfos[uri];
                    }
                }

                std::swap(vidyoParticipantSpecificInfos, vidyoParticipantSpecificInfosNext);
                break;
            }
            case VIDYO_CLIENT_OUT_EVENT_VIDEO_FRAME_RECEIVED:
            {
                VidyoClientOutEventVideoFrameReceived *videoFrameEvent = (VidyoClientOutEventVideoFrameReceived *)param;
                if (currentStatus == IN_CONFERENCE &&
                    (videoFrameEvent->mediaType == VIDYO_CLIENT_MEDIA_CONTROL_TYPE_VIDEO
                   || videoFrameEvent->mediaType == VIDYO_CLIENT_MEDIA_CONTROL_TYPE_APPLICATION)) {

                    // maxParticipants = 0 don't callback
#if !ENABLE_DYNAMIC_WATCH
                    if(maxParticipants == 0) {
                        break;
                    }

                    if(currentNumberOfParticipants > 1
                            && videoFrameEvent->viewPriority != 24 /* 24 is self raw frame */
                            && videoFrameEvent->viewPriority > (maxParticipants- 1)) {

                        break;
                    }

                    //VIDYO_CLIENT_REQUEST_SET_CONFIGURATION selfViewLoopbackPolicyを1(disable)にすると
                    //参加者が自分だけの場合でも24になる模様
                    if(!selfViewRenderingState.load() && videoFrameEvent->viewPriority == 24) {
                        break;
                    }
#endif
                    VideoRawFrame frame;
                    frame.sourceId = videoFrameEvent->sourceId;
                    frame.width = videoFrameEvent->frame.width;
                    frame.height = videoFrameEvent->frame.height;
                    frame.buffer = videoFrameEvent->frame.data;
                    frame.viewPriority = (int)videoFrameEvent->viewPriority;
                    frame.participantURI = videoFrameEvent->participantURI;
                    frame.mediaType = videoFrameEvent->mediaType;
                    auto callback = eventSink.lock();
                    if (callback)
                        callback->onReceiveVideoFrame(frame);
                }
                break;
            };
            case VIDYO_CLIENT_OUT_EVENT_ADD_SHARE:
            {
                auto& addShare = *static_cast<VidyoClientOutEventAddShare const*>(param);
                auto callback = eventSink.lock();
                if (callback && currentStatus == IN_CONFERENCE)
                    callback->onAddShare(addShare.URI, isUriSelf(addShare.URI));
                break;
            }
            case VIDYO_CLIENT_OUT_EVENT_REMOVE_SHARE:
            {
                auto& removeShare = *static_cast<VidyoClientOutEventRemoveShare const*>(param);
                auto callback = eventSink.lock();
                if (callback && currentStatus == IN_CONFERENCE)
                    callback->onRemoveShare(removeShare.URI, isUriSelf(removeShare.URI));
                break;
            }
            case VIDYO_CLIENT_OUT_EVENT_MUTED_AUDIO_IN:
            {
                auto const& data = *static_cast<VidyoClientOutEventMuted*>(param);
                auto const isMuted = (data.isMuted != VIDYO_FALSE);
                auto const noError = (data.errorCode == VIDYO_CLIENT_ERROR_OK);
                auto needRetry = false;

                // 収束しない場合はリトライ回数に制限を設ける
                {
                    lock_guard l(mtx);
                    needRetry = (noError && (isMuted != microphoneMuteState) && IN_CONFERENCE == currentStatus && !microphoneMuteRequesting);
                    microphoneMuteRequesting = needRetry;
                }
                
                if (needRetry)
                {
                    VidyoClient::setAudioInMute(microphoneMuteState);
                }
                break;
            }
            case VIDYO_CLIENT_OUT_EVENT_MUTED_AUDIO_OUT:
            {
                auto const& data = *static_cast<VidyoClientOutEventMuted*>(param);
                auto const isMuted = (data.isMuted != VIDYO_FALSE);
                auto const noError = (data.errorCode == VIDYO_CLIENT_ERROR_OK);
                auto needRetry = false;

                // 収束しない場合はリトライ回数に制限を設ける
                {
                    lock_guard l(mtx);
                    needRetry = (noError && (isMuted != speakerMuteState) && IN_CONFERENCE == currentStatus && !speakerMuteRequesting);
                    speakerMuteRequesting = needRetry;
                }

                if (needRetry)
                {
                    VidyoClient::setAudioOutMute(speakerMuteState);
                }
                break;
            }
            case VIDYO_CLIENT_OUT_EVENT_MUTED_VIDEO:
            {
                auto const& data = *static_cast<VidyoClientOutEventMuted*>(param);
                auto const isMuted = (data.isMuted != VIDYO_FALSE);
                auto const noError = (data.errorCode == VIDYO_CLIENT_ERROR_OK);
                auto needRetry = false;

                cameraMuteChangeMtoUM = false;
                
                // 収束しない場合はリトライ回数に制限を設ける
                {
                    lock_guard l(mtx);
                    needRetry = (noError && isMuted != cameraMuteState && IN_CONFERENCE == currentStatus && !cameraMuteRequesting);
                    cameraMuteRequesting = needRetry;
                }

                if (needRetry)
                {
                    VidyoClient::setVideoMute(cameraMuteState);
                }
                
                if( selectDeviceReserve )
                {
                    (getDeviceConfiguration())->setConf(V5_DEVICE_TYPE_VIDEO,deviceIndexVideo);
                    
                    /* ここの処理も変更が必要 */
                    // デバイスのカレント番号を変更する必要がある。
                    // カレント番号はsetConf内で設定しているので、ここでは更新しない。
                    // updateDeviceConfiguration();
                    
                    auto devsink = deviceSink.lock();
                    if(devsink)
                    {
                        devsink->onVideoDeviceListChanged(deviceIndexVideo);
                    }
                    selectDeviceReserve = false;
                }
                
                break;
            }
            case VIDYO_CLIENT_OUT_EVENT_PARTICIPANTS_LIMIT:
            {
                auto const& limit = *static_cast<VidyoClientOutEventParticipantsLimit*>(param);
                auto needRetry = false;

                // 収束しない場合はリトライ回数に制限を設ける
                {
                    lock_guard l(mtx);
                    needRetry = (limit.UserLimit != maxParticipants && IN_CONFERENCE == currentStatus && !maxParticipantsRequesting);
                    maxParticipantsRequesting = needRetry;
                }

                if (needRetry)
                {
                    VidyoClient::setMaxParticipants(maxParticipants);
                }
                break;
            }
            case VIDYO_CLIENT_OUT_EVENT_FLOATING_WINDOW:
            {
                auto const& floating = *static_cast<VidyoClientOutEventFloatingWindow*>(param);
                auto callback = eventSink.lock();
                if (callback)
                    callback->onFloatingWindow(floating.window);
                break;
            }
            default:
                break;
        }
        
    }
    else
    {
        V5LOG(V5_LOG_INFO, "Unknown event: %d\n", static_cast<int>(actionType));
    }
}

V5CODE V5ClientImpl::changeParticipantVideoFrame(const char* participantURI, int width, int height, int frameRate, int minFrameInterval)
{
    std::string uri(participantURI);
    auto& specificInfo = vidyoParticipantSpecificInfos[uri];
    if (!specificInfo) {
        specificInfo.reset(new VidyoParticipantInfo());
    }
    auto& setting = specificInfo->videoSorceSetting;
    if (!setting)
    {
        setting.reset(new VidyoClientInEventStartWatchVideoSource);
        memset(setting.get(), 0, sizeof(*setting));
    }
    vrms5::utils::utf8cpy(setting->source.participantURI, uri);
    setting->source.mediaType = VIDYO_CLIENT_MEDIA_CONTROL_TYPE_VIDEO;
    setting->source.sourceId = 0;
    setting->width = width;
    setting->height = height;
    setting->frameRate = frameRate;
    setting->minFrameInterval = minFrameInterval;
    setting->watchData = nullptr;

    if (isRetrieveRawFrame) {

        // 1. stop current frame retrievals.
        
        VidyoClientInEventStopWatchVideoSource stopEvent = {0};
        vrms5::utils::utf8cpy(stopEvent.source.participantURI, uri);
        stopEvent.source.mediaType = VIDYO_CLIENT_MEDIA_CONTROL_TYPE_VIDEO;
        stopEvent.source.sourceId = 0;
        
        if (VidyoClient::sendEvent(VIDYO_CLIENT_IN_EVENT_STOP_WATCH_VIDEO_SOURCE, stopEvent) == false)
        {
            V5LOG(V5_LOG_INFO, "Failed to get response of called event"); /*E*/
            invokeAsyncCallBack(V5_ECODE_CHANGE_VIDEO_FRAME,
                                V5_COMPONENT_VIDYO,
                                V5_VIDYO_CLIENT_IN_EVENT_STOP_WATCH_VIDEO_SOURCE_EVENT_REQUEST_FAILED_TO_CHANGE_PARTICIPANT);
        }

        // 2. start frame retrievals with new settings.
        if (VidyoClient::sendEvent(VIDYO_CLIENT_IN_EVENT_START_WATCH_VIDEO_SOURCE, *setting) == false) {
            V5LOG(V5_LOG_INFO, "Failed to get response of called event"); /*E*/
            invokeAsyncCallBack(V5_ECODE_CHANGE_VIDEO_FRAME,
                                V5_COMPONENT_VIDYO,
                                V5_VIDYO_CLIENT_IN_EVENT_START_WATCH_VIDEO_SOURCE_EVENT_REQUEST_FAILED_TO_CHANGE_PARTICIPANT);
        }
    }
    return V5OK;
}

V5CODE V5ClientImpl::sendInviteMail(const char* mailAddress)
{
    std::string addr(mailAddress);
    this->ud->sendMail = std::make_shared<V5SendInvitationMail>(this->sessionToken, this->conferenceID, this->ud->env->escape(mailAddress));

    bool ret = this->ud->env->sendAsync(this->ud->sendMail->query,[=](picojson::value& jsonObj, V5ClientHttpResult result)->void {
        
        const char* mailAddress = addr.c_str();
        if( result == V5_HTTP_REQUEST_SEND_NG )
        {
            V5LOG(V5_LOG_ERROR, "Send Invitation Mail Event HTTP Request Send NG.\n");
            invokeInviteMailCallBack(V5_ECODE_SEND_INVITE_MAIL,
                                     V5_COMPONENT_VCUBE_PORTAL,
                                     V5_PORTAL_INVITE_MAIL_HTTP_REQUEST_SEND_NG,
                                     this->ud->sendMail->status.c_str(),
                                     mailAddress);
            return; /*E*/
        }
        
        if ( result == V5_HTTP_RESPONSE_PARSE_NG )
        {
            V5LOG(V5_LOG_ERROR, "Send Invitation Mail Event HTTP Response Data JsonParse NG.\n");
            invokeInviteMailCallBack(V5_ECODE_SEND_INVITE_MAIL,
                                     V5_COMPONENT_VCUBE_PORTAL,
                                     V5_PORTAL_INVITE_MAIL_HTTP_RESPONSE_DATA_PARSE_NG,
                                     this->ud->sendMail->status.c_str(),
                                     mailAddress);

            return; /*E*/
        }
        
        this->ud->sendMail->parse(jsonObj);
        if (! this->ud->sendMail->isParsed)
        {
            V5LOG(V5_LOG_ERROR, "Send Invitation Mail: failed (not filled with data).\n");
            
            invokeInviteMailCallBack(V5_ECODE_SEND_INVITE_MAIL,
                                     V5_COMPONENT_VCUBE_PORTAL,
                                     V5_PORTAL_INVITE_MAIL_HTTP_RESPONSE_NOT_FILLED_WITH_DATA,
                                     this->ud->sendMail->status.c_str(),
                                     mailAddress);
            return; /*E*/

        }
    
        if (! ((this->ud->sendMail->result == "OK")&&(this->ud->sendMail->status == "200")))
        {
            if (this->ud->sendMail->status == "701")
            {
                V5LOG(V5_LOG_ERROR, "Send Invitation Mail : failed (Email Address is Empty).\n", this->ud->sendMail->status.c_str());
                invokeInviteMailCallBack(V5_ECODE_SEND_INVITE_MAIL,
                                         V5_COMPONENT_VCUBE_PORTAL,
                                         V5_PORTAL_INVITE_MAIL_HTTP_RESPONSE_EMAIL_ADDRESS_EMPTY,
                                         this->ud->sendMail->status.c_str(),
                                         mailAddress);
                return;
            }
            
            V5LOG(V5_LOG_ERROR, "Send Invitation Mail : response error. status(%s).\n", this->ud->sendMail->status.c_str());
            V5LOG(V5_LOG_ERROR, "Send Invitation Mail : response error. description(%s).\n", this->ud->sendMail->description.c_str());
            
            invokeInviteMailCallBack(V5_ECODE_SEND_INVITE_MAIL,
                                     V5_COMPONENT_VCUBE_PORTAL,
                                     V5_PORTAL_INVITE_MAIL_HTTP_RESPONSE_RESULT_NG,
                                     this->ud->sendMail->status.c_str(),
                                     mailAddress);
            
            
            return; /*E*/
        }
        
        V5LOG(V5_LOG_INFO, "Send Invitation Mail : HTTP Request Send to Portal Server. (Mail Address = %s).\n", mailAddress);
        invokeInviteMailCallBack(V5_ECODE_SEND_INVITE_MAIL,
                                 V5_COMPONENT_VCUBE_PORTAL,
                                 V5_PORTAL_INVITE_MAIL_EMAIL_SEND_SUCCESS,
                                 this->ud->sendMail->status.c_str(),
                                 mailAddress);
    });
    
    if (! ret) {
        V5LOG(V5_LOG_ERROR, "Send Invite Mail: failed (sendAsync Function Call failure).\n");
        
        invokeInviteMailCallBack(V5_ECODE_SEND_INVITE_MAIL,
                                 V5_COMPONENT_VCUBE_PORTAL,
                                 V5_PORTAL_INVITE_MAIL_HTTP_SENDASYNC_FUNCCALL_NG,
                                 this->ud->sendMail->status.c_str(),
                                 mailAddress);
        
        return V5Error; /*E*/
    }

    return V5OK;
}

V5CODE V5ClientImpl::setFrameDirection(V5Rect rect)
{
    V5CODE ret = V5OK;
//    (void)VidyoClientSendRequest(VIDYO_CLIENT_REQUEST_SET_LAYOUT_RECT, &rect, sizeof(V5Rect));
    
    VidyoClientInEventResize event;
    event.height = rect.height;
    event.width = rect.width;
    event.x = rect.xPos;
    event.y = rect.yPos;
    (void)VidyoClientSendEvent(VIDYO_CLIENT_IN_EVENT_RESIZE, &event, sizeof(event));
    
    return ret;
}

V5CODE V5ClientImpl::setOrientation(V5Orientation orie)
{
    V5CODE ret = V5OK;
    VidyoClientInEventSetOrientation event = {};
    
    event.orientation = (VidyoClientOrientation)orie;
    event.control = VIDYO_CLIENT_ORIENTATION_CONTROL_BOTH;
    
    V5LOG(V5_LOG_INFO, "Orientation changed %d\n", orie);
    // always send event to VidyoClient to set orientation, whether new or cached,
    // in case AppFramework objects were not created in time for first orientation change/poll
    (void)VidyoClientSendEvent(VIDYO_CLIENT_IN_EVENT_SET_ORIENTATION,
                               &event, sizeof(VidyoClientInEventSetOrientation));
    return ret;
}

void V5ClientImpl::shareAppWindow(V5WindowCapturerWindowId windowId, int width, int height)
{
    VidyoClientInEventShare shareEvent;
    memset(&shareEvent, 0, sizeof(shareEvent));

#if VIDYO_HAS_NEXT_SHARE_API
    shareEvent.shareType = VIDYO_CLIENT_CONTENTS_SHARE_TYPE_APPLICATION_WINDOW;
#endif
    shareEvent.window = windowId;
    shareEvent.windowSize.xPos = 0;
    shareEvent.windowSize.yPos = 0;
    shareEvent.windowSize.width = width;
    shareEvent.windowSize.height = height;

    VidyoClientSendEvent(VIDYO_CLIENT_IN_EVENT_SHARE, &shareEvent, sizeof(VidyoClientInEventShare));
}

void V5ClientImpl::shareSysDesktop(V5Uint windowId, int width, int height)
{
    VidyoClientInEventShare shareEvent;
    memset(&shareEvent, 0, sizeof(shareEvent));

#if VIDYO_HAS_NEXT_SHARE_API
    shareEvent.shareType = VIDYO_CLIENT_CONTENTS_SHARE_TYPE_DESKTOP_WINDOW;
#endif
    shareEvent.window = reinterpret_cast<VidyoWindowCapturerWindowId>(windowId);
    shareEvent.windowSize.xPos = 0;
    shareEvent.windowSize.yPos = 0;
    shareEvent.windowSize.width = width;
    shareEvent.windowSize.height = height;

    VidyoClientSendEvent(VIDYO_CLIENT_IN_EVENT_SHARE, &shareEvent, sizeof(VidyoClientInEventShare));
}

void V5ClientImpl::unshare()
{
	VidyoClientSendEvent(VIDYO_CLIENT_IN_EVENT_UNSHARE, nullptr, 0);
}

void V5ClientImpl::enableActiveSpeakerLayout( bool showActiveSpeaker )
{
    // 2015/04/10時点では会議入室前に本APIをコールしても
    // 設定が反映されないみたいなので、予約しておく。
    // イベント自体のコールは会議参加前の時点でも実施すべき？
    auto tmpCurrentStatus = currentStatus.load();
    if( ( tmpCurrentStatus != IN_CONFERENCE ) &&
        ( tmpCurrentStatus != CONNECTING_TO_CONFERENCE_SERVER ) )
    {
        showActiveSpeakerReserve = true;
        nextShowActiveSpeaker = showActiveSpeaker;
        
        // 今々は予約したらイベントの呼び出しはしないでおく。
        return;
    }
    
    VidyoClientInEventLayout layout = {0};
    if( showActiveSpeaker )
        layout.numPreferred = 1;
    else
        layout.numPreferred = 0;

    VidyoClientSendEvent(VIDYO_CLIENT_IN_EVENT_LAYOUT, &layout, sizeof(VidyoClientInEventLayout));
}

void V5ClientImpl::changeSelfPreviewMode( V5ClientPreviewMode previewMode )
{
    // 2015/04/10時点では会議入室前に本APIをコールしても
    // 設定が反映されないみたいなので、予約しておく。
    // イベント自体のコールは会議参加前の時点でも実施すべき？
    auto tmpCurrentStatus = currentStatus.load();
    if( ( tmpCurrentStatus != IN_CONFERENCE ) &&
        ( tmpCurrentStatus != CONNECTING_TO_CONFERENCE_SERVER ) )
    {
        selfPreviewModeReserve = true;
        nextPreviewMode = previewMode;
        
        // 今々は予約したらイベントの呼び出しはしないでおく。
        return;
    }
    
    VidyoClientInEventPreview mode;
    memset(&mode, 0, sizeof(mode));
    switch (previewMode) {
        case  V5_CLIENT_PREVIEW_MODE_NONE:
            mode.previewMode = VIDYO_CLIENT_PREVIEW_MODE_NONE;
            break;
        case V5_CLIENT_PREVIEW_MODE_PIP:
            mode.previewMode = VIDYO_CLIENT_PREVIEW_MODE_PIP;
            break;
        case V5_CLIENT_PREVIEW_MODE_DOCK:
            mode.previewMode = VIDYO_CLIENT_PREVIEW_MODE_DOCK;
            break;
        case V5_CLIENT_PREVIEW_MODE_OTHER:
            mode.previewMode = VIDYO_CLIENT_PREVIEW_MODE_OTHER;
            break;
        default:
            break;
    }
    VidyoClientSendEvent(VIDYO_CLIENT_IN_EVENT_PREVIEW,&mode,sizeof(VidyoClientInEventPreview));
}

void V5ClientImpl::setSelfViewLoopbackPolicy(V5ClientSelfViewLoopbackPolicy viewPolicy)
{    
    VidyoClientRequestConfiguration conf = {0};
    VidyoUint error;
    if ((error = VidyoClientSendRequest(VIDYO_CLIENT_REQUEST_GET_CONFIGURATION, &conf, sizeof(VidyoClientRequestConfiguration))) != VIDYO_CLIENT_ERROR_OK) {
        V5LOG(V5_LOG_ERROR, "Failed to request get configuration with error: %d\n", error);
    } else {
        
        switch (viewPolicy) {
            case V5_CLIENT_LOOPBACK_POLICY_ENABLE:
                conf.selfViewLoopbackPolicy = 0;
                break;
            case V5_CLIENT_LOOPBACK_POLICY_DISABLE:
                conf.selfViewLoopbackPolicy = 1;
                break;
            case V5_CLIENT_LOOPBACK_POLICY_AUTO:
                conf.selfViewLoopbackPolicy = 2;
                break;
                
            default:
                break;
        }
        
        if ((error = VidyoClientSendRequest(VIDYO_CLIENT_REQUEST_SET_CONFIGURATION, &conf, sizeof(VidyoClientRequestConfiguration))) != VIDYO_CLIENT_ERROR_OK) {
            V5LOG(V5_LOG_ERROR, "Failed to request set configuration with error: %d\n", error);
        }
    }
}



void V5ClientImpl::leaveConferenceSync()
{
    leaveConferenceAsync();

    vidyoSignoutFlag.wait();
    confDisconnectedFlag.wait();
    vidyoIdleFlag.wait();
}

AsyncResult V5ClientImpl::leaveConferenceAsync()
{
    return terminateConference(V5_ECODE_LEAVE_CONFERENCE,
                               V5_COMPONENT_APPLICATION,
                               V5_APP_LEAVE_CONFERENCE_REASON_SUCCESS);
}

void V5ClientImpl::setConferenceEventSink(std::nullptr_t) {
	eventSink.reset();
}

void V5ClientImpl::setConferenceEventSink(std::weak_ptr<IV5ConferenceEventSink> const& sink) {
	eventSink = sink;
    statInfo->setConferenceCallback( sink );
}

void V5ClientImpl::setChatEventSink(std::weak_ptr<IV5ChatEventSink> const& sink) {
    chatEventSink = sink;
    conferenceClient->SetChatCallback(sink);
}

void V5ClientImpl::setChatEventSink(std::nullptr_t) {
	chatEventSink.reset();
	conferenceClient->SetChatCallback(nullptr);
}

void V5ClientImpl::sendChatMessage(const char* SendMessage) {
    conferenceClient->SendMessage( CLIENT_DEST_ALL, SendMessage );
}

void V5ClientImpl::lockConference( bool LockStatus )
{
    conferenceClient->LockConference( LockStatus );
}

void V5ClientImpl::rejectParticipant( const char *RejectPID )
{
    conferenceClient->RejectParticipant(RejectPID);
}

void V5ClientImpl::updateConferenceName( const char *ConferenceName )
{
    conferenceClient->UpdateConferenceName( ConferenceName );
}

std::shared_ptr<const V5ConferenceStatus> V5ClientImpl::getConferenceStatus() const
{
    return conferenceClient->GetConferenceStatus();
}

void V5ClientImpl::setDeviceEventSink(std::nullptr_t) {
	deviceSink.reset();
}

void V5ClientImpl::setDeviceEventSink(std::weak_ptr<IV5DeviceEventSink> const& sink) {
	deviceSink = sink;
}

V5ParticipantList const& V5ClientImpl::getParticipants(){
//    conferenceClient->GetParticipantList( &ParticipantList );
    static const V5ParticipantList empty;
    return empty;
}

std::shared_ptr<const V5Participant> V5ClientImpl::getSelfParticipant()
{
    return std::move(conferenceClient->GetSelfParticipant());
}

void V5ClientImpl::SendWorkerCrash()
{
    conferenceClient->WorkerCrashSend();
}

void V5ClientImpl::DebugFuncCall()
{
    conferenceClient->DebugSend();
}


void V5ClientImpl::startWatchSharedRawFrame(const char * uri)
{
    VidyoClientInEventStartWatchVideoSource vidyoEvent = {0};
        
    vrms5::utils::utf8cpy(vidyoEvent.source.participantURI, uri);
        
    vidyoEvent.source.mediaType = VIDYO_CLIENT_MEDIA_CONTROL_TYPE_APPLICATION;
    vidyoEvent.source.sourceId  = 0 ;
        
    vidyoEvent.width            = shareSize.width;
    vidyoEvent.height           = shareSize.height;
    vidyoEvent.frameRate        = shareSize.frameRate;
    vidyoEvent.minFrameInterval = shareSize.minFrameInterval;
    vidyoEvent.watchData        = nullptr   ;
        
    if (VidyoClientSendEvent(VIDYO_CLIENT_IN_EVENT_START_WATCH_VIDEO_SOURCE, &vidyoEvent, sizeof(vidyoEvent)) == false)
    {
        V5LOG(V5_LOG_INFO, "Failed to get response of called event"); /*E*/
        invokeAsyncCallBack(V5_ECODE_START_SHARED_RAW_FRAME,
                            V5_COMPONENT_VIDYO,
                            V5_VIDYO_CLIENT_IN_EVENT_START_WATCH_VIDEO_SOURCE_EVENT_REQUEST_FAILED_TO_SHARED_RAWFRAME);
    }
}

void V5ClientImpl::stopWatchSharedRawFrame(const char * uri)
{
    VidyoClientInEventStopWatchVideoSource vidyoEvent = {0};
    
    vrms5::utils::utf8cpy(vidyoEvent.source.participantURI, uri);
    
    vidyoEvent.source.mediaType = VIDYO_CLIENT_MEDIA_CONTROL_TYPE_APPLICATION;
    vidyoEvent.source.sourceId  = 0 ;
    
    if (VidyoClientSendEvent(VIDYO_CLIENT_IN_EVENT_STOP_WATCH_VIDEO_SOURCE, &vidyoEvent, sizeof(vidyoEvent)) == false)
    {
        V5LOG(V5_LOG_INFO, "Failed to get response of called event"); /*E*/
        invokeAsyncCallBack(V5_ECODE_STOP_SHARED_RAW_FRAME,
                            V5_COMPONENT_VIDYO,
                            V5_VIDYO_CLIENT_IN_EVENT_STOP_WATCH_VIDEO_SOURCE_EVENT_REQUEST_FAILED_TO_SHARED_RAWFRAME);
    }
    
}

void V5ClientImpl::startWatchShared(const char * uri)
{
	VidyoClientRequestWindowShares shares = { static_cast<VidyoClientWindowShareRequestType>(0) };
	auto error = VidyoClient::getRemoteShares(&shares);
    if (error != VIDYO_CLIENT_ERROR_OK)
	{
		V5LOG(V5_LOG_INFO, "Failed to send a request for window shares, Error response %u", error); /*E*/
        invokeAsyncCallBack(V5_ECODE_START_WINDOW_SHARES,
                            V5_COMPONENT_VIDYO,
                            V5_VIDYO_CLIENT_REQUEST_GET_WINDOW_SHARES_EVENT_REQUEST_FAILED);
	}

	/* See if we have a share in a list. If we do set current share to a new one. */
	for (int i = 1 /* index starts from 1 */; i <= shares.shareList.numApp; i++) {
		if (strcmp(shares.shareList.remoteAppUri[i], uri) == 0) {
			shares.requestType = CHANGE_SHARING_WINDOW;
			shares.shareList.currApp = shares.shareList.newApp = i;
			error = VidyoClientSendRequest(VIDYO_CLIENT_REQUEST_SET_WINDOW_SHARES, static_cast<void*>(&shares), sizeof(VidyoClientRequestWindowShares));
			if (error != VIDYO_CLIENT_ERROR_OK) {
				V5LOG(V5_LOG_INFO, "Failed to send a request to change window share. Error response %u", error); /*E*/
                invokeAsyncCallBack(V5_ECODE_START_WINDOW_SHARES,
                                    V5_COMPONENT_VIDYO,
                                    V5_VIDYO_CLIENT_REQUEST_SET_WINDOW_SHARES_START_EVENT_REQUEST_FAILED);

				break;
			}
		}
	}
}

void V5ClientImpl::stopWatchShared(const char * uri)
{
	VidyoClientRequestWindowShares shares = { static_cast<VidyoClientWindowShareRequestType>(0) };
	shares.requestType = STOP_SHARING_WINDOW;
	VidyoUint error = VidyoClientSendRequest(VIDYO_CLIENT_REQUEST_SET_WINDOW_SHARES, static_cast<void*>(&shares), sizeof(VidyoClientRequestWindowShares));
	if (error != VIDYO_CLIENT_ERROR_OK) {
		V5LOG(V5_LOG_INFO, "Failed to send a request to stop window share. Error response %u", error); /*E*/
        invokeAsyncCallBack(V5_ECODE_STOP_WINDOW_SHARES,
                            V5_COMPONENT_VIDYO,
                            V5_VIDYO_CLIENT_REQUEST_SET_WINDOW_SHARES_STOP_EVENT_REQUEST_FAILED);
	}
}

void V5ClientImpl::onReceiveJoinConferenceRequest(const char *token, const char *V5LiteURL) {
    auto callback = eventSink.lock();
    if (callback) {
        callback->onReceiveJoinConferenceRequest(token, V5LiteURL);
    }
}

class HTTPDListener : public IV5HTTPDListener {
private:
	V5ClientImpl *client;

public:
	explicit HTTPDListener(V5ClientImpl *client_)
        : client(client_)
    {
		assert(client != nullptr);
	}
	~HTTPDListener() {}

	virtual V5State getStatus()
    {
        //return (client->isBusy()) ? Busy : Idle;
        return (client->isLogicStarted()) ? Busy : Idle;
	}

	// path「/api/joinConference/」に対するリクエスト時に呼び出される
	virtual bool joinConference(std::string volatleToken, std::string portalURL)
    {
		client->onReceiveJoinConferenceRequest(volatleToken.c_str(), portalURL.c_str());
		return true;
	}
};

bool V5ClientImpl::startHTTPD(unsigned short port) {
	if ( !httpd ) {
		httpd = serviceLocator->createHttpd(httpdListener);
	}

	httpd->HTTPDStart(port);
	return true;
}

bool V5ClientImpl::stopHTTPD() {
	httpd->HTTPDStop();
	return true;
}

V5ClientImpl::WindowAndDesktopsPtrT V5ClientImpl::getWindowsAndDesktops()
{
    VidyoClientRequestGetWindowsAndDesktops windowsAndDesktops = {0};

    if(VidyoClient::sendRequest(VIDYO_CLIENT_REQUEST_GET_WINDOWS_AND_DESKTOPS, windowsAndDesktops) != VIDYO_CLIENT_ERROR_OK)
    {
        V5LOG(V5_LOG_ERROR, "get windows and desktops failed.\n");
        invokeAsyncCallBack(V5_ECODE_WINDOW_AND_DESKTOP,
                            V5_COMPONENT_VIDYO,
                            V5_VIDYO_CLIENT_REQUEST_GET_WINDOWS_AND_DESKTOPS_EVENT_REQUEST_FAILED);
    }
    return std::move(std::make_shared<const V5WindowsAndDesktops>(windowsAndDesktops));
}

void V5ClientImpl::forceRendering(std::shared_ptr<std::string> const& pid)
{
#if !ENABLE_DYNAMIC_WATCH
    if(targetingPid != nullptr && *targetingPid == *pid) {
        return;
    }
    renderingTargetPidOnlyMode.store(true);
    std::atomic_store(&targetingPid, pid);
    std::string checkPid(*pid);
    {
        std::lock_guard<std::mutex> lock(activeParticipantUriListMtx);
        VidyoClient::stopDynamicWatchVideoSource();
        for(std::vector<std::string>::iterator ite = activeParticipantUriList->begin(); ite != activeParticipantUriList->end(); ++ite) {
            if((*ite).find(checkPid ,0) == std::string::npos) {
//                stopWatchVideoSource(*ite);
            }else {
                startWatchVideoSource(*ite);
            }
        }
    }
#endif
    return;
}

void V5ClientImpl::removeForceRendering()
{
#if !ENABLE_DYNAMIC_WATCH
    if(!renderingTargetPidOnlyMode.load()) {
        return;
    }
    renderingTargetPidOnlyMode.store(false);

    {
        std::lock_guard<std::mutex> lock(activeParticipantUriListMtx);
        for(std::vector<std::string>::iterator ite = activeParticipantUriList->begin(); ite != activeParticipantUriList->end(); ++ite) {
            if((*ite).find(*targetingPid ,0) != std::string::npos) {
                //stopWatchVideoSource(*ite);
            }
            //startWatchVideoSource(*ite);
        }
        VidyoClient::startDynamicWatchVideoSource(maxParticipants.load(), videoSize);
    }
    std::shared_ptr<std::string> sharedStr = std::make_shared<std::string>();
    std::atomic_store(&targetingPid, sharedStr);
#endif
};

/******************************/
/* Conference Event Callbacks */
/******************************/

void V5ClientImpl::onParticipantJoined(V5Participant const& participant)
{
    auto callback = eventSink.lock();
    if (callback)
    {
        callback->onParticipantJoined(participant);
    }
}

void V5ClientImpl::onParticipantLeaved(V5Participant const& participant)
{
    auto callback = eventSink.lock();
    if (callback)
    {
        callback->onParticipantLeaved(participant);
    }
}

void V5ClientImpl::onJoinedConferenceFail(V5ErrorInfo const& errInfo)
{
    {
        lock_guard l(mtx);
        confDisconnectedFlag.setSignal();
    }
    terminateConference(errInfo);
}

void V5ClientImpl::onSelectedParticipantsUpdated(VidyoClientRequestParticipants const& selected)
{
    VidyoClientRequestParticipants all = { 0 };
    if (VidyoClient::getParticipants(&all) != VIDYO_CLIENT_ERROR_OK)
    {
        // エラー条件を"会議入室中にVIDYO_CLIENT_REQUEST_GET_PARTICIPANTSイベント要求が失敗した場合"
        // という条件に変更。
        if( currentStatus.load() == IN_CONFERENCE )
        {
            invokeAsyncCallBack(V5_ECODE_CALLBACK_SELECTED_PARTICIPANTS,
                                V5_COMPONENT_VIDYO,
                                V5_VIDYO_CLIENT_REQUEST_GET_PARTICIPANTS_REQUEST_FAILED); // error case /*E*/
        }
    }

    std::set<std::string> selectedUriSet;
    std::vector<std::string> selectedUriVec;
    selectedUriVec.reserve(selected.numberParticipants);
    for (VidyoUint i = 0; i < selected.numberParticipants; i++)
    {
        std::string uri(selected.URI[i]);
        selectedUriVec.push_back(uri);
        selectedUriSet.insert(selected.URI[i]);
    }

#if !ENABLE_DYNAMIC_WATCH
    if (isRetrieveRawFrame)
    {
        for (VidyoUint i = 0; i < all.numberParticipants; i++)
        {
            // 1. add raw frame retrieval list.
            std::string uri(all.URI[i]);
            if (selectedUriSet.find(uri) != selectedUriSet.end())
            {

                if (!renderingTargetPidOnlyMode.load()) {
                    startWatchVideoSource(uri);
                }
            }
            else
            {
                if (!renderingTargetPidOnlyMode.load()) {
                    stopWatchVideoSource(uri);
                }
            }
        }
    }
#endif

    V5LOG(V5_LOG_INFO, "[NP1] number of participants (selected = %d).\n", selectedUriVec.size());
    V5LOG(V5_LOG_INFO, "[NP2] number of participants (all-p.   = %d).\n", all.numberParticipants);

    // event callback fires here.
    auto callback = eventSink.lock();
    if (callback)
        callback->onSelectedParticipantsUpdated(selectedUriVec);

    currentNumberOfParticipants = selectedUriVec.size();
    {
        std::lock_guard<std::mutex> lock(activeParticipantUriListMtx);
        activeParticipantUriList = std::make_shared<std::vector<std::string>>(selectedUriVec);
    }
}

void V5ClientImpl::notifyCurrentSelectedParticipants()
{
    VidyoClientRequestParticipants selected = { 0 };
    if (VidyoClient::getSelectedParticipants(&selected) != VIDYO_CLIENT_ERROR_OK)
    {
        // error callback should be here.
    }
    onSelectedParticipantsUpdated(selected);
}

void V5ClientImpl::notifyAllAddedShare()
{
    auto callback = eventSink.lock();
    if (!callback)
        return;

    VidyoClientRequestWindowShares shares = { static_cast<VidyoClientWindowShareRequestType>(0) };
    auto error = VidyoClient::getRemoteShares(&shares);
    if (error != VIDYO_CLIENT_ERROR_OK)
    {
        V5LOG(V5_LOG_INFO, "Failed to send a request for window shares, Error response %u", error); /*E*/
        invokeAsyncCallBack(V5_ECODE_START_WINDOW_SHARES,
                            V5_COMPONENT_VIDYO,
                            V5_VIDYO_CLIENT_REQUEST_GET_WINDOW_SHARES_EVENT_REQUEST_FAILED_TO_NOTIFY_ALL_ADDSHARE);
        return;
    }

    // remoteAppUri[0] が空であるため、配列全てをチェックする
    for (auto& uri: shares.shareList.remoteAppUri)
    {
        if (uri[0] != '\0')
            callback->onAddShare(uri, isUriSelf(uri));
    }

}

void V5ClientImpl::onJoinedConference(V5ConferenceInfo  const& conferenceInfo)
{
    V5LOG(V5_LOG_INFO, "%1.3lf[sec]: joined to conference.\n", joinConfTime.elapsed());
    
    if (!transitionStatus(CONNECTING_TO_CONFERENCE_SERVER, IN_CONFERENCE))
    {
        // 既に退室もしくは終了シーケンスに入っていると思われる
        // TODO: シーケンスを確認し、切断処理に移行するべき
        terminateConference(V5_ECODE_CALLBACK_CONFERENCE_ACTIVE,
                            V5_COMPONENT_VIDYO,
                            V5_INTERNAL_STATUS_CHANGE_FAILED_IN_CONFERENCE);
        return;
    }

    // recover client mute state.
    if (!VidyoClient::setAudioInMute(microphoneMuteState))
    {
        // 失敗
        invokeAsyncCallBack(V5_ECODE_CALLBACK_CONFERENCE_ACTIVE,
                            V5_COMPONENT_VIDYO,
                            V5_VIDYO_CLIENT_IN_EVENT_MUTE_AUDIO_IN_EVENT_RECOVER_FAILED);
    }
    if (!VidyoClient::setAudioOutMute(speakerMuteState))
    {
        // 失敗
        invokeAsyncCallBack(V5_ECODE_CALLBACK_CONFERENCE_ACTIVE,
                            V5_COMPONENT_VIDYO,
                            V5_VIDYO_CLIENT_IN_EVENT_MUTE_AUDIO_OUT_EVENT_RECOVER_FAILED);
    }
    if (!VidyoClient::setVideoMute(cameraMuteState))
    {
        // 失敗
        invokeAsyncCallBack(V5_ECODE_CALLBACK_CONFERENCE_ACTIVE,
                            V5_COMPONENT_VIDYO,
                            V5_VIDYO_CLIENT_IN_EVENT_MUTE_VIDEO_EVENT_RECOVER_FAILED);
    }
    if (!VidyoClient::setMaxParticipants(maxParticipants))
    {
        // iOS などでは必ず失敗する
    }
    
    auto callback = eventSink.lock();
    if (callback)
    {
        callback->onJoinedConference(conferenceInfo);
    }

    // invoke first selected participants updated callback with calculations.
    notifyCurrentSelectedParticipants();
    notifyAllAddedShare();

}

void V5ClientImpl::onLeavedConference(V5ErrorInfo  const& errInfo)
{
    {
        lock_guard l(mtx);
        confDisconnectedFlag.setSignal();
    }
    terminateConference(errInfo);
}

void V5ClientImpl::onParticipantListUpdated(const V5Participant * const participants[], unsigned long length)
{
    auto callback = eventSink.lock();
    if (callback)
    {
        callback->onParticipantListUpdated(participants, length);
    }
}

void V5ClientImpl::onConferenceStatusUpdated(V5ConferenceStatus const& conferenceStatus)
{
    auto callback = eventSink.lock();
    if (callback)
    {
        callback->onConferenceStatusUpdated(conferenceStatus);
    }
}

void V5ClientImpl::onReconnectConference(V5ConferenceInfo  const& conferenceInfo)
{
    auto callback = eventSink.lock();
    if (callback)
    {
        callback->onReconnectConference(conferenceInfo);
    }
}


void V5ClientImpl::onAsyncError(V5ErrorInfo const& errInfo)
{
    auto callback = eventSink.lock();
    if (callback)
    {
        callback->onAsyncError(errInfo);
    }
}

void V5ClientImpl::onInviteMailIsSent(V5ErrorInfo const& errInfo, const char* status, const char* mailAddress)
{
    auto callback = eventSink.lock();
    if (callback)
    {
        callback->onInviteMailIsSent(errInfo, status, mailAddress);
    }
}



void V5ClientImpl::onRcvRejectParticipant(V5Participant const& participant)
{
    auto callback = eventSink.lock();
    if (callback)
    {
        callback->onRcvRejectParticipant(participant);
    }
}

void V5ClientImpl::onPastTimeStart()
{
    auto callback = eventSink.lock();
    if (callback)
    {
        callback->onPastTimeStart();
    }
}

void V5ClientImpl::onPastTimeStop()
{
    auto callback = eventSink.lock();
    if (callback)
    {
        callback->onPastTimeStop();
    }
}

void V5ClientImpl::onConferenceNameChangeFailed( const char* noeConferenceName )
{
    auto callback = eventSink.lock();
    if (callback)
    {
        callback->onConferenceNameChangeFailed(noeConferenceName);
    }
}

void V5ClientImpl::onPeriodicStatisticsChanged(V5BigStatistics const& stat)
{
    
}

bool V5ClientImpl::transitionStatus(V5ClientStatus from, V5ClientStatus to)
{
    V5ClientStatus requried = from;
    bool succeeded;
    
    {
        lock_guard l(mtx);
        succeeded = currentStatus.compare_exchange_strong(from, to);
    }
    
    if (!succeeded)
    {
        V5LOG(V5_LOG_INFO,"failed Transition from:%d(required:%d), to:%d",from,requried,to);
        return false;
    }
    V5LOG(V5_LOG_INFO,"success Transition from:%d, to:%d",from,to);
    
    return true;
}

void V5ClientImpl::transitionStatus(V5ClientStatus to)
{
    V5ClientStatus from;
    {
        lock_guard l(mtx);
        from = currentStatus.exchange(to);
    }
    V5LOG(V5_LOG_INFO,"Transition from:%d, to:%d",from,to);
}

AsyncResult V5ClientImpl::terminateConference(V5ErrorCode code,
                                              V5Component component,
                                              V5ErrorReason reason,
                                              std::string const& status)
{
    V5ErrorInfo info;
    info.code_deprecated = code;
    info.category = component;
    info.reason = reason;
    info.status = status;
    return terminateConference(info);
}

AsyncResult V5ClientImpl::terminateConference(V5ErrorCode code, V5Component component, V5ErrorReason reason)
{
    V5ErrorInfo info;
    info.code_deprecated = code;
    info.category   = component;
    info.reason     = reason;
    return terminateConference(info);
}

AsyncResult V5ClientImpl::terminateConference(V5ErrorInfo const& reason)
{
    bool completeLeave = false;
    bool completeRejectJoin = false;
    AsyncResult result = AsyncResult::ASYNC_PENDING;
    V5ClientStatus lastStatus;

    // 能動・受動に関わらず、切断を完了するために呼ばれる
    // 以下を待機する
    // ・WebSocket の切断
    // ・VidyoClient の切断
    // ・VidyoClient の SIGNED_OUT
    {
        lock_guard l(mtx);

        result = disconnect();
        lastStatus = currentStatus.load();
        switch (lastStatus)
        {
            case CLIENT_IDLE:
            {
                break;
            }

            case START_CONFERENCE_SESSION:
            case CLIENT_LINKED_SESSION:
                V5LOG(V5_LOG_INFO,"Transition To TERMINATING\n");
                transitionStatus(TERMINATING);
                break;

            case PREPARE_JOIN:
            case SIGNIN_TO_MEDIA_SERVER:
            case CONNECTING_TO_MEDIA_SERVER:
            case CONNECTING_TO_CONFERENCE_SERVER:
            {
                terminateReason = reason;
                currentStatus = PROCESSING_REJECT_JOIN_CONFERENCE;
                if (result == AsyncResult::ASYNC_COMPLETE)
                {
                    V5LOG(V5_LOG_INFO,"Transition To TERMINATING\n");
                    transitionStatus(TERMINATING);
                    completeRejectJoin = true;
                }
                break;
            }

            case IN_CONFERENCE:
            {
                terminateReason = reason;
                currentStatus = PROCESSING_LEAVE_CONFERENCE;
                if (result == AsyncResult::ASYNC_COMPLETE)
                {
                    V5LOG(V5_LOG_INFO,"Transition To TERMINATING\n");
                    transitionStatus(TERMINATING);
                    completeLeave = true;
                }
                break;
            }

            case PROCESSING_REJECT_JOIN_CONFERENCE:
            {
                if (result == AsyncResult::ASYNC_COMPLETE)
                {
                    V5LOG(V5_LOG_INFO,"Transition To TERMINATING\n");
                    transitionStatus(TERMINATING);
                    completeRejectJoin = true;
                }
                break;
            }
            case PROCESSING_LEAVE_CONFERENCE:
            {
                if (result == AsyncResult::ASYNC_COMPLETE)
                {
                    V5LOG(V5_LOG_INFO,"Transition To TERMINATING\n");
                    transitionStatus(TERMINATING);
                    completeLeave = true;
                }
                break;
            }
            case TERMINATING:
            {
                break;
            }
        }
    }

    if (lastStatus != TERMINATING && currentStatus == TERMINATING)
    {
        this->joinConferenceTimer->stop();
        
        if( this->confEndTimeOverNoticeTimer )
        {
            // 会議終了時間超過タイマー処理終了
            this->confEndTimeOverNoticeTimer->stop();
            
            // 会議終了時間超過タイマー実行中フラグ
            confEndTimeOverTimerRunning = false;
        }
        
        lock_guard l(mtx);
        transitionStatus(CLIENT_IDLE);
    }

    auto callback = eventSink.lock();
    if (completeRejectJoin)
    {
        if (callback)
        {
            callback->onJoinConferenceFailed(terminateReason);
        }
    }
    else if (completeLeave)
    {
        if (callback)
        {
            callback->onLeavedConference(terminateReason);
        }
    }

    return result;
}

AsyncResult V5ClientImpl::disconnectCheck()
{
    lock_guard l(mtx);
    if (vidyoSignoutFlag.isSignal() &&
        confDisconnectedFlag.isSignal() &&
        vidyoIdleFlag.isSignal())
    {
        return ASYNC_COMPLETE;
    }
    return ASYNC_PENDING;
}

AsyncResult V5ClientImpl::disconnect()
{
    lock_guard l(mtx);
    
    VidyoClient::loginCancel();
    if (!vidyoSignoutFlag.isSignal())
        VidyoClient::signOff();
    VidyoClient::leave();
    conferenceClient->Close();
    
    if (vidyoSignoutFlag.isSignal() &&
        confDisconnectedFlag.isSignal() &&
        vidyoIdleFlag.isSignal())
    {
        return ASYNC_COMPLETE;
    }
    return ASYNC_PENDING;
}

CONFCNCT_INT V5ClientImpl::connectToConferenceServer(
    const std::string& uri,
    const std::string& TOKEN,
    const std::string& conferenceID,
    const std::string& FromPID,
    const std::string& DName)
{
    lock_guard l(mtx);
    confDisconnectedFlag.resetSignal();
    auto result = conferenceClient->Connect(uri, TOKEN, conferenceID, FromPID, DName);
    if (result != CCLIENT_OK)
    {
        confDisconnectedFlag.setSignal();
    }
    return result;
}

void V5ClientImpl::renderSelfView(bool render) {
    VidyoClientRequestConfiguration conf = {0};
    VidyoUint error;
    if ((error = VidyoClientSendRequest(VIDYO_CLIENT_REQUEST_GET_CONFIGURATION, &conf, sizeof(VidyoClientRequestConfiguration))) != VIDYO_CLIENT_ERROR_OK) {
        V5LOG(V5_LOG_ERROR, "Failed to request get configuration with error: %d\n", error);
    } else {
        conf.selfViewLoopbackPolicy = render ? 0 : 1;
        if ((error = VidyoClientSendRequest(VIDYO_CLIENT_REQUEST_SET_CONFIGURATION, &conf, sizeof(VidyoClientRequestConfiguration))) != VIDYO_CLIENT_ERROR_OK) {
            V5LOG(V5_LOG_ERROR, "Failed to request set configuration with error: %d\n", error);
        }
    }

    selfViewRenderingState.store(render);
}

void V5ClientImpl::setSelectDeviceReserve(unsigned int type, unsigned int index )
{
    selectDeviceReserve = true;
    
    if( type == V5_DEVICE_TYPE_VIDEO )
    {
        deviceIndexVideo = index;
    }
    
}

bool V5ClientImpl::isCameraMuteChangeState()
{
    return cameraMuteChangeMtoUM;
}

V5CODE V5ClientImpl::beginSession(const char *vcubeURL_, const char *token_, const char *version_ )
{
    //currentStatus = CLIENT_IDLE;        // コードが安定するまで IDLE にしてしまう
    if (!transitionStatus(CLIENT_IDLE, START_CONFERENCE_SESSION))
    {
        return V5Busy;
    }
    
#if 0
    V5LOG(V5_LOG_INFO, "vURL =%s\n", vcubeURL_);
    V5LOG(V5_LOG_INFO, "token=%s\n", token_);
    V5LOG(V5_LOG_INFO, "version=%s\n", version_);
#endif

    SharedString vcubeURL = std::make_shared<std::string>(vcubeURL_);
    SharedString volatileToken = std::make_shared<std::string>(token_);
    SharedString version = std::make_shared<std::string>(version_);
    beginSessionResult = std::async(std::launch::async, [this, vcubeURL, volatileToken, version]
    {
        assert(currentStatus == START_CONFERENCE_SESSION);
        
        V5ClientEnv env(vcubeURL->c_str(), proxySetting);

        std::string hashString(*volatileToken);
        for (int x = 0; x < V5_HASH_REPETITION_COUNT; x++) {
            hashString = getSHA1String(hashString);
        }
        
        V5StartSConferenceSession startConferenceSession( hashString , version->c_str() );
        
        vrms5::utils::timer sw;
        V5LOG(V5_LOG_INFO, "request startSession.\n");
        V5ClientHttpResult ret1 = env.send(startConferenceSession);
        V5LOG(V5_LOG_INFO, "response startSession (%1.3lf[sec]).\n", sw.elapsed());
        
        if( ret1 == V5_HTTP_REQUEST_SEND_NG )
        {
            V5LOG(V5_LOG_ERROR, "Start Conference Session HTTP Request Send failed.\n");
            return rejectBeginSession(V5_ECODE_JOIN_IMPL_START_CONFERENCE_SESSION,
                                      V5_COMPONENT_VCUBE_PORTAL,
                                      V5_PORTAL_START_CONFERENCE_SESSION_HTTP_REQUEST_SEND_NG);
        }
        
        if ( ret1 == V5_HTTP_RESPONSE_PARSE_NG)
        {
            V5LOG(V5_LOG_ERROR, "Start Conference Session http Response Data JsonParse Failed.\n");
            return rejectBeginSession(V5_ECODE_JOIN_IMPL_START_CONFERENCE_SESSION,
                                      V5_COMPONENT_VCUBE_PORTAL,
                                      V5_PORTAL_START_CONFERENCE_SESSION_HTTP_RESPONSE_DATA_PARSE_NG);
        }
        
        if ( !startConferenceSession.isParsed )
        {
            V5LOG(V5_LOG_ERROR, "Start Conference Session : failed (not filled with data).\n");
            return rejectBeginSession(V5_ECODE_JOIN_IMPL_START_CONFERENCE_SESSION,
                                      V5_COMPONENT_VCUBE_PORTAL,
                                      V5_PORTAL_START_CONFERENCE_SESSION_HTTP_RESPONSE_NOT_FILLED_WITH_DATA);
        }
        
        if ((startConferenceSession.result != "OK") || (startConferenceSession.status != "200")) {
            
            if( startConferenceSession.status == "308" )
            {
                // バージョンチェックのエラーは個別のコールバックであり
                // 条件が１つしかないため、エラーコードは作成しない。
                return VersionUnmatched(startConferenceSession.requireVersion.c_str());
            }
            
            if( startConferenceSession.status == "309" )
            {
                return NowMaintenance( startConferenceSession.maintenanceStartTime,
                                      startConferenceSession.maintenanceEndTime);
            }
            
            V5LOG(V5_LOG_ERROR, "Start Conference Session : response error. status(%s) \n", startConferenceSession.status.c_str());
            V5LOG(V5_LOG_ERROR, "Start Conference Session : response error. description(%s) \n", startConferenceSession.description.c_str());
            
            // 個別にエラーを判定していない(判定できない)場合は第4引数にエラーのStatusを通知するように変更
            return rejectBeginSession(V5_ECODE_JOIN_IMPL_START_CONFERENCE_SESSION,
                                      V5_COMPONENT_VCUBE_PORTAL,
                                      V5_PORTAL_START_CONFERENCE_SESSION_HTTP_RESPONSE_RESULT_NG,
                                      startConferenceSession.status);
        }
        
        this->sessionToken = startConferenceSession.token;
        this->vcubeURL = *vcubeURL;
        
        //ud->prepareJoin = std::make_shared<V5PrepareJoinConference>(this->token, displayName, password);
        if (!transitionStatus(START_CONFERENCE_SESSION, CLIENT_LINKED_SESSION))
        {
            // 既に退室もしくは終了シーケンスに入っていると思われる
            // TODO: シーケンスを確認し、切断処理に移行するべき

            // ここでの処理はステータス遷移に失敗した場合の処理であり、
            // rejectBeginSessionを起動してもステータスの遷移に失敗して
            // UI側にコントロールを戻せない。
            // そのため、rejectBeginSessionを起動せずに直接コールバック関数を起動し、
            // UI側にコントロールを戻す。
            
            auto sink = eventSink.lock();
            if (sink)
            {
                V5ErrorInfo info;
                info.code_deprecated = V5_ECODE_JOIN_IMPL_START_CONFERENCE_SESSION;
                info.category = V5_COMPONENT_VCUBE_PORTAL;
                info.reason = V5_INTERNAL_STATUS_CHANGE_FAILED_CLIENT_LINKED_SESSION;
                sink->onBeginSessionFailed(info);
            }
            return;
        }

        auto p = this->eventSink.lock();
        if (p != nullptr) {
            SharedString displayName = std::make_shared<std::string>(startConferenceSession.displayName);
            p->onSessionLinked(displayName->c_str(), startConferenceSession.logoUrl);
        }

    });
    //future.get();
    return V5OK;
}

void V5ClientImpl::rejectBeginSession(V5ErrorCode code,
                                      V5Component component,
                                      V5ErrorReason reason,
                                      std::string const& status)
{
    V5ErrorInfo info;
    info.code_deprecated = code;
    info.category = component;
    info.reason = reason;
    info.status = status;
    rejectBeginSession(info);
}


void V5ClientImpl::rejectBeginSession(V5ErrorCode code, V5Component component, V5ErrorReason reason)
{
    V5ErrorInfo info;
    info.code_deprecated = code;
    info.category = component;
    info.reason = reason;
    rejectBeginSession(info);
}

void V5ClientImpl::rejectBeginSession(V5ErrorInfo const& errInfo)
{
    if (!transitionStatus(START_CONFERENCE_SESSION, CLIENT_IDLE))
    {
        // 既に退室もしくは終了シーケンスに入っていると思われる
        // TODO: シーケンスを確認し、切断処理に移行するべき
        
        // 現在のステータスチェック
        auto nowStatus = currentStatus.load();
        if( ( nowStatus != CLIENT_IDLE ) &&
           ( nowStatus != PROCESSING_REJECT_JOIN_CONFERENCE )&&
           ( nowStatus != PROCESSING_LEAVE_CONFERENCE )&&
           ( nowStatus != TERMINATING ) )
        {
            // ステータス変更に失敗して復帰が望めないので、
            // アプリを終了するコールバックを起動する。
            libFatalError(V5_ECODE_JOIN_IMPL_REJECT_BIGEN_SESSION,
                          V5_COMPONENT_INTERNAL,
                          V5_FATAL_STATUS_CHANGE_FAILED_REJECT_BEGIN_SESSION);
            
            return;
        }
        
        // ステータス遷移に失敗した場合でもUI側にコントロールを戻す必要があるため、
        // 現在のステータスをチェックして退室処理中のステータスの場合は
        // 処理を継続してコールバック関数を起動する。
    }
    
    auto sink = eventSink.lock();
    if (sink)
    {
        sink->onBeginSessionFailed(errInfo);
    }
}

void V5ClientImpl::VersionUnmatched( const char *version )
{
    // UI側へコントロールを戻すために強制的にステータスを変更するメソッドを呼んでIDLEに戻したあと、
    // コールバックを起動する。
    if (!transitionStatus(START_CONFERENCE_SESSION, CLIENT_IDLE))
    {
        // 既に退室もしくは終了シーケンスに入っていると思われる
        // TODO: シーケンスを確認し、切断処理に移行するべき
        
        // 現在のステータスチェック
        auto nowStatus = currentStatus.load();
        if( ( nowStatus != CLIENT_IDLE ) &&
            ( nowStatus != PROCESSING_REJECT_JOIN_CONFERENCE )&&
            ( nowStatus != PROCESSING_LEAVE_CONFERENCE )&&
            ( nowStatus != TERMINATING ) )
        {
            // ステータス変更に失敗して復帰が望めないので、
            // アプリを終了するコールバックを起動する。
            libFatalError(V5_ECODE_JOIN_IMPL_REJECT_BIGEN_SESSION,
                          V5_COMPONENT_INTERNAL,
                          V5_FATAL_STATUS_CHANGE_FAILED_VERSION_UNMUCHED);
            
            return;
        }
        
        // ステータス遷移に失敗した場合でもUI側にコントロールを戻す必要があるため、
        // 現在のステータスをチェックして退室処理中のステータスの場合は
        // 処理を継続してコールバック関数を起動する。
    }
    
    auto sink = eventSink.lock();
    if (sink)
    {
        std::string requireVersion(version);
        sink->onVersionUnmatched(requireVersion.c_str());
    }
}

void V5ClientImpl::libFatalError( V5ErrorCode code, V5Component component, V5ErrorReason reason )
{
    auto sink = eventSink.lock();
    if (sink)
    {
        V5ErrorInfo info;
        info.code_deprecated = code;
        info.category = component;
        info.reason = reason;
        sink->onLibFatalError(info);
    }
}

void V5ClientImpl::NowMaintenance( std::string const& maintenanceStartTime, std::string const& maintenanceEndTime )
{
    auto sink = eventSink.lock();
    if (sink)
    {
        sink->onNowMaintenance( std::stoll( maintenanceStartTime ),
                               std::stoll( maintenanceEndTime ) );
    }

}

void V5ClientImpl::onJoinConferenceTimeout()
{    
    terminateConference(V5_ECODE_JOIN_CONFERENCE_TO,
                        V5_COMPONENT_TIMER,
                        V5_TIMER_EVENT_TYPE_JOIN_CONFERENCE_SEQUENCE_TIMER_TIMEOUT,
                        std::to_string(currentStatus.load()));
}

void V5ClientImpl::startWatchVideoSource(std::string &uri) {
    VidyoClientInEventStartWatchVideoSource startEvent = { 0 };
    startEvent.source.mediaType = VIDYO_CLIENT_MEDIA_CONTROL_TYPE_VIDEO;
    startEvent.source.sourceId = 0;
    startEvent.width = videoSize.width;
    startEvent.height = videoSize.height;
    startEvent.frameRate = videoSize.frameRate;
    startEvent.minFrameInterval = videoSize.minFrameInterval;
    startEvent.watchData = nullptr;

    auto* startEventSpecific = &startEvent;
    auto& specificInfo = vidyoParticipantSpecificInfos[uri];
    if (specificInfo && specificInfo->videoSorceSetting)
    {
        startEventSpecific = specificInfo->videoSorceSetting.get();
    }
    vrms5::utils::utf8cpy(startEvent.source.participantURI, uri);
    if (VidyoClient::sendEvent(VIDYO_CLIENT_IN_EVENT_START_WATCH_VIDEO_SOURCE, *startEventSpecific) == false)
    {
        V5LOG(V5_LOG_INFO, "Failed to get response of called event"); /*E*/
        invokeAsyncCallBack(V5_ECODE_CALLBACK_SELECTED_PARTICIPANTS,
                            V5_COMPONENT_VIDYO,
                            V5_VIDYO_CLIENT_IN_EVENT_START_WATCH_VIDEO_SOURCE_EVENT_REQUEST_FAILED_TO_VIDEO_SOURCE);
    }
}

void V5ClientImpl::stopWatchVideoSource(std::string &uri) {
    VidyoClientInEventStopWatchVideoSource stopEvent = { 0 };
    stopEvent.source.mediaType = VIDYO_CLIENT_MEDIA_CONTROL_TYPE_VIDEO;
    stopEvent.source.sourceId = 0;
    vrms5::utils::utf8cpy(stopEvent.source.participantURI, uri);
    if (VidyoClient::sendEvent(VIDYO_CLIENT_IN_EVENT_STOP_WATCH_VIDEO_SOURCE, stopEvent) == false)
    {
        invokeAsyncCallBack(V5_ECODE_CALLBACK_SELECTED_PARTICIPANTS,
                            V5_COMPONENT_VIDYO,
                            V5_VIDYO_CLIENT_IN_EVENT_STOP_WATCH_VIDEO_SOURCE_EVENT_REQUEST_FAILED_TO_VIDEO_SOURCE);
        V5LOG(V5_LOG_INFO, "Failed to get response of called event"); /*E*/
    }
}

unsigned int V5ClientImpl::getMicrophoneVolume()
{
    assert( isBusy() == true );
    
    VidyoClientRequestVolume volume = {0};
    auto error = VidyoClient::getMicrophoneVolume( &volume );
    if (error != VIDYO_CLIENT_ERROR_OK)
    {
        V5LOG(V5_LOG_ERROR, "Failed to get microphone volume.\n"); /*E*/
    }
    else
    {
        V5LOG(V5_LOG_ERROR, "Success to get microphone volume.\n"); /*E*/
    }
    
    
    return volume.volume;
}

void V5ClientImpl::setMicrophoneVolume( unsigned int volume )
{
    assert( isBusy() == true );

    VidyoClientRequestVolume eventVolume = {0};
    eventVolume.volume = volume;
    auto error = VidyoClient::setMicrophoneVolume( eventVolume );
    if (error != VIDYO_CLIENT_ERROR_OK)
    {
        V5LOG(V5_LOG_ERROR, "Failed to set microphone volume.\n"); /*E*/
    }
    else
    {
        V5LOG(V5_LOG_ERROR, "Success to set microphone volume.\n"); /*E*/

    }
}

unsigned int V5ClientImpl::getSpeakerVolume()
{
    assert( isBusy() == true );

    VidyoClientRequestVolume volume = {0};
    auto error = VidyoClient::getSpeakerVolume( &volume );
    if (error != VIDYO_CLIENT_ERROR_OK)
    {
        V5LOG(V5_LOG_ERROR, "Failed to get speaker volume.\n"); /*E*/
    }
    else
    {
        V5LOG(V5_LOG_ERROR, "Success to get speaker volume.\n"); /*E*/
    }
    
    return volume.volume;
}

void V5ClientImpl::setSpeakerVolume( unsigned int volume )
{
    assert( isBusy() == true );

    VidyoClientRequestVolume eventVolume = {0};
    eventVolume.volume = volume;
    auto error = VidyoClient::setSpeakerVolume( eventVolume );
    if (error != VIDYO_CLIENT_ERROR_OK)
    {
        V5LOG(V5_LOG_ERROR, "Failed to set speaker volume.\n"); /*E*/
    }
    else
    {
        V5LOG(V5_LOG_ERROR, "Success to set speaker volume.\n"); /*E*/
    }
}

void V5ClientImpl::setProxySetting(std::string const &address, long port, std::string const &id, std::string const &password)
{
    V5LOG(V5_LOG_INFO, "Webproxy address   : %s\n",address.c_str() );
    V5LOG(V5_LOG_INFO, "Webproxy port      : %d\n",port );
    V5LOG(V5_LOG_INFO, "Webproxy id        : %s\n",id.c_str() );
    V5LOG(V5_LOG_INFO, "Webproxy pw        : %s\n",password.c_str() );
    
    (*proxySetting).setProxyAddress(address);
    (*proxySetting).setProxyPort(port);

    //create env for urlEncode
    V5ClientEnv env("", proxySetting);

    if(id != "") {
        (*proxySetting).setProxyId(env.urlEncode(id));
    }
    if(password != "") {
        (*proxySetting).setProxyPassword(env.urlEncode(password));
    }
    
    conferenceClient->SetProxyInfo(address, port, env.urlEncode(id), env.urlEncode(password));
    
    proxyAddressChanged = true;
}

void V5ClientImpl::setVidyoConfiguration()
{
    if( (proxyForceChanged == false) &&
        (proxyAddressChanged == false) &&
        (proxyIEOptionChanged == false) )
    {
        return;
    }
    
    VidyoClientRequestConfiguration conf = { 0 };
    auto error = VidyoClient::getConfiguration(&conf);
    if (error != VIDYO_CLIENT_ERROR_OK)
    {
        // should call another callback
        V5LOG(V5_LOG_ERROR, "Failed to request configuration with error: %d\n", error); /*E*/
        //terminateConference(V5_ECODE_CALLBACK_LOGIC_STARTED, V5_COMPONENT_VIDYO, V5_VIDYO_GET_CONFIG_ERROR);
    }
    else
    {
        // VidyoProxy使用設定フラグは初期化もかねているため、強制的に設定する。
        if( proxyForce )
        {
            conf.proxySettings = PROXY_VIDYO_FORCE;
        }
        else
        {
            conf.proxySettings = 0;
        }
        
        auto tmpProxyIEOption = proxyIEOption.load();
        switch( tmpProxyIEOption )
        {
            case V5_PROXY_VIDYO_IE:
            {
                conf.proxySettings |= PROXY_WEB_IE;
                V5LOG(V5_LOG_INFO, "[VIDYO WEB PROXY SETTING] PROXY_WEB_IE On.\n");
                break;
            }
                
            case V5_PROXY_VISYO_IE_AUTO_DETECT:
            {
                conf.proxySettings |= PROXY_WEB_IE_AUTO_DETECT;
                V5LOG(V5_LOG_INFO, "[VIDYO WEB PROXY SETTING] PROXY_WEB_IE_AUTO_DETECT On.\n");
                break;
            }
                
            case V5_PROXY_VIDYO_IE_SCRIPT:
            {
                conf.proxySettings |= PROXY_WEB_IE_AUTO_CONFIG_SCRIPT;
                V5LOG(V5_LOG_ERROR, "[VIDYO WEB PROXY SETTING] PROXY_WEB_IE_AUTO_CONFIG_SCRIPT On.\n");

                break;
            }
            case V5_PROXY_VIDYO_IE_MANUAL:
            {
                conf.proxySettings |= PROXY_WEB_IE_MANUAL;
                V5LOG(V5_LOG_INFO, "[VIDYO WEB PROXY SETTING] PROXY_WEB_IE_MANUAL On.\n");
                break;
            }
                
            case V5_PROXY_VIDYO_NO_SETTING:
            default:
            {
                // 特定のIEオプションを指定されていないため、変更しない。
                V5LOG(V5_LOG_INFO, "[VIDYO WEB PROXY SETTING] V5_PROXY_VIDYO_NO_SETTING or default.\n");
                break;
            }
        }
        
        V5LOG(V5_LOG_ERROR, "conf.proxySettings  : %x\n",conf.proxySettings);
        
        if( proxyAddressChanged ||
            tmpProxyIEOption == V5_PROXY_VIDYO_IE_MANUAL )
        {
            V5LOG(V5_LOG_INFO, "[VIDYO WEB PROXY SETTING] Before.\n");
            V5LOG(V5_LOG_INFO, "conf.webProxyAddress  : %s\n",conf.webProxyAddress);
            V5LOG(V5_LOG_INFO, "conf.webProxyPort     : %s\n",conf.webProxyPort);
            V5LOG(V5_LOG_INFO, "conf.webProxyUsername : %s\n",conf.webProxyUsername);
            V5LOG(V5_LOG_INFO, "conf.webProxyPassword : %s\n",conf.webProxyPassword);
            
            // Proxy情報のみを設定してきた場合を想定して
            // PROXY_WEB_IE_MANUALも強制的に設定するべきか？
            // 今々は設定しないようにする。
            
            //conf.proxySettings |= PROXY_WEB_IE_MANUAL;
            
            utf8cpy(conf.webProxyAddress, *proxySetting->getProxyAddress());
            utf8cpy(conf.webProxyPort, std::to_string(proxySetting->getProxyPort()));
            utf8cpy(conf.webProxyUsername, *proxySetting->getProxyId());
            utf8cpy(conf.webProxyPassword, *proxySetting->getProxyPassword());
            
            V5LOG(V5_LOG_INFO, "[VIDYO WEB PROXY SETTING] After.\n");
            V5LOG(V5_LOG_INFO, "conf.webProxyAddress  : %s\n",conf.webProxyAddress);
            V5LOG(V5_LOG_INFO, "conf.webProxyPort     : %s\n",conf.webProxyPort);
            V5LOG(V5_LOG_INFO, "conf.webProxyUsername : %s\n",conf.webProxyUsername);
            V5LOG(V5_LOG_INFO, "conf.webProxyPassword : %s\n",conf.webProxyPassword);
        }
        else
        {
            V5LOG(V5_LOG_INFO, "[VIDYO WEB PROXY SETTING] Setting No Changed.\n");
        }
        
        error = VidyoClient::setConfiguration(conf);
        if (error != VIDYO_CLIENT_ERROR_OK)
        {
            // should call another callback
            //terminateConference(V5_ECODE_CALLBACK_LOGIC_STARTED, V5_COMPONENT_VIDYO, V5_VIDYO_SET_CONFIG_ERROR);
            V5LOG(V5_LOG_ERROR, "Failed to set configuration.\n"); /*E*/
        }
        else
        {
            V5LOG(V5_LOG_INFO, "Success to set configuration.\n");
            proxyForceChanged = false;
            proxyAddressChanged = false;
            proxyIEOptionChanged = false;
        }
    }
}




void V5ClientImpl::setVideoPreferences(V5VideoPreferences preferences) {
    videoPreferences = preferences;
    (getDeviceConfiguration())->setVideoPreferences(preferences);
}

V5VideoPreferences V5ClientImpl::getVideoPreferences() {
    return (getDeviceConfiguration())->getVideoPreferences();
}

void V5ClientImpl::setProxyForce( bool force )
{
    proxyForce = force;
    proxyForceChanged = true;
}

void V5ClientImpl::setProxyIEOption( V5ProxyVidyoIEType option )
{
    proxyIEOption = option;
    proxyIEOptionChanged = true;
}


void V5ClientImpl::setSelfDisplayLabel(const char *aLavel)
{
    VidyoClientRequestSetLabel displayLabel;
    memset(&displayLabel, 0, sizeof(displayLabel));
    vrms5::utils::utf8cpy(displayLabel.labelText, aLavel);
    displayLabel.labelID = VIDYO_CLIENT_LABEL_SELF_VIEW;
     VidyoUint error;
    if ((error = VidyoClientSendRequest(VIDYO_CLIENT_REQUEST_SET_LABEL, &displayLabel, sizeof(VidyoClientRequestSetLabel))) != VIDYO_CLIENT_ERROR_OK) {
        V5LOG(V5_LOG_ERROR, "Failed to change self display label with error: %d\n", error);
    }
}

bool V5ClientImpl::isUriSelf(const char * uri) const
{
    // ':' と ';' で囲まれる文字列を検索する（ただし、 ; 以下は省略されていても検出する）
    static const std::regex rx("[^:]+:([^;]+)(;.*)?");
    
    std::cmatch match;
    return (std::regex_match(uri, match, rx) && match[1] == ud->PID);
}

void V5ClientImpl::startStatisticsInfo( int millisecs )
{
    this->statInfo->startStatisticsInfo( millisecs );
}

void V5ClientImpl::startStatisticsInfo()
{
    V5LOG(V5_LOG_ERROR, "startStatisticsInfo Called");
    this->statInfo->startStatisticsInfo();
}

void V5ClientImpl::stopStatisticsInfo()
{
    V5LOG(V5_LOG_ERROR, "stopStatisticsInfo Called");
    this->statInfo->stopStatisticsInfo();
}

bool V5ClientImpl::setSendMaxKBPS(int kbps)
{
    auto status = currentStatus.load();
    if( ( status != IN_CONFERENCE ) &&
        ( status != CONNECTING_TO_CONFERENCE_SERVER ) )
    {
        V5LOG(V5_LOG_ERROR, "Reserved to SendMaxKBPS");
        sendMaxKbps = kbps;
        sendMaxKbpsReserve = true;
        return true;
    }
    
    if( VidyoClient::setSendMaxKBPS(kbps) != VIDYO_CLIENT_ERROR_OK )
    {
        V5LOG(V5_LOG_ERROR, "Failed to setSendMaxKBPS");
        return false;
    }
    
    sendMaxKbps.store(kbps);
    
    V5LOG(V5_LOG_ERROR, "Success to setSendMaxKBPS");
    V5LOG(V5_LOG_ERROR, "kbps : %d",kbps);
    return true;
}

int V5ClientImpl::getSendMaxKBPS()
{
    int maxkbps = 0;
    auto status = currentStatus.load();
    if( ( status != IN_CONFERENCE ) &&
        ( status != CONNECTING_TO_CONFERENCE_SERVER ) )
    {
        maxkbps = sendMaxKbps.load();
    }
    else
    {
        VidyoClient::getSendMaxKBPS(&maxkbps);
    }
    
    V5LOG(V5_LOG_ERROR, "Success to getSendMaxKBPS");
    return maxkbps;
}

bool V5ClientImpl::setRecvMaxKBPS(int kbps)
{
    auto status = currentStatus.load();
    if( ( status != IN_CONFERENCE ) &&
        ( status != CONNECTING_TO_CONFERENCE_SERVER ) )
    {
        V5LOG(V5_LOG_ERROR, "Reserved to RecvMaxKBPS");
        recvMaxKbps = kbps;
        recvMaxKbpsReserve = true;
        return true;
    }

    if( VidyoClient::setRecvMaxKBPS(kbps) != VIDYO_CLIENT_ERROR_OK )
    {
        V5LOG(V5_LOG_ERROR, "Failed to setSendMaxKBPS");
        return false;
    }
    
    recvMaxKbps.store(kbps);
    
    V5LOG(V5_LOG_ERROR, "Success to setRecvMaxKBPS");
    V5LOG(V5_LOG_ERROR, "kbps : %d",kbps);
    return true;
}

int V5ClientImpl::getRecvMaxKBPS()
{
    int maxkbps = 0;
    
    auto status = currentStatus.load();
    if( ( status != IN_CONFERENCE ) &&
        ( status != CONNECTING_TO_CONFERENCE_SERVER ) )
    {
        maxkbps = recvMaxKbps.load();
    }
    else
    {
        VidyoClient::getRecvMaxKBPS(&maxkbps);
    }
    V5LOG(V5_LOG_ERROR, "Success to getRecvMaxKBPS");
    return maxkbps;
}

bool V5ClientImpl::setSendBandWidth( V5SLayer layer, V5SendBandWidth bandWidth )
{
    if( VidyoClient::setBandWidth( (unsigned int)layer, (int)bandWidth ) == false )
    {
        V5LOG(V5_LOG_ERROR, "Failed to setSendBandWidth");
        return false;
    }
    
    V5LOG(V5_LOG_ERROR, "Success to setSendBandWidth");
    V5LOG(V5_LOG_ERROR, "sLayers    : %d",layer);
    V5LOG(V5_LOG_ERROR, "bandwidth  : %d",bandWidth);
    
    return true;
}

void V5ClientImpl::reservedParameterSettings()
{
    if( showActiveSpeakerReserve )
    {
        VidyoClientInEventLayout layout = {0};
        layout.numPreferred = nextShowActiveSpeaker;
        if(VidyoClientSendEvent(VIDYO_CLIENT_IN_EVENT_LAYOUT, &layout, sizeof(VidyoClientInEventLayout)) != VIDYO_CLIENT_ERROR_OK )
        {
            V5LOG(V5_LOG_ERROR, "set ActiveSpeaker Event Call failed.\n"); /*E*/
        }
        else
        {
            showActiveSpeakerReserve = false;
        }
    }
    
    if( selfPreviewModeReserve )
    {
        VidyoClientInEventPreview mode;
        memset(&mode, 0, sizeof(mode));
        switch (nextPreviewMode.load()) {
            case  V5_CLIENT_PREVIEW_MODE_NONE:
                mode.previewMode = VIDYO_CLIENT_PREVIEW_MODE_NONE;
                break;
            case V5_CLIENT_PREVIEW_MODE_PIP:
                mode.previewMode = VIDYO_CLIENT_PREVIEW_MODE_PIP;
                break;
            case V5_CLIENT_PREVIEW_MODE_DOCK:
                mode.previewMode = VIDYO_CLIENT_PREVIEW_MODE_DOCK;
                break;
            case V5_CLIENT_PREVIEW_MODE_OTHER:
                mode.previewMode = VIDYO_CLIENT_PREVIEW_MODE_OTHER;
                break;
            default:
                // イリーガルパラメータはないのでここには来ないはずだが、
                // 暫定的にVIDYO_CLIENT_PREVIEW_MODE_OTHERを設定
                mode.previewMode = VIDYO_CLIENT_PREVIEW_MODE_OTHER;
                break;
        }
        
        if(VidyoClientSendEvent(VIDYO_CLIENT_IN_EVENT_PREVIEW, &mode,sizeof(VidyoClientInEventPreview)) != VIDYO_CLIENT_ERROR_OK )
        {
            V5LOG(V5_LOG_ERROR, "set selfPreviewMode Event Call failed.\n"); /*E*/
        }
        else
        {
            selfPreviewModeReserve = false;
        }
    }
    
    if( sendMaxKbpsReserve )
    {
        if( VidyoClient::setSendMaxKBPS(sendMaxKbps.load()) != VIDYO_CLIENT_ERROR_OK )
        {
            V5LOG(V5_LOG_ERROR, "Failed to setSendMaxKBPS in reservedParameterSettings");
        }
        else
        {
            V5LOG(V5_LOG_INFO, "Success to setSendMaxKBPS in reservedParameterSettings");
        }
        
        sendMaxKbpsReserve = false;
    }
    
    if( recvMaxKbpsReserve )
    {
        if( VidyoClient::setRecvMaxKBPS(recvMaxKbps.load()) != VIDYO_CLIENT_ERROR_OK )
        {
            V5LOG(V5_LOG_ERROR, "Failed to setRecvMaxKBPS in reservedParameterSettings");
        }
        else
        {
            V5LOG(V5_LOG_INFO, "Success to setRecvMaxKBPS in reservedParameterSettings");
        }
        recvMaxKbpsReserve = false;
    }
    
    return;
}

void V5ClientImpl::onSetOverTimeCountTimer( unsigned long remainTime )
{
    V5LOG(V5_LOG_INFO, "onSetOverTimeCountTimer in c++");

    // 会議終了時間超過通知フラグ判定
    if( confEndTimeOverTimerRunning )
    {
        return;
    }
    
    // 会議終了時間超過通知フラグ設定
    confEndTimeOverTimerRunning = true;
    
    unsigned long firstWaitTimeSpan = defaultNoticeTimeSpan.load();
    
    if( remainTime )
    {
        // 会議終了時間計算 + 判定
        // 超過時間(秒) / 300秒(5分) * 5
        // = 5分間隔での超過回数 / 5(分刻み) = 超過時間(5分の倍数)
        long long elapsedTime = (remainTime / 300) * 5;
        if( elapsedTime > 0 )
        {
            V5LOG(V5_LOG_INFO, "Login Timing ConferenceEndTime Over!");
            V5LOG(V5_LOG_INFO, "elapsedTime : %lld",elapsedTime);

            // 経過時間が5分以上の場合は即通知
            auto sink = eventSink.lock();
            if (sink)
            {
                sink->onConferenceEndTimeOverNotice( elapsedTime );
            }
        }
        
        // 初回タイマー待ち時間計算
        // 次の通知までの残り秒数算出後、ミリ秒に変換
        firstWaitTimeSpan = (300 - (remainTime % 300)) * 1000;
    }
    
    V5LOG(V5_LOG_INFO, "firstWaitTimeSpan : %lld",firstWaitTimeSpan);
    
    // タイマー処理生成
    confEndTimeOverNoticeTimer = std::make_shared<Timer>();
    
    /////////////////////////////////////////////////////////////////////////////////////
    // 会議入室のタイミングですでに会議終了時間を超過していた場合、
    // 通知時の計算で5分のずれが発生するため、取得した時間から会議超過時間分遡った時間を保持しておく
    /////////////////////////////////////////////////////////////////////////////////////
    
    // タイマー開始時間取得
    auto nowTime =
    std::chrono::duration_cast<std::chrono::milliseconds>( steady_clock::now().time_since_epoch() ).count();
    confEndTimeOverNoticeStart = nowTime - (remainTime * 1000);
    
    V5LOG(V5_LOG_INFO, "confEndTimeOverNoticeStart : %lld",confEndTimeOverNoticeStart.load());

    // 会議終了時間超過通知タイマー起動(1ループ)
    confEndTimeOverNoticeTimer->start( firstWaitTimeSpan,
                                      std::bind(&V5ClientImpl::onConferenceEndTimeOverNotice, this));
    
}

void V5ClientImpl::onCancelOverTimeCountTimer()
{
    // 会議終了時間超過通知タイマー停止
    confEndTimeOverNoticeTimer->stop();
    
    // 会議終了時間超過通知フラグ設定
    confEndTimeOverTimerRunning = false;
    
}

void V5ClientImpl::onConferenceEndTimeOverNotice()
{
    V5LOG(V5_LOG_INFO, "onConferenceEndTimeOverNotice in c++");
    
    // 会議終了時間タイマー起動開始からの経過時間より通知時間計算
    long long nowTime = std::chrono::duration_cast<std::chrono::milliseconds>( steady_clock::now().time_since_epoch() ).count();
    // タイマー待ち時間計算
    auto waitTImeSpan = (nowTime - confEndTimeOverNoticeStart) % defaultNoticeTimeSpan.load();
    waitTImeSpan = defaultNoticeTimeSpan.load() - waitTImeSpan;
    
    // 確認用処理
    //    waitTImeSpan = defaultNoticeTimeSpan.load();
    
    V5LOG(V5_LOG_INFO, "resetting TimerSpan : %lld",waitTImeSpan);
    
    // 会議終了時間超過通知タイマー起動(1回指定起動)
    confEndTimeOverNoticeTimer->resetting( waitTImeSpan,
                                          std::bind(&V5ClientImpl::onConferenceEndTimeOverNotice, this));
    
    long long elapsedTime = (nowTime - confEndTimeOverNoticeStart);
    elapsedTime = (elapsedTime / defaultNoticeTimeSpan) * 5;
    
    auto sink = eventSink.lock();
    if (sink)
    {
        sink->onConferenceEndTimeOverNotice( elapsedTime );
    }
    
}

