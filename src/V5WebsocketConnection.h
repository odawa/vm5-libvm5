﻿//
//  V5WebsocketConnection.h
//  LibV5Lite
//

#ifndef LibV5Lite_V5WebsocketConnection_h
#define LibV5Lite_V5WebsocketConnection_h

#include <V5Lite/LibV5LiteCommon.h>

#ifdef V5_CLANG
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wconversion"
#endif

#include <websocketpp/config/asio_no_tls_client.hpp>
#include <websocketpp/config/asio_client.hpp>
#include <websocketpp/client.hpp>

#include <websocketpp/common/thread.hpp>
#include <websocketpp/common/memory.hpp>

#ifdef V5_CLANG
#pragma clang diagnostic pop
#endif

#include <picojson/picojson.h>

#include <V5Lite/IV5ConferenceEventSink.h>
#include <V5Lite/IV5ChatEventSink.h>
#include "V5LibEventSink.h"
#include <V5Lite/V5DataTypes.h>
#include <V5Lite/V5ErrorReason.h>

#include "utils.h"

#include "V5Log.h"

#include <cstdlib>
#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <mutex>
#include <list>
#include <map>
#include <stdlib.h>
#include <atomic>
#include <stdarg.h>


/* DEBUG FUNCTION define */
#define WEBSOCKET_FUNC_DEBUG 1

// Connecting Words
#define CONNECT_URI_FIRST_WORD_WS "ws://"
#define CONNECT_URI_FIRST_WORD_WSS "wss://"


// CHAT SEND EVENT NAME
#define CHAT_EV_SND_PONG        "Pong"
#define CHAT_EV_SND_SUBSCRIBE   "Subscribe"
#define CHAT_EV_SND_CHATHIST    "ChatHistory"
#define CHAT_EV_SND_SNDMSG      "SendMessage"
#define CHAT_EV_SND_RMVFMCONF   "RemoveFromConference"
#define CHAT_EV_SND_REJCTPTCPNT "RejectParticipant"
#define CHAT_EV_SND_LOCKCONF    "LockConference"
#define CHAT_EV_SND_UPDATECNAME "UpdateConferenceName"


// CHAT RECV EVENT NAME
#define CHAT_EV_RCV_PING        "Ping"
#define CHAT_EV_RCV_PJOIN       "ParticipantJoin"
#define CHAT_EV_RCV_PLEAVE      "ParticipantLeave"
#define CHAT_EV_RCV_ESTABLISHED "Established"
#define CHAT_EV_RCV_SNDMSG      "SendMessage"
#define CHAT_EV_RCV_CLSFMSRV    "CloseFromServer"
#define CHAT_EV_RCV_CONFSTSUPD  "ConferenceStatusUpdate"
#define CHAT_EV_RCV_REJCTPTCPNT "RejectParticipant"
#define CHAT_EV_RCV_METHODNALLW "MethodNotAllowed"
#define CHAT_EV_RCV_NOTIFFAIL   "NotificationFailed"


// DEBUG SPETIAL MESSAGE(Send)
#define CHAT_EV_SND_DEBUG       "Debug"
#define CHAT_EV_SND_CRASH       "Crash"

// DEBUG SPETIAL MESSAGE(Recieve)
#define CHAT_EV_RCV_DEBUG       "Debug"


/* ALL USER DESTINATION */
#define CLIENT_DEST_ALL     "all"



using boost::asio::ssl::context;

using websocketpp::lib::bind;
using websocketpp::lib::shared_ptr;
using websocketpp::lib::thread;
using websocketpp::connection_hdl;
using websocketpp::lib::error_code;
using websocketpp::lib::make_shared;

struct ParticipantCluster;
struct ParticipantListStorage;
struct ChatLogCluster;
struct ChatLogStorage;
struct ConferenceStatusStorage;
struct ProxyStorage;


typedef std::shared_ptr<const V5Participant> ParticipantPtrT;
typedef std::shared_ptr<const ParticipantCluster> ParticipantClusterPtr;    // コールバック呼び出し中の破壊的変更がないことを保障するため、自身も常に const として保持する
typedef std::shared_ptr<ParticipantListStorage> ParticipantListStoragePtr;
typedef std::shared_ptr<const ChatLogCluster> ChatLogClusterPtrT;           // コールバック呼び出し中の破壊的変更がないことを保障するため、自身も常に const として保持する
typedef std::shared_ptr<ChatLogStorage> ChatLogStoragePtrT;
typedef std::shared_ptr<const ConferenceStatusStorage> ConfClusterPtr;      // Getter 利用中の破壊的変更がないことを保障するため、自身も常に const として保持する
typedef std::shared_ptr<const V5ConferenceStatus> V5ConferenceStatusPtrT;
typedef std::shared_ptr<ProxyStorage> ProxyStoragePtr;


struct ParticipantCluster
    : public V5Participant
{
    ParticipantCluster();
    ParticipantCluster(ParticipantCluster const&) = delete; // 禁止ではなく、未実装
    ParticipantCluster(ParticipantCluster&&) = delete; // 禁止ではなく、未実装（おそらく default でも動作する）
    explicit ParticipantCluster(picojson::value const& json);

    ParticipantCluster const& operator=(ParticipantCluster const&) = delete; // 禁止ではなく、未実装
    ParticipantCluster const& operator=(ParticipantCluster&&) = delete; // 禁止ではなく、未実装（おそらく default でも動作する）
    explicit ParticipantCluster(std::string const& pid, std::string const& displayName );
    
    std::string const& getID() const { return participantIDStorage; }
private:
    std::string         displayNameStorage;
    std::string         participantIDStorage;
    friend std::ostream& operator<<(std::ostream& os, const ParticipantCluster& cluster);
};

struct ParticipantListStorage
{
    ParticipantListStorage(ParticipantListStorage const&) = delete; // 禁止ではなく、未実装
    ParticipantListStorage(ParticipantListStorage&&) = delete; // 禁止ではなく、未実装（おそらく default でも動作する）
    explicit ParticipantListStorage(picojson::value const& json, std::string const& selfPID);
    
    ParticipantListStorage const& operator=(ParticipantListStorage const&) = delete; // 禁止ではなく、未実装
    ParticipantListStorage const& operator=(ParticipantListStorage&&) = delete; // 禁止ではなく、未実装（おそらく default でも動作する）

    std::size_t count() const { return participantList.size(); }
    V5Participant const* const* getList() const { return participantList.data(); }
    V5Participant const* getSelf() const { return self.get(); }
    V5Participant const* find(std::string const& id) const {
        auto ite = participantMap.find(id);
        return (ite != participantMap.end()) ? ite->second.get() : nullptr;
    }
    ParticipantClusterPtr add(picojson::value const& json);
    ParticipantClusterPtr remove(picojson::value const& json);
    ParticipantClusterPtr const& getSelfCluster() const { return self; }

private:
    ParticipantClusterPtr self;
    std::vector<const V5Participant*> participantList;                      // self は入っていない
    std::map<std::string, ParticipantClusterPtr> participantMap;            // self も入っている
    friend std::ostream& operator<<(std::ostream& os, const ParticipantListStorage& cluster);
};


struct ChatLogCluster
    : public V5ChatMessage
{
    ChatLogCluster(ChatLogCluster const&) = delete; // 禁止ではなく、未実装
    ChatLogCluster(ChatLogCluster&&) = delete; // 禁止ではなく、未実装（おそらく default でも動作する）
    explicit ChatLogCluster(picojson::value const& json);

    ChatLogCluster const& operator=(ChatLogCluster const&) = delete; // 禁止ではなく、未実装
    ChatLogCluster const& operator=(ChatLogCluster&&) = delete; // 禁止ではなく、未実装（おそらく default でも動作する）
private:
    std::string     textStorage;
    std::string     fromStorage;
    std::string     displayNameStorage;
    friend std::ostream& operator<<(std::ostream& os, const ChatLogCluster& cluster);
};

struct ChatLogStorage
{
    ChatLogStorage(ChatLogStorage const&) = delete; // 禁止ではなく、未実装
    ChatLogStorage(ChatLogStorage&&) = delete; // 禁止ではなく、未実装（おそらく default でも動作する）
    explicit ChatLogStorage(picojson::value const& json, std::string const& selfPID);

    ChatLogStorage const& operator=(ChatLogStorage const&) = delete; // 禁止ではなく、未実装
    ChatLogStorage const& operator=(ChatLogStorage&&) = delete; // 禁止ではなく、未実装（おそらく default でも動作する）

    ChatLogClusterPtrT addMessage(picojson::value const& json, std::string const& selfPID);
    auto getList() const -> std::vector<const V5ChatMessage*> const& {
        return messageList;
    }

private:
    std::vector<const V5ChatMessage*> messageList;
    std::list<ChatLogClusterPtrT> clusterStorage;
    friend std::ostream& operator<<(std::ostream& os, const ChatLogStorage& cluster);
};

struct ConferenceStatusStorage
    : public V5ConferenceStatus
{
    ConferenceStatusStorage(ConferenceStatusStorage const& rhs);
    ConferenceStatusStorage(ConferenceStatusStorage&&) = delete; // 禁止ではなく、未実装（おそらく default でも動作する）
    explicit ConferenceStatusStorage(picojson::value const& json);

    ConferenceStatusStorage const& operator=(ConferenceStatusStorage const&) = delete; // 禁止ではなく、未実装
    ConferenceStatusStorage const& operator=(ConferenceStatusStorage&&) = delete; // 禁止ではなく、未実装（おそらく default でも動作する）

    void update(picojson::value const& json);

    std::string getConferenceName(){ return conferenceNameStorage; };
    
    bool getConfEndTimeCountFlg() const;
    
private:
    // 最新の会議情報パラメータ
    std::string conferenceNameStorage;
    std::string roomNameStorage;
    std::string conferenceURLStorage;
    std::string conferencePinCodeStorage;
    std::string documentSenderStorage;
    
    // 変更前パラメータ
    std::string oldConferenceNameStorage;
    std::string oldDocumentSenderStorage;
    
    // 会議の故理事官測定開始フラグ
    bool confEndTimeCountFlg;
    
    friend std::ostream& operator<<(std::ostream& os, const ConferenceStatusStorage& cluster);
};

struct ProxyStorage
{
    ProxyStorage();
    ProxyStorage(ProxyStorage const&) = delete; // 禁止ではなく、未実装
    ProxyStorage(ProxyStorage&&) = delete; // 禁止ではなく、未実装（おそらく default でも動作する）
    explicit ProxyStorage(std::string const& address,
                          long const& port,
                          std::string const& id,
                          std::string const& pw);
    
    ProxyStorage const& operator=(ProxyStorage const&) = delete; // 禁止ではなく、未実装
    ProxyStorage const& operator=(ProxyStorage&&) = delete; // 禁止ではなく、未実装（おそらく default でも動作する）
    
    void update(std::string const& address,
                long const& port,
                std::string const& id,
                std::string const& pw);
    void erase();
    
    bool isProxy() const { return isProxySet; }
    std::string const& getAddress() const { return proxyAddress; }
    std::string const& getPort() const { return proxyPort; }
    std::string const& getID() const { return proxyID; }
    std::string const& getPassword() const { return proxyPW; }
    
private:
    std::atomic_bool    isProxySet;
    std::string         proxyAddress;
    std::string         proxyPort;
    std::string         proxyID;
    std::string         proxyPW;
    
    friend std::ostream& operator<<(std::ostream& os, const ProxyStorage& Proxy);
};


typedef websocketpp::lib::shared_ptr<boost::asio::deadline_timer> timer_ptr;

/* Websocket Typedef */
typedef websocketpp::client<websocketpp::config::asio_client> client;
typedef websocketpp::client<websocketpp::config::asio_tls_client> client_tls;
typedef websocketpp::lib::shared_ptr<boost::asio::ssl::context> context_ptr;
typedef websocketpp::config::asio_tls_client::message_type::ptr message_ptr;

typedef std::shared_ptr<V5ErrorInfo> V5ErrorInfoPtr;

typedef enum {
    CCLIENT_CON_WORDS_INIT = 0,
    CCLIENT_CON_WORDS_WS,
    CCLIENT_CON_WORDS_WSS
} CONWS_TYPE;

typedef enum {
    CCLIENT_OK                  =  0,
    CCLIENT_CON_NON_PRM         ,
    CCLIENT_CON_CONNECTED       ,
    CCLIENT_CON_WORD_NG         ,
    CCLIENT_CON_CONNECT_NG      ,
    CCLIENT_CON_PXY_URI_SET_NG  ,
    CCLIENT_CON_PXY_AUTH_SET_NG ,
    
    CCLIENT_SUB_NON_CONNECT     ,
    CCLIENT_SUB_NON_PRM         ,
    CCLIENT_SUB_SEND_ERR        ,
        
    CCLIENT_MSG_NON_PRM         ,
    CCLIENT_MSG_NON_CONNECT     ,
    CCLIENT_MSG_SEND_STS_NG     ,
    CCLIENT_MSG_SEND_ERR        ,
    
    CCLIENT_RCN_CONNECT_ERR     ,
    
    CCLIENT_CLS_NON_CONNECT     ,
    CCLIENT_CLS_SEND_STS_NG     ,
    CCLIENT_CLS_SEND_ERR        ,
    CCLIENT_CLS_RESERVE         ,
    
    CCLIENT_REJ_NON_CONNECT     ,
    CCLIENT_REJ_NON_PRM         ,
    CCLIENT_REJ_SEND_STS_NG     ,
    CCLIENT_REJ_SEND_ERR        ,

    CCLIENT_LOC_NON_CONNECT     ,
    CCLIENT_LOC_SEND_STS_NG     ,
    CCLIENT_LOC_SEND_ERR        ,
    
    CCLIENT_UCN_NON_CONNECT     ,
    CCLIENT_UCN_SEND_STS_NG     ,
    CCLIENT_UCN_SEND_ERR        ,
    
    CCLIENT_ILLEGAL_ERROR   = 999,
    
} CONFCNCT_INT ;

typedef enum {
    INITIALIZE              = 0,
    CONNECTING              ,
    EST_RCV_STANDBY         ,
    EST_TIMEOUT             ,
    SENDMSG_OK              ,
    RECONNECTING            ,
    RECON_EST_RCV_STANDBY   ,
    RECON_EST_TIMEOUT       ,
    CLOSING                 ,
    
} CONNECT_STS ;

typedef enum {
    DEBUG_EV_PLIST      = 0,
    DEBUG_EV_CHATLOG    ,
    DEBUG_EV_CONFSTS    ,
    
} V5DEBUG_EVENT;

typedef enum {
    WEBSOCKET_TO_EV_DEFAULT         = 0,
    WEBSOCKET_TO_EV_ESTABLISHED     = 100,
    WEBSOCKET_TO_EV_CLOSEFROMSERVER = 200,
    
} V5TIMEOUT_EVENT_LIST;

enum {
    ESTABLISHED_RCV_WAITTIME     = 1000,
    CLOSEFROMSERVER_RCV_WAITTIME = 1000,
};

typedef enum {
    CCLIENT_PARSE_DEBUG = 0,
    CCLIENT_PARSE_PRODUCT,
    
} CONFCLIENT_PARSE_EV;

//typedef enum {
//    WEBSOCKET_LOG_OUTPUT_LV = 1,
//    WEBSOCKET_LOG_NON_OUT   = -1,
//    WEBSOCKET_LOG_INFO      = 0,
//    WEBSOCKET_LOG_WARNING   ,
//    WEBSOCKET_LOG_ERROR     ,
//    WEBSOCKET_LOG_FATAL     ,
//} WEBSOCKET_LOG_LEVEL;

namespace vrms5 { class IConferenceClientEventSink; }

class ConferenceClient {
public:
    typedef std::lock_guard<std::recursive_mutex> lock_guard_t;

    typedef std::weak_ptr<vrms5::IConferenceClientEventSink> const& CallbackArgT;
    typedef std::weak_ptr<IV5ChatEventSink> const& ChatCallbackArgT;
    typedef vrms5::utils::ts_weak_ptr<vrms5::IConferenceClientEventSink> CallbackStorageT;
    typedef vrms5::utils::ts_weak_ptr<IV5ChatEventSink> ChatCallbackStorageT;

    /**********/
    /* Method */
    /**********/
    
    ConferenceClient(  );
    ~ConferenceClient();
    
#if 0
    void             SetConferenceCallback( std::weak_ptr<IV5ConferenceEventSink> const& ConferenceEventSink_P );
    void             SetConferenceCallback( std::nullptr_t );
#endif
    
    void            SetChatCallback(ChatCallbackArgT sink);
    void            SetChatCallback(std::nullptr_t);
    
    void            SetLibCallback(CallbackArgT sink);
    void            SetLibCallback(std::nullptr_t);

    void            SetProxyInfo(std::string const& address,
                                 long const& port,
                                 std::string const& id,
                                 std::string const& pw );
    
    CONFCNCT_INT    Connect( const std::string& uri,
                             const std::string& TOKEN,
                             const std::string& conferenceID,
                             const std::string& FromPID,
                             const std::string& DisplayName);
    CONFCNCT_INT    Reconnect(  );
    CONFCNCT_INT    SendMessage( const std::string& DestPID, const std::string& Data );
    CONFCNCT_INT    Close();
    
    CONFCNCT_INT    RejectParticipant( const std::string& tgtPID );
    CONFCNCT_INT    LockConference( bool LockStatus );
    CONFCNCT_INT    UpdateConferenceName( const std::string& NewConfName );
    
    
    /* Getter Method */
    V5ConferenceStatusPtrT GetConferenceStatus() const;
    
    void GetParticipantList( );
    ParticipantPtrT GetSelfParticipant() const;
    
    void DebugSend();
    void WorkerCrashSend();
    
private:
    /**************/
    /* Parameters */
    /**************/
#if 0
    vrms5::utils::ts_weak_ptr<IV5ConferenceEventSink>   m_ConfCallbacks;    // Conference Class Ponter
#endif
    ChatCallbackStorageT        m_ChatCallbacks;    // CallBack Class Pointer
    CallbackStorageT            m_libCallbacks;     // LibV5Lite Callback pointer
    
    shared_ptr<thread>          m_thread;           // Websocket Client Thread(ws)
    shared_ptr<thread>          m_thread_tls;       // Websocket Client Thread(wss)
    
    client                      m_client;           // Conference Client
    client::connection_ptr      m_connection;       // Connection
    
    client_tls                  m_client_tls;       // Conference Client(tls)
    client_tls::connection_ptr  m_connection_tls;   // Connection(tls)
    
    CONWS_TYPE                  m_connect_words;    // Websocket connect Protocol flg(WSS or WS)
    std::atomic_int             m_status;           // Connect Status
    std::atomic_bool            m_pongflg;          // Pong Send Flag
    std::atomic_bool            m_reconnect_try;    // Reconnect Try Flag
    std::atomic_bool            m_close_try;        // Close Try Flag
    
    std::atomic_bool            m_ConfEndCallbackCalled;  // onLeaveConference Call Flag
    std::atomic_bool            m_onJoinConfFailCall;   // Callback Method (true=onJoinedConferenceFail Call)
    
    std::atomic_bool            m_pastTimeSts;      // PastTime Status
    
    timer_ptr                   m_est_timer_ptr;    // Established wait time pointer
    timer_ptr                   m_cfs_timer_ptr;    // CloseFromServer wait time pointer
    
    std::string                 userConURI;         // Connect URI
    std::string                 userTOKEN;          // User TOKEN
    std::string                 userPID;            // User ParticipantID
    std::string                 userConfID;         // ConferenceID
    std::string                 userDName;          // DisplayName
    
    ProxyStoragePtr             proxyStorage;       // Proxy Infomation
    mutable std::recursive_mutex pxymtx;            // Proxy Address Changed Mutex

    
    mutable std::recursive_mutex    mtx;            // コールバック中の再帰的ロックに備える。Mutex を複数用意してもならない。
    ParticipantListStoragePtr   participantListStorage;
    ChatLogStoragePtrT          chatLogStorage;     // ChatLogStorage（リストに対する破壊的操作があるため、リストへのゲッターを提供することはできない。）
    
    ConfClusterPtr              ConfStsCluster;     // Conference Status
    
    std::atomic_bool            m_errInfoSet;       // ErrorIntomation SetFlag
    V5ErrorInfoPtr              m_errInfo;          // ErrorInfomation
    
    std::atomic_bool            m_confEndTimeOverTimerStatus;
    
    std::map<int,std::string>   ErrReason;          // Error Reason
    
    /**********/
    /* Method */
    /**********/
    CONFCNCT_INT Connect_WS( const std::string& uri );
    CONFCNCT_INT Connect_WSS( const std::string& uri );
    
    /* ALL Status and Parameter Clear Method */
    void ClientInitialize();
    
    /* Websocket Connect Close Method */
    void CloseConnection();
    
    /* Timer Method */
    void SetMsgTimer( long latency , V5TIMEOUT_EVENT_LIST Event );
    
    /* Connection CallBack Method */
    void onOpen( websocketpp::connection_hdl hdl );
    void onFail( websocketpp::connection_hdl hdl );
    void onClose( websocketpp::connection_hdl hdl );
    void onMessage( websocketpp::connection_hdl hdl, message_ptr msg );
    void onTermination( websocketpp::connection_hdl hdl );
    
    void onTimeout( unsigned int Event, websocketpp::lib::error_code const & ec );
    
    /* Connection(tls only) CallBack Method */
    context_ptr onInitTLS( websocketpp::connection_hdl hdl );
    
    /* Recieve Message Method */
    void RcvPing(picojson::value const& message);
    void RcvSendMessage(picojson::value const& message);
    void RcvParticipantJoin(picojson::value const& message);
    void RcvParticipantLeave(picojson::value const& message);
    void RcvConferenceStatusUpdate(picojson::value const& message);
    void RcvMethodNotAllowed(picojson::value const& message);
    void RcvEstablished(picojson::value const& message);
    void RcvRejectParticipant(picojson::value const& message);
    void RcvCloseFromServer(picojson::value const& message);
    void RcvNotificationFailed(picojson::value const& message);
    
    
    void RcvDebug(picojson::value const& message);

    /* JSON Parse Method */
    void JsonParseEvent( const std::string& JSONParseMsg );
    
    /* ERROR REASON */
    void SetErrorReason();
    char* GetErrReason();
        
    /* Debug Log Method */
	void Debuglog(V5DEBUG_EVENT Event) const;
    
//    void DebugLogPrint( WEBSOCKET_LOG_LEVEL logLv,const char *fmt,...) const;
    
};

std::ostream& operator<<(std::ostream& os, const ParticipantCluster& cluster);
std::ostream& operator<<(std::ostream& os, const ParticipantListStorage& cluster);
std::ostream& operator<<(std::ostream& os, const ChatLogCluster& cluster);
std::ostream& operator<<(std::ostream& os, const ChatLogStorage& storage);
std::ostream& operator<<(std::ostream& os, const ConferenceStatusStorage& cluster);

#endif
