﻿#pragma once
#ifndef INCLUDE_SERVICE_LOCATOR_H
#define INCLUDE_SERVICE_LOCATOR_H

#include "IHttpd.h"
#include "LocalHTTPD_IF.h"
#include <memory>

class AbstractServiceLocator
{
public :
	static std::shared_ptr<AbstractServiceLocator> createInstance();

	virtual std::shared_ptr<IHttpd> createHttpd(std::weak_ptr<IV5HTTPDListener> listener) = 0;
};

template< class HTTPD_TYPE >
class ServiceLocator : public AbstractServiceLocator
{
public:
	virtual std::shared_ptr<IHttpd> createHttpd(std::weak_ptr<IV5HTTPDListener> listener) { 
		return std::make_shared<HTTPD_TYPE>(listener);
	}
};

#endif // INCLUDE_SERVICE_LOCATOR_H
