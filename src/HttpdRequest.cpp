﻿//
//  HttpdRequest.cpp
//  LibWebRequestListener
//
//  Created by V-0757 on 2015/03/13.
//  Copyright (c) 2015年 V-cube, Inc. All rights reserved.
//

#include "HttpdRequest.h"

HttpdRequest::HttpdRequest()
{
    httpd = LocalHTTPD::createInstance();
    
    httpd->addResouce("/crossdomain.xml",
                      std::bind(&HttpdRequest::request_crossdomain,
                                this,
                                std::placeholders::_1,
                                std::placeholders::_2
                                )
                      );
    httpd->addResouce("/api/alive/",
                      std::bind(&HttpdRequest::request_alive,
                                this,
                                std::placeholders::_1,
                                std::placeholders::_2
                                )
                      );
    httpd->addResouce("/api/joinConference/",
                      std::bind(&HttpdRequest::request_joinConference,
                                this, std::placeholders::_1,
                                std::placeholders::_2
                                )
                      );
}

HttpdRequest::~HttpdRequest()
{
    httpd->removeResouce("/crossdomain.xml");
    httpd->removeResouce("/api/alive/");
    httpd->removeResouce("/api/joinConference/");
}

HttpdRequest::instance_ptr_t HttpdRequest::createInstance()
{
    self_ptr_t instancePtr(new HttpdRequest());
    return std::move(instancePtr);
}

void HttpdRequest::setHttpdEventSink( std::weak_ptr<IV5HttpdEventSink> sink ) {
    m_httpdEventSink = sink;
};

V5HTTPD_START HttpdRequest::HTTPDStart(unsigned short port) {
    return httpd->start(port);
}

void HttpdRequest::HTTPDStop() {
    httpd->stop();
}

/*******************************************************************************************************/
/* private Method                                                                                      */
/*******************************************************************************************************/

bool HttpdRequest::request_crossdomain(std::shared_ptr<requestQuery> query,
                                        HttpdRequest::HTTPServerResponse& resp)
{
    // クロスドメインファイル
    std::string crossDomainXML;
    crossDomainXML += "<?xml version=\"1.0\"?>\n";
    crossDomainXML += "  <cross-domain-policy>\n";
    crossDomainXML += "  <site-control permitted-cross-domain-policies=\"all\" />\n";
    crossDomainXML += "  <allow-access-from domain=\"*\" />\n";
    crossDomainXML += "  <allow-http-request-headers-from domain=\"*\" headers=\"*\"/>\n";
    crossDomainXML += "</cross-domain-policy>\n";
    
    // レスポンス送信
    resp.setStatus(HTTPResponse::HTTP_OK);
    resp.setReason(HTTPResponse::HTTP_REASON_OK);
    resp.setContentType("text/xml");
    std::ostream& respMsg = resp.send();
    respMsg << crossDomainXML;
    respMsg.flush();
    
    return true;
}

// /api/alive/リクエストを受けた際に呼び出し、
bool HttpdRequest::request_alive(std::shared_ptr<requestQuery> query, HTTPServerResponse& resp)
{
    
    resp.setStatus(HTTPResponse::HTTP_OK);
    resp.setReason(HTTPResponse::HTTP_REASON_OK);
    resp.setContentType("application/json");
    std::ostream& respMsg = resp.send();
    
    /* ここにUI側のコールバック起動処理を実装 */
    
    unsigned int status = IV5HTTPDListener::V5State::Unknown;
    
    auto callback = m_httpdEventSink.lock();
    if( callback )
    {
        status = callback->onAliveResponse();
    }
    
    /* コールバックの応答でステータスをもらうまで応答を待たせる。 */
    /* Sessionがわからなくなるため。 */
    
    switch (status){
        case IV5HTTPDListener::V5State::Busy:
            respMsg << "{\"message\":\"busy\"}";
            break;
        case IV5HTTPDListener::V5State::Idle:
            respMsg << "{\"message\":\"idle\"}";
            break;
        case IV5HTTPDListener::V5State::Unknown:
        default:
            respMsg << "{\"message\":\"unknown\"}";
    }
    respMsg.flush();
    
    return true;
}

bool HttpdRequest::request_joinConference( std::shared_ptr<HttpdRequest::requestQuery> query,
                                                   HttpdRequest::HTTPServerResponse& resp)
{
    
    // Token取り出し
    std::string token;
    requestQuery::iterator it = query->find("token");
    if ( it != query->end() ) {
        token = it->second.c_str();
    }
    
    // portalURL取り出し
    std::string portal;
    it = query->find("portal");
    if ( it != query->end() ) {
        portal = it->second.c_str();
    }
    
    if ( token.empty() || portal.empty()) {
        resp.setStatus(HTTPResponse::HTTP_BAD_REQUEST);
        resp.setReason(HTTPResponse::HTTP_REASON_BAD_REQUEST);
        resp.setContentType("plane/text");
        std::ostream& respMsg = resp.send();
        respMsg.flush();
        return true;
    }
    
    bool ret = false;
    
    auto callback = m_httpdEventSink.lock();
    if( callback )
    {
        ret = callback->onRequestJoinConfernce( token.c_str(), portal.c_str() );
    }
    
    if ( ret ) {
        resp.setStatus(HTTPResponse::HTTP_OK);
        resp.setReason(HTTPResponse::HTTP_REASON_OK);
        resp.setContentType("plane/text");
    } else {
        resp.setStatus(HTTPResponse::HTTP_BAD_REQUEST);
        resp.setReason(HTTPResponse::HTTP_REASON_BAD_REQUEST );
        resp.setContentType("plane/text");
    }
    
    std::ostream& respMsg = resp.send();
    respMsg.flush();
    
    return true;
}
