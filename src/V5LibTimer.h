﻿//
//  V5LibTimer.h
//  LibV5Lite
//
//  Created by V-0757 on 2015/03/06.
//  Copyright (c) 2015 V-cube, Inc. All rights reserved.
//

#ifndef __LibV5Lite__V5LibTimer__
#define __LibV5Lite__V5LibTimer__


#pragma once

#ifdef V5_CLANG
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wconversion"
#endif

#include <boost/asio.hpp>

#ifdef V5_CLANG
#pragma clang diagnostic pop
#endif

#include <atomic>
#include <chrono>

class TimerService;

class Timer
{
    typedef boost::asio::deadline_timer deadline_timer;
    typedef boost::asio::io_service io_service;
    typedef boost::system::error_code error_code;
    
    typedef std::chrono::steady_clock steady_clock;
    typedef std::chrono::milliseconds milliseconds;
    
public:
    typedef std::function<void()> timer_handler;
    
    Timer(Timer&&) = delete;
    Timer(Timer const&) = delete;
    Timer const& operator=(Timer&&) = delete;
    Timer const& operator=(Timer const&) = delete;
    explicit Timer();
    
    virtual ~Timer();
    
    void start( long millisec, timer_handler cb);
    void start( long millisec, timer_handler cb, unsigned int repeat);
    void stop();

    void resetting(long millisec, timer_handler cb);
    void resetting(long millisec, timer_handler cb,unsigned int repeat);
    
private:
    void handler(error_code const& err, timer_handler const& callback);
    static std::shared_ptr<TimerService> getStaticService();
    
    void statusReset();
    
private:
    std::shared_ptr<TimerService> service;
    deadline_timer deadlineTimer;
    std::atomic_bool m_running;
    std::atomic_bool m_resetFlg;
    
    
    std::atomic<long> m_millisec;
    std::atomic<unsigned int> m_repeatCount;
    std::atomic<unsigned int> m_currentCount;
    
    std::atomic<steady_clock::time_point> m_startTimePoint;
    
};

#endif /* defined(__LibV5Lite__V5LibTimer__) */
