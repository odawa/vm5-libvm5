﻿//
//  URLLoader.cpp
//  LibV5Lite
//
//  Created by V-1094 on 2015/01/26.
//  Copyright (c) 2015年 V-cube. All rights reserved.
//

#include "precompile.h"
#include "URLLoader.h"
#include <curl/curl.h>
#include <thread>
#include "WebAccess.h"
#include "utils.h"
#include "V5Log.h"
#include <string>

void URLLoader::load(const char* accessUrl, const char* queryString) {

    std::string myUrl = std::string(accessUrl);
    std::string myQueryString = std::string(queryString);
    std::shared_ptr<URL_HANDLER_RESPONSE_CALLBACK> callback = _callback;
    std::shared_ptr<vrms5::ProxySetting> proxySetting = _proxySetting.lock();
    std::thread thread = std::thread([=]() {
        bool ret = false;

        MemoryStruct chunk;

        //curl_global_init(CURL_GLOBAL_ALL);
        CURL* curl;
        char ebuf[CURL_ERROR_SIZE];
        curl = curl_easy_init();
        curl_easy_setopt(curl, CURLOPT_ERRORBUFFER, &ebuf);
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteMemoryCallback);

        chunk.memory = reinterpret_cast<char *>(malloc(1));  /* will be grown as needed by the realloc above */
        chunk.size = 0;    /* no data at this point */
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, static_cast<void *>(&chunk));
        if(proxySetting != nullptr) {
            injectProxySetting(curl, *proxySetting);
        }
        V5LOG(V5_LOG_INFO,"[XXXX] url=%s\n", myUrl.c_str());
		curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_easy_setopt(curl, CURLOPT_URL, myUrl.c_str());
        curl_easy_setopt(curl, CURLOPT_POSTFIELDS, myQueryString.c_str());

        if (curl_easy_perform(curl) != CURLE_OK) {
            V5LOG(V5_LOG_ERROR,"request failed...: %s\n", ebuf);
            ret = false;
        } else {
            ret = true;
        }

        if(callback) {
            (*callback)(ret, chunk.memory, chunk.size);
        }

        if(chunk.memory) {
            free(chunk.memory);
        }
        if(curl) {
            curl_easy_cleanup(curl);
            //curl_global_cleanup();
        }
    });
    thread.detach();
}

void URLLoader::setResponseCallback(URL_HANDLER_RESPONSE_CALLBACK callback){
    _callback = std::make_shared<URL_HANDLER_RESPONSE_CALLBACK>(callback);
}

void URLLoader::setProxySetting(std::weak_ptr<vrms5::ProxySetting> proxySetting) {
    this->_proxySetting = proxySetting;
}
