﻿#pragma once
#ifndef __LibV5Lite__utils__
#define __LibV5Lite__utils__

#include <V5Lite/LibV5LiteCommon.h>

#include "ts_weak_ptr.h"

#include <cstddef>
#include <cstring>
#include <string>
#include <chrono>

#ifdef V5_WIN32

#include <Windows.h>

#else

#include <unistd.h>

#endif


namespace vrms5 {
	namespace utils {

		inline void utf8cpy(char * dest, const char * src, std::size_t count)
		{
#ifdef V5_WIN32
			strncpy_s(dest, count, src, _TRUNCATE);
#else
			strlcpy(dest, src, count);
#endif
		}
        
        template <std::size_t N>
        inline void utf8cpy(char(&dest)[N], const char * src)
        {
            return utf8cpy(dest, src, N);
        }

        template <std::size_t N>
        inline void utf8cpy(unsigned char(&dest)[N], const unsigned char * src)
        {
            return utf8cpy(reinterpret_cast<char*>(&dest[0]), reinterpret_cast<const char*>(src), N);
        }

        template <std::size_t N>
        inline void utf8cpy(char(&dest)[N], std::string const& src)
        {
            return utf8cpy(dest, src.c_str(), N);
        }

        template <std::size_t N>
        inline void utf8cpy(unsigned char(&dest)[N], std::string const& src)
        {
            return utf8cpy(reinterpret_cast<char*>(&dest[0]), src.c_str(), N);
        }

        inline void utf8cpy(char *dest, std::string const& src, std::size_t count)
        {
            return utf8cpy(dest, src.c_str(), count);
        }

		inline void utf8cat(char *dest, const char * src, std::size_t count)
		{
#ifdef V5_WIN32
			strncat_s(dest, count, src, _TRUNCATE);
#else
			strlcat(dest, src, count);
#endif
		}

        template <std::size_t N>
        inline void utf8cat(char (&dest)[N], const char * src)
        {
            return utf8cat(dest, src, N);
        }

		// ms = Milliseconds
		inline void sleepForMillseconds(int milleseconds) 
		{
#ifdef V5_WIN32
			Sleep(milleseconds);
#else
			usleep(milleseconds * 1000);
#endif
			
		}
        
        bool base64Encode( const std::string& src, std::string *dst );
        bool base64Decode( const std::string& src, std::string *dst );

        class timer
        {
            typedef std::chrono::system_clock       system_clock;
            typedef system_clock::time_point        time_point;
            typedef std::chrono::milliseconds       milliseconds;
            typedef std::chrono::duration<double>   duration;
            time_point startPoint;
            
        public:
            typedef duration::rep rep;
            
            timer() : startPoint(system_clock::now())
            {
            }
            
            void reset()
            {
                startPoint = system_clock::now();
            }
            
            rep elapsed() const
            {
                using namespace std::chrono;
                
                duration sec = system_clock::now() - startPoint;
                return sec.count();
            }
            
        };
        
        
	}
}


#endif /* defined(__LibV5Lite__utils__) */