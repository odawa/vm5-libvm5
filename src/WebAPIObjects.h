﻿//
//  WebAPIObjects.h
//  V5ClientExample
//
//  Created by V-1094 on 2014/12/12.
//  Copyright (c) 2015 V-cube, Inc. All rights reserved.
//

#ifndef V5ClientExample_WebAPIObjects_h
#define V5ClientExample_WebAPIObjects_h

#include "WebAccess.h"
#include <V5lite/V5DataTypes.h>
#include <memory>
#include <boost/lexical_cast.hpp>

typedef std::map<std::string,std::string> V5MapType;

struct V5APIsObject {
    typedef std::string const& string_arg_t;
    typedef std::shared_ptr<std::string> const& shared_string_arg_t;

    virtual ~V5APIsObject() {}
    V5APIsObject(){}
    virtual bool parse(picojson::value const& response) = 0;
    
    std::string        url;
    V5MapType          query;
    bool               isParsed = false;
};

struct V5JoinConferenceWebServer: V5APIsObject {
    V5JoinConferenceWebServer(){}
    bool parse(picojson::value const& response) override;
};

struct V5JoinConference : V5APIsObject
{
    V5JoinConference(){}
	V5JoinConference(std::string token) {
		query["token"] = token;
        query["action_JoinConference"] = "";
    }

    bool parse(picojson::value const& response) override;
    std::string result;
    std::string status;
    //: "200" or SOME_ERROR_CODE,
    std::string description;
    //: SOME_MESSAGE_FROM_SERVER

};

struct V5PrepareJoinConference: V5APIsObject
{
    typedef std::shared_ptr<V5PasswordInfo> const& shared_passwd_info_arg_t;
    
    V5PrepareJoinConference(){}
    V5PrepareJoinConference(string_arg_t session,
                            shared_string_arg_t displayName,
                            shared_passwd_info_arg_t passwd
                            )
    {
        query["token"] = session;
        if (displayName) {
            query["displayName"] = *displayName;
        }
        if (passwd) {
            query["password"]    = passwd->passwd;
            switch (passwd->hashType)
            {
                case V5_HASH_TYPE_SHA1:
                    query["hashType"]   = "SHA1";
                    break;
                case V5_HASH_TYPE_RAW:
                    query["hashType"]   = "RAW";
                    break;
            }
            if (passwd->hashCount > 1) {
                query["hashCount"] = boost::lexical_cast<std::string>(passwd->hashCount).c_str();
            }
            if (passwd->salt) {
                query["hashSalt"]   = *passwd->salt;
            }
        }
        query["action_PrepareJoinConference"] = "";
    }
    
    bool parse(picojson::value const& response) override;

    std::string conferencePassword;
    //:”REQUIRED” or “INVALID” or “VALID”
    std::string un;
    //:  "VIDYO_USER_UN",
    std::string pw;
    //:  "VIDYO_USER_PW",
    std::string portalAddress;
    //: "VIDYO_PORTAL_ADDRESS",
    std::string conferenceAddress;
    //: "V5_CONF_ADDRESS"
    std::string result;
    //: "OK" or "ERROR"
    std::string status;
    //: "200" or SOME_ERROR_CODE,
    std::string description;
    //: SOME_MESSAGE_FROM_SERVER
    std::string conferenceID;
    //: ID_OF_CONFERENCE
    std::string requireDisplayName;
    //:”REQUIRED” or “INVALID” or “VALID”
    std::string displayName;
    // ”NAME_OF_PARTICIPANT”
    std::string locationTag;
    // "jp" or "cn"
    std::string isGuest;
    // "true" or "false"
};

struct V5StartSConferenceSession: V5APIsObject
{
    V5StartSConferenceSession(){}
    V5StartSConferenceSession(std::string const& volatileToken, std::string const& version ) {
        query["action_StartConferenceSession"] = "";
        query["volatileToken"] = volatileToken;
        query["version"] = version;
        query["type"] = std::to_string(PLATFORM_TYPE);
    }
    
    bool parse(picojson::value const& response) override;
    std::string token;
    std::string status;
    std::string result;
    std::string description;
    std::string displayName;
    std::string requireVersion;
    std::string maintenanceStartTime;
    std::string maintenanceEndTime;
    picojson::object logoUrl;
};

struct V5SendInvitationMail: V5APIsObject
{
    V5SendInvitationMail(){}
    V5SendInvitationMail(std::string token, std::string conferenceID, std::string mailAddress) {
        query["action_SendInvitationMail"] = "";
        query["token"] = token;
        query["conferenceID"] = conferenceID;
        query["mailAddress"] = mailAddress;
    }
    
    bool parse(picojson::value const& response) override;
    std::string token;
    std::string status;
    std::string result;
    std::string description;
};

#endif
