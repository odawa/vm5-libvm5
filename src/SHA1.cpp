﻿#include "precompile.h"

#include "SHA1.h"
#include <sstream>
#include <boost/uuid/sha1.hpp>
#include <boost/cstdint.hpp>
#include <boost/array.hpp>
#include <iostream>

#include "V5Log.h"

typedef boost::array<boost::uint8_t,20> hash_data_t;

hash_data_t getSHA1Hash(const void *data, const std::size_t byte_count)
{
    boost::uuids::detail::sha1 sha1;
    sha1.process_bytes( data, byte_count );
    unsigned int digest[5];
    sha1.get_digest( digest );
    hash_data_t hash_data;
    for( int i = 0; i < 5; ++i )
    {
        hash_data[ i * 4 ]     = (digest[ i ] & 0xFF000000) >> 24;
        hash_data[ i * 4 + 1 ] = (digest[ i ] & 0x00FF0000) >> 16;
        hash_data[ i * 4 + 2 ] = (digest[ i ] & 0x0000FF00) >> 8;
        hash_data[ i * 4 + 3 ] = (digest[ i ] & 0x000000FF);
    }
    return std::move(hash_data);
}

hash_data_t getSHA1HashFromString(const char * seed, std::size_t length)
{
//    V5LOG(V5_LOG_INFO,"blob=%s", seed);
    return std::move(getSHA1Hash(seed, length));
}

std::string toStringFromHash(const hash_data_t& hash)
{
    std::ostringstream os;
    hash_data_t::const_iterator itr = hash.begin();
    const hash_data_t::const_iterator end_itr = hash.end();
    for( ; itr != end_itr; ++itr )
    {
        os << std::hex << ( (*itr  & 0xf0 ) >> 4 )
            << std::hex << (*itr  & 0x0f );
    }
//    V5LOG(V5_LOG_INFO,"|hash=%s", os.str().c_str() );
    return std::move(os.str());
}

std::string getSHA1String(const char * seed, std::size_t length)
{
    return std::move(toStringFromHash(getSHA1HashFromString(seed, length)));
}

int main_dummy()
{
    std::cout << std::hex;

    hash_data_t hash = getSHA1Hash("foobar", 6);
    hash_data_t::const_iterator itr = hash.begin();
    const hash_data_t::const_iterator end_itr = hash.end();
    for( ; itr != end_itr; ++itr )
    {
        std::cout << ( (*itr  & 0xf0 ) >> 4 )
                    << (*itr  & 0x0f );
    }
    std::cout << std::endl;

    std::string out = toStringFromHash(hash);
    std::cout << out << std::endl;
    
    return 0;
}
