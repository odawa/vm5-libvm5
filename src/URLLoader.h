﻿//
//  URLLoader.h
//  LibV5Lite
//
//  Created by V-1094 on 2015/01/26.
//  Copyright (c) 2015 V-cube, Inc. All rights reserved.
//

#ifndef __LIBV5_LITE_URL_LOADER_H_
#define __LIBV5_LITE_URL_LOADER_H_

#include <memory>
#include <functional>
#include <picojson/picojson.h>
#include "ProxySetting.h"

#include <V5Lite/V5Enumerations.h>

typedef std::function<void(bool result, char* response, size_t responseSize)> URL_HANDLER_RESPONSE_CALLBACK;

typedef std::function<void(picojson::value& value, V5ClientHttpResult result)> CALL_ASYNC_RESPONSE_CALLBACK;

class URLLoader {
public:
    URLLoader() {
    }

    void load(const char* url, const char* queryString);
    void setResponseCallback(URL_HANDLER_RESPONSE_CALLBACK callback);
    void setProxySetting(std::weak_ptr<vrms5::ProxySetting> proxySetting);

private:
    std::shared_ptr<URL_HANDLER_RESPONSE_CALLBACK> _callback;
    std::weak_ptr<vrms5::ProxySetting> _proxySetting;
};


#endif // __LIBV5_LITE_URL_LOADER_H_
