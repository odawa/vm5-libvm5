﻿#pragma once
#ifndef __LocalHTTPD_IF__
#define __LocalHTTPD_IF__

#include <string>

class IV5HTTPDListener {
public:
	enum V5State{
		Unknown = 0,
		Busy,
		Idle,
	};

	virtual ~IV5HTTPDListener() {}

	virtual V5State getStatus() = 0;
	virtual bool joinConference(std::string volatleToken, std::string portalURL) = 0;
};

#endif /* defined(__LocalHTTPD_IF__) */