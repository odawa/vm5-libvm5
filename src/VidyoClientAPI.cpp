﻿#include "precompile.h"
#include "VidyoClientAPI.h"
#include "V5Log.h"
#include "utils.h"
#include <Vidyo/VidyoClientPrivate.h>
#include <assert.h>
#include <map>
#include <boost/lexical_cast.hpp>

#define NAMEMAP_ELEMENT(x) { x, #x }
#define NAMEMAP_PRIVATE_ELEMENT(x) { static_cast<VidyoClientOutEvent>(x), #x }

namespace vrms5
{
    namespace vidyo
    {
        using namespace vrms5::utils;

        VidyoClientCallState VidyoClient::getCallStatus()
        {
            VidyoClientRequestCallState status;
            auto error = sendRequest(VIDYO_CLIENT_REQUEST_GET_CALL_STATE, status);
            assert(error == VIDYO_CLIENT_ERROR_OK);     // まだ入室している状態で stop を呼ぶと失敗する
            return status.callState;
        }

        bool VidyoClient::login(arg_string portalAddress, arg_string un, arg_string pw)
        {
            VidyoClientInEventLogIn event = { 0 };

            utf8cpy(event.portalUri, portalAddress.c_str());
            utf8cpy(event.userName, un.c_str());
            utf8cpy(event.userPass, pw.c_str());

            if (!sendEvent(VIDYO_CLIENT_IN_EVENT_LOGIN, event))
            {
                V5LOG(V5_LOG_ERROR, "Login error.\n");
                return false;
            }
            return true;
        }

        bool VidyoClient::loginCancel()
        {
            if (!sendEvent(VIDYO_CLIENT_IN_EVENT_LOGIN_CANCEL))
            {
                V5LOG(V5_LOG_ERROR, "Login Cancel error.\n");
                return false;
            }
            return true;
        }

        bool VidyoClient::leave()
        {
            if (isStarted() && getCallStatus() != VIDYO_CLIENT_CALL_STATE_IDLE && !sendEvent(VIDYO_CLIENT_IN_EVENT_LEAVE))
            {
                return false;
            }
            return true;
        }

        bool VidyoClient::signOff()
        {
            if (!sendEvent(VIDYO_CLIENT_IN_EVENT_SIGNOFF))
            {
                V5LOG(V5_LOG_ERROR, "Sign-Off error.\n");
                return false;
            }
            return true;
        }

        bool VidyoClient::getVideoMuted()
        {
            VidyoClientRequestGetMuted muted = { 0 };
            auto error = VidyoClient::sendRequest(VIDYO_CLIENT_REQUEST_GET_MUTED_VIDEO, muted);
            if (error != VIDYO_CLIENT_ERROR_OK)
            {
                V5LOG(V5_LOG_ERROR, "Failed to get video muted.\n");
            }
            return (muted.isMuted == VIDYO_TRUE);
        }

        bool VidyoClient::getAudioInMuted()
        {
            VidyoClientRequestGetMuted muted = { 0 };
            auto error = VidyoClient::sendRequest(VIDYO_CLIENT_REQUEST_GET_MUTED_AUDIO_IN, muted);
            if (error != VIDYO_CLIENT_ERROR_OK)
            {
                V5LOG(V5_LOG_ERROR, "Failed to get audio-in muted.\n");
            }
            return (muted.isMuted == VIDYO_TRUE);
        }

        bool VidyoClient::getAudioOutMuted()
        {
            VidyoClientRequestGetMuted muted = { 0 };
            auto error = VidyoClient::sendRequest(VIDYO_CLIENT_REQUEST_GET_MUTED_AUDIO_OUT, muted);
            if (error != VIDYO_CLIENT_ERROR_OK)
            {
                V5LOG(V5_LOG_ERROR, "Failed to get audio-out muted.\n");
            }
            return (muted.isMuted == VIDYO_TRUE);
        }

        bool VidyoClient::setVideoMute(bool muted)
        {
            VidyoClientInEventMute event = { 0 };
            event.willMute = (muted) ? VIDYO_TRUE : VIDYO_FALSE;
            if (!sendEvent(VIDYO_CLIENT_IN_EVENT_MUTE_VIDEO, event))
            {
                V5LOG(V5_LOG_ERROR, "in event mute vido failed.\n");
                return false;
            }
            return true;
        }

        bool VidyoClient::setAudioInMute(bool muted)
        {
            VidyoClientInEventMute event = { 0 };
            event.willMute = (muted) ? VIDYO_TRUE : VIDYO_FALSE;
            if (!sendEvent(VIDYO_CLIENT_IN_EVENT_MUTE_AUDIO_IN, event))
            {
                V5LOG(V5_LOG_ERROR, "in event mute audio in failed.\n");
                return false;
            }
            return true;
        }

        bool VidyoClient::setAudioOutMute(bool muted)
        {
            VidyoClientInEventMute event = { 0 };
            event.willMute = (muted) ? VIDYO_TRUE : VIDYO_FALSE;
            if (!sendEvent(VIDYO_CLIENT_IN_EVENT_MUTE_AUDIO_OUT, event))
            {
                V5LOG(V5_LOG_ERROR, "in event mute audio out failed.\n");
                return false;
            }
            return true;
        }
        
        bool VidyoClient::setMaxParticipants(unsigned int maxParticipants)
        {
            VidyoClientInEventParticipantsLimit limit = { 0 };
            limit.maxNumParticipants = maxParticipants;
            // Moblie では必ずエラー値が返却されるのでエラー制御しない
            return sendEvent(VIDYO_CLIENT_IN_EVENT_PARTICIPANTS_LIMIT, limit);
        }
        
        bool VidyoClient::setRendererVisible(bool visible)
        {
            VidyoClientInEventSetRendererVisible param = {0};
            param.visible = (visible) ? VIDYO_TRUE: VIDYO_FALSE;
            return sendEvent(VIDYO_CLIENT_IN_EVENT_SET_RENDERER_VISIBLE, param);
        }
        
        VidyoUint VidyoClient::getParticipants(VidyoClientRequestParticipants* participants)
        {
            return VidyoClient::sendRequest(VIDYO_CLIENT_REQUEST_GET_PARTICIPANTS, participants);
        }
        
        VidyoUint VidyoClient::getSelectedParticipants(VidyoClientRequestParticipants* participants)
        {
            return VidyoClient::sendRequest(VIDYO_CLIENT_REQUEST_GET_SELECTED_PARTICIPANTS, participants);
        }

        VidyoUint VidyoClient::getRemoteShares(VidyoClientRequestWindowShares* shares)
        {
            shares->requestType = LIST_SHARING_WINDOWS;
            return VidyoClient::sendRequest(VIDYO_CLIENT_REQUEST_GET_WINDOW_SHARES, shares);
        }

        VidyoUint VidyoClient::getConfiguration(VidyoClientRequestConfiguration* conf)
        {
            auto error = VidyoClient::sendRequest(VIDYO_CLIENT_REQUEST_GET_CONFIGURATION, conf);
            if (error != VIDYO_CLIENT_ERROR_OK)
            {
                V5LOG(V5_LOG_ERROR, "Failed to request configuration with error: %d\n", error);
            }
            return error;
        }
        
        VidyoUint VidyoClient::setConfiguration(VidyoClientRequestConfiguration& conf)
        {
            auto error = VidyoClient::sendRequest(VIDYO_CLIENT_REQUEST_SET_CONFIGURATION, conf);
            if (error != VIDYO_CLIENT_ERROR_OK)
            {
                V5LOG(V5_LOG_ERROR, "Failed to set configuration.\n");
            }
            return error;
        }
        
        bool VidyoClient::stopDynamicWatchVideoSource() {
            VidyoClientInEventSetDynamicWatchVideoSource source = {0};
            source.numParticipants = 0;
            if(VidyoClientSendEvent(VIDYO_CLIENT_IN_EVENT_SET_DYNAMIC_WATCH_VIDEO_SOURCE, &source, sizeof(VidyoClientInEventSetDynamicWatchVideoSource)) == VIDYO_FALSE) {
                V5LOG(V5_LOG_ERROR, "set stop dynamic watch video source failed.\n"); /*E*/
                return false;
            }
            return true;
        }
        
        bool VidyoClient::startDynamicWatchVideoSource(unsigned int numParticipants, DefaultFrameSize &videoSize) {
            VidyoClientInEventSetDynamicWatchVideoSource source = {0};
            source.numParticipants = numParticipants;
            for(VidyoUint i = 0; i < source.numParticipants; i++) {
                source.dynamicViews[i].width = videoSize.width;
                source.dynamicViews[i].height = videoSize.height;
                source.dynamicViews[i].maxFrameRate = videoSize.frameRate;
            }

            if(VidyoClientSendEvent(VIDYO_CLIENT_IN_EVENT_SET_DYNAMIC_WATCH_VIDEO_SOURCE, &source, sizeof(VidyoClientInEventSetDynamicWatchVideoSource)) == VIDYO_FALSE) {
                V5LOG(V5_LOG_ERROR, "set start dynamic watch video source failed.\n"); /*E*/
                return false;
            }
            return true;
        }
        
        bool VidyoClient::setBandWidth(unsigned int sLayer, int bandWidth )
        {
            VidyoClientInEventSetSendBandwidth sendBandWidth = {0};
            sendBandWidth.sendBandwidth = (VidyoInt)bandWidth;
            sendBandWidth.sLayers = (VidyoUint)sLayer;
            
            if(VidyoClientSendEvent(VIDYO_CLIENT_IN_EVENT_SET_SEND_BANDWIDTH, &sendBandWidth, sizeof(VidyoClientInEventSetSendBandwidth)) == VIDYO_FALSE) {
                V5LOG(V5_LOG_ERROR, "set SendBandWidth failed.\n"); /*E*/
                return false;
            }
            
            return true;
        }
        
        VidyoUint VidyoClient::getMicrophoneVolume(VidyoClientRequestVolume* volume)
        {
            auto error = VidyoClient::sendRequest(VIDYO_CLIENT_REQUEST_GET_VOLUME_AUDIO_IN, *volume);
            if (error != VIDYO_CLIENT_ERROR_OK)
            {
                V5LOG(V5_LOG_ERROR, "Failed to request get microphone volume with error: %d\n", error);
            }
            return error;
        }

        VidyoUint VidyoClient::setMicrophoneVolume(VidyoClientRequestVolume& volume)
        {
            auto error = VidyoClient::sendRequest(VIDYO_CLIENT_REQUEST_SET_VOLUME_AUDIO_IN, volume);
            if (error != VIDYO_CLIENT_ERROR_OK)
            {
                V5LOG(V5_LOG_ERROR, "Failed to request set microphone with error: %d\n", error);
            }
            return error;
        }

        VidyoUint VidyoClient::getSpeakerVolume(VidyoClientRequestVolume* volume)
        {
            auto error = VidyoClient::sendRequest(VIDYO_CLIENT_REQUEST_GET_VOLUME_AUDIO_OUT, *volume);
            if (error != VIDYO_CLIENT_ERROR_OK)
            {
                V5LOG(V5_LOG_ERROR, "Failed to request get speaker volume with error: %d\n", error);
            }
            return error;
        }
        
        VidyoUint VidyoClient::setSpeakerVolume(VidyoClientRequestVolume& volume)
        {
            auto error = VidyoClient::sendRequest(VIDYO_CLIENT_REQUEST_SET_VOLUME_AUDIO_OUT, volume);
            if (error != VIDYO_CLIENT_ERROR_OK)
            {
                V5LOG(V5_LOG_ERROR, "Failed to request set speaker with error: %d\n", error);
            }
            return error;
        }
        
        VidyoUint VidyoClient::webProxySettingValid(VidyoClientRequestWebProxySettingValid *valid)
        {
            auto error = VidyoClient::sendRequest(VIDYO_CLIENT_REQUEST_GET_WEB_PROXY_SETTING_VALID, *valid);
            if (error != VIDYO_CLIENT_ERROR_OK)
            {
                V5LOG(V5_LOG_ERROR, "Failed to request set speaker with error: %d\n", error);
            }
            return error;
        }
        
        VidyoUint VidyoClient::setSendMaxKBPS(int kbps)
        {
            VidyoInt maxKbps = kbps;
            
            auto error = VidyoClient::sendRequest(VIDYO_CLIENT_REQUEST_SET_SEND_MAX_KBPS, &maxKbps);
            if (error != VIDYO_CLIENT_ERROR_OK)
            {
                V5LOG(V5_LOG_ERROR, "Failed to set setSendMaxKBPS.\n");
            }
            return error;
        }
        
        VidyoUint VidyoClient::getSendMaxKBPS(int *kbps)
        {
            VidyoInt maxkbps = 0;
            
            auto error = VidyoClient::sendRequest(VIDYO_CLIENT_REQUEST_GET_SEND_MAX_KBPS, &maxkbps);
            if (error != VIDYO_CLIENT_ERROR_OK)
            {
                V5LOG(V5_LOG_ERROR, "Failed to set getSendMaxKBPS.\n");
            }
            *kbps = maxkbps;
            return error;
        }

        VidyoUint VidyoClient::setRecvMaxKBPS(int kbps)
        {
            VidyoInt maxKbps = kbps;
            
            auto error = VidyoClient::sendRequest(VIDYO_CLIENT_REQUEST_SET_RECV_MAX_KBPS, &maxKbps);
            if (error != VIDYO_CLIENT_ERROR_OK)
            {
                V5LOG(V5_LOG_ERROR, "Failed to set setRecvMaxKBPS.\n");
            }
            return error;
        }
        VidyoUint VidyoClient::getRecvMaxKBPS(int *kbps)
        {
            VidyoInt maxkbps = 0;
            
            auto error = VidyoClient::sendRequest(VIDYO_CLIENT_REQUEST_GET_RECV_MAX_KBPS, &maxkbps);
            if (error != VIDYO_CLIENT_ERROR_OK)
            {
                V5LOG(V5_LOG_ERROR, "Failed to set getRecvMaxKBPS.\n");
            }
            *kbps = maxkbps;
            return error;
        }
        
        
#ifdef V5DEBUG
        std::string toString(::VidyoClientOutEvent event)
        {
            static const std::map<::VidyoClientOutEvent, std::string> NAME_MAP{
                NAMEMAP_ELEMENT(VIDYO_CLIENT_OUT_EVENT_USER_MESSAGE),
                NAMEMAP_ELEMENT(VIDYO_CLIENT_OUT_EVENT_LOGIC_STARTED),
                NAMEMAP_ELEMENT(VIDYO_CLIENT_OUT_EVENT_SIGN_IN),
                NAMEMAP_ELEMENT(VIDYO_CLIENT_OUT_EVENT_SIGN_OUT),
                NAMEMAP_ELEMENT(VIDYO_CLIENT_OUT_EVENT_SUBSCRIBING),
                NAMEMAP_ELEMENT(VIDYO_CLIENT_OUT_EVENT_APP_EXIT),
                NAMEMAP_ELEMENT(VIDYO_CLIENT_OUT_EVENT_DEVICE_SELECT),
                NAMEMAP_ELEMENT(VIDYO_CLIENT_OUT_EVENT_DEVICES_CHANGED),
                NAMEMAP_ELEMENT(VIDYO_CLIENT_OUT_EVENT_DEVICE_SELECTION_CHANGED),
                NAMEMAP_ELEMENT(VIDYO_CLIENT_OUT_EVENT_SELECTED_DEVICE_ON_OS_CHANGED),
                NAMEMAP_ELEMENT(VIDYO_CLIENT_OUT_EVENT_RINGING),
                NAMEMAP_ELEMENT(VIDYO_CLIENT_OUT_EVENT_RING_PROGRESS),
                NAMEMAP_ELEMENT(VIDYO_CLIENT_OUT_EVENT_END_RINGING),
                NAMEMAP_ELEMENT(VIDYO_CLIENT_OUT_EVENT_CALLING),
                NAMEMAP_ELEMENT(VIDYO_CLIENT_OUT_EVENT_CALL_PROGRESS),
                NAMEMAP_ELEMENT(VIDYO_CLIENT_OUT_EVENT_END_CALLING),
                NAMEMAP_ELEMENT(VIDYO_CLIENT_OUT_EVENT_JOINING),
                NAMEMAP_ELEMENT(VIDYO_CLIENT_OUT_EVENT_JOIN_PROGRESS),
                NAMEMAP_ELEMENT(VIDYO_CLIENT_OUT_EVENT_RETRYING),
                NAMEMAP_ELEMENT(VIDYO_CLIENT_OUT_EVENT_RETRY_PROGRESS),
                NAMEMAP_ELEMENT(VIDYO_CLIENT_OUT_EVENT_END_PROCESSING),
                NAMEMAP_ELEMENT(VIDYO_CLIENT_OUT_EVENT_CONFERENCE_ACTIVE),
                NAMEMAP_ELEMENT(VIDYO_CLIENT_OUT_EVENT_CONFERENCE_ENDED),
                NAMEMAP_ELEMENT(VIDYO_CLIENT_OUT_EVENT_PARTICIPANTS_CHANGED),
                NAMEMAP_ELEMENT(VIDYO_CLIENT_OUT_EVENT_INCOMING_CALL),
                NAMEMAP_ELEMENT(VIDYO_CLIENT_OUT_EVENT_END_INCOMING_CALL),
                NAMEMAP_ELEMENT(VIDYO_CLIENT_OUT_EVENT_PREVIEW),
                NAMEMAP_ELEMENT(VIDYO_CLIENT_OUT_EVENT_MUTED_AUDIO_IN),
                NAMEMAP_ELEMENT(VIDYO_CLIENT_OUT_EVENT_MUTED_AUDIO_OUT),
                NAMEMAP_ELEMENT(VIDYO_CLIENT_OUT_EVENT_MUTED_VIDEO),
                NAMEMAP_ELEMENT(VIDYO_CLIENT_OUT_EVENT_MUTED_SERVER_AUDIO_IN),
                NAMEMAP_ELEMENT(VIDYO_CLIENT_OUT_EVENT_MUTED_SERVER_VIDEO),
                NAMEMAP_ELEMENT(VIDYO_CLIENT_OUT_EVENT_ADD_SHARE),
                NAMEMAP_ELEMENT(VIDYO_CLIENT_OUT_EVENT_REMOVE_SHARE),
                NAMEMAP_ELEMENT(VIDYO_CLIENT_OUT_EVENT_SHARE_CLOSED),
                NAMEMAP_ELEMENT(VIDYO_CLIENT_OUT_EVENT_RESIZE),
                NAMEMAP_ELEMENT(VIDYO_CLIENT_OUT_EVENT_PARTICIPANTS_LIMIT),
                NAMEMAP_ELEMENT(VIDYO_CLIENT_OUT_EVENT_ECHO_DETECTED),
                NAMEMAP_ELEMENT(VIDYO_CLIENT_OUT_EVENT_KEYDOWN),
                NAMEMAP_ELEMENT(VIDYO_CLIENT_OUT_EVENT_KEYUP),
                NAMEMAP_ELEMENT(VIDYO_CLIENT_OUT_EVENT_MOUSE_DOWN),
                NAMEMAP_ELEMENT(VIDYO_CLIENT_OUT_EVENT_MOUSE_UP),
                NAMEMAP_ELEMENT(VIDYO_CLIENT_OUT_EVENT_MOUSE_MOVE),
                NAMEMAP_ELEMENT(VIDYO_CLIENT_OUT_EVENT_MOUSE_SCROLL),
                NAMEMAP_ELEMENT(VIDYO_CLIENT_OUT_EVENT_TOUCH),
                NAMEMAP_ELEMENT(VIDYO_CLIENT_OUT_EVENT_FLOATING_WINDOW),
                NAMEMAP_ELEMENT(VIDYO_CLIENT_OUT_EVENT_FECC_LIST_UPDATED),
                NAMEMAP_ELEMENT(VIDYO_CLIENT_OUT_EVENT_CONTROL_CAMERA),
                NAMEMAP_ELEMENT(VIDYO_CLIENT_OUT_EVENT_SET_PARTICIPANT_VIDEO_MODE_DONE),
                NAMEMAP_ELEMENT(VIDYO_CLIENT_OUT_EVENT_CONFERENCE_INFO_UPDATE),
                NAMEMAP_ELEMENT(VIDYO_CLIENT_OUT_EVENT_FECC_STATUS_UPDATE),
                NAMEMAP_ELEMENT(VIDYO_CLIENT_OUT_EVENT_PARTICIPANT_BUTTON_CLICK),
                NAMEMAP_ELEMENT(VIDYO_CLIENT_OUT_EVENT_MEDIA_CONTROL),
                NAMEMAP_ELEMENT(VIDYO_CLIENT_OUT_EVENT_LOGIN),
                NAMEMAP_ELEMENT(VIDYO_CLIENT_OUT_EVENT_LICENSE),
                NAMEMAP_ELEMENT(VIDYO_CLIENT_OUT_EVENT_LINKED),
                NAMEMAP_ELEMENT(VIDYO_CLIENT_OUT_EVENT_SIGNED_IN),
                NAMEMAP_ELEMENT(VIDYO_CLIENT_OUT_EVENT_SIGNED_OUT),
                NAMEMAP_ELEMENT(VIDYO_CLIENT_OUT_EVENT_ROOM_LINK),
                NAMEMAP_ELEMENT(VIDYO_CLIENT_OUT_EVENT_CLIENT_VERSION_ON_SERVER),
                NAMEMAP_ELEMENT(VIDYO_CLIENT_OUT_EVENT_ENABLED_STATUS_BAR),
                NAMEMAP_ELEMENT(VIDYO_CLIENT_OUT_EVENT_ENABLED_BUTTON_BAR),
                NAMEMAP_ELEMENT(VIDYO_CLIENT_OUT_EVENT_SELECTED_PARTICIPANTS_CHANGED),
                NAMEMAP_ELEMENT(VIDYO_CLIENT_OUT_EVENT_CALL_STATE),
                NAMEMAP_ELEMENT(VIDYO_CLIENT_OUT_EVENT_GROUP_CHAT),
                NAMEMAP_ELEMENT(VIDYO_CLIENT_OUT_EVENT_PRIVATE_CHAT),
                NAMEMAP_ELEMENT(VIDYO_CLIENT_OUT_EVENT_ALARMS_RAISED),
                NAMEMAP_ELEMENT(VIDYO_CLIENT_OUT_EVENT_ALARMS_CLEARED),
                NAMEMAP_ELEMENT(VIDYO_CLIENT_OUT_EVENT_PORTAL_PREFIX),
                NAMEMAP_ELEMENT(VIDYO_CLIENT_OUT_EVENT_PORTAL_FEATURES),
                NAMEMAP_ELEMENT(VIDYO_CLIENT_OUT_EVENT_CPU_USAGE_ACTIONS),
                NAMEMAP_ELEMENT(VIDYO_CLIENT_OUT_EVENT_LECTURE_MODE_STAGE),
                NAMEMAP_ELEMENT(VIDYO_CLIENT_OUT_EVENT_LECTURE_MODE_PRESENTER_CHANGED),
                NAMEMAP_ELEMENT(VIDYO_CLIENT_OUT_EVENT_LECTURE_HAND_CLEARED),
                NAMEMAP_ELEMENT(VIDYO_CLIENT_OUT_EVENT_AUDIO_FRAME_RECEIVED),
                NAMEMAP_ELEMENT(VIDYO_CLIENT_OUT_EVENT_VIDEO_FRAME_RECEIVED),
                NAMEMAP_ELEMENT(VIDYO_CLIENT_OUT_EVENT_CONTROL_CONFERENCE_ROOM_SETTINGS),
                NAMEMAP_ELEMENT(VIDYO_CLIENT_OUT_EVENT_PRECALL_TEST_DEVICE),
                NAMEMAP_ELEMENT(VIDYO_CLIENT_OUT_EVENT_SHARED_WINDOW_CLOSED),
                NAMEMAP_ELEMENT(VIDYO_CLIENT_OUT_EVENT_SHARED_SCREEN_DISCONNECTED),

                NAMEMAP_PRIVATE_ELEMENT(VIDYO_CLIENT_PRIVATE_OUT_EVENT_SHARED_APP_ADD),
                NAMEMAP_PRIVATE_ELEMENT(VIDYO_CLIENT_PRIVATE_OUT_EVENT_SHARED_APP_REM),
                NAMEMAP_PRIVATE_ELEMENT(VIDYO_CLIENT_PRIVATE_OUT_EVENT_ALARMS_RAISED),
                NAMEMAP_PRIVATE_ELEMENT(VIDYO_CLIENT_PRIVATE_OUT_EVENT_ALARMS_CLEARED),
                NAMEMAP_PRIVATE_ELEMENT(VIDYO_CLIENT_PRIVATE_OUT_EVENT_PARTICIPANTS_LIMIT_BANDW),
                NAMEMAP_PRIVATE_ELEMENT(VIDYO_CLIENT_PRIVATE_OUT_EVENT_PARTICIPANTS_LIMIT_SAFE),
                NAMEMAP_PRIVATE_ELEMENT(VIDYO_CLIENT_PRIVATE_OUT_EVENT_PARTICIPANTS_CHANGED),
                NAMEMAP_PRIVATE_ELEMENT(VIDYO_CLIENT_PRIVATE_OUT_EVENT_GET_CONFIGURATION_RESPONSE),
                NAMEMAP_PRIVATE_ELEMENT(VIDYO_CLIENT_PRIVATE_OUT_EVENT_VCSOAP),
                NAMEMAP_PRIVATE_ELEMENT(VIDYO_CLIENT_PRIVATE_OUT_EVENT_PORTAL_DATA_CACHE),
                NAMEMAP_PRIVATE_ELEMENT(VIDYO_CLIENT_PRIVATE_OUT_EVENT_PORTAL_COMMUNICATION_STATUS),
                NAMEMAP_PRIVATE_ELEMENT(VIDYO_CLIENT_PRIVATE_OUT_EVENT_SIGNIN_UPDATE),
                NAMEMAP_PRIVATE_ELEMENT(VIDYO_CLIENT_PRIVATE_OUT_EVENT_LOGIN_STARTED),
                NAMEMAP_PRIVATE_ELEMENT(VIDYO_CLIENT_PRIVATE_OUT_EVENT_NO_LOGIN_CREDENTIAL),
                NAMEMAP_PRIVATE_ELEMENT(VIDYO_CLIENT_PRIVATE_OUT_EVENT_KIOSK_DEVICE_STATE_CHANGED),
                NAMEMAP_PRIVATE_ELEMENT(VIDYO_CLIENT_PRIVATE_OUT_EVENT_LOW_MASTER_VOLUME),
                NAMEMAP_PRIVATE_ELEMENT(VIDYO_CLIENT_PRIVATE_OUT_EVENT_VISCA),
                NAMEMAP_PRIVATE_ELEMENT(VIDYO_CLIENT_PRIVATE_OUT_EVENT_SWB),
                NAMEMAP_PRIVATE_ELEMENT(VIDYO_CLIENT_PRIVATE_OUT_EVENT_USB_CAPTURE),
                NAMEMAP_PRIVATE_ELEMENT(VIDYO_CLIENT_PRIVATE_OUT_EVENT_CHANGE_FOCUS),
                NAMEMAP_PRIVATE_ELEMENT(VIDYO_CLIENT_PRIVATE_OUT_EVENT_NETWORK_CONTROL),
                NAMEMAP_PRIVATE_ELEMENT(VIDYO_CLIENT_PRIVATE_OUT_EVENT_CHANGE_CAMERA_MODE),
                NAMEMAP_PRIVATE_ELEMENT(VIDYO_CLIENT_PRIVATE_OUT_EVENT_FRAMEWORK_DONE),
                NAMEMAP_PRIVATE_ELEMENT(VIDYO_CLIENT_PRIVATE_OUT_EVENT_ADJUST_MONITORS),
                NAMEMAP_PRIVATE_ELEMENT(VIDYO_CLIENT_PRIVATE_OUT_EVENT_SET_VISCA),
                NAMEMAP_PRIVATE_ELEMENT(VIDYO_CLIENT_PRIVATE_OUT_EVENT_REMOTE_SOURCE_ADD),
                NAMEMAP_PRIVATE_ELEMENT(VIDYO_CLIENT_PRIVATE_OUT_EVENT_REMOTE_SOURCE_REM),
                NAMEMAP_PRIVATE_ELEMENT(VIDYO_CLIENT_PRIVATE_OUT_EVENT_RENDERER_DOCS_CHANGED),
                NAMEMAP_PRIVATE_ELEMENT(VIDYO_CLIENT_PRIVATE_OUT_EVENT_PLUGIN_CONNECTION_SUCCESS),
                NAMEMAP_PRIVATE_ELEMENT(VIDYO_CLIENT_PRIVATE_OUT_EVENT_PLUGIN_CONNECTION_FAILURE),
                NAMEMAP_PRIVATE_ELEMENT(VIDYO_CLIENT_PRIVATE_OUT_EVENT_GET_WINDOWS_EXT),

            };
            auto ite = NAME_MAP.find(event);
            if (ite == NAME_MAP.end())
                return boost::lexical_cast<std::string>(event);
            return ite->second;
        }
#endif

    }
}