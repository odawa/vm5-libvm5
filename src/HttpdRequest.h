﻿//
//  HttpdRequest.h
//  LibWebRequestListener
//
//  Created by V-0757 on 2015/03/13.
//  Copyright (c) 2015年 V-cube, Inc. All rights reserved.
//

#ifndef __LibWebRequestListener__HttpdRequest__
#define __LibWebRequestListener__HttpdRequest__

#include <stdio.h>


#include <Poco/Net/HTTPRequestHandler.h>
#include <Poco/Net/HTTPRequestHandlerFactory.h>
#include <Poco/Net/HTTPResponse.h>
#include <Poco/Net/HTTPServer.h>
#include <Poco/Net/HTTPServerParams.h>
#include <Poco/Net/HTTPServerRequest.h>
#include <Poco/Net/HTTPServerResponse.h>
#include <Poco/Net/ServerSocket.h>
#include <Poco/Net/HTMLForm.h>
#include <Poco/Util/ServerApplication.h>
#include <Poco/ThreadPool.h>
#include <Poco/Environment.h>
#include <Poco/URI.h>

#include "LocalHTTPD.h"
#include "LocalHTTPD_IF.h"
#include "IHttpd.h"

#include "ts_weak_ptr.h"

#include <V5Lite/IV5HttpdEventSink.h>

#include "RequestListener.h"
#include <V5Lite/RequestListenerCommon.h>

using Poco::Net::NameValueCollection;
using Poco::Net::HTTPRequestHandler;
using Poco::Net::HTTPRequestHandlerFactory;
using Poco::Net::HTTPResponse;
using Poco::Net::HTTPServer;
using Poco::Net::HTTPServerParams;
using Poco::Net::HTTPServerRequest;
using Poco::Net::HTTPServerResponse;
using Poco::Net::ServerSocket;
using Poco::Net::HTMLForm;
using Poco::Util::ServerApplication;
using Poco::ThreadPool;
using Poco::Environment;
using Poco::IOException;
using Poco::URI;

namespace vrms5 { class IV5HttpdEventSink; }

class HttpdRequest
    : public IHttpd
    , public RequestListener
{
public:
    typedef std::shared_ptr<HttpdRequest> self_ptr_t;
    typedef vrms5::utils::ts_weak_ptr<IV5HttpdEventSink> httpCallbackStorageT;
    typedef std::shared_ptr<HttpdRequest> instance_ptr_t;

    typedef LocalHTTPD::HTTPServerResponse HTTPServerResponse;
    typedef LocalHTTPD::requestQuery requestQuery;

    HttpdRequest();
    ~HttpdRequest();
    
    static instance_ptr_t createInstance();

    virtual void setHttpdEventSink( std::weak_ptr<IV5HttpdEventSink> sink ) override;
    virtual V5HTTPD_START HTTPDStart(unsigned short port)  override;
    virtual void HTTPDStop() override;
private:
    bool request_crossdomain(std::shared_ptr<requestQuery> query, HTTPServerResponse& resp);
    bool request_alive(std::shared_ptr<requestQuery> query, HTTPServerResponse& resp);
    bool request_joinConference(std::shared_ptr<requestQuery> query, HTTPServerResponse& resp);
    
    std::shared_ptr<LocalHTTPD> httpd;
    
    httpCallbackStorageT m_httpdEventSink;
};



#endif /* defined(__LibWebRequestListener__HttpdRequest__) */
