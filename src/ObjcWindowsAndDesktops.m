//
//  ObjcWindowsAndDesktops.m
//  LibV5Lite_Mac
//
//  Created by Hiromi on 2015/02/19.
//  Copyright (c) 2015 V-cube, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <V5Lite/ObjcWindowsAndDesktops.hh>
#import "ObjcWindowsAndDesktops.hh"


@implementation ObjcV5Rect : NSObject
@end

@implementation ObjcDesktopInfo : NSObject
@end

@implementation ObjcWindowArray {
    __strong NSArray* _array;
};
- (NSUInteger)count {
    if(_array == nil) {
        return 0;
    }
    return _array.count;
}

- (ObjcWindow *)objectAtIndexedSubscript:(NSUInteger)index {
    return _array[index];
}

- (void)setWindows:(NSArray *)array {
    _array = array;
}

- (void)iterate:(void (^)(ObjcWindow *)) iterateClosure {
    if(_array == nil) {
        return;
    }
    for(ObjcWindow* window in _array) {
        iterateClosure(window);
    }
}
@end

@implementation ObjcDesktopArray{
    NSArray* _array;
};
- (NSUInteger)count {
    if(_array == nil) {
        return 0;
    }
    return _array.count;
}

- (ObjcDesktop *)objectAtIndexedSubscript:(NSUInteger)index {
    return _array[index];
}

- (void)setDesktop:(NSArray *)array {
    _array = array;
}

- (void)iterate:(void (^)(ObjcDesktop *)) iterateClosure {
    if(_array == nil) {
        return;
    }
    for(ObjcDesktop* desktop in _array) {
        iterateClosure(desktop);
    }
}
@end


@implementation ObjcWindowsAndDesktops : NSObject
- (instancetype)init {
    self = [super init];
    if (self) {
        _windowList = [[ObjcWindowArray alloc] init];
        _desktopList = [[ObjcDesktopArray alloc] init];
    }
    return self;
}
@end

@implementation ObjcWindow : NSObject
@end

@implementation ObjcDesktop : NSObject
@end
