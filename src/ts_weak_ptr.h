﻿#pragma once
#ifndef TS_WEAK_PTR_H_INCLUDED
#define TS_WEAK_PTR_H_INCLUDED

#include <V5lite/LibV5LiteCommon.h>
#include <memory>

#ifdef V5_CLANG
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wconversion"
#endif

#include <boost/thread.hpp>

#ifdef V5_CLANG
#pragma clang diagnostic pop
#endif

namespace vrms5 {

    namespace utils {

        template <typename T> class ts_weak_ptr;

        namespace detail {

            class ts_weak_ptr_base
            {
                template <typename T> friend class vrms5::utils::ts_weak_ptr;
                
                typedef boost::shared_mutex                             shared_mutex;
                typedef boost::upgrade_lock<shared_mutex>               upgrade_lock;
                typedef boost::upgrade_to_unique_lock<shared_mutex>     write_lock;
                typedef boost::shared_lock<shared_mutex>                read_lock;

                static shared_mutex mtx;
            };
        }

        // スレッドセーフな weak_ptr
        // ts_weak_ptr への読み書きや lock がスレッドセーフである
        // ポインタのさし先に対するスレッドセーフを保障するものではない
        template <typename T>
        class ts_weak_ptr : detail::ts_weak_ptr_base
        {
            std::weak_ptr<T> wptr;


        public:
            ts_weak_ptr()
            {
            }

            ts_weak_ptr(ts_weak_ptr<T> const& r) : wptr(r.clone())
            {
            }
            
            template <typename Y>
            ts_weak_ptr(ts_weak_ptr<Y> const& r) : wptr(r.clone())
            {
            }

            ts_weak_ptr(ts_weak_ptr<T>&& r) : wptr(std::move(r.wptr))   // 無名変数はロック不要
            {
            }

            template <typename Y>
            ts_weak_ptr(ts_weak_ptr<Y>&& r) : wptr(std::move(r.wptr))   // 無名変数はロック不要
            {
            }

            template <typename Y>
            ts_weak_ptr(std::weak_ptr<Y> const& r) : wptr(r)
            {
            }
            
            template <typename Y>
            ts_weak_ptr(std::weak_ptr<Y>&& r) : wptr(r)
            {
            }

            template <typename Y>
            ts_weak_ptr(std::shared_ptr<Y> const& r) : wptr(r)
            {
            }

            ~ts_weak_ptr()
            {
            }

            ts_weak_ptr& operator=(const ts_weak_ptr<T>& r)
            {
                return operator=<T>(r.clone());
            }

            template< class Y >
            ts_weak_ptr& operator=(const ts_weak_ptr<Y>& r)
            {
                return operator = <Y>(r.clone());
            }

            ts_weak_ptr& operator=(ts_weak_ptr<T>&& r)
            {
                return operator = <T>(std::move(r.wptr));  // 無名変数はロック不要
            }

            template< class Y >
            ts_weak_ptr& operator=(ts_weak_ptr<Y>&& r)
            {
                return operator = <T>(std::move(r.wptr));  // 無名変数はロック不要
            }

            template< class Y >
            ts_weak_ptr& operator=(const std::weak_ptr<Y>& r)
            {
                upgrade_lock ulock(mtx);
                write_lock l(ulock);
                wptr = r;
                return *this;
            }

            template< class Y >
            ts_weak_ptr& operator=(std::weak_ptr<Y>&& r)
            {
                upgrade_lock ulock(mtx);
                write_lock l(ulock);
                wptr = r;
                return *this;
            }


            template< class Y >
            ts_weak_ptr& operator=(const std::shared_ptr<Y>& r)
            {
                upgrade_lock ulock(mtx);
                write_lock l(ulock);
                wptr = r;
                return *this;
            }

            void reset()
            {
                upgrade_lock ulock(mtx);
                write_lock l(ulock);
                wptr.reset();
            }

            long user_count() const
            {
                read_lock read_lock(mtx);
                return wptr.use_count();
            }

            bool expired() const
            {
                read_lock read_lock(mtx);
                return wptr.expired();
            }

            std::shared_ptr<T> lock() const
            {
                return clone().lock();
            }

        private:
            std::weak_ptr<T> clone() const
            {
                read_lock read_lock(mtx);
                return wptr;
            }

            void swap();        // スレッドセーフな swap は実現が難しいため対応しない

        };



    }
}

#endif
