﻿#include "precompile.h"
#include "V5ConferenceStatistics.h"

#include "VidyoClientAPI.h"

using namespace vrms5::vidyo;


V5StatisticsInfo::V5StatisticsInfo(  )
: m_io_service( new boost::asio::io_service() )
, m_work_handler( new boost::asio::io_service::work(*m_io_service) )
, m_strand( std::make_shared<boost::asio::strand>( *m_io_service ) )
, m_millisecs( 5000 )
{
    m_getStatisticsFlg = false;
    m_io_thread = std::make_shared<boost::thread>( boost::bind( &io_service::run, m_io_service ) );
}

V5StatisticsInfo::V5StatisticsInfo( ConferenceCallbackStorageT sink )
{
    V5StatisticsInfo( 5000, sink );
}

V5StatisticsInfo::V5StatisticsInfo(int millisecs, ConferenceCallbackStorageT sink )
: m_io_service( new boost::asio::io_service() )
, m_work_handler( new boost::asio::io_service::work(*m_io_service) )
, m_strand( std::make_shared<boost::asio::strand>( *m_io_service ) )

{
    m_conferenceCallback = sink;
    m_millisecs = millisecs;
    m_getStatisticsFlg = false;
    
    m_io_thread = std::make_shared<boost::thread>( boost::bind( &io_service::run, m_io_service ) );
}

V5StatisticsInfo::~V5StatisticsInfo()
{
    m_work_handler.reset();
    if( m_io_thread )
    {
        m_io_thread->join();
    }
    delete m_io_service;
    
}

void V5StatisticsInfo::setConferenceCallback(std::nullptr_t)
{
    V5LOG(V5_LOG_INFO,"[V5StatisticsInfo setConferenceCallback(nullptr)] ---> in");
    m_conferenceCallback.reset();
    V5LOG(V5_LOG_INFO,"[V5StatisticsInfo setConferenceCallback(nullptr)] <--- out");
}

void V5StatisticsInfo::setConferenceCallback(ConferenceCallbackStorageT sink)
{
    V5LOG(V5_LOG_INFO,"[V5StatisticsInfo setConferenceCallback] ---> in");
    m_conferenceCallback = sink;
    V5LOG(V5_LOG_INFO,"[V5StatisticsInfo setConferenceCallback] <--- out");
}

void V5StatisticsInfo::setWaitTime( int millisecs )
{
    V5LOG(V5_LOG_INFO,"[V5StatisticsInfo setWaitTime] ---> in");
    m_millisecs = millisecs;
    V5LOG(V5_LOG_INFO,"[V5StatisticsInfo setWaitTime] <--- out");
}

bool V5StatisticsInfo::startStatisticsInfo( int millisecs )
{
    m_millisecs = millisecs;
    return startStatisticsInfo();
}


bool V5StatisticsInfo::startStatisticsInfo()
{
    V5LOG(V5_LOG_INFO,"[V5StatisticsInfo startStatisticsInfo] ---> in");

    if( m_getStatisticsFlg )
    {
        return true;
    }
    
    m_getStatisticsFlg = true;
    lock_guard_t lock(mtx);
    
    // 起動直後の初回分情報を取得する
    bool ret = getStatisticsInfoFuncCall();
    if( ret )
    {
        auto callback_p = m_conferenceCallback.lock();
        if( callback_p )
        {
            callback_p->onConferenceStatisticsNotice( *std::atomic_load( &(this->m_confStatInfo) ) );
            V5LOG(V5_LOG_INFO,"onConferenceStatisticsNotice called");
        }
    }
    else
    {
        // Vidyoの統計情報関連のAPI呼び出しに失敗した場合、
        // 次回以降の呼び出しを止める。
        m_getStatisticsFlg = false;
        return false;
    }
    
    m_timerPtr =
    std::make_shared<deadline_timer>( *m_io_service,
                                      boost::posix_time::milliseconds( m_millisecs )
                                     );
    
    m_timerPtr->async_wait( m_strand->wrap( std::bind( &V5StatisticsInfo::onGetStatisticsInfo,
                                                     this,
                                                     std::placeholders::_1
                                                    )
                                            )
                            );
    
    V5LOG(V5_LOG_INFO,"[V5StatisticsInfo startStatisticsInfo] <--- out");
    return true;
}

bool V5StatisticsInfo::stopStatisticsInfo()
{
    V5LOG(V5_LOG_INFO,"[V5StatisticsInfo stopStatisticsInfo] ---> in");
    
    if( m_getStatisticsFlg != true )
    {
        return true;
    }
    
    m_getStatisticsFlg = false;
    
    if( m_timerPtr )
    {
        m_timerPtr->cancel();
        m_timerPtr = nullptr;
    }
    
    V5LOG(V5_LOG_INFO,"[V5StatisticsInfo stopStatisticsInfo] <--- out");
    return true;
}

/******************************************************************/
/* Private Method */
/******************************************************************/

void V5StatisticsInfo::onGetStatisticsInfo(const boost::system::error_code &error)
{
    V5LOG(V5_LOG_INFO,"[V5StatisticsInfo onGetStatisticsInfo] ---> in");
    
    if( !m_getStatisticsFlg )
    {
        V5LOG(V5_LOG_INFO,"[V5StatisticsInfo onGetStatisticsInfo] <--- out");
        return;
    }
    
    if( error )
    {
        if( error == boost::asio::error::operation_aborted )
        {
            V5LOG(V5_LOG_INFO,"Error Pattern : Timer Cnacel.");
            // タイマーキャンセルしたときに通るルート
            // タイマーキャンセルは情報表示終了の通知なので処理を終了する。
            return;
        }
        else
        {
            V5LOG(V5_LOG_ERROR,"Error Pattern : Not Timer Cnacel.");
        }
    }
    else
    {
        V5LOG(V5_LOG_INFO,"No Error Data.");
    }
    
    bool ret = getStatisticsInfoFuncCall();
    if( ret )
    {
        auto callback_p = m_conferenceCallback.lock();
        if( callback_p )
        {
            callback_p->onConferenceStatisticsNotice( *std::atomic_load( &(this->m_confStatInfo) ) );
            V5LOG(V5_LOG_INFO,"onConferenceStatisticsNotice called");
        }
    }
    else
    {
        // Vidyoの統計情報関連のAPI呼び出しに失敗した場合、
        // 次回以降の呼び出しを止める。
        m_getStatisticsFlg = false;
        return;
    }
        
    // タイマー再登録
    m_timerPtr =
    std::make_shared<deadline_timer>( *m_io_service,
                                      boost::posix_time::milliseconds( m_millisecs )
                                     );

    m_timerPtr->async_wait( m_strand->wrap( std::bind( &V5StatisticsInfo::onGetStatisticsInfo,
                                                     this,
                                                     std::placeholders::_1
                                                    )
                                         )
                         );
    
    
    V5LOG(V5_LOG_INFO,"[V5StatisticsInfo onGetStatisticsInfo] <--- out");
}

bool V5StatisticsInfo::getStatisticsInfoFuncCall()
{
    V5LOG(V5_LOG_INFO,"[V5StatisticsInfo getStatisticsInfoFuncCall] ---> in");
    
    // 起動されるタイミングがわからないため、
    // VIDYO-APIの起動結果が失敗だった場合でも
    // そのまま処理を継続する。
    
    auto confStatInfoCpy = std::make_shared<V5ConferenceStatisticsAllInfo>();
    
    VidyoClientRequestCommunicationStatus comm_status = {0};
    if(VidyoClient::sendRequest(VIDYO_CLIENT_REQUEST_GET_COMMUNICATION_STATUS, &comm_status) != VIDYO_CLIENT_ERROR_OK)
    {
        V5LOG(V5_LOG_ERROR, "Get Communication Status Failed.\n");
    }
    else
    {
        auto comInfoPtr = &confStatInfoCpy->communicationInfo;
        comInfoPtr->vmCommunicationStatus          = (comm_status.vmCommunicationStatus        == VIDYO_TRUE);
        comInfoPtr->vmCommunicationViaVidyoProxy   = (comm_status.vmCommunicationViaVidyoProxy == VIDYO_TRUE);
        comInfoPtr->vmCommunicationViaWebProxy     = (comm_status.vmCommunicationViaWebProxy   == VIDYO_TRUE);
        comInfoPtr->vrCommunicationStatus          = (comm_status.vrCommunicationStatus        == VIDYO_TRUE);
        comInfoPtr->vrCommunicationViaVidyoProxy   = (comm_status.vrCommunicationViaVidyoProxy == VIDYO_TRUE);
        comInfoPtr->vrCommunicationViaWebProxy     = (comm_status.vrCommunicationViaWebProxy   == VIDYO_TRUE);
    }
    
    VidyoClientRequestGetEndpointStatus endp_status;
    endp_status.endPointStatus = VIDYO_CLIENT_ENDPOINT_STATUS_INACTIVE;
    if(VidyoClient::sendRequest(VIDYO_CLIENT_REQUEST_GET_ENDPOINT_STATUS, endp_status) != VIDYO_CLIENT_ERROR_OK)
    {
        V5LOG(V5_LOG_ERROR, "Get Endpoint Status Failed.\n");
    }
    else
    {
        confStatInfoCpy->endpointStatus.endPointStatus = endp_status.endPointStatus;
    }
    
    VidyoClientRequestBandwidthInfo bw_info = {0};
    if(VidyoClient::sendRequest(VIDYO_CLIENT_REQUEST_GET_BANDWIDTH_INFO, &bw_info) != VIDYO_CLIENT_ERROR_OK)
    {
        V5LOG(V5_LOG_ERROR, "Get Bandwidth Info Failed.\n");
    }
    else
    {
        auto bwInfoPtr = &confStatInfoCpy->bandwidthInfo;
        bwInfoPtr->AvailSendBwVideo          = bw_info.AvailSendBwVideo;
        bwInfoPtr->AvailSendBwAudio          = bw_info.AvailSendBwAudio;
        bwInfoPtr->AvailSendBwApplication    = bw_info.AvailSendBwApplication;
        bwInfoPtr->AvailSendBwMax            = bw_info.AvailSendBwMax;
        bwInfoPtr->ActualSendBwVideo         = bw_info.ActualSendBwVideo;
        bwInfoPtr->ActualSendBwAudio         = bw_info.ActualSendBwAudio;
        bwInfoPtr->ActualSendBwApplication   = bw_info.ActualSendBwApplication;
        bwInfoPtr->ActualSendBwMax           = bw_info.ActualSendBwMax;
        bwInfoPtr->AvailRecvBwVideo          = bw_info.AvailRecvBwVideo;
        bwInfoPtr->AvailRecvBwAudio          = bw_info.AvailRecvBwAudio;
        bwInfoPtr->AvailRecvBwApplication    = bw_info.AvailRecvBwApplication;
        bwInfoPtr->AvailRecvBwMax            = bw_info.AvailRecvBwMax;
        bwInfoPtr->ActualRecvBwVideo         = bw_info.ActualRecvBwVideo;
        bwInfoPtr->ActualRecvBwAudio         = bw_info.ActualRecvBwAudio;
        bwInfoPtr->ActualRecvBwApplication   = bw_info.ActualRecvBwApplication;
        bwInfoPtr->ActualRecvBwMax           = bw_info.ActualRecvBwMax;
    }
    
    VidyoClientRequestConferenceInfo conf_info = {0};
    if(VidyoClient::sendRequest(VIDYO_CLIENT_REQUEST_GET_CONFERENCE_INFO, &conf_info) != VIDYO_CLIENT_ERROR_OK)
    {
        V5LOG(V5_LOG_ERROR, "Get Conference Info Failed.\n");
    }
    else
    {
        auto confInfoPtr = &confStatInfoCpy->conferenceInfo;
        confInfoPtr->recording = (conf_info.recording != VIDYO_FALSE);
        confInfoPtr->webcast = (conf_info.webcast != VIDYO_FALSE);
    }
    
    VidyoClientRequestConnectivityInfo conn_info = {0};
    if(VidyoClient::sendRequest(VIDYO_CLIENT_REQUEST_GET_CONNECTIVITY_INFO, &conn_info) != VIDYO_CLIENT_ERROR_OK)
    {
        V5LOG(V5_LOG_ERROR, "Get Conference Info Failed.\n");
    }
    else
    {
        auto connctivityPtr = &confStatInfoCpy->connectivity;
        
        memcpy( connctivityPtr->serverAddress,
                conn_info.serverAddress,
                V5_SIGNIN_SERVERADDRESS_SIZE );
        
        memcpy( connctivityPtr->serverPort,
                conn_info.serverPort,
                V5_SIGNIN_SERVERPORT_SIZE );
        
        memcpy( connctivityPtr->vmIdentity,
                conn_info.vmIdentity,
                V5_VM_ID_SIZE );
        
        memcpy( connctivityPtr->userName,
                conn_info.userName,
                V5_SIGNIN_USERNAME_SIZE );
        
        memcpy( connctivityPtr->portalAddress,
                conn_info.portalAddress,
                V5_SIGNIN_PORTALADDRESS_SIZE );
        
        memcpy( connctivityPtr->portalVersion,
                conn_info.portalVersion,
                V5_SIGNIN_PORTALVERSION_SIZE );
        
        memcpy( connctivityPtr->locationTag,
                conn_info.locationTag,
                V5_ENDPOINT_LOCATION_TAG_SIZE );

        memcpy( connctivityPtr->vidyoProxyAddress,
                conn_info.vidyoProxyAddress,
                V5_SIGNIN_VIDYO_PROXY_ADDRESS_SIZE );
        
        memcpy( connctivityPtr->vidyoProxyPort,
                conn_info.vidyoProxyPort,
                V5_SIGNIN_VIDYO_PROXY_PORT_SIZE );
        
        memcpy( connctivityPtr->clientExternalIPAddress,
                conn_info.clientExternalIPAddress ,
                V5_MAX_IPADDRESS_LEN );
        
        memcpy( connctivityPtr->reverseProxyAddress,
                conn_info.reverseProxyAddress,
                V5_SIGNIN_VIDYO_PROXY_ADDRESS_SIZE );
        
        memcpy( connctivityPtr->reverseProxyPort,
                conn_info.reverseProxyPort,
                V5_SIGNIN_VIDYO_PROXY_PORT_SIZE );
        
        connctivityPtr->serverSecured = (conn_info.serverSecured != VIDYO_FALSE);
        connctivityPtr->guestLogin = (conn_info.guestLogin != VIDYO_FALSE);
        connctivityPtr->proxyType     = conn_info.proxyType;

    }
    
    
    VidyoClientRequestSessionDisplayInfo disp_info;
    disp_info.sessionDisplayContext = VIDYO_CLIENT_SESSION_DISPLAY_CONTEXT_IDLE;
    memset( disp_info.sessionDisplayText, 0x00, V5_DISPLAY_CONFERENCE_SIZE );
    if(VidyoClient::sendRequest(VIDYO_CLIENT_REQUEST_GET_CURRENT_SESSION_DISPLAY_INFO, &disp_info) != VIDYO_CLIENT_ERROR_OK)
    {
        V5LOG(V5_LOG_ERROR, "Get Current Session Display Info Failed.\n");
    }
    else
    {
        auto displayInfoPtr = &confStatInfoCpy->sessionDisplayInfo;
        displayInfoPtr->sessionDisplayContext = disp_info.sessionDisplayContext;
        memcpy( displayInfoPtr->sessionDisplayText,
                disp_info.sessionDisplayText,
                V5_DISPLAY_CONFERENCE_SIZE );
    }

    VidyoClientRequestMediaInfo media_info = {0};
    if(VidyoClient::sendRequest(VIDYO_CLIENT_REQUEST_GET_MEDIA_INFO, &media_info) != VIDYO_CLIENT_ERROR_OK)
    {
        V5LOG(V5_LOG_ERROR, "Get Media Info Failed.\n");
    }
    else
    {
        auto mediaInfoPtr = &confStatInfoCpy->mediaInfo;
        mediaInfoPtr->numIFrames = media_info.numIFrames;
        mediaInfoPtr->numFirs    = media_info.numFirs;
        mediaInfoPtr->numNacks   = media_info.numNacks;
        mediaInfoPtr->mediaRTT   = media_info.mediaRTT;
    }
    
    VidyoClientRequestParticipantInfo par_info = {0};
    if(VidyoClient::sendRequest(VIDYO_CLIENT_REQUEST_GET_PARTICIPANT_INFO, &par_info) != VIDYO_CLIENT_ERROR_OK)
    {
        V5LOG(V5_LOG_ERROR, "Get Participant Info Failed.\n");
    }
    else
    {
        auto participantInfoPtr = &confStatInfoCpy->participantInfo;
        
        participantInfoPtr->numberParticipants = par_info.numberParticipants;
        
        for (unsigned long i = 0; i < participantInfoPtr->numberParticipants && i < V5_MAX_PARTICIPANTS_NUM; i++)
        {
            memcpy( participantInfoPtr->Name[i],
                    par_info.Name[i],
                    V5_FIELD_SIZE );
            
            memcpy( participantInfoPtr->URI[i],
                    par_info.URI[i],
                    V5_URI_LEN );
            
            participantInfoPtr->bytesRcvd[i]             = par_info.bytesRcvd[i];
            participantInfoPtr->numFirsSent[i]           = par_info.numFirsSent[i];
            participantInfoPtr->numNacksSent[i]          = par_info.numNacksSent[i];
            participantInfoPtr->numDistinctNacksSent[i]  = par_info.numDistinctNacksSent[i];
            participantInfoPtr->receivedFrameRate[i]     = par_info.receivedFrameRate[i];
            participantInfoPtr->decodedFrameRate[i]      = par_info.decodedFrameRate[i];
            participantInfoPtr->displayedFrameRate[i]    = par_info.displayedFrameRate[i];
            participantInfoPtr->receivedPacketRate[i]    = par_info.receivedPacketRate[i];
            participantInfoPtr->receivedBpsVideo[i]      = par_info.receivedBpsVideo[i];
            participantInfoPtr->receivedBpsAudio[i]      = par_info.receivedBpsAudio[i];
            participantInfoPtr->receivedWidth[i]         = par_info.receivedWidth[i];
            participantInfoPtr->receivedHeight[i]        = par_info.receivedHeight[i] ;
            participantInfoPtr->receivedBytesVideo[i]    = par_info.receivedBytesVideo[i];
            participantInfoPtr->receivedBytesAudio[i]    = par_info.receivedBytesAudio[i];
            
        }
    }
    
    VidyoClientRequestRateShaperInfo rs_info = {0};
    if(VidyoClient::sendRequest(VIDYO_CLIENT_REQUEST_GET_RATE_SHAPER_INFO, &rs_info) != VIDYO_CLIENT_ERROR_OK)
    {
        V5LOG(V5_LOG_ERROR, "Get Rate Shaper Info Failed.\n");
    }
    else
    {
        auto rateShaperPtr = &confStatInfoCpy->rateShaperInfo;
        
        rateShaperPtr->delayVideoPriorytyNormal          = rs_info.delayVideoPriorytyNormal;
        rateShaperPtr->numPacketsVideoPriorytyNormal     = rs_info.numPacketsVideoPriorytyNormal;
        rateShaperPtr->numFramesVideoPriorytyNormal      = rs_info.numFramesVideoPriorytyNormal;
        rateShaperPtr->numDroppedVideoPriorytyNormal     = rs_info.numDroppedVideoPriorytyNormal;
        rateShaperPtr->delayVideoPriorutyRetransmit      = rs_info.delayVideoPriorutyRetransmit;
        rateShaperPtr->numPacketsVideoPriorutyRetransmit = rs_info.numPacketsVideoPriorutyRetransmit;
        rateShaperPtr->numFramesVideoPriorutyRetransmit  = rs_info.numFramesVideoPriorutyRetransmit;
        rateShaperPtr->numDroppedVideoPriorutyRetransmit = rs_info.numDroppedVideoPriorutyRetransmit;
        rateShaperPtr->delayAppPriorityNormal            = rs_info.delayAppPriorityNormal;
        rateShaperPtr->numPacketsAppPriorityNormal       = rs_info.numPacketsAppPriorityNormal;
        rateShaperPtr->numFramesAppPriorityNormal        = rs_info.numFramesAppPriorityNormal;
        rateShaperPtr->numDroppedAppPriorityNormal       = rs_info.numDroppedAppPriorityNormal;
        rateShaperPtr->delayAppPriorityRetransmit        = rs_info.delayAppPriorityRetransmit;
        rateShaperPtr->numPacketsAppPriorityRetransmit   = rs_info.numPacketsAppPriorityRetransmit;
        rateShaperPtr->numFramesAppPriorityRetransmit    = rs_info.numFramesAppPriorityRetransmit;
        rateShaperPtr->numDroppedAppPriorityRetransmit   = rs_info.numDroppedAppPriorityRetransmit;
        
    }
    
    VidyoClientRequestFrameRateInfo fr_info = {0};
    if(VidyoClient::sendRequest(VIDYO_CLIENT_REQUEST_GET_VIDEO_FRAME_RATE_INFO, &fr_info) != VIDYO_CLIENT_ERROR_OK)
    {
        V5LOG(V5_LOG_ERROR, "Get Frame Rate Info Failed.\n");
    }
    else
    {
        auto frameRateInfoPtr = &confStatInfoCpy->frameRateInfo;
        frameRateInfoPtr->captureFrameRate  = fr_info.captureFrameRate;
        frameRateInfoPtr->encodeFrameRate   = fr_info.encodeFrameRate;
        frameRateInfoPtr->sendFrameRate     = fr_info.sendFrameRate;
        
    }
    
    VidyoClientRequestParticipantStatisticsList pstat_list = {0};
    pstat_list.startIndex = 0;
    if(VidyoClient::sendRequest(VIDYO_CLIENT_REQUEST_GET_PARTICIPANT_STATISTICS_LIST, &pstat_list) != VIDYO_CLIENT_ERROR_OK)
    {
        V5LOG(V5_LOG_ERROR, "Get Frame Rate Info Failed.\n");
    }
    else
    {
        auto participantStatListPtr = &confStatInfoCpy->participantStatisticsList;
        
        // startIndexパラメータはINパラメータのため取得しない。
        participantStatListPtr->numParticipants = pstat_list.numParticipants;
        
        for( unsigned int i = 0 ;
             i < participantStatListPtr->numParticipants && i < V5_MAX_GET_PARTICIPANT_STATISTICS;
             i++ )
        {
            auto savePlistStatPtr = &participantStatListPtr->statistics[i];
            auto dataPlistStatPtr = &pstat_list.statistics[i];
            
            memcpy( savePlistStatPtr->uri,
                    dataPlistStatPtr->uri,
                    V5_MAX_URI_LEN );
            
            memcpy( savePlistStatPtr->name,
                    dataPlistStatPtr->name,
                    V5_MAX_URI_LEN );
            
            savePlistStatPtr->videoResolution.xPos      = dataPlistStatPtr->videoResolution.xPos;
            savePlistStatPtr->videoResolution.yPos      = dataPlistStatPtr->videoResolution.yPos;
            savePlistStatPtr->videoResolution.width     = dataPlistStatPtr->videoResolution.width;
            savePlistStatPtr->videoResolution.height    = dataPlistStatPtr->videoResolution.height;
            savePlistStatPtr->videoKBitsPerSecRecv      = dataPlistStatPtr->videoKBitsPerSecRecv;
            savePlistStatPtr->audioKBitsPerSecRecv      = dataPlistStatPtr->audioKBitsPerSecRecv;
            savePlistStatPtr->firs                      = dataPlistStatPtr->firs;
            savePlistStatPtr->nacks                     = dataPlistStatPtr->nacks;
            savePlistStatPtr->videoFrameRate            = dataPlistStatPtr->videoFrameRate;
            savePlistStatPtr->videoDecodedFrameRate     = dataPlistStatPtr->videoDecodedFrameRate;
            savePlistStatPtr->videoDisplayedFrameRate   = dataPlistStatPtr->videoDisplayedFrameRate;
            
        }
        
    }
    
    std::atomic_store(&(this->m_confStatInfo), confStatInfoCpy);
    
    V5LOG(V5_LOG_INFO,"[V5StatisticsInfo getStatisticsInfoFuncCall] <--- out");
    return true;
}

