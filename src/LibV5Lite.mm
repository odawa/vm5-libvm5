//
//  NSObject+LibV5Lite.m
//  LibV5Lite
//
//  Created by V-1094 on 2015/01/14.
//  Copyright (c) 2015 V-cube, Inc. All rights reserved.
//
#import <V5Lite/V5Client.h>
#import <V5Lite/LibV5Lite.hh>

#include <V5Lite/IV5ConferenceEventSink.h>
#include <V5Lite/IV5ChatEventSink.h>

void CNSLog(const char * __restrict file,
            int line,
            const char * __restrict function,
            const char * __restrict format,
            ...)
{
    va_list args;
    va_start(args, format);
    
    NSString *fmt = [NSString stringWithFormat:@"%s(%d): %s", file, line, format];
    if (![fmt hasSuffix: @"\n"])
    {
        fmt = [fmt stringByAppendingString: @"\n"];
    }
    
    NSLogv(fmt, args);
    va_end(args);
}


@implementation V5DevList: NSObject

- (V5DevList *)initWithLength: (int)length
{
    if( self = [super init])
    {
        _leng = length;
    }
    return (V5DevList *)self;
}

- (void)setArrays: (NSArray *)arr1 arr2:(NSArray *)arr2 index:(int)index type:(V5DeviceType)type
{
    _name    = arr1;
    _phyName = arr2;
    _index   = index;
    _type    = type;
}

- (int)length
{
    return _leng;
}

- (NSString *)description
{
    return [NSString stringWithFormat: @"name: %@, phyname: %@",
            [NSString stringWithCString:[_name.description cStringUsingEncoding:NSASCIIStringEncoding] encoding:NSNonLossyASCIIStringEncoding],
            [NSString stringWithCString:[_phyName.description cStringUsingEncoding:NSASCIIStringEncoding] encoding:NSNonLossyASCIIStringEncoding]];
}
@end

@interface LibV5Lite()
@property std::shared_ptr<vrms5::V5Client> cli;

@end

// Callback Wrapper for Objective-C(Swift)
class V5ConferenceEventSink : public IV5ConferenceEventSink{
public:
    V5ConferenceEventSink(id<IV5ConferenceEventSinkProtocol> protocol) {
        _protocol = protocol;
        isFirstParticipantJoined = YES;
    }
    
    virtual ~V5ConferenceEventSink() {
        
    }
    
    virtual void onSessionLinked( const char *displayName, any_arg logoUrl ) override;
    virtual void onBeginSessionFailed(V5ErrorInfo const& errInfo) override;
    virtual void onVersionUnmatched( const char *requireVersion ) override;
    virtual void onParticipantJoined(V5Participant const& participant) override;
    virtual void onParticipantLeaved(V5Participant const& participant) override;
    virtual void onReceiveJoinConferenceRequest(const char *token, const char *V5LiteURL) override;
    virtual void onJoinConferenceFailed(V5ErrorInfo const& errInfo) override;
    virtual void onReceiveVideoFrame(VideoRawFrame const& rawFrame) override;
    virtual void onJoinedConference(V5ConferenceInfo const& conferenceInfo ) override;
    virtual void onLeavedConference(V5ErrorInfo const& errInfo) override;
    virtual void onParticipantListUpdated( const V5Participant * const participants[], unsigned long length ) override;
    virtual void onConferenceStatusUpdated(V5ConferenceStatus const& conferenceStatus ) override;
    virtual void onAddShare( const char * uri, bool isSelfShare ) override;
    virtual void onRemoveShare( const char * uri, bool isSelfShare ) override;
    virtual void onRemoteSourceAdded(V5RemoteSourceChanged const& change) override;
    virtual void onRemoteSourceRemoved(V5RemoteSourceChanged const& change) override;
    virtual void onSelectedParticipantsUpdated(std::vector<std::string> const& selectedURI) override;
    virtual void onReconnectConference( V5ConferenceInfo const& conferenceInfo ) override;

    virtual void onRcvRejectParticipant( V5Participant const& participant ) override;
    virtual void onPastTimeStart() override;
    virtual void onPastTimeStop() override;
    virtual void onAsyncError(V5ErrorInfo const& errInfo) override;
    virtual void onInviteMailIsSent(V5ErrorInfo const& errInfo, const char* status, const char* mailAddress) override;

    // Windows側のみ必要な機能とおぼしき機能がライブラリの根幹についかされていて、
    // iOS,Macでコンパイルが通らないため、処理確認までは暫定的に追加しておく。
    virtual void onFloatingWindow(V5WindowId window) override;

    virtual void onConferenceNameChangeFailed( const char* nowConferenceName ) override;
    virtual void onNowMaintenance( long long maintenanceStartTime, long long maintenanceEndTime ) override;
    virtual void onConferenceStatisticsNotice( V5ConferenceStatisticsAllInfo const& confStatInfo ) override;
    virtual void onConferenceEndTimeOverNotice( long minutes ) override;
    
    virtual void onLibFatalError(V5ErrorInfo const& errInfo) override;
    
private:
    id convertToId(any_arg obj);
    
private:
    id<IV5ConferenceEventSinkProtocol> _protocol;
    BOOL isFirstParticipantJoined;
};

void V5ConferenceEventSink::onParticipantJoined(const V5Participant &participant) {
    if(_protocol) {
        NSLog(@"onParticipantJoined[ObjC]");
#if 0
        NSLog(@"[Participant Data]");
        NSLog(@"displayName : %s",participant.displayName);
        NSLog(@"id          : %s",participant.id);
#endif
        ObjcParticipant *objcParticipant = [[ObjcParticipant alloc] init];
        [objcParticipant setParticipant:&participant];
        [_protocol onParticipantJoined:objcParticipant];
    }
}

void V5ConferenceEventSink::onParticipantLeaved(const V5Participant &participant) {
    if(_protocol) {
        NSLog(@"onParticipantLeaved[ObjC]");
#if 0
        NSLog(@"[Participant Data]");
        NSLog(@"displayName : %s",participant.displayName);
        NSLog(@"id          : %s",participant.id);
#endif
        ObjcParticipant *objcParticipant = [[ObjcParticipant alloc] init];
        [objcParticipant setParticipant:&participant];
        [_protocol onParticipantLeaved:objcParticipant];
    }
}

void V5ConferenceEventSink::onReceiveJoinConferenceRequest(const char *token , const char *V5LiteURL) {
    if(_protocol){
        [_protocol onReceiveJoinConferenceRequest:[NSString stringWithCString:token encoding:NSUTF8StringEncoding]
                                        v5LiteURL:[NSString stringWithCString:V5LiteURL encoding:NSUTF8StringEncoding]];
    }
}

void V5ConferenceEventSink::onSessionLinked( const char *displayName, any_arg logoUrl )
{
    NSLog(@"onSessionLinked[ObjC]");
    if (_protocol) {
        auto displayName_ = [NSString stringWithCString:displayName encoding:NSUTF8StringEncoding];
        id logoUrl_ = convertToId(logoUrl);
        [_protocol onSessionLinked:displayName_ logoURL:logoUrl_];
    }
}

void V5ConferenceEventSink::onBeginSessionFailed(V5ErrorInfo const& errInfo)
{
    NSLog(@"onBeginSessionFailed[ObjC]");
#if 0
    NSLog(@"errInfo.code_deprecated = %d", errInfo.code_deprecated);
    NSLog(@"errInfo.category = %d",errInfo.category);
    NSLog(@"errInfo.reason = %d",errInfo.reason);
#endif
    if(_protocol) {
        ObjcErrorInfo *objcErrorInfo = [[ObjcErrorInfo alloc] init];
        [objcErrorInfo setErrorInfo:&errInfo  ];
        [_protocol onBeginSessionFailed:objcErrorInfo ];
    }
}

void V5ConferenceEventSink::onVersionUnmatched( const char *requireVersion )
{
    NSLog(@"onVersionUnmatched[ObjC]");
#if 0
    NSLog(@"requireVersion = %d", @(requireVersion));
#endif
    if(_protocol) {
        [_protocol onVersionUnmatched:[NSString stringWithCString:requireVersion encoding:NSUTF8StringEncoding] ];
    }
}

void V5ConferenceEventSink::onJoinConferenceFailed(V5ErrorInfo const& errInfo) {
    NSLog(@"onJoinConferenceFailed[ObjC]");
#if 0
    NSLog(@"errInfo.code_deprecated = %d", errInfo.code_deprecated);
    NSLog(@"errInfo.category = %d",errInfo.category);
    NSLog(@"errInfo.reason = %d",errInfo.reason);
#endif
    if(_protocol) {
//        [_protocol onJoinConferenceFailed:reason
//                                    token:[NSString stringWithCString:token encoding:NSUTF8StringEncoding]
//                                v5LiteURL:[NSString stringWithCString:V5LiteURL encoding:NSUTF8StringEncoding]];
        ObjcErrorInfo *objcErrorInfo = [[ObjcErrorInfo alloc] init];
        [objcErrorInfo setErrorInfo:&errInfo  ];
        [_protocol onJoinConferenceFailed:objcErrorInfo ];
    }
}

void V5ConferenceEventSink::onReceiveVideoFrame(VideoRawFrame const& rawFrame) {
    if(_protocol){
        ObjcVideoRawFrame *frame = [[ObjcVideoRawFrame alloc] init];
        frame.sourceId = rawFrame.sourceId;
        frame.width = rawFrame.width;
        frame.height = rawFrame.height;
        frame.data = rawFrame.buffer;
        frame.viewPriority = rawFrame.viewPriority;
        frame.participantURI = [NSString stringWithCString:rawFrame.participantURI encoding:NSUTF8StringEncoding];
        frame.mediaType = rawFrame.mediaType;
        [_protocol onReceiveVideoFrame:frame];
    }
}

void V5ConferenceEventSink::onJoinedConference(V5ConferenceInfo const& conferenceInfo)
{
    NSLog(@"onJoinedConference[ObjC]");
    if(_protocol) {
        auto& confSts = conferenceInfo.ConfSts;
        auto& participants = conferenceInfo.participants;
        auto& selfParticipant = conferenceInfo.selfParticipant;
        if( ( confSts == nullptr) ||
            ( participants == nullptr ) )
        {
            return;
        }
#if 0
        NSLog(@"-----------------------------------");
        NSLog(@"[conferenceInfo Data]");
        NSLog(@"ConferenceName          : %s",confSts->ConferenceName);
        NSLog(@"RoomName                : %s",confSts->RoomName);
        NSLog(@"ConferenceURL           : %s",confSts->ConferenceURL);
        NSLog(@"ConferencePinCode       : %s",confSts->ConferencePinCode);
        NSLog(@"DocumentSender          : %s",confSts->DocumentSender);
        NSLog(@"ConferenceStartTime     : %lld",confSts->ConferenceStartTime);
        NSLog(@"ConferenceEndTime       : %lld",confSts->ConferenceEndTime);
        NSLog(@"RemainTime              : %lld",confSts->RemainTime);
        NSLog(@"PastTime                : %lld",confSts->PastTime);
        NSLog(@"MaintenanceStartTime    : %lld",confSts->MaintenanceStartTime);
        NSLog(@"MaintenanceRemainTime   : %lld",confSts->MaintenanceRemainTime);
        NSLog(@"isReserved              : %d",confSts->isReserved);
        NSLog(@"isRecoading             : %d",confSts->isRecoading);
        NSLog(@"isLocked                : %d",confSts->isLocked);
        
        NSLog(@"OldConferenceName           : %@",@(confSts->OldConferenceName));
        NSLog(@"OldDocumentSender           : %@",@(confSts->OldDocumentSender));
        NSLog(@"OldRemainTime               : %d",confSts->OldRemainTime);
        NSLog(@"OldMaintenanceStartTime     : %d",confSts->OldMaintenanceStartTime);
        NSLog(@"OldMaintenanceRemainTime    : %d",confSts->OldMaintenanceRemainTime);
        NSLog(@"OldisRecoading              : %d",confSts->OldisRecoading);
        NSLog(@"OldisLocked                 : %d",confSts->OldisLocked);

        NSLog(@"ConferenceNameChanged           : %d",confSts->ConferenceNameChanged);
        NSLog(@"DocumentSenderChanged           : %d",confSts->DocumentSenderChanged);
        NSLog(@"RemainTimeChanged               : %d",confSts->RemainTimeChanged);
        NSLog(@"MaintenanceStartTimeChanged     : %d",confSts->MaintenanceStartTimeChanged);
        NSLog(@"MaintenanceRemainTimeChanged    : %d",confSts->MaintenanceRemainTimeChanged);
        NSLog(@"isRecoadingChanged              : %d",confSts->isRecoadingChanged);
        NSLog(@"isLockedChanged                 : %d",confSts->isLockedChanged);

        NSLog(@"-----------------------------------");
        NSLog(@"[ParticipantList Data]");
        NSLog(@"length      : %ld",conferenceInfo.length);
        for( unsigned long i = 0  ; i < conferenceInfo.length ; i++ )
        {
            NSLog(@"[Participant No.%ld ]",i);
            NSLog(@"displayName         : %s",participants[i]->displayName);
            NSLog(@"ParticipantID(id)   : %s",participants[i]->id);
        }
        
        NSLog(@"-----------------------------------");
        NSLog(@"[SelfParticipant Data]");
        NSLog(@"displayName         : %s",selfParticipant->displayName);
        NSLog(@"ParticipantID(id)   : %s",selfParticipant->id);
#endif
        ObjcConferenceInfo *info = [[ObjcConferenceInfo alloc] init];
        [info setConferenceStatusData:confSts];
        [info setParticipantArray:participants length:conferenceInfo.length];
        [info setSelfParticipantData:selfParticipant];

        [_protocol onJoinedConference:info];
    }
}

void V5ConferenceEventSink::onLeavedConference(V5ErrorInfo const& errInfo)
{
    NSLog(@"onLeavedConference[ObjC]");
    NSLog(@"errInfo.code_deprecated     : %d",errInfo.code_deprecated);
    NSLog(@"errInfo.category : %d",errInfo.category);
    NSLog(@"errInfo.reason   : %d",errInfo.reason);
    
    if (_protocol) {
        ObjcErrorInfo *objcErrorInfo = [[ObjcErrorInfo alloc] init];
        [objcErrorInfo setErrorInfo:&errInfo  ];
        [_protocol onLeavedConference:objcErrorInfo];
    }
}

void V5ConferenceEventSink::onParticipantListUpdated( const V5Participant * const participants[], unsigned long length )
{
    NSLog(@"onParticipantListUpdated[ObjC]");
    
#if 0
    for( unsigned long i = 0 ; i < length ; i++ )
    {
        std::cout
        << "-----------------------------" << std::endl
        << "participants[" << i << "]" <<  std::endl
        << "ParticipantID : " << participants[i]->id  << std::endl
        << "displayName : " << participants[i]->displayName << std::endl;
    }
#endif
    
    if (_protocol) {
        
        NSMutableArray *PList = [[NSMutableArray array] init];
        
        for( unsigned long usrcnt = 0; usrcnt < length ; usrcnt++  )
        {
            ObjcParticipant *ObjPData = [[ObjcParticipant alloc] init ];
            [ObjPData setParticipant:participants[usrcnt]];

            [ PList addObject:ObjPData ];
#if 0
            ObjcParticipant *PreObjPData = [PList objectAtIndex:usrcnt];
            NSLog(@"--------------------------------------------------");
            NSLog(@"usrcnt      : %lu",usrcnt);
            NSLog(@"displayName :\n%@",PreObjPData.displayName);
            NSLog(@"id          :\n%@",PreObjPData.id);
#endif
        }

        ObjcParticipantArray *participantArray = [[ObjcParticipantArray alloc] init];
        [participantArray setParticipants:[PList copy]];
        [_protocol onParticipantListUpdated:participantArray];
    }
}

void V5ConferenceEventSink::onConferenceStatusUpdated(V5ConferenceStatus const& conferenceStatus)
{
    NSLog(@"onConferenceStatusUpdated[ObjC]");
#if 0
    NSLog(@"-----------------------------------");
    NSLog(@"[conferenceInfo Data]");
    
    NSLog(@"ConferenceName          : %@",@(conferenceStatus.ConferenceName));
    NSLog(@"RoomName                : %@",@(conferenceStatus.RoomName));
    NSLog(@"ConferenceURL           : %@",@(conferenceStatus.ConferenceURL));
    NSLog(@"ConferencePinCode       : %@",@(conferenceStatus.ConferencePinCode));
    NSLog(@"DocumentSender          : %@",@(conferenceStatus.DocumentSender));
    NSLog(@"ConferenceStartTime     : %lld",conferenceStatus.ConferenceStartTime);
    NSLog(@"ConferenceEndTime       : %lld",conferenceStatus.ConferenceEndTime);
    NSLog(@"RemainTime              : %lld",conferenceStatus.RemainTime);
    NSLog(@"PastTime                : %lld",conferenceStatus.PastTime);
    NSLog(@"MaintenanceStartTime    : %lld",conferenceStatus.MaintenanceStartTime);
    NSLog(@"MaintenanceRemainTime   : %lld",conferenceStatus.MaintenanceRemainTime);
    NSLog(@"isReserved              : %d",conferenceStatus.isReserved);
    NSLog(@"isRecoading             : %d",conferenceStatus.isRecoading);
    NSLog(@"isLocked                : %d",conferenceStatus.isLocked);
    
    NSLog(@"OldConferenceName           : %@",@(conferenceStatus.OldConferenceName));
    NSLog(@"OldDocumentSender           : %@",@(conferenceStatus.OldDocumentSender));
    NSLog(@"OldRemainTime               : %lld",conferenceStatus.OldRemainTime);
    NSLog(@"OldMaintenanceStartTime     : %lld",conferenceStatus.OldMaintenanceStartTime);
    NSLog(@"OldMaintenanceRemainTime    : %lld",conferenceStatus.OldMaintenanceRemainTime);
    NSLog(@"OldisRecoading              : %d",conferenceStatus.OldisRecoading);
    NSLog(@"OldisLocked                 : %d",conferenceStatus.OldisLocked);
    
    NSLog(@"ConferenceNameChanged           : %d",conferenceStatus.ConferenceNameChanged);
    NSLog(@"DocumentSenderChanged           : %d",conferenceStatus.DocumentSenderChanged);
    NSLog(@"RemainTimeChanged               : %d",conferenceStatus.RemainTimeChanged);
    NSLog(@"MaintenanceStartTimeChanged     : %d",conferenceStatus.MaintenanceStartTimeChanged);
    NSLog(@"MaintenanceRemainTimeChanged    : %d",conferenceStatus.MaintenanceRemainTimeChanged);
    NSLog(@"isRecoadingChanged              : %d",conferenceStatus.isRecoadingChanged);
    NSLog(@"isLockedChanged                 : %d",conferenceStatus.isLockedChanged);
    
    NSLog(@"-----------------------------------");
#endif
    if (_protocol) {
        ObjcConferenceStatus *status = [[ObjcConferenceStatus alloc] init];
        [status setConferenceStatus:&conferenceStatus];
        [_protocol onConferenceStatusUpdated:status];
        
    }
    
    return;
}
void V5ConferenceEventSink::onReconnectConference( V5ConferenceInfo const& conferenceInfo )
{
    NSLog(@"onRecconectConference[ObjC]");
    if(_protocol) {
#if 0
        NSLog(@"-----------------------------------");
        NSLog(@"[conferenceInfo Data]");
        NSLog(@"ConferenceName          : %@",@(conferenceInfo.ConfSts->ConferenceName));
        NSLog(@"RoomName                : %@",@(conferenceInfo.ConfSts->RoomName));
        NSLog(@"ConferenceURL           : %@",@(conferenceInfo.ConfSts->ConferenceURL));
        NSLog(@"ConferencePinCode       : %@",@(conferenceInfo.ConfSts->ConferencePinCode));
        NSLog(@"DocumentSender          : %@",@(conferenceInfo.ConfSts->DocumentSender));
        NSLog(@"ConferenceStartTime     : %lld",conferenceInfo.ConfSts->ConferenceStartTime);
        NSLog(@"ConferenceEndTime       : %lld",conferenceInfo.ConfSts->ConferenceEndTime);
        NSLog(@"RemainTime              : %lld",conferenceInfo.ConfSts->RemainTime);
        NSLog(@"PastTime                : %lld",conferenceInfo.ConfSts->PastTime);
        NSLog(@"MaintenanceStartTime    : %lld",conferenceInfo.ConfSts->MaintenanceStartTime);
        NSLog(@"MaintenanceRemainTime   : %lld",conferenceInfo.ConfSts->MaintenanceRemainTime);
        NSLog(@"isReserved              : %d",conferenceInfo.ConfSts->isReserved);
        NSLog(@"isRecoading             : %d",conferenceInfo.ConfSts->isRecoading);
        NSLog(@"isLocked                : %d",conferenceInfo.ConfSts->isLocked);
        
        NSLog(@"OldConferenceName           : %@",@(conferenceInfo.ConfSts->OldConferenceName));
        NSLog(@"OldDocumentSender           : %@",@(conferenceInfo.ConfSts->OldDocumentSender));
        NSLog(@"OldRemainTime               : %lld",conferenceInfo.ConfSts->OldRemainTime);
        NSLog(@"OldMaintenanceStartTime     : %lld",conferenceInfo.ConfSts->OldMaintenanceStartTime);
        NSLog(@"OldMaintenanceRemainTime    : %lld",conferenceInfo.ConfSts->OldMaintenanceRemainTime);
        NSLog(@"OldisRecoading              : %d",conferenceInfo.ConfSts->OldisRecoading);
        NSLog(@"OldisLocked                 : %d",conferenceInfo.ConfSts->OldisLocked);
        
        NSLog(@"ConferenceNameChanged           : %d",conferenceInfo.ConfSts->ConferenceNameChanged);
        NSLog(@"DocumentSenderChanged           : %d",conferenceInfo.ConfSts->DocumentSenderChanged);
        NSLog(@"RemainTimeChanged               : %d",conferenceInfo.ConfSts->RemainTimeChanged);
        NSLog(@"MaintenanceStartTimeChanged     : %d",conferenceInfo.ConfSts->MaintenanceStartTimeChanged);
        NSLog(@"MaintenanceRemainTimeChanged    : %d",conferenceInfo.ConfSts->MaintenanceRemainTimeChanged);
        NSLog(@"isRecoadingChanged              : %d",conferenceInfo.ConfSts->isRecoadingChanged);
        NSLog(@"isLockedChanged                 : %d",conferenceInfo.ConfSts->isLockedChanged);
        
        NSLog(@"-----------------------------------");
        NSLog(@"[ParticipantList Data]");
        NSLog(@"length      : %ld",conferenceInfo.length);
        for( unsigned long i = 0  ; i < conferenceInfo.length ; i++ )
        {
            NSLog(@"[Participant No.%ld ]",i);
            NSLog(@"displayName         : %s",conferenceInfo.participants[i]->displayName);
            NSLog(@"ParticipantID(id)   : %s",conferenceInfo.participants[i]->id);
        }
        
        NSLog(@"-----------------------------------");
        NSLog(@"[SelfParticipant Data]");
        NSLog(@"displayName         : %s",conferenceInfo.selfParticipant->displayName);
        NSLog(@"ParticipantID(id)   : %s",conferenceInfo.selfParticipant->id);
#endif
        ObjcConferenceInfo *info = [[ObjcConferenceInfo alloc] init];
        [info setConferenceStatusData:conferenceInfo.ConfSts];
        [info setParticipantArray:conferenceInfo.participants length:conferenceInfo.length];
        [info setSelfParticipantData:conferenceInfo.selfParticipant];
        
        [_protocol onReconnectConference:info];
    }
}


void V5ConferenceEventSink::onAsyncError(V5ErrorInfo const& errInfo)
{
    NSLog(@"onAsyncError[ObjC]");
    NSLog(@"errInfo.category : %d",errInfo.category);
    NSLog(@"errInfo.reason   : %d",errInfo.reason);

    if (_protocol) {
        ObjcErrorInfo *objcErrorInfo = [[ObjcErrorInfo alloc] init];
        [objcErrorInfo setErrorInfo:&errInfo  ];
        
        NSLog(@"objcErrorInfo.category : %d",objcErrorInfo.category);
        NSLog(@"objcErrorInfo.reason   : %d",objcErrorInfo.reason);

        [_protocol onAsyncError:objcErrorInfo];
    }
}

void V5ConferenceEventSink::onInviteMailIsSent(V5ErrorInfo const& errInfo, const char* status, const char *mailAddress)
{
    NSLog(@"onInviteMailIsSent[ObjC]");
    NSLog(@"errInfo.category : %d",errInfo.category);
    NSLog(@"errInfo.reason   : %d",errInfo.reason);
    
    if (_protocol) {
        ObjcErrorInfo *objcErrorInfo = [[ObjcErrorInfo alloc] init];
        [objcErrorInfo setErrorInfo:&errInfo  ];
        
        NSLog(@"objcErrorInfo.category : %d",objcErrorInfo.category);
        NSLog(@"objcErrorInfo.reason   : %d",objcErrorInfo.reason);
        
        [_protocol onInviteMailIsSent:objcErrorInfo status:[NSString stringWithUTF8String:status] mailAddress:[NSString stringWithUTF8String:mailAddress]];
    }
}

void V5ConferenceEventSink::onAddShare( const char * uri, bool isSelfShare )
{
    NSLog(@"onAddShare[ObjC]");
    if (_protocol) {
        [_protocol onAddShare: [NSString stringWithUTF8String:uri] isSelfShare:isSelfShare];
    }
}

void V5ConferenceEventSink::onRemoveShare( const char * uri, bool isSelfShare )
{
    NSLog(@"onRemoveShare[ObjC]");
    if (_protocol) {
        [_protocol onRemoveShare: [NSString stringWithUTF8String:uri] isSelfShare:isSelfShare];
    }
}

void V5ConferenceEventSink::onRemoteSourceAdded(V5RemoteSourceChanged const& change)
{
    NSLog(@"onRemoteSourceAdded[ObjC]");
    if (_protocol)
    {
        [_protocol onRemoteSourceAdded:[ObjcRemoteSourceChanged createFrom:&change]];
    }
}

void V5ConferenceEventSink::onRemoteSourceRemoved(V5RemoteSourceChanged const& change)
{
    NSLog(@"onRemoteSourceRemoved[ObjC]");
    if (_protocol)
    {
        [_protocol onRemoteSourceRemoved:[ObjcRemoteSourceChanged createFrom:&change]];
    }
}

void V5ConferenceEventSink::onSelectedParticipantsUpdated(std::vector<std::string> const& selectedURI)
{
    NSLog(@"on selected participants updated!");
	auto number = selectedURI.size();
    if (_protocol) {
        NSLog(@"number = %zu",number);
        
        for( size_t i = 0 ; i < number ; i++ )
        {
            NSLog(@"selectedURI[%zu] : %s",i,selectedURI[i].c_str());
        }
        
        NSMutableArray* arr = [NSMutableArray arrayWithCapacity:number];
        for (size_t i = 0; i < number ; i++) {
            [arr addObject:[NSString stringWithUTF8String:selectedURI[i].c_str()]];
        }
        [_protocol onSelectedParticipantsUpdated:(int)number selectedURI:arr];
    }
}

void V5ConferenceEventSink::onRcvRejectParticipant( V5Participant const& participant )
{
    NSLog(@"onRcvRejectParticipant[ObjC]");
    if (_protocol) {
        ObjcParticipant *objcParticipant = [[ObjcParticipant alloc] init];
        [objcParticipant setParticipant:&participant];
        [_protocol onRcvRejectParticipant:objcParticipant];
    }
}

void V5ConferenceEventSink::onPastTimeStart()
{
    NSLog(@"onPastTimeStart[ObjC]");
    if (_protocol) {
        [_protocol onPastTimeStart];
    }
}

void V5ConferenceEventSink::onPastTimeStop()
{
    NSLog(@"onPastTimeStop[ObjC]");
    if (_protocol) {
        [_protocol onPastTimeStop];
    }
}

void V5ConferenceEventSink::onFloatingWindow(V5WindowId window)
{
    NSLog(@"onFloatingWindow[ObjC]");
    
    // iOS.Mac版に必要な機能かの判別がつかないため、
    // ここまでの口は作ってコンパイルをとおるようにするが、
    // 必要ない場合、コメントアウトすること。
    if (_protocol) {
        __weak NSObject *ns = (__bridge NSObject *)window;

        [_protocol onFloatingWindow:ns];
    }

}

void V5ConferenceEventSink::onConferenceNameChangeFailed( const char* nowConferenceName )
{
    NSLog(@"onPastTimeStop[ObjC]");
    if (_protocol) {
        NSString *objcConferenceName = [NSString stringWithCString:nowConferenceName encoding:NSUTF8StringEncoding];
        [_protocol onConferenceNameChangeFailed:objcConferenceName ];
    }
}

void V5ConferenceEventSink::onNowMaintenance( long long maintenanceStartTime, long long maintenanceEndTime )
{
    NSLog(@"onNowMaintenance[ObjC]");
    if (_protocol) {
        [_protocol onNowMaintenance:maintenanceStartTime maintenanceEndTime:maintenanceEndTime ];
    }
}

void V5ConferenceEventSink::onConferenceStatisticsNotice( V5ConferenceStatisticsAllInfo const& confStatInfo )
{
    ObjcConferenceStatisticsAllInfo* info = [[ObjcConferenceStatisticsAllInfo alloc] init];

    ObjcV5StatisticsCommunicationInfo  communicationInfo;
    communicationInfo.vmCommunicationStatus = confStatInfo.communicationInfo.vmCommunicationStatus;
    communicationInfo.vmCommunicationViaVidyoProxy = confStatInfo.communicationInfo.vmCommunicationViaVidyoProxy;
    communicationInfo.vmCommunicationViaWebProxy = confStatInfo.communicationInfo.vmCommunicationViaWebProxy;
    communicationInfo.vrCommunicationStatus = confStatInfo.communicationInfo.vrCommunicationStatus;
    communicationInfo.vrCommunicationViaVidyoProxy = confStatInfo.communicationInfo.vrCommunicationViaVidyoProxy;
    communicationInfo.vrCommunicationViaWebProxy = confStatInfo.communicationInfo.vrCommunicationViaWebProxy;
    
    info.communicationInfo = communicationInfo;
    
#if 0
    NSLog(@"onConferenceStatisticsNotice[ObjC]");
    
    
    NSLog(@"------------------------------------------------");
    NSLog(@"[confStatInfo.communicationInfo]");
    auto comInfoPtr = &confStatInfo.communicationInfo;
    NSLog(@"vmCommunicationStatus           : %@",@(comInfoPtr->vmCommunicationStatus?"Connecting To VM":"Non Connect To VM"));
    NSLog(@"vmCommunicationViaVidyoProxy    : %@",@(comInfoPtr->vmCommunicationViaVidyoProxy?"VProxy Routing To VM":"Non VProxy Routing To VM"));
    NSLog(@"vmCommunicationViaWebProxy      : %@",@(comInfoPtr->vmCommunicationViaWebProxy?"WebProxy Routing To VM":"Non WebProxy Routing To VM"));
    NSLog(@"vrCommunicationStatus           : %@",@(comInfoPtr->vrCommunicationStatus?"Connecting To VR":"Non Connect To VR"));
    NSLog(@"vrCommunicationViaVidyoProxy    : %@",@(comInfoPtr->vrCommunicationViaVidyoProxy?"VProxy Routing To VR":"Non VProxy Routing To VR"));
    NSLog(@"vrCommunicationViaWebProxy      : %@",@(comInfoPtr->vrCommunicationViaWebProxy?"WebProxy Routing To VR":"Non WebProxy Routing To VR"));
#endif
    
    ObjcV5StatisticsEndpointStatus endpointStatus;
    endpointStatus.endPointStatus = confStatInfo.endpointStatus.endPointStatus;
    info.endpointStatus = endpointStatus;
    
#if 0
    NSLog(@"------------------------------------------------");
    NSLog(@"[confStatInfo.endpointStatus]");
    switch( confStatInfo.endpointStatus.endPointStatus )
    {
        case 0:
        {
            NSLog(@"endpointStatus : %@",@"Client not registered to server.");
            break;
        }
            
        case 1:
        {
            NSLog(@"endpointStatus : %@",@"Registered to server, but not bound to a user. ");
            break;
        }

        case 2:
        {
            NSLog(@"endpointStatus : %@",@"Client is registered and bound to a user - ready for use.");
            break;
        }

    }
#endif
    
    ObjcV5StatisticsBandwidthInfo bandwidthInfo;
    bandwidthInfo.actualRecvBwApplication = confStatInfo.bandwidthInfo.ActualRecvBwApplication;
    bandwidthInfo.actualRecvBwAudio = confStatInfo.bandwidthInfo.ActualRecvBwAudio;
    bandwidthInfo.actualRecvBwVideo = confStatInfo.bandwidthInfo.ActualRecvBwVideo;
    bandwidthInfo.actualRecvBwMax = confStatInfo.bandwidthInfo.ActualRecvBwMax;


    bandwidthInfo.actualSendBwApplication = confStatInfo.bandwidthInfo.ActualSendBwApplication;
    bandwidthInfo.actualSendBwAudio = confStatInfo.bandwidthInfo.ActualSendBwAudio;
    bandwidthInfo.actualSendBwVideo = confStatInfo.bandwidthInfo.ActualSendBwVideo;
    bandwidthInfo.actualSendBwMax = confStatInfo.bandwidthInfo.ActualSendBwMax;
    
    bandwidthInfo.availRecvBwApplication = confStatInfo.bandwidthInfo.AvailRecvBwApplication;
    bandwidthInfo.availRecvBwAudio = confStatInfo.bandwidthInfo.AvailRecvBwAudio;
    bandwidthInfo.availRecvBwVideo = confStatInfo.bandwidthInfo.AvailRecvBwVideo;
    bandwidthInfo.availRecvBwMax = confStatInfo.bandwidthInfo.AvailRecvBwMax;
    
    bandwidthInfo.availSendBwApplication = confStatInfo.bandwidthInfo.AvailSendBwApplication;
    bandwidthInfo.availSendBwAudio = confStatInfo.bandwidthInfo.AvailSendBwAudio;
    bandwidthInfo.availSendBwVideo = confStatInfo.bandwidthInfo.AvailSendBwVideo;
    bandwidthInfo.availSendBwMax = confStatInfo.bandwidthInfo.AvailSendBwMax;
    
    info.bandwidthInfo = bandwidthInfo;
    
    
#if 0
    NSLog(@"------------------------------------------------");
    NSLog(@"[confStatInfo.bandwidthInfo]");
    auto bwInfoPtr = &confStatInfo.bandwidthInfo;
    NSLog(@"AvailSendBwVideo         : %d kbps ",bwInfoPtr->AvailSendBwVideo);
    NSLog(@"AvailSendBwAudio         : %d kbps",bwInfoPtr->AvailSendBwAudio);
    NSLog(@"AvailSendBwApplication   : %d kbps",bwInfoPtr->AvailSendBwApplication);
    NSLog(@"AvailSendBwMax           : %d kbps",bwInfoPtr->AvailSendBwMax);
    NSLog(@"ActualSendBwVideo        : %d kbps",bwInfoPtr->ActualSendBwVideo);
    NSLog(@"ActualSendBwAudio        : %d kbps",bwInfoPtr->ActualSendBwAudio);
    NSLog(@"ActualSendBwApplication  : %d kbps",bwInfoPtr->ActualSendBwApplication);
    NSLog(@"ActualSendBwMax          : %d kbps",bwInfoPtr->ActualSendBwMax);
    NSLog(@"AvailRecvBwVideo         : %d kbps",bwInfoPtr->AvailRecvBwVideo);
    NSLog(@"AvailRecvBwAudio         : %d kbps",bwInfoPtr->AvailRecvBwAudio);
    NSLog(@"AvailRecvBwApplication   : %d kbps",bwInfoPtr->AvailRecvBwApplication);
    NSLog(@"AvailRecvBwMax           : %d kbps",bwInfoPtr->AvailRecvBwMax);
    NSLog(@"ActualRecvBwVideo        : %d kbps",bwInfoPtr->ActualRecvBwVideo);
    NSLog(@"ActualRecvBwAudio        : %d kbps",bwInfoPtr->ActualRecvBwAudio);
    NSLog(@"ActualRecvBwApplication  : %d kbps",bwInfoPtr->ActualRecvBwApplication);
    NSLog(@"ActualRecvBwMax          : %d kbps",bwInfoPtr->ActualRecvBwMax);
#endif
    
    ObjcV5StatisticsConferenceInfo conferenceInfo;
    conferenceInfo.recording = confStatInfo.conferenceInfo.recording;
    conferenceInfo.webcast = confStatInfo.conferenceInfo.webcast;
    info.conferenceInfo = conferenceInfo;
#if 0
    NSLog(@"------------------------------------------------");
    NSLog(@"[confStatInfo.conferenceInfo]");
    auto confInfoPtr = &confStatInfo.conferenceInfo;
    NSLog(@"recording  : %@",@(confInfoPtr->recording?"Now recording":"No recording"));
    NSLog(@"webcast    : %@",@(confInfoPtr->webcast?"On webcasting":"Nowebcast"));
#endif
    
    
    info.connectivity.serverAddress = [NSString stringWithCString:confStatInfo.connectivity.serverAddress encoding:NSUTF8StringEncoding];
    info.connectivity.serverPort = [NSString stringWithCString:confStatInfo.connectivity.serverPort encoding:NSUTF8StringEncoding];
    info.connectivity.vmIdentity = [NSString stringWithCString:confStatInfo.connectivity.vmIdentity encoding:NSUTF8StringEncoding];
    info.connectivity.userName = [NSString stringWithCString:confStatInfo.connectivity.userName encoding:NSUTF8StringEncoding];
    info.connectivity.portalAddress = [NSString stringWithCString:confStatInfo.connectivity.portalAddress encoding:NSUTF8StringEncoding];
    info.connectivity.portalVersion = [NSString stringWithCString:confStatInfo.connectivity.portalVersion encoding:NSUTF8StringEncoding];
    info.connectivity.locationTag = [NSString stringWithCString:confStatInfo.connectivity.locationTag encoding:NSUTF8StringEncoding];
    info.connectivity.vidyoProxyAddress = [NSString stringWithCString:confStatInfo.connectivity.vidyoProxyAddress encoding:NSUTF8StringEncoding];
    info.connectivity.vidyoProxyPort = [NSString stringWithCString:confStatInfo.connectivity.vidyoProxyPort encoding:NSUTF8StringEncoding];
    info.connectivity.clientExternalIPAddress = [NSString stringWithCString:confStatInfo.connectivity.clientExternalIPAddress encoding:NSUTF8StringEncoding];
    info.connectivity.reverseProxyAddress = [NSString stringWithCString:confStatInfo.connectivity.reverseProxyAddress encoding:NSUTF8StringEncoding];
    info.connectivity.reverseProxyPort = [NSString stringWithCString:confStatInfo.connectivity.reverseProxyPort encoding:NSUTF8StringEncoding];
    info.connectivity.reverseProxyPort = [NSString stringWithCString:confStatInfo.connectivity.reverseProxyPort encoding:NSUTF8StringEncoding];
    info.connectivity.serverSecured = confStatInfo.connectivity.serverSecured;
    info.connectivity.guestLogin = confStatInfo.connectivity.guestLogin;
    info.connectivity.proxyType = confStatInfo.connectivity.proxyType;
#if 0
    NSLog(@"------------------------------------------------");
    NSLog(@"[confStatInfo.connectivity]");
    auto connectivityPtr = &confStatInfo.connectivity;
    NSLog(@"serverAddress           : %@",@(connectivityPtr->serverAddress));
    NSLog(@"serverPort              : %@",@(connectivityPtr->serverPort));
    NSLog(@"vmIdentity              : %@",@(connectivityPtr->vmIdentity));
    NSLog(@"userName                : %@",@(connectivityPtr->userName));
    NSLog(@"portalAddress           : %@",@(connectivityPtr->portalAddress));
    NSLog(@"portalVersion           : %@",@(connectivityPtr->portalVersion));
    NSLog(@"locationTag             : %@",@(connectivityPtr->locationTag));
    NSLog(@"vidyoProxyAddress       : %@",@(connectivityPtr->vidyoProxyAddress));
    NSLog(@"vidyoProxyPort          : %@",@(connectivityPtr->vidyoProxyPort));
    NSLog(@"clientExternalIPAddress : %@",@(connectivityPtr->clientExternalIPAddress));
    NSLog(@"reverseProxyAddress     : %@",@(connectivityPtr->reverseProxyAddress));
    NSLog(@"reverseProxyPort        : %@",@(connectivityPtr->reverseProxyPort));
    NSLog(@"serverSecured           : %@",@(connectivityPtr->serverSecured?"Yes":"No"));
    NSLog(@"guestLogin              : %@",@(connectivityPtr->guestLogin?"Yes":"No"));
    NSLog(@"proxyType               : %d",connectivityPtr->proxyType);
#endif
    // このデータは出す必要がなさそう。
    info.sessionDisplayInfo.sessionDisplayContext = confStatInfo.sessionDisplayInfo.sessionDisplayContext;
    info.sessionDisplayInfo.sessionDisplayText = [NSString stringWithCString:confStatInfo.sessionDisplayInfo.sessionDisplayText encoding:NSUTF8StringEncoding];
    
#if 0
    
    NSLog(@"------------------------------------------------");
    NSLog(@"[confStatInfo.sessionDisplayInfo]");
    auto sessionDisplayInfoPtr = &confStatInfo.sessionDisplayInfo;
    NSLog(@"sessionDisplayContext : %d",sessionDisplayInfoPtr->sessionDisplayContext);
    NSLog(@"sessionDisplayText    : %@",@(sessionDisplayInfoPtr->sessionDisplayText));
#endif
    
    ObjcV5StatisticsMediaInfo mediaInfo;
    mediaInfo.numIFrames = confStatInfo.mediaInfo.numIFrames;
    mediaInfo.numFirs = confStatInfo.mediaInfo.numFirs;
    mediaInfo.numNacks = confStatInfo.mediaInfo.numNacks;
    mediaInfo.mediaRTT = confStatInfo.mediaInfo.mediaRTT;
    info.mediaInfo = mediaInfo;
#if 0
    NSLog(@"------------------------------------------------");
    NSLog(@"[confStatInfo.mediaInfo]");
    auto mediaInfoPtr = &confStatInfo.mediaInfo;
    NSLog(@"IFrames Total Counter : %d",mediaInfoPtr->numIFrames);
    NSLog(@"Firs Total Counter    : %d",mediaInfoPtr->numFirs);
    NSLog(@"Nacks Total Counter   : %d",mediaInfoPtr->numNacks);
    NSLog(@"mediaRTT              : %d  / millisecond (Round trip estimated time)",mediaInfoPtr->mediaRTT);
#endif
    auto participantInfoPtr = &confStatInfo.participantInfo;
    NSMutableArray *mutableArray = [[NSMutableArray alloc] init];

    for( int i = 0 ; i < participantInfoPtr->numberParticipants && i < V5_MAX_PARTICIPANTS_NUM ; i++ )
    {
        ObjcV5StatisticsParticipantInfo* partInfo = [[ObjcV5StatisticsParticipantInfo alloc] init];
        partInfo.URI = [NSString stringWithCString:participantInfoPtr->URI[i] encoding:NSUTF8StringEncoding];
        partInfo.name = [NSString stringWithCString:participantInfoPtr->Name[i] encoding:NSUTF8StringEncoding];
        partInfo.bytesRcvd = participantInfoPtr->bytesRcvd[i];
        partInfo.numFirsSent = participantInfoPtr->numFirsSent[i];
        partInfo.numNacksSent = participantInfoPtr->numNacksSent[i];
        partInfo.numDistinctNacksSent = participantInfoPtr->numDistinctNacksSent[i];
        partInfo.decodedFrameRate = participantInfoPtr->decodedFrameRate[i];
        partInfo.displayedFrameRate = participantInfoPtr->displayedFrameRate[i];
        partInfo.receivedFrameRate = participantInfoPtr->receivedFrameRate[i];
        partInfo.receivedPacketRate = participantInfoPtr->receivedPacketRate[i];
        partInfo.receivedFrameRate = participantInfoPtr->receivedFrameRate[i];
        partInfo.receivedBpsVideo = participantInfoPtr->receivedBpsVideo[i];
        partInfo.receivedBpsAudio = participantInfoPtr->receivedBpsAudio[i];
        partInfo.receivedWidth = participantInfoPtr->receivedWidth[i];
        partInfo.receivedHeight = participantInfoPtr->receivedHeight[i];
        partInfo.receivedHeight = participantInfoPtr->receivedHeight[i];
        partInfo.receivedBytesVideo = participantInfoPtr->receivedBytesVideo[i];
        partInfo.receivedBytesAudio = participantInfoPtr->receivedBytesAudio[i];
        [mutableArray addObject:partInfo];
    }
    
    [info.participantInfoCollection setParticipants:[mutableArray copy]];
#if 0
    NSLog(@"------------------------------------------------");
    NSLog(@"[confStatInfo.participantInfo]");
    
    NSLog(@"numberParticipants : %ld",participantInfoPtr->numberParticipants);
    
    for( int i = 0 ; i < participantInfoPtr->numberParticipants && i < V5_MAX_PARTICIPANTS_NUM ; i++ )
    {
        NSLog(@"Name[%d]                 : %@",i,@(participantInfoPtr->Name[i]));
        NSLog(@"URI[%d]                  : %@",i,@(participantInfoPtr->URI[i]));
        NSLog(@"receivedFrameRate[%d]    : %lu",i,participantInfoPtr->receivedFrameRate[i]);
        NSLog(@"decodedFrameRate[%d]     : %lu",i,participantInfoPtr->decodedFrameRate[i]);
        NSLog(@"displayedFrameRate[%d]   : %lu",i,participantInfoPtr->displayedFrameRate[i]);
        NSLog(@"receivedBpsVideo[%d]     : %lu",i,participantInfoPtr->receivedBpsVideo[i]);
        NSLog(@"receivedWidth[%d]        : %lu",i,participantInfoPtr->receivedWidth[i]);
        NSLog(@"receivedHeight[%d]       : %lu",i,participantInfoPtr->receivedHeight[i]);
        NSLog(@"receivedBytesVideo[%d]   : %lu",i,participantInfoPtr->receivedBytesVideo[i]);
        NSLog(@"receivedBytesAudio[%d]   : %lu",i,participantInfoPtr->receivedBytesAudio[i]);
    }
#endif
    ObjcV5StatisticsRateShaperInfo rateShaperInfo;
    
    rateShaperInfo.delayApp = confStatInfo.rateShaperInfo.delayAppPriorityNormal;
    rateShaperInfo.numQueuedAppPackets = confStatInfo.rateShaperInfo.numPacketsAppPriorityNormal;
    rateShaperInfo.numDroppedAppPackets = confStatInfo.rateShaperInfo.numDroppedAppPriorityNormal;
//    rateShaperInfo.numFramesApp = confStatInfo.rateShaperInfo.numFramesAppPriorityNormal;

    rateShaperInfo.delayAppHigh = confStatInfo.rateShaperInfo.delayAppPriorityRetransmit;
    rateShaperInfo.numQueuedAppHighPackets = confStatInfo.rateShaperInfo.numPacketsAppPriorityRetransmit;
    rateShaperInfo.numDroppedAppHighPackets = confStatInfo.rateShaperInfo.numDroppedAppPriorityRetransmit;
//    rateShaperInfo.numFramesAppHigh = confStatInfo.rateShaperInfo.numFramesAppPriorityRetransmit;

    rateShaperInfo.delayVideo = confStatInfo.rateShaperInfo.delayVideoPriorytyNormal;
    rateShaperInfo.numQueuedVideoPackets = confStatInfo.rateShaperInfo.numPacketsVideoPriorytyNormal;
    rateShaperInfo.numDroppedVideoPackets = confStatInfo.rateShaperInfo.numDroppedVideoPriorytyNormal;
//    rateShaperInfo.numFramesVideo = confStatInfo.rateShaperInfo.numFramesVideoPriorytyNormal;

    rateShaperInfo.delayVideoHigh = confStatInfo.rateShaperInfo.delayVideoPriorutyRetransmit;
    rateShaperInfo.numQueuedVideoHighPackets = confStatInfo.rateShaperInfo.numPacketsVideoPriorutyRetransmit;
    rateShaperInfo.numDroppedVideoHighPackets = confStatInfo.rateShaperInfo.numDroppedVideoPriorutyRetransmit;
//    rateShaperInfo.numFramesVideoHigh = confStatInfo.rateShaperInfo.numFramesVideoPriorutyRetransmit;

    info.rateShaperInfo = rateShaperInfo;
#if 0
    NSLog(@"------------------------------------------------");
    NSLog(@"[confStatInfo.rateShaperInfo]");
    auto rateShaperInfoPtr = &confStatInfo.rateShaperInfo;
    
    NSLog(@"delayVideoPriorytyNormal            : %d",rateShaperInfoPtr->delayVideoPriorytyNormal);
    NSLog(@"numPacketsVideoPriorytyNormal       : %d",rateShaperInfoPtr->numPacketsVideoPriorytyNormal);
//    NSLog(@"numFramesVideoPriorytyNormal        : %d",rateShaperInfoPtr->numFramesVideoPriorytyNormal);
    NSLog(@"numDroppedVideoPriorytyNormal       : %d",rateShaperInfoPtr->numDroppedVideoPriorytyNormal);
    
    NSLog(@"delayVideoPriorutyRetransmit        : %d",rateShaperInfoPtr->delayVideoPriorutyRetransmit);
    NSLog(@"numPacketsVideoPriorutyRetransmit   : %d",rateShaperInfoPtr->numPacketsVideoPriorutyRetransmit);
//    NSLog(@"numFramesVideoPriorutyRetransmit    : %d",rateShaperInfoPtr->numFramesVideoPriorutyRetransmit);
    NSLog(@"numDroppedVideoPriorutyRetransmit   : %d",rateShaperInfoPtr->numDroppedVideoPriorutyRetransmit);
    
    NSLog(@"delayAppPriorityNormal              : %d",rateShaperInfoPtr->delayAppPriorityNormal);
    NSLog(@"numPacketsAppPriorityNormal         : %d",rateShaperInfoPtr->numPacketsAppPriorityNormal);
//    NSLog(@"numFramesAppPriorityNormal          : %d",rateShaperInfoPtr->numFramesAppPriorityNormal);
    NSLog(@"numDroppedAppPriorityNormal         : %d",rateShaperInfoPtr->numDroppedAppPriorityNormal);
    
    NSLog(@"delayAppPriorityRetransmit          : %d",rateShaperInfoPtr->delayAppPriorityRetransmit);
    NSLog(@"numPacketsAppPriorityRetransmit     : %d",rateShaperInfoPtr->numPacketsAppPriorityRetransmit);
//    NSLog(@"numFramesAppPriorityRetransmit      : %d",rateShaperInfoPtr->numFramesAppPriorityRetransmit);
    NSLog(@"numDroppedAppPriorityRetransmit     : %d",rateShaperInfoPtr->numDroppedAppPriorityRetransmit);
#endif
    ObjcV5StatisticsFrameRateInfo frameRateInfo;
    frameRateInfo.captureFrameRate = confStatInfo.frameRateInfo.captureFrameRate;
    frameRateInfo.encodeFrameRate = confStatInfo.frameRateInfo.encodeFrameRate;
    frameRateInfo.sendFrameRate = confStatInfo.frameRateInfo.sendFrameRate;
    info.frameRateInfo = frameRateInfo;
#if 0
    
    NSLog(@"------------------------------------------------");
    NSLog(@"[confStatInfo.frameRateInfo]");
    auto frameRateInfoPtr       = &confStatInfo.frameRateInfo;
    NSLog(@"captureFrameRate    : %d",frameRateInfoPtr->captureFrameRate);
    NSLog(@"encodeFrameRate     : %d",frameRateInfoPtr->encodeFrameRate);
    NSLog(@"sendFrameRate       : %d",frameRateInfoPtr->sendFrameRate);
#endif
    
    auto participantStatListPtr = &confStatInfo.participantStatisticsList;
    NSMutableArray *anArray = [[NSMutableArray alloc] init];

    for( int i = 0 ;
        i < participantStatListPtr->numParticipants && i < V5_MAX_GET_PARTICIPANT_STATISTICS;
        i++ )
    {
        ObjcV5StatisticsParticipantData* aData = [[ObjcV5StatisticsParticipantData alloc] init];
        auto PlistStatPtr = &participantStatListPtr->statistics[i];
        aData.name =  [NSString stringWithCString:PlistStatPtr->name encoding:NSUTF8StringEncoding];
        aData.URI = [NSString stringWithCString:PlistStatPtr->uri encoding:NSUTF8StringEncoding];
        ObjcV5VRect videoResolution;
        videoResolution.xPos = PlistStatPtr->videoResolution.xPos;
        videoResolution.yPos = PlistStatPtr->videoResolution.yPos;
        videoResolution.width = PlistStatPtr->videoResolution.width;
        videoResolution.height = PlistStatPtr->videoResolution.height;
        aData.videoResolution = videoResolution;
        aData.videoKBitsPerSecRecv = PlistStatPtr->videoKBitsPerSecRecv;
        aData.audioKBitsPerSecRecv = PlistStatPtr->audioKBitsPerSecRecv;
        aData.firs = PlistStatPtr->firs;
        aData.nacks = PlistStatPtr->nacks;
        aData.videoFrameRate = PlistStatPtr->videoFrameRate;
        aData.videoDecodedFrameRate = PlistStatPtr->videoDecodedFrameRate;
        aData.videoDisplayedFrameRate = PlistStatPtr->videoDisplayedFrameRate;
        [anArray addObject:aData];
    }
    [info.participantStatisticsCollection setParticipants:[anArray copy]];
#if 0
    NSLog(@"------------------------------------------------");
    NSLog(@"[confStatInfo.participantStatisticsList]");
    NSLog(@"numParticipants : %d",participantStatListPtr->numParticipants);

    for( int i = 0 ;
         i < participantStatListPtr->numParticipants && i < V5_MAX_GET_PARTICIPANT_STATISTICS;
         i++ )
    {
        auto PlistStatPtr = &participantStatListPtr->statistics[i];
        
        NSLog(@"uri[%d]                     : %@",i,@(PlistStatPtr->uri));
        NSLog(@"name[%d]                    : %@",i,@(PlistStatPtr->name));
        NSLog(@"videoResolution.xPos[%d]    : %d",i,PlistStatPtr->videoResolution.xPos);
        NSLog(@"videoResolution.yPos[%d]    : %d",i,PlistStatPtr->videoResolution.yPos);
        NSLog(@"videoResolution.width[%d]   : %d",i,PlistStatPtr->videoResolution.width);
        NSLog(@"videoResolution.height[%d]  : %d",i,PlistStatPtr->videoResolution.height);
        NSLog(@"videoKBitsPerSecRecv[%d]    : %@",i,@(PlistStatPtr->videoKBitsPerSecRecv));
        NSLog(@"audioKBitsPerSecRecv[%d]    : %@",i,@(PlistStatPtr->audioKBitsPerSecRecv));
        NSLog(@"firs[%d]                    : %@",i,@(PlistStatPtr->firs));
        NSLog(@"nacks[%d]                   : %@",i,@(PlistStatPtr->nacks));
        NSLog(@"videoFrameRate[%d]          : %@",i,@(PlistStatPtr->videoFrameRate));
        NSLog(@"videoDecodedFrameRate[%d]   : %@",i,@(PlistStatPtr->videoDecodedFrameRate));
        NSLog(@"videoDisplayedFrameRate[%d] : %@",i,@(PlistStatPtr->videoDisplayedFrameRate));
        
    }
#endif
    
    if (_protocol) {
        [_protocol onConferenceStatisticsNotice:info];
    }
}

void V5ConferenceEventSink::onConferenceEndTimeOverNotice( long minutes )
{
    NSLog(@"onConferenceEndTimeOverNotice[ObjC]");
    NSLog(@"minutes : %ld",minutes);
    
    if (_protocol) {
        [_protocol onConferenceEndTimeOverNotice:minutes ];
    }
}


void V5ConferenceEventSink::onLibFatalError(V5ErrorInfo const& errInfo)
{
    NSLog(@"onLibFatalError[ObjC]");
    NSLog(@"errInfo.category : %d",errInfo.category);
    NSLog(@"errInfo.reason   : %d",errInfo.reason);
    
    if (_protocol) {
        ObjcErrorInfo *objcErrorInfo = [[ObjcErrorInfo alloc] init];
        [objcErrorInfo setErrorInfo:&errInfo  ];
        
        NSLog(@"objcErrorInfo.category : %d",objcErrorInfo.category);
        NSLog(@"objcErrorInfo.reason   : %d",objcErrorInfo.reason);
        
        [_protocol onLibFatalError:objcErrorInfo];
    }
}


// Websocket(Chat) Callback Wrapper for Objective-C(Swift)
class V5ChatEventSink :public IV5ChatEventSink {
public:
    V5ChatEventSink(id<IV5ChatEventSinkProtocol> protocol){
        _chat_protocol = protocol;

    }
    virtual ~V5ChatEventSink(){};
    virtual void onChatMessageAdded( const V5ChatMessage& message );
    virtual void onChatLogUpdated( const V5ChatMessage * const messages[], unsigned long length );
    virtual void onErrorNotification();
private:
    id<IV5ChatEventSinkProtocol> _chat_protocol;
};

void V5ChatEventSink::onChatMessageAdded( const V5ChatMessage& message ) {

    NSLog(@"onChatMessageAdded[ObjC]");

    if(_chat_protocol) {
        
        ObjcMessage *ObjMsg = [[ ObjcMessage alloc ] init ];
        
        ObjMsg.text = @(message.text);
        ObjMsg.from = @(message.from);
        ObjMsg.displayName = @(message.displayName);
        ObjMsg.timestamp = message.timestamp;
        ObjMsg.isSelfMessage = message.isSelfMessage ;
        
        [_chat_protocol onChatMessageAdded:ObjMsg];
    }
}

void V5ChatEventSink::onChatLogUpdated( const V5ChatMessage * const messages[], unsigned long length ) {

    NSLog(@"onChatLogUpdated[ObjC]");
    
#if 0
    NSLog(@"-----------------------------------");
    NSLog(@"[ChatLog Data]");
    for( unsigned long cnt = 0 ; cnt < length ; cnt++ )
    {
        NSLog(@"- - - - - - - - - - - - - - - -");
        NSLog(@"messages[%ld]",cnt);
        NSLog(@"text            : %@",@(messages[cnt]->text));
        NSLog(@"from            : %@",@(messages[cnt]->from));
        NSLog(@"displayName     : %@",@(messages[cnt]->displayName));
        NSLog(@"timestamp       : %lld",messages[cnt]->timestamp);
        NSLog(@"isSelfMessage   : %s",messages[cnt]->isSelfMessage?"true":"false");
    }
    
#endif
    
    if(_chat_protocol) {
        
        NSMutableArray *ChatLogs = [[NSMutableArray array] init];
        
        for( unsigned long cnt = 0 ; cnt < length ; cnt++ )
        {
            ObjcMessage *ObjMsg = [[ ObjcMessage alloc ] init ];
            
            ObjMsg.text = @(messages[cnt]->text);
            ObjMsg.from = @(messages[cnt]->from);
            ObjMsg.timestamp = messages[cnt]->timestamp;
            ObjMsg.isSelfMessage = messages[cnt]->isSelfMessage ;
            ObjMsg.displayName = @(messages[cnt]->displayName);
            
            [ ChatLogs addObject:ObjMsg ];
        }
        
        [_chat_protocol onChatLogUpdated:ChatLogs length:length];
    }
}

void V5ChatEventSink::onErrorNotification(){
    if(_chat_protocol) {

    }
}

// Device Sink

class V5DeviceEventSink : public IV5DeviceEventSink{
public:
    V5DeviceEventSink(id<IV5DeviceEventSinkProtocol> protocol, LibV5Lite* cli) {
        _protocol = protocol;
        _lib = cli;
    }
    
    virtual ~V5DeviceEventSink() {
        
    }
    virtual void onAudioInDeviceListChanged(int index);
    virtual void onAudioOutDeviceListChanged(int index);
    virtual void onVideoDeviceListChanged(int index);
    
    virtual void onSelectedSystemAudioInDevicesChanged(int index);
    virtual void onSelectedSystemAudioOutDevicesChanged(int index);

private:
    id<IV5DeviceEventSinkProtocol> _protocol;
    LibV5Lite* _lib;
};

void V5DeviceEventSink::onAudioInDeviceListChanged(int index)
{
    if (_protocol) {
        V5DevList* li = [_lib getDeviceList:V5_DEVICE_TYPE_AUDIO_IN];
        [_protocol onAudioInDeviceListChanged:(V5DevList*)li];
    }
}

void V5DeviceEventSink::onAudioOutDeviceListChanged(int index)
{
    if (_protocol) {
        V5DevList* li = [_lib getDeviceList:V5_DEVICE_TYPE_AUDIO_OUT];
        [_protocol onAudioOutDeviceListChanged:(V5DevList*)li];
    }
}

void V5DeviceEventSink::onVideoDeviceListChanged(int index)
{
    if (_protocol) {
        V5DevList* li = [_lib getDeviceList:V5_DEVICE_TYPE_VIDEO];
        [_protocol onVideoDeviceListChanged:(V5DevList*)li];
    }
}

void V5DeviceEventSink::onSelectedSystemAudioInDevicesChanged(int index)
{
    if (_protocol) {
        V5DevList* li = [_lib getDeviceList:V5_DEVICE_TYPE_AUDIO_IN];
        [_protocol onSelectedSystemAudioInDevicesChanged:(V5DevList*)li];
    }
}

void V5DeviceEventSink::onSelectedSystemAudioOutDevicesChanged(int index)
{
    if (_protocol) {
        V5DevList* li = [_lib getDeviceList:V5_DEVICE_TYPE_AUDIO_OUT];
        [_protocol onSelectedSystemAudioOutDevicesChanged:(V5DevList*)li];
    }
}

id V5ConferenceEventSink::convertToId(any_arg any)
{
    if (any.is<bool>())
    {
        return [NSNumber numberWithBool:any.get<bool>()];
    }
    else if (any.is<double>())
    {
        return [NSNumber numberWithDouble:any.get<double>()];
    }
    else if (any.is<std::string>())
    {
        return [NSString stringWithCString:any.get<std::string>().c_str() encoding:NSUTF8StringEncoding];
    }
    else if (any.is<any::array>())
    {
        auto& cppary = any.get<any::array>();
        NSMutableArray* arr = [NSMutableArray arrayWithCapacity:cppary.size()];
        for (auto& element : cppary) {
            [arr addObject: convertToId(element)];
        }
        return arr;
    }
    else if (any.is<any::object>())
    {
        auto& cppobj = any.get<any::object>();
        NSMutableDictionary* dic = [NSMutableDictionary dictionary];
        for (auto element : cppobj)
        {
            id value = convertToId(element.second);
            NSString* key = [NSString stringWithCString:element.first.c_str() encoding:NSUTF8StringEncoding];
            [dic setValue:value forKey:key];
        }
        return dic;
    }
    return nullptr;
}

@implementation LibV5Lite {
    std::shared_ptr<V5ConferenceEventSink> _sink;
    std::shared_ptr<IV5ChatEventSink> _chatSink;
    std::shared_ptr<IV5DeviceEventSink> _deviceSink;
}

-(id)init {
    self = [super init];
    if (self) {
        self.cli = vrms5::V5Client::createInstance();
    }
    return self;
}

+ (V5CODE)libInit: (NSString *)logPath logPriority:(NSString *)logPriority
{
    V5CODE ret = V5OK;
    
    ret = vrms5::V5Client::initialize(
                                      [logPath cStringUsingEncoding:NSUTF8StringEncoding],
                                      [logPriority cStringUsingEncoding:NSUTF8StringEncoding]
                                      );
    
    return ret;
}

+ (V5CODE)libFinal
{
    return vrms5::V5Client::finalize();
}

#if defined(V5_MACOS)
- (void)startLogic: (NSView*)parentWindow videoRect:(V5Rect *)rect fontPath:(NSString *)fontPath certificatePath:(NSString *)certificatePath
{
    assert(self.cli != nil);
    
    const char* fpath = ( fontPath != nil ) ? [fontPath cStringUsingEncoding:NSUTF8StringEncoding] : NULL;
    const char* cpath = (certificatePath != nil) ? [certificatePath cStringUsingEncoding:NSUTF8StringEncoding] : NULL;
    
    self.cli->startLogic((__bridge void*)parentWindow, rect, fpath, cpath);
}
#else
- (void)startLogic: (V5WindowId)parentWindow videoRect:(V5Rect *)rect
{
    assert(self.cli != nil);
    
    self.cli->startLogic(parentWindow, rect, nil, nil);
}
#endif


- (void)stopLogic
{
    self.cli->stopLogic();
}

/* estimated capacity:
   max_mail_address_length * max_participants_in_conference = 254 * 24 <= 6096
   if 20% of strings consume for escape strings: may be 7200
 
   actually, 65536 is sufficient for this usage of concatenations.
*/
- (V5CODE)sendInviteMail: (NSArray *)mailAddress
{
    V5CODE ret = V5OK;
    
    NSMutableString *concatenated = [NSMutableString stringWithCapacity: 65536];
    
    if ([mailAddress count] == 0) {
        return V5Error;
    }
    
    [concatenated appendString: mailAddress[0]];
    
    for (int i = 1 ; i < [mailAddress count]; i++) {
        [concatenated appendString: @","];
        [concatenated appendString: mailAddress[i]];
    }
    
    ret = self.cli->sendInviteMail([concatenated cStringUsingEncoding:NSUTF8StringEncoding]);
    return ret;
}

- (V5CODE)beginSession: (NSString *)url token:(NSString *)token version:(NSString *)version
{
    return self.cli->beginSession(
                                 [url cStringUsingEncoding:NSUTF8StringEncoding],
                                 [token cStringUsingEncoding:NSUTF8StringEncoding],
                                 [version cStringUsingEncoding:NSUTF8StringEncoding]
                                 );
}



- (V5CODE)joinConference: (NSString *)screeenName roomPasswdInfo:(ObjcPasswdInfo *)roomPasswdInfo;
{
    V5CODE ret = V5OK;
    std::shared_ptr<V5PasswordInfo> passInfo;
    
    if (roomPasswdInfo != nil && roomPasswdInfo.passwd != nil)
    {
        passInfo = std::make_shared<V5PasswordInfo>();
        passInfo->passwd = [roomPasswdInfo.passwd cStringUsingEncoding:NSUTF8StringEncoding];
        passInfo->hashCount = roomPasswdInfo.hashCount;
        passInfo->hashType = roomPasswdInfo.hashType;
        if (roomPasswdInfo.salt != nil) {
            passInfo->salt = [roomPasswdInfo.salt cStringUsingEncoding:NSUTF8StringEncoding];
        }
    }
    
    ret = self.cli->joinConference(
            [screeenName cStringUsingEncoding:NSUTF8StringEncoding],
            passInfo);
    return ret;
}

- (void)setConferenceEventSink:(id<IV5ConferenceEventSinkProtocol>)protocol {
    _sink = std::make_shared<V5ConferenceEventSink>(protocol);
    self.cli->setConferenceEventSink(_sink);
}

- (void)setChatEventSink:(id<IV5ChatEventSinkProtocol>)protocol{
    _chatSink = std::make_shared<V5ChatEventSink>(protocol);
    self.cli->setChatEventSink(_chatSink);
}

- (void)setDeviceEventSink:(id<IV5DeviceEventSinkProtocol>)protocol lib:(LibV5Lite*)lib {
    _deviceSink = std::make_shared<V5DeviceEventSink>(protocol,lib);
    self.cli->setDeviceEventSink(_deviceSink);
}

- (void)sendChatMessage:(NSString*)message
{
    self.cli->sendChatMessage(message.UTF8String);
}

- (void)logout
{
    self.cli->leaveConferenceAsync();
}

- (AsyncResult)leaveConferenceAsync
{
    return self.cli->leaveConferenceAsync();
}

- (void)leaveConferenceSync
{
    self.cli->leaveConferenceSync();
}

- (void)dealloc {
    //[super dealloc];
    self.cli.reset();
}

//- (bool)userClientReady {
//    return self.ud->userClientReady;
//}

- (void)setFrameDirection: (int)x y:(int)y w:(unsigned int)w h:(unsigned int)h
{
    V5Rect rect = {x,y,w,h};
    V5CODE co = self.cli->setFrameDirection(rect);
    co = co;
}

// calling timing: before JoinConference, after libInit
- (void)setVideoFramePreferSizeWithWitdh: (int)width height:(int)height frameRate:(int)frameRate minFrameIntervalByMilliseconds:(int)minFrameInterval
{
    self.cli->setVideoFrameSize(width, height, frameRate, minFrameInterval);
}

// calling timing: before JoinConference, after libInit
- (void)setShareFramePreferSizeWithWidth: (int)width height:(int)height frameRate:(int)frameRate minFrameIntervalByMilliseconds:(int)minFrameInterval
{
    self.cli->setShareFrameSize(width, height, frameRate, minFrameInterval);
}

#if defined(V5_IOS)
- (void)setOrientation:(UIDeviceOrientation)orie
{
    V5Orientation v5orie = V5_ORIENTATION_UP;
    BOOL isChange = YES;
    NSLog(@"orie=%d", (int)orie);
    
    switch(orie) {
        case UIDeviceOrientationPortrait:
            NSLog(@"up");
            v5orie = V5_ORIENTATION_UP;
            break;
        case UIDeviceOrientationPortraitUpsideDown:
            NSLog(@"down");
            v5orie = V5_ORIENTATION_DOWN;
            break;
        case UIDeviceOrientationLandscapeLeft:
            NSLog(@"left");
            v5orie = V5_ORIENTATION_RIGHT;
            break;
        case UIDeviceOrientationLandscapeRight:
            NSLog(@"right");
            v5orie = V5_ORIENTATION_LEFT;
            break;
        case UIDeviceOrientationFaceUp:
            NSLog(@"face up");
            isChange = NO;
            break;
        default:
            isChange = NO;
            break;
    }
    V5CODE co;
    if (isChange == YES) {
        co = self.cli->setOrientation(v5orie);
    }
}
#endif

- (void)muteCamera: (bool)muted
{
    self.cli->muteCamera(muted);
}

- (void)muteMicrophone: (bool)muted
{
    self.cli->muteMicrophone(muted);
}

- (void)muteSpeaker: (bool)muted
{
    self.cli->muteSpeaker(muted);
}

- (V5DevList*)getDeviceList:(V5DeviceType)type
{
    std::shared_ptr<V5DeviceConfiguration> p = self.cli->getDeviceConfiguration();
    
    int num = p->getNumber(type);
    int index = p->getCurrent(type);
    NSMutableArray* marr = [[NSMutableArray alloc] initWithCapacity:num];
    NSMutableArray* phyarr = [[NSMutableArray alloc] initWithCapacity:num];
    
    std::shared_ptr<V5DeviceName> q = std::make_shared<V5DeviceName>();
    q->name = std::string("Unknown Device");
    q->phyDevName = std::string("Unknown Device");
    for (int i = 0 ; i < num ; i++ ) {
        switch (type) {
            case V5_DEVICE_TYPE_AUDIO_IN:
            {
                q = p->getMicrophoneItem(i);
                break;
            }
            case V5_DEVICE_TYPE_AUDIO_OUT:
            {
                q = p->getSpeakerItem(i);
                break;
            }
            case V5_DEVICE_TYPE_VIDEO:
            {
                q = p->getCameraItem(i);
                break;
            }
            default:
            {
                break;
            }
        }
        NSString *str1 = [NSString stringWithUTF8String:((q->name).c_str())];
        NSString *str2 = [NSString stringWithUTF8String:((q->phyDevName).c_str())];
        [marr insertObject:str1 atIndex:i];
        [phyarr insertObject:str2 atIndex:i];
    }

    NSArray *name1 = [NSArray arrayWithArray:marr];
    NSArray *phy2  = [NSArray arrayWithArray:phyarr];
    
    V5DevList* devlist = [[V5DevList alloc] initWithLength:num];
    [devlist setArrays: name1 arr2:phy2 index:index type:type];
    
    return devlist;
}

- (void)selectDevice:(V5DeviceType)type index:(int)index
{
    if( type == V5_DEVICE_TYPE_VIDEO )
    {
        bool CameraChangeState = self.cli->isCameraMuteChangeState();
//        printf("CameraState = %s\n",CameraChangeState?"true":"false");
        if( CameraChangeState == true )
        {
//            printf("setSelectDeviceReserve Call.\n");
            self.cli->setSelectDeviceReserve(type,index);
            return;
        }
    }

    (self.cli->getDeviceConfiguration())->setConf(type,index);
    // デバイスの切り替えではデバイス一覧を再取得する必要がないのでコメントアウト
//    self.cli->updateDeviceConfiguration();
    
    if (! _deviceSink) {
        return;
    }
    switch (type) {
        case V5_DEVICE_TYPE_AUDIO_IN:
        {
            _deviceSink->onAudioInDeviceListChanged(index);
            break;
        }
        case V5_DEVICE_TYPE_AUDIO_OUT:
        {
            _deviceSink->onAudioOutDeviceListChanged(index);
            break;
        }
        case V5_DEVICE_TYPE_VIDEO:
        {
            _deviceSink->onVideoDeviceListChanged(index);
            break;
        }
        default:
        {
            NSLog(@"[Select Device]: illegal device type (%d)", (int)type);
            break;
        }
    }
}

- (ObjcParticipant*)getSelfParticipant
{
    auto participant = self.cli->getSelfParticipant();
    ObjcParticipant *objcParticipant = [[ObjcParticipant alloc] init];
    [objcParticipant setParticipant:participant.get()];
    return objcParticipant;
}

- (void)startWatchSharedRawFrame:(NSString*)uri
{
    self.cli->startWatchSharedRawFrame(uri.UTF8String);
}

- (void)stopWatchSharedRawFrame:(NSString*)uri
{
    self.cli->stopWatchSharedRawFrame(uri.UTF8String);
}

- (void)startWatchShared:(NSString*) uri
{
    self.cli->startWatchShared(uri.UTF8String);
}
- (void)stopWatchShared:(NSString*) uri
{
    self.cli->stopWatchShared(uri.UTF8String);
}

- (void)lockConference:(bool) LockStatus
{
    self.cli->lockConference( LockStatus );
}

- (void)rejectParticipant:(NSString*) rejectPID
{
//    std::shared_ptr<const V5Participant> myPData;
//    myPData = self.cli->getSelfParticipant();
//    self.cli->rejectParticipant(myPData->id);
    
    self.cli->rejectParticipant(rejectPID.UTF8String);
}

- (void)updateConferenceName:(NSString*) conferenceName
{
    self.cli->updateConferenceName( conferenceName.UTF8String );
}

-(void)enableActiveSpeakerLayout:(bool)showActiveSpeaker
{
    self.cli->enableActiveSpeakerLayout(showActiveSpeaker);
}

-(void)changeSelfPreviewMode:(V5ClientPreviewMode) previewMode
{
    self.cli->changeSelfPreviewMode(previewMode);
}

-(void)setSelfViewLoopbackPolicy:(V5ClientSelfViewLoopbackPolicy) viewPolicy
{
    self.cli->setSelfViewLoopbackPolicy(viewPolicy);

}

- (bool)isGuest
{
    return self.cli->isGuest();
}

- (void)SendWorkerCrash
{
    self.cli->SendWorkerCrash();
}

- (void)SendDebug
{
    self.cli->DebugFuncCall();
}

- (void)shareAppWindow:(V5WindowCapturerWindowId) windowId width:(int)width height:(int)height
{
    self.cli->shareAppWindow(windowId, width, height);
}
- (void)shareSysDesktop:(V5Uint) desktopId width:(int)width height:(int)height
{
    self.cli->shareSysDesktop(desktopId, width, height);
}
- (void)unshare
{
    self.cli->unshare();
}

- (unsigned int)getMicrophoneVolume
{
    return self.cli->getMicrophoneVolume();
}

- (void)setMicrophoneVolume:(unsigned int)volume
{
    self.cli->setMicrophoneVolume(volume);
}

- (unsigned int)getSpeakerVolume
{
    return self.cli->getSpeakerVolume();
}

- (void)setSpeakerVolume:(unsigned int)volume
{
    self.cli->setSpeakerVolume(volume);
}


- (void)changeParticipantVideoFrame: (NSString *)participantURI width:(int)width height:(int)height frameRate:(int)frameRate  minFrameInterval:(int)minFrameInterval
{
    V5CODE codex = self.cli->changeParticipantVideoFrame(participantURI.UTF8String, width, height, frameRate, minFrameInterval);
    codex = codex;
}

#if !defined(V5_IOS)
- (ObjcWindowsAndDesktops *)getWindowsAndDesktops
{
    auto wad = self.cli->getWindowsAndDesktops();
    NSMutableArray *windowArray = [[NSMutableArray alloc] init];
    NSMutableArray *desktopArray = [[NSMutableArray alloc] init];

    for (auto& src:wad->windows)
    {
        ObjcWindow *window = [[ObjcWindow alloc] init];
        window.appWindowName = [NSString stringWithCString:src.appWindowName.c_str() encoding:NSUTF8StringEncoding];
        window.appWindowAppName = [NSString stringWithCString:src.appWindowAppName.c_str() encoding:NSUTF8StringEncoding];
        
        window.appWindowId = src.appWindowId;
        window.xPos = src.appWindowsRect.xPos;
        window.yPos = src.appWindowsRect.yPos;
        window.width = src.appWindowsRect.width;
        window.height = src.appWindowsRect.height;
        [windowArray addObject:window];
    }

    for (auto& src:wad->desktops)
    {
        ObjcDesktop *desktop = [[ObjcDesktop alloc] init];
        desktop.sysDesktopName = [NSString stringWithCString:src.sysDesktopName.c_str() encoding:NSUTF8StringEncoding];
        desktop.sysDesktopId = src.sysDesktopId;
        desktop.xPos = src.sysDesktopRect.xPos;
        desktop.yPos = src.sysDesktopRect.yPos;
        desktop.width = src.sysDesktopRect.width;
        desktop.height = src.sysDesktopRect.height;
        desktop.workArea = [[ObjcV5Rect alloc] init];
        desktop.workArea.xPos = src.sysDesktopWorkArea.xPos;
        desktop.workArea.yPos = src.sysDesktopWorkArea.yPos;
        desktop.workArea.width = src.sysDesktopWorkArea.width;
        desktop.workArea.height = src.sysDesktopWorkArea.height;
        desktop.info = [[ObjcDesktopInfo alloc] init];
        desktop.info.xDpi = src.sysDesktopInfo.xDpi;
        desktop.info.yDpi = src.sysDesktopInfo.yDpi;
        desktop.info.isPrimary = src.sysDesktopInfo.isPrimary;
        
        [desktopArray addObject:desktop];
    }

    ObjcWindowsAndDesktops *windowsAndDesktops = [[ObjcWindowsAndDesktops alloc] init];
    [windowsAndDesktops.windowList setWindows:[windowArray copy]];
    [windowsAndDesktops.desktopList setDesktop:[desktopArray copy]];

    return windowsAndDesktops;
}
#endif

- (void)setMaxParticipants:(int) maxParticipants {
    self.cli->setMaxParticipants(static_cast<uint>(maxParticipants));
}

- (int)getMaxParticipants {
    return self.cli->getMaxParticipants();
}

- (void)forceRendering:(NSString *) pid {
    std::string pid_([pid cStringUsingEncoding:NSUTF8StringEncoding]);
    std::shared_ptr<std::string> sharedPid = std::make_shared<std::string>(pid_);

    self.cli->forceRendering(sharedPid);
}

- (void)removeForceRendering {
    self.cli->removeForceRendering();
}

- (void)renderSelfView:(bool)render {
    self.cli->renderSelfView(render);
}

- (void)startHTTPD:(unsigned short)prot {
    self.cli->startHTTPD(prot);
}

- (void)setProxySetting:(NSString *)address port:(long) port id:(NSString *)id password:(NSString *)password {
    std::string addr = "";
    std::string id_ = "";
    std::string password_ = "";
    if(address) {
        addr = std::string([address cStringUsingEncoding:NSUTF8StringEncoding]);
    }

    if(id) {
        id_ = std::string([id cStringUsingEncoding:NSUTF8StringEncoding]);
    }

    if(password) {
        password_ = std::string([password cStringUsingEncoding:NSUTF8StringEncoding]);
    }

    self.cli->setProxySetting(addr, port, id_, password_);
}

- (void)setVideoPreferences:(VideoPreferences)preferences {
    self.cli->setVideoPreferences((V5VideoPreferences)preferences);
}

- (VideoPreferences)getVideoPreferences {
    return (VideoPreferences)(self.cli->getVideoPreferences());
}

- (void)setProxyForce:(bool)force
{
    self.cli->setProxyForce(force);
}

- (void)setProxyIEOption:(V5ProxyVidyoIEType)option
{
    self.cli->setProxyIEOption(option);
}

- (void)setSendMaxKbps:(int)kbps
{
    self.cli->setSendMaxKBPS(kbps);
}

- (void)setRecvMaxKbps:(int)kbps
{
    self.cli->setRecvMaxKBPS(kbps);
}

- (int)getSendMaxKbps
{
    return self.cli->getSendMaxKBPS();
}

- (int)getRecvMaxKbps
{
    return self.cli->getRecvMaxKBPS();
}

- (void)setSendBandWidth:(unsigned int)layer sendBandWidth:(int)sendBandWidth
{
    self.cli->setSendBandWidth((V5SLayer)layer,(V5SendBandWidth)sendBandWidth);
}


- (void)setSelfDisplayLabel:(NSString *)aLabel {
    self.cli->setSelfDisplayLabel(aLabel.UTF8String);
}

- (void)startStatisticsInfo:(int)millisecs
{
    self.cli->startStatisticsInfo(millisecs);
}

- (void)startStatisticsInfo
{
    self.cli->startStatisticsInfo();
}

- (void)stopStatisticsInfo
{
    self.cli->stopStatisticsInfo();
}


@end
